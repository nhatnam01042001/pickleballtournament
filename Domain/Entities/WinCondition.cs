﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class WinCondition : BaseEntity
    {
        public string ConditionName {  get; set; }
        public string Description { get; set; }
        public virtual ICollection<PickleballMatch>? PickleballMatches { get; set; }
    }
}
