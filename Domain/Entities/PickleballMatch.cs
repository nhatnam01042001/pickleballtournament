﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class PickleballMatch : BaseEntity
    {
        public int MatchOrder {  get; set; }
        //public uint? RoundId { get; set; }
        //public Round? Round { get; set; }
        public uint? FirstTeamId { get; set; }
        public virtual Team? FirstTeam { get; set; }
        public uint? SecondTeamId { get; set; }
        public virtual Team? SecondTeam { get; set; }
        public uint? WinningTeamId { get; set; }
        public virtual Team? WinningTeam { get; set; }
        public DateTime MatchDate { get; set; }
        public uint? CourtId { get; set; }
        public virtual Court? Court { get; set; }
        public uint? WinConditionId { get; set; }
        public virtual WinCondition? WinCondition { get; set; }
        public MatchStatus MatchStatus { get; set; }
        public uint? RoundGroupId { get; set; }
        public virtual RoundGroup? RoundGroup { get; set; }
        public virtual ICollection<Set>? Sets { get; set; }
    }
}
