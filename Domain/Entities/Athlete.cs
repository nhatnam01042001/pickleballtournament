﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Athlete : BaseEntity
    {        
        public uint TournamentCampaignId { get; set; }
        public TournamentCampaign TournamentCampaign { get; set; }
        public uint? UserId { get; set; }
        public virtual User? User { get; set; }
        public ParticipantType ParticipantType { get; set; }
        public uint? GuestPlayerId { get; set; }
        public virtual CampaignRegistration? GuestPlayer { get; set; }
        public virtual ICollection<Team>? Teams { get; set; }
        public virtual ICollection<TournamentRegistration>? TournamentRegistrations { get; set; }
        public virtual ICollection<TournamentAthlete>? TournamentAthletes { get; set; }
    }
}
