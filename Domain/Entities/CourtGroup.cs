﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class CourtGroup : BaseEntity
    {
        public string CourtGroupName { get; set; }
        public string Address { get; set; }
        public string? EmailContact { get; set; }
        public string? PhoneNumber { get; set; }
        public string? latitude { get; set; }
        public string? longitude { get; set; }
        public virtual ICollection<Court>? Courts { get; set; }
        public virtual ICollection<CourtGroupTournamentCampaign>? CourtGroupTournamentCampaigns { get; set; }
    }
}
