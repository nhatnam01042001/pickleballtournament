﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TournamentRankingTeam : BaseEntity
    {
        public uint TournamentId { get; set; }
        public virtual Tournament Tournament { get; set; }
        public uint TeamId { get; set; }
        public virtual Team Team { get; set; }
        public string? AchievedRound { get; set; }
        public int Rank { get; set; }
        public int MatchWins { get; set; }
        public int MatchLosses { get; set; }
        public int SetDifference { get; set; }
        public int ScoresDifference { get; set ; }
        public int TotalPoints { get; set; }
        public double PrizeMoney { get; set; }
    }
}
