﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public abstract class BaseEntity
    {
        public uint Id { get; set; }

        public DateTime CreationDate { get; set; }

        public uint CreatedBy { get; set; }

        public DateTime? ModificationDate { get; set; }

        public uint? ModificationBy { get; set; }

        public DateTime? DeletionDate { get; set; }

        public uint? DeleteBy { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
    }
}
