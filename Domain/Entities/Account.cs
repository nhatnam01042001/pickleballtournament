﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Account : BaseEntity
    {
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public uint UserId { get; set; }
        public virtual User? User { get; set; }
        public virtual ICollection<Comment>? Comments { get; set; }
    }
}
