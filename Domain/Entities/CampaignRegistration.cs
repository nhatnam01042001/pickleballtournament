﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class CampaignRegistration : BaseEntity
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public double Rank { get; set; }
        public TournamentRegistrationStatus RegistrationStatus { get; set; }
        public uint TournamentCampaignId { get; set; }
        public virtual TournamentCampaign? TournamentCampaign { get; set; }
        public virtual Athlete? Athlete { get; set; }
    }

}
