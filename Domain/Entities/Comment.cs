﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Comment : BaseEntity
    {
        public string? CommentText { get; set; }
        public uint AccountId { get; set; }
        public virtual Account? Account { get; set; }
        public DateTime CreateDate { get; set; }
        public uint TournamentId { get; set; }
        public virtual TournamentCampaign? Tournament { get; set; }
    }
}
