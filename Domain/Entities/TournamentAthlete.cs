﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TournamentAthlete : BaseEntity
    {
        public uint AthleteId { get; set; }
        public virtual Athlete Athlete { get; set; }
        public uint TournamentId { get; set; }
        public virtual Tournament Tournament { get; set; }        
    }
}
