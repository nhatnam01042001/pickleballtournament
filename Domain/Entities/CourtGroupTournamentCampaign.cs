﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class CourtGroupTournamentCampaign : BaseEntity
    {
        public uint CourtGroupId { get; set; }
        public virtual CourtGroup? CourtGroup { get; set; }
        public uint TournamentCampaignId { get; set; }
        public virtual TournamentCampaign? TournamentCampaign { get; set; }
    }
}
