﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Tournament : BaseEntity
    {
        public string? TournamentName { get; set; }
        public TournamentType TournamentType { get; set; }
        public FormatType FormatType { get; set; }
        public int NumberOfTeams { get; set; }
        public int NumberOfSets { get; set; }
        public double Rank { get; set; }
        public uint TournamentCampaignId { get; set; }
        public virtual TournamentCampaign? TournamentCampaign { get; set; }
        public string? ImageUrl { get; set; }
        public virtual ICollection<Team>? Teams { get; set; }
        public virtual ICollection<Round>? Rounds { get; set; }
        public virtual ICollection<TournamentRegistration>? TournamentRegistrations { get; set; }
        public virtual ICollection<TournamentAthlete>? TournamentAthletes { get; set; }
        public virtual ICollection<TournamentRankingTeam>? TournamentRankingTeams { get; set; }
        public MatchStatus TournamentStatus { get; set; }
    }

}
