﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TournamentCampaign : BaseEntity
    {
        public string? TournamentName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string? Location { get; set; }
        public DateTime? RegisterExpiredDate { get; set; }
        public MatchStatus CampaignStatus { get; set; }
        public string? ImageUrl { get; set; }      
        public virtual ICollection<Tournament>? Tournaments { get; set; }
        public virtual ICollection<Comment>? Comments { get; set; }
        public virtual ICollection<Athlete>? Athletes { get; set; }
        public virtual ICollection<CourtGroupTournamentCampaign>? CourtGroupTournamentCampaigns { get; set; }
        public virtual ICollection<CampaignRegistration>? CampaignRegistrations { get; set; }
    }

}
