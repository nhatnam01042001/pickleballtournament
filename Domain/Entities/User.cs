﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class User : BaseEntity
    {        
        public string? FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Gender { get; set; }
        public double? Rank { get; set; }
        public Role? Role { get; set; }
        public string? ImageUrl { get; set; }
        public virtual Account? Account { get; set; }   
        public virtual UserRegistration Registration { get; set; }
        public ICollection<Athlete>? Athletes { get; set; }
        public UserStatus Status { get; set; } = UserStatus.Active;
    }
}
