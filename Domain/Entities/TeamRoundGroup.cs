﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TeamRoundGroup : BaseEntity
    {
        public uint TeamId { get; set; }
        public virtual Team Team { get; set; }
        public uint RoundGroupId { get; set; }
        public TeamStatus TeamStatus { get; set; }
        public virtual RoundGroup RoundGroup { get; set; }
        public virtual TeamRoundGroupStatistics TeamRoundGroupStatistics { get; set; }
    }
}
