﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class NewsArticle : BaseEntity
    {
        public string? NewsTitle { get; set; }
        public string? SubTitle { get; set; }
        public string? NewsContent {  get; set; }
        public bool NewsArticleStatus { get; set; }
        public NewsType NewsType { get; set; }
        public string? ImageUrl { get; set; }
    }
}
