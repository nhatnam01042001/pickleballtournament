﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Court : BaseEntity
    {
        public string CourtName { get; set; }
        public uint? CourtGroupId { get; set; }
        public virtual CourtGroup? CourtGroup { get; set; }
        public virtual ICollection<PickleballMatch>? PickleballMatches { get; set; }
    }
}
