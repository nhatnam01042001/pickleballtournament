﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Team : BaseEntity
    {
        public string? TeamName { get; set; }
        public double AverageRank { get; set; }
        public string? Qualification { get; set; }
        public string? TournamentResult { get; set; }
        public uint TournamentId { get; set; }
        public virtual Tournament Tournament { get; set; }
        public virtual TournamentRankingTeam TournamentRankingTeam { get; set; }
        public uint FirstAthleteId { get; set; }
        public uint? SecondAthleteId { get; set; }
        public virtual Athlete FirstAthlete { get; set; }
        public virtual Athlete? SecondAthlete { get; set; }
        public FormatType TeamType { get; set; }
        public virtual ICollection<PickleballMatch>? FirstTeamMatches { get; set; }
        public virtual ICollection<PickleballMatch>? SecondTeamMatches { get; set; }
        public virtual ICollection<PickleballMatch>? MatchesWon { get; set; }
        public virtual ICollection<Set>? SetsWon { get; set; }
        public virtual ICollection<TeamRoundGroup> TeamRoundGroups { get; set; }
        public virtual ICollection<TeamRoundGroupStatistics>? Statistics { get; set; }
    }
}
