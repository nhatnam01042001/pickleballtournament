﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class NewsCategory : BaseEntity
    {
        public string NewsCategoryName { get; set; }
        public string NewsCategoryDescription { get; set; }
        public ICollection<NewsArticle>? NewsArticles { get; set;}
    }
}
