﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Round : BaseEntity
    {
        public int RoundOrder { get; set; }
        public string RoundName { get; set; }
        public uint TournamentId { get; set; }
        public virtual Tournament Tournament { get; set; }
        public MatchStatus RoundStatus { get; set; }
        //public int NumberOfTeams { get; set; }
        public virtual ICollection<Team>? Teams { get; set; }
        //public ICollection<PickleballMatch>? Matches { get; set; }
        public virtual ICollection<RoundGroup> RoundGroups { get; set; }
    }
}
