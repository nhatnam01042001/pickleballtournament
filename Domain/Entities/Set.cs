﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Set : BaseEntity
    {
        public int SetOrder {  get; set; }
        public int FirstTeamScore { get; set; }
        public int SecondTeamScore { get; set; }
        public MatchStatus SetStatus { get; set; }
        public uint PickleballMatchId { get; set; }
        public virtual PickleballMatch? PickleballMatch { get; set; }
        public uint? WinningTeamId { get; set; }
        public virtual Team? WinningTeam { get; set; }
    }
}
