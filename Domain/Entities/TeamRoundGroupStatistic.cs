﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class TeamRoundGroupStatistics : BaseEntity
    {
        public uint TeamRoundGroupId { get; set; }
        public virtual TeamRoundGroup TeamRoundGroup { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int Draws { get; set; }
        public int Points { get; set; }
        public int SetDifference {  get; set; }
        public int PointsDifference { get; set; }
    }
}
