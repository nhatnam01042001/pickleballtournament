﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class RoundGroup : BaseEntity
    {
        public string RoundGroupName { get; set; }
        public uint RoundId { get; set; }
        public virtual Round? Round { get; set; }
        public MatchStatus GroupStatus { get; set; }
        public virtual ICollection<PickleballMatch>? Matches { get; set; }
        public virtual ICollection<TeamRoundGroup>? TeamRoundGroups { get; set; }
        public virtual ICollection<TeamRoundGroupStatistics>? Statistics { get; set; }
    }
}
