﻿ using Application;
using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Infrastructures.Mappers;
using Infrastructures.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services, IConfiguration configuration, string databaseConnection)
        {
            services.AddScoped<ITournamentRepository, TournamentRepository>(); 
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITournamentCampaignRepository, TournamentCampaignRepository>();
            services.AddScoped<ICampaignRegistRepository, CampaignRegistRepository>();
            services.AddScoped<ICampaignRegistService, CampaignRegistrationService>();
            //services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<IPickleballMatchRepository, PickleballMatchRepository>();
            services.AddScoped<IPickleballMatchService, PickleballMatchService>();
            services.AddScoped<IAthleteRepository, AthleteRepository>();
            services.AddScoped<IAthleteService, AthleteService>();
            services.AddScoped<ITournamentCampaignService, TournamentCampaignService>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton<ICurrentTime, CurrentTime>();
            //services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITournamentRepository, TournamentRepository>();
            services.AddScoped<ITournamentService, TournamentService>();
            services.AddScoped<IRoundRepository, RoundRepository>();
            services.AddScoped<IRoundGroupRepository, RoundGroupRepository>();
            services.AddScoped<ICourtService, CourtService>();
            services.AddScoped<ICourtRepository, CourtRepository>();
            services.AddScoped<ITeamRoundGroupRepository, TeamRoundGroupRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<INewsArticleRepository, NewsArticleRepository>();
            services.AddScoped<INewsArticleService, NewsArticleService>();
            services.AddScoped<ICourtGroupRepository, CourtGroupRepository>();
            services.AddScoped<ICourtGroupService, CourtGroupService>();
            services.AddScoped<IWinConditionRepository, WinConditionRepository>();
            services.AddScoped<IWinCondtionService, WinConditionService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IGroupStatisticRepository, GroupStatisticRepository>();
            services.AddScoped<IRoundGroupService, RoundGroupService>();
            services.AddScoped<IGroupStatisticRepository,  GroupStatisticRepository>();
            services.AddScoped<IRoundService, RoundService>();
            services.AddScoped<ITournamentAthleteRepository, TournamentAthleteRepository>();
            services.AddScoped<ITournamentRegistrationRepository, TournamentRegistrationRepository>();
            services.AddScoped<ITournamentRegistrationService, TournamentRegistrationService>();
            services.AddScoped<ITeamRoundGroupService, TeamRoundGroupService>();
            services.AddScoped<ISetRepository, SetRepository>();
            services.AddScoped<ISetService, SetService>();
            services.AddScoped<IUserRegistrationRepository, UserRegistrationRepository>();
            services.AddScoped<IUserRegistrationService, UserRegistrationService>();
            services.AddScoped<ICourtGroupCampaignRepository, CourtGroupCampaignRepository>();
            services.AddScoped<ITournamentRankingTeamRepository, TournamentRankingTeamRepository>();
            services.AddScoped<ITournamentRankingTeamService, TournamentRankingTeamService>();
            services.AddDbContext<AppDbContext>(option => option.UseSqlServer(databaseConnection));
            services.Configure<JWTSection>(configuration.GetSection("JWTSection"));
            services.Configure<ManagerAccount>(configuration.GetSection("ManagerAccount"));
            services.Configure<ManagerEmailForSend>(configuration.GetSection("ManagerEmailForSend"));
            services.AddAutoMapper(typeof(MapperConfigurationsProfile).Assembly);
            return services;
        }
    }
}
