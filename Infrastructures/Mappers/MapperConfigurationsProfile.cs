﻿using Application.ViewModels.TournamentsViewModels;
using Application.ViewModels.TournamentViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.ViewModels.CommentViewModel;
using Application.ViewModels.NewsArticleViewModels;
using Application.ViewModels.CourtGroupViewModels;

namespace Infrastructures.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {
        public MapperConfigurationsProfile()
        {
            CreateMap<UserLoginDTO, User>();
            CreateMap<UserRegisterDto, User>();
            //CreateMap<CreateTournamentCampaignViewModel, TournamentCampaign>();
            CreateMap<TournamentCampaign, TournamentCampaignViewModel>();
            CreateMap<User, GetUserDTO>();
            CreateMap<CreateUserDTO, User>();
            CreateMap<UpdatedUserDTO, User>();
            CreateMap<AddCommentViewModel, Comment>().ReverseMap(); ;
            CreateMap<CommentViewModel, Comment>().ReverseMap();
            CreateMap<UpdateCommentViewModel, Comment>().ReverseMap();
            CreateMap<AddNewsArticleViewModel, NewsArticle>();
            CreateMap<UpdateNewsArticleViewModel, NewsArticle>();
            CreateMap<NewsArticle, NewsArticleViewModel>();
            CreateMap<CourtGroupViewModel, CourtGroup>().ReverseMap();
            CreateMap<AddCourtGroupViewModel, CourtGroup>().ReverseMap();
            CreateMap<UpdateCourtGroupViewModel, CourtGroup>().ReverseMap();
            CreateMap<UpdateManagerDTO, User>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status));
        }
    }
}
