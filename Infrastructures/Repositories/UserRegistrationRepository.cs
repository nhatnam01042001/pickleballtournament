﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class UserRegistrationRepository : GenericRepository<UserRegistration>, IUserRegistrationRepository
    {
        private readonly AppDbContext _context;
        public UserRegistrationRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService) 
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<bool> CheckExistedUserRegistration(string email, string phoneNumber)
        {
            var isExisted = await _context.UserRegistration
                .AnyAsync(u => u.Email == email ||  u.PhoneNumber == phoneNumber);
            return isExisted;
        }
    }
}
