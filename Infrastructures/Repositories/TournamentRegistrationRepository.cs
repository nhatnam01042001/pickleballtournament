﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TournamentRegistrationRepository : GenericRepository<TournamentRegistration>, ITournamentRegistrationRepository
    {
        private readonly AppDbContext _context;

        public TournamentRegistrationRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }
        public async Task<bool> CheckRegistrationExisted(uint tournamentId, string email, string phone)
        {
            var isExisted = await _context.TournamentRegistration
                .Include(t => t.Athlete)
                .AnyAsync(t => t.Athlete.GuestPlayer.Email == email
                || t.Athlete.GuestPlayer.PhoneNumber == phone);
            return isExisted;
        }

        public async Task<List<TournamentRegistration>> GetTournamentRegistrationsAsync(uint tournamentId)
        {
            var registration = await _context.TournamentRegistration
                .Include(t => t.Athlete)
                .ThenInclude(a => a.GuestPlayer)
                .Include(t => t.Tournament)
                .Where(r => r.TournamentId ==  tournamentId)
                .ToListAsync();
            return registration;
        }
    }
}
