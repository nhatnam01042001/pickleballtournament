﻿using Application.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CourtRepository : GenericRepository<Court>, ICourtRepository
    {
        private readonly AppDbContext _context;
        public CourtRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<Court>> GetCourtsAsync()
        {
            var courts = await _context.Court
                .Include(c => c.CourtGroup)
                .ToListAsync();
            return courts;
        }

        public async Task<Court?> GetCourtByIdAsync(uint id)
        {
            var courts = await _context.Court
                .Include(c => c.CourtGroup)
                .FirstOrDefaultAsync(c => c.Id == id);
            return courts;
        }

        public async Task<List<Court>> GetCourtsByCourtGroupIdAsync(uint courtId)
        {
            var courts = await _context.Court
                .Include(c => c.CourtGroup)
                .Where(c => c.CourtGroupId == courtId)
                .ToListAsync();

            return courts;
        }

        public async Task<List<Court>> GetCourtsByCampaignIdAsync(uint campaignId)
        {
            var courts = await _context.Court
                .Include(c => c.CourtGroup)
                .ThenInclude(c => c.CourtGroupTournamentCampaigns)
                .Where(c => 
                c.CourtGroup.CourtGroupTournamentCampaigns
                .Any(cg => cg.TournamentCampaignId == campaignId))
                .ToListAsync();

            return courts;
        }

        public async Task<List<Court>> GetCourtsByTournamentIdAsync(uint tournamentId)
        {
            var courts = await _context.Court
                .Include(c => c.CourtGroup)
                .ThenInclude(c => c.CourtGroupTournamentCampaigns)
                .ThenInclude(c => c.TournamentCampaign)
                .ThenInclude(t => t.Tournaments)
                .Where(c => c.CourtGroup.CourtGroupTournamentCampaigns
                .Any(ctgc => ctgc.TournamentCampaign.Tournaments
                .Any(t => t.Id == tournamentId))).ToListAsync();

            return courts;
        }

        public async Task<List<Court>> GetAvailableCourtsAsync(uint matchId)
        {
            // Fetch the match that you're trying to assign to a court
            var matchToAssign = await _context.PickleballMatch
                .Where(m => m.Id == matchId)
                .FirstOrDefaultAsync();

            if (matchToAssign == null)
                throw new InvalidOperationException($"Match with Id {matchId} not found.");

            var matchDate = matchToAssign.MatchDate.Date; // Extract only the date
            var minTimeDifference = TimeSpan.FromHours(2);

            // Fetch courts where no matches on the same date conflict based on the match time
            var availableCourts = await _context.Court
                .Include(c => c.PickleballMatches) // Include related matches
                .Where(c => c.PickleballMatches
                    .All(m => m.Id != matchId &&                      // Exclude the match being assigned
                              (m.MatchStatus == Domain.Enums.MatchStatus.Completed || m.MatchDate == null ||                 // Allow courts with no assigned matches
                              m.MatchDate.Date != matchDate ||        // Exclude matches on different dates
                              Math.Abs(EF.Functions.DateDiffMinute(m.MatchDate, matchToAssign.MatchDate)) >= minTimeDifference.TotalMinutes))) // Ensure 2 hours gap
                .ToListAsync();

            return availableCourts;
        }

        public async Task AddCourtAsync(Court court)
        {
            await _dbSet.AddAsync(court);
        }

        public void UpdateCourt(Court court)
        {
            _dbSet.Update(court);
        }

        public void DeleteCourt(Court court)
        {
            _dbSet.Remove(court);
        }
    }
}