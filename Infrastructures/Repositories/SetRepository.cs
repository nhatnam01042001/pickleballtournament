﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class SetRepository : GenericRepository<Set>, ISetRepository
    {
        private readonly AppDbContext _context;

        public SetRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<Set>> GetSetsByMatchIdAsync(uint matchId)
        {
            var sets = await _context.Set
                .Include(s => s.WinningTeam)
        .Include(s => s.PickleballMatch)
            .ThenInclude(m => m.RoundGroup)
        .Include(s => s.PickleballMatch)
            .ThenInclude(m => m.FirstTeam)
        .Include(s => s.PickleballMatch)
            .ThenInclude(m => m.SecondTeam)
        .Where(s => s.PickleballMatchId == matchId)
        .OrderBy(s => s.SetOrder)
        .ToListAsync();
            return sets;
        }

        public async Task<Set> GetSetByIdAsync(uint setId)
        {
            var set = await _context.Set
                .Include(s => s.PickleballMatch)
                .Include(s => s.WinningTeam)
                .Where(s => s.Id == setId)
                .FirstOrDefaultAsync();
            return set;
        }

        public async Task<Set>GetLatestSetByMatchIdAsync(uint matchId)
        {
            var set = await _context.Set
                .Include(s => s.PickleballMatch)
                .Include(s => s.WinningTeam)
                .LastOrDefaultAsync(s => s.PickleballMatchId == matchId);

            return set;
        }
    }
}
