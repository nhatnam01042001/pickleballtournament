﻿using Application.Interfaces;
using Application.Repositories;
using AutoMapper.Internal;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class AthleteRepository : GenericRepository<Athlete>, IAthleteRepository
    {
        private readonly AppDbContext _context;

        public AthleteRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<Athlete>> GetParticipantsInTournamentAsync(uint tournamentId)
        {
            var tournamentParticipants = await _context.Athlete
                                .Where(p => p.TournamentCampaignId == tournamentId)
                                .Include(p => p.User)
                                .Include(p => p.GuestPlayer)
                                .Include(p => p.TournamentAthletes)
                                .Include(p => p.Teams).ToListAsync();
            return tournamentParticipants;
        }
        public async Task<List<Athlete>> GetAthletesWithNonTeamAsync(uint tournamentId)
        {
            var athletes = await _context.TournamentAthlete
                .Include(ta => ta.Athlete)
                .ThenInclude(u => u.User)
                .Include(ta => ta.Athlete)
                .ThenInclude(g => g.GuestPlayer)
                .Where(ta => ta.TournamentId == tournamentId)
                .Select(ta => ta.Athlete).ToListAsync();

            var teamsInTournament = await _context
                .Team.Where(t => t.TournamentId == tournamentId)
                             
                .ToListAsync();

            var athletesWithoutTeam = athletes
    .Where(a => !teamsInTournament.Any(t => t.FirstAthleteId == a.Id || t.SecondAthleteId == a.Id))
    .ToList();

            // Return the list of athletes without a team
            return athletesWithoutTeam;
        }

        public async Task<List<Athlete>> GetAthletesWithTeam(uint tournamentId)
        {
            var athletes = await _context.Team
               .Where(team => team.TournamentId == tournamentId)
               .SelectMany(team => _context.Athlete
                   .Where(athlete => athlete.Teams.Any(t => t.Id == team.Id)))
               .Distinct()
               .ToListAsync();
            return athletes;
        }

        public async Task<Athlete> GetAthleteByIdAsync(uint athleteId)
        {
            var athlete = await _context.Athlete.Include(a => a.User)
                .Include(a => a.GuestPlayer)
                .Include(p => p.TournamentAthletes)
                .FirstOrDefaultAsync(a => a.Id == athleteId);
            return athlete;
        }

        public async Task<bool> CheckAthleteExisted(uint tournamentId, uint userId)
        {
            var athleteExisted = await _context.Athlete.AnyAsync(a => a.TournamentCampaignId == tournamentId
            && a.UserId == userId);
            return athleteExisted;
        }

        public async Task<Athlete> GetAthleteByUserIdAsync(uint userId)
        {
            var athlete = await _context.Athlete
                .Include(p => p.TournamentAthletes)
                .Where(a => a.UserId == userId).FirstOrDefaultAsync();
            return athlete;
        }

        public async Task<List<Athlete>> GetAthletesByCampaignIdAsync(uint campaignId)
        {
            var athletes = await _context.Athlete
                .Include(a => a.User)
                .Include(a => a.GuestPlayer)
                .Include(p => p.TournamentAthletes)
                .Where(a => a.TournamentCampaignId == campaignId).ToListAsync();

            var sortedAthletes = athletes
        .OrderBy(a => a.User != null ? a.User.FullName : a.GuestPlayer.FullName)
        .ToList();

            return sortedAthletes;
            //return athletes;
        }

        public async Task<Athlete> GetAthleteByEmailAndPhoneNumber(uint campaignId, string email, string phoneNumber)
        {
            var athlete = await _context.Athlete
                .Include(a => a.GuestPlayer)
                .Include(p => p.TournamentAthletes)
                .Where(a => a.GuestPlayer.Email == email
                && a.GuestPlayer.PhoneNumber == phoneNumber
                && a.TournamentCampaignId == campaignId)
                .FirstOrDefaultAsync();
            return athlete;
        }
                
    }
}
