﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TeamRepository : GenericRepository<Team>, ITeamRepository
    {
        private readonly AppDbContext _context;
        public TeamRepository(AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public Task<List<Team>> GetSingleTeamsAsync(uint id)
        {
            throw new NotImplementedException();
        }
        /*public async Task<List<Team>> GetSingleTeamsAsync(uint id)
{
   var singleTeams = await _dbContext.Team.Where(t => t.TeamType == Domain.Enums.TeamType.Single)
       .Include(t => t.FirstParticipant).ToListAsync();
   var listTournamentTeams = singleTeams.Where(t => t.FirstParticipant.TournamentId == id).ToList();
   return listTournamentTeams;
}*/
        public async Task<bool> CheckTeamMemberExistedInTournament(uint tournamentId, uint athleteId)
        {
            var teams = await _context.Team.Where(t => t.TournamentId == tournamentId).ToListAsync();
            var existed = teams.Any(t => t.FirstAthleteId == athleteId || t.SecondAthleteId == athleteId);
            return existed;
        }

        public async Task<List<Team>> GetTeamsByTournamentIdAsync(uint tournamentId)
        {
            var teams = await _context.Team.Where(t => t.TournamentId == tournamentId).ToListAsync();
            return teams;
        }

        public async Task<List<Team>> GetTeamsByRoundGroupIdAsync(uint roundGroupId)
        {
            var teams = await (from team in _context.Team
                               join teamRoundGroup in _context.TeamRoundGroup on team.Id equals teamRoundGroup.TeamId
                               where teamRoundGroup.RoundGroupId == roundGroupId
                               select team).ToListAsync();
            return teams;
        }

        public async Task<List<Team>> GetListTeamByIdAsync(List<uint> teamIds)
        {
            return await _context.Team.Where(t => teamIds.Contains(t.Id)).ToListAsync();
        }
        
    }
}
