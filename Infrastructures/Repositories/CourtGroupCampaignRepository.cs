﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CourtGroupCampaignRepository : GenericRepository<CourtGroupTournamentCampaign>, ICourtGroupCampaignRepository
    {
        private readonly AppDbContext _context;

        public CourtGroupCampaignRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService) 
            : base(context, timeService, claimsService)
        {
            _context = context;
        }
        public async Task<List<CourtGroup>> GetCourtGroupByCampaignIdAsync(uint campaignId)
        {
            var courtGroups = await _context.CourtGroupTournamentCampaign
                .Include(c => c.TournamentCampaign)
                .Include(c => c.CourtGroup)
                .Where(c => c.TournamentCampaignId == campaignId)
                .Select(c => c.CourtGroup).ToListAsync();

            return courtGroups;
        }

        public async Task<List<CourtGroup>> GetCourtGroupsNotExistedInCampaign(uint campaignId)
        {
            var courtGroups = await _context.CourtGroupTournamentCampaign
                .Include(c => c.TournamentCampaign)
                .Include(c => c.CourtGroup)
                .Where(c => c.TournamentCampaignId != campaignId)
                .Select(c => c.CourtGroup).ToListAsync();

            return courtGroups;
        }
    }
}
