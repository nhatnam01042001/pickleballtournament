﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        private readonly AppDbContext _dbContext;
        public AccountRepository(AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _dbContext = context;
        }
        public Task<bool> CheckUserNameExited(string username) => _dbContext.Account.AnyAsync(u => u.UserName == username);

        public async Task<Account> GetAccountByUserNameAsync(string username)
        {
            var account = await _dbContext.Account.Include(a => a.User).FirstOrDefaultAsync(u => u.UserName == username);
            return account;
        }
        public async Task<Account> GetAccountByEmailAsync(string email)
        {
            var account = await _dbContext.Account.Include(a => a.User).FirstOrDefaultAsync(u => u.User.Email == email);
            return account;
        }
        

        public async Task<Account>? GetManagerAccountAsync()
        {
            var accountWithUser = _dbContext.Account.Include(u => u.User);
            var accountManager = await _dbContext.Account.Where(a => a.User.Role == Domain.Enums.Role.Manager).FirstOrDefaultAsync();
            return accountManager;
        }
        public async Task<bool> ExistsAsync(uint id)
        {
            return await _dbSet.AnyAsync(a => a.Id == id);
        }

        public async Task UpdateAccountAsync(Account account)
        {
            _dbContext.Account.Update(account);
            await _dbContext.SaveChangesAsync();
        }
    }
}
