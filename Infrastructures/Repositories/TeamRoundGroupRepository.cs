﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.TeamRoundGroupViewModels;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TeamRoundGroupRepository : GenericRepository<TeamRoundGroup>, ITeamRoundGroupRepository
    {
        private readonly AppDbContext _context;

        public TeamRoundGroupRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<TeamRoundGroup> GetSpecificTeamRoundGroupAsync(uint teamId, uint roundGroupId)
        {
            var teamRoundGroup = await _context.TeamRoundGroup
                .Include(trg => trg.Team)
                .Include(trg => trg.RoundGroup)
                .Where(trg => trg.TeamId == teamId && trg.RoundGroupId == roundGroupId)
                .FirstOrDefaultAsync();
            return teamRoundGroup;
        }

        public async Task<TeamRoundGroupStatistics> GetGroupTeamStatisticAsync(uint teamId, uint roundGroupId)
        {
            var statistic = await _context.TeamRoundGroup
                .Include(trg => trg.Team)
                .Include(trg => trg.RoundGroup)
                .Include(trg => trg.TeamRoundGroupStatistics)
                .Where(trg => trg.TeamId == teamId && trg.RoundGroupId == roundGroupId)
                .Select(trg => trg.TeamRoundGroupStatistics)
                .FirstOrDefaultAsync();
            return statistic;
        }

        public async Task<List<TeamRoundGroup>> GetTeamRoundGroupsByRoundGroupIdAsync(uint roundGroupId)
        {
            var teamRoundGroups = await _context.TeamRoundGroup
                .Include(trg => trg.Team)
                .Include(trg => trg.TeamRoundGroupStatistics)
                .Include(trg => trg.RoundGroup)
                .Where(trg => trg.RoundGroupId == roundGroupId)
                .ToListAsync();
            return teamRoundGroups;
        }

        public async Task<TeamRoundGroup> GetTeamRoundGroupByIdAsync(uint teamRoundGroupId)
        {
            var teamRoundGroup = await _context.TeamRoundGroup
                .Include(trg => trg.RoundGroup)
                .Include(trg => trg.Team)
                .Where(trg => trg.Id == teamRoundGroupId)
                .FirstOrDefaultAsync();
            return teamRoundGroup;
        }

        public async Task<List<TeamRoundGroup>> GetTeamRoundGroupByRoundId(uint roundId)
        {
            var teamRoundGroups = await _context.TeamRoundGroup
                .Include(trg => trg.RoundGroup)
                .Where(trg => trg.RoundGroup.RoundId == roundId)
                .ToListAsync();
            return teamRoundGroups;
        }

        public async Task<int> GetNumberOfTeamInRound(uint roundId)
        {
            var numberOfTeams = await _context.TeamRoundGroup
                .Include(t => t.RoundGroup)
                .CountAsync(t => t.RoundGroup.RoundId == roundId);

            return numberOfTeams;
        }

        public async Task<Dictionary<uint, List<TeamRoundGroupViewModel>>> GetListRankedTeamStatistic(uint roundId)
        {
            var groupedRankedTeams = await (from teamRoundGroup in _context.TeamRoundGroup
                                            join team in _context.Team on teamRoundGroup.TeamId equals team.Id
                                            join roundGroup in _context.RoundGroup on teamRoundGroup.RoundGroupId equals roundGroup.Id
                                            join round in _context.Round on roundGroup.RoundId equals round.Id
                                            join statistics in _context.TeamRoundGroupStatistic on teamRoundGroup.Id equals statistics.TeamRoundGroupId
                                            where round.Id == roundId
                                            group new { team, statistics, teamRoundGroup } by teamRoundGroup.RoundGroupId into grouped
                                            select new
                                            {
                                                RoundGroupId = grouped.Key,
                                                Teams = grouped
                                                    .OrderByDescending(g => g.statistics.Points)
                                                    .ThenByDescending(g => g.statistics.SetDifference)
                                                    .ThenByDescending(g => g.statistics.PointsDifference)
                                                    .ThenByDescending(g => g.statistics.Wins)
                                                    .ThenBy(g => g.statistics.Losses)
                                                    .Select(g => new TeamRoundGroupViewModel
                                                    {
                                                        TeamId = g.team.Id,
                                                        TeamName = g.team.TeamName,
                                                        Wins = g.statistics.Wins,
                                                        Losses = g.statistics.Losses,
                                                        Draws = g.statistics.Draws,
                                                        Points = g.statistics.Points,
                                                        SetDifference = g.statistics.SetDifference,
                                                        PointsDifference = g.statistics.PointsDifference,
                                                        TeamRoundGroupId = g.team.Id,
                                                        TeamStatus = g.teamRoundGroup.TeamStatus.ToString()
                                                    })
                                                    .ToList()
                                            }).ToListAsync();

            // Convert the result to a dictionary with RoundGroupId as the key
            var result = groupedRankedTeams.ToDictionary(g => g.RoundGroupId, g => g.Teams);

            return result;
        }

        public async Task<Dictionary<uint, List<TeamRoundGroupViewModel>>> GetListRankedTeam(uint roundId)
        {
            var groupedRankedTeams = await (from teamRoundGroup in _context.TeamRoundGroup
                                            join team in _context.Team on teamRoundGroup.TeamId equals team.Id
                                            join roundGroup in _context.RoundGroup on teamRoundGroup.RoundGroupId equals roundGroup.Id
                                            join round in _context.Round on roundGroup.RoundId equals round.Id
                                            join statistics in _context.TeamRoundGroupStatistic on teamRoundGroup.Id equals statistics.TeamRoundGroupId
                                            where round.Id == roundId
                                            group new { team, statistics, teamRoundGroup } by teamRoundGroup.RoundGroupId into grouped
                                            select new
                                            {
                                                RoundGroupId = grouped.Key,
                                                Teams = grouped
                                                    .OrderByDescending(g => g.statistics.Points)
                                                    .ThenByDescending(g => g.statistics.SetDifference)
                                                    .ThenByDescending(g => g.statistics.PointsDifference)
                                                    .ThenByDescending(g => g.statistics.Wins)
                                                    .ThenBy(g => g.statistics.Losses)
                                                    .Select(g => new TeamRoundGroupViewModel
                                                    {
                                                        TeamId = g.team.Id,
                                                        TeamName = g.team.TeamName,
                                                        TeamRoundGroupId = g.teamRoundGroup.Id,
                                                        TeamStatus = g.teamRoundGroup.TeamStatus.ToString(),
                                                        Wins = g.statistics.Wins,
                                                        Losses = g.statistics.Losses,
                                                        Draws = g.statistics.Draws,
                                                        Points = g.statistics.Points,
                                                        SetDifference = g.statistics.SetDifference,
                                                        PointsDifference = g.statistics.PointsDifference
                                                    })
                                                    .ToList()
                                            }).ToListAsync();
            var result = groupedRankedTeams.ToDictionary(g => g.RoundGroupId, g => g.Teams);

            return result;
        }
        /*public async Task<List<TeamRoundGroup>> GetTeamsRoundGroupByMatchId(uint matchId)
        {
            var teamsRoundGroup = await _context.TeamRoundGroup
                .Include(trg => trg.RoundGroup)
                .Include(trg)
        }*/

        public async Task<List<Team>> GetTeamWithoutMatchAsync(uint roundId)
        {
            var teams = await _context.TeamRoundGroup
                .Include(t => t.Team)
                .Include(t => t.RoundGroup)
                .Where(t => t.RoundGroup.RoundId == roundId)                
                .Select(t => t.Team).ToListAsync();

            var matches = await _context.PickleballMatch
                .Include(m => m.RoundGroup)
                .Where(m => m.RoundGroup.RoundId == roundId)
                .ToListAsync();

            var teamsWithoutMatch = teams.Where(t => !matches.Any(m => m.FirstTeamId == t.Id
            || m.SecondTeamId == t.Id)).ToList();
            return teamsWithoutMatch;
        }
    }
}
