﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class RoundRepository : GenericRepository<Round>, IRoundRepository
    {
        private readonly AppDbContext _context;

        public RoundRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<Round> GetNextRoundByRoundIdAsync(uint roundId)
        {
            var currentRound = await _context.Round.FindAsync(roundId);
            if (currentRound == null)
            {
                return null;
            }

            // Find the next round within the same tournament
            var nextRound = await _context.Round
                .Where(r => r.TournamentId == currentRound.TournamentId && r.RoundOrder > currentRound.RoundOrder)
                .OrderBy(r => r.RoundOrder)
                .FirstOrDefaultAsync();

            return nextRound;
        }

        public async Task<Round> GetPreviousRoundByRoundIdAsync(uint roundId)
        {
            var currentRound = await _context.Round.FindAsync(roundId);
            if(currentRound == null)
            {
                return null;
            }

            var previousRound = await _context.Round
                .Where(r => r.TournamentId == currentRound.TournamentId && r.RoundOrder < currentRound.RoundOrder)
                .OrderByDescending(r => r.RoundOrder)
                .FirstOrDefaultAsync();

            return previousRound;
        }

        public async Task<List<Round>> GetRoundByTournamentIdAsync(uint tournamentId)
        {
            var rounds = await _context.Round
                .Include(r => r.Tournament)
                .Where(r => r.TournamentId == tournamentId)
                .ToListAsync();
            return rounds;
        }

        public async Task<Round> GetRoundByRoundOrderAsync(uint tournamentId, int roundOrder)
        {
            var round = await _context.Round
                .Include(r => r.Tournament)
                .Where(r => r.TournamentId == tournamentId && r.RoundOrder == roundOrder)
                .FirstOrDefaultAsync();

            return round;
        }
    }
}
