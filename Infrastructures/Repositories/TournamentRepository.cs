﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TournamentRepository : GenericRepository<Tournament>, ITournamentRepository
    {
        private readonly AppDbContext _context;
        public TournamentRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService) 
            : base(context, timeService, claimsService)
        {
            _context = context;
        }
        public async Task<List<Tournament>> GetTournamentWithType(uint campaignId)
        {
            var tournament = await _context.Tournament
                .Include(t => t.TournamentCampaign)
                .Include(t => t.TournamentAthletes)
                .Where(t => t.TournamentCampaignId == campaignId).ToListAsync();
            return tournament;
        }
        public async Task<Tournament> GetTournamentByIdAndCampaignId(uint id, uint campaignId)
        {
            var tournament = await _context.Tournament.Where(t => t.Id == id && t.TournamentCampaignId == campaignId)
                .FirstOrDefaultAsync();
            return tournament;
        }
        public async Task<Tournament> GetTournamentByIdAsync(uint tournamentId)
        {
            var tournament = await _context.Tournament
                .Include(t => t.TournamentCampaign)
                .Include(t => t.TournamentAthletes)
                .FirstOrDefaultAsync(t => t.Id == tournamentId);

            return tournament;
        }
        public async Task<bool> CheckFormatTypeExisted(uint campaignId, Domain.Enums.FormatType formatType)
        {
            var formatTypeExisted = await _context.Tournament
                .Include(t => t.TournamentCampaign)
                .AnyAsync(t => t.TournamentCampaignId == campaignId && t.FormatType == formatType);

            return formatTypeExisted;
        }

        public async Task<List<Tournament>> GetTournamentsAsync()
        {
            var tournaments = await _context.Tournament
                .Include(t => t.TournamentCampaign)
                .Include(t => t.TournamentAthletes)
                .ToListAsync();

            return tournaments;
        }
    }
}
