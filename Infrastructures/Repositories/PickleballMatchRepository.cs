﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.PickleballMatchViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class PickleballMatchRepository : GenericRepository<PickleballMatch>, IPickleballMatchRepository
    {
        private readonly AppDbContext _context;

        public PickleballMatchRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<PickleballMatchViewModel>> GetMatchesByTournamentIdAsync(uint tournamentId)
        {
            /*var matchViewModels = await (from match in _context.PickleballMatch
                                         join roundGroup in _context.RoundGroup on match.RoundGroupId equals roundGroup.Id
                                         join round in _context.Round on roundGroup.RoundId equals round.Id
                                         join tournament in _context.Tournament on round.TournamentId equals tournament.Id
                                         join set in _context.Set on match.Id equals set.PickleballMatchId
                                         //join winCondition in _context.WinCondition on match.WinConditionId equals winCondition.Id
                                         orderby match.MatchDate
                                         where tournament.Id == tournamentId
                                         select new PickleballMatchViewModel
                                         {
                                             MatchId = match.Id,
                                             TournamentId = tournament.Id,
                                             FirstTeamId = match.FirstTeamId ?? 0, // Handle nullable
                                             SecondTeamId = match.SecondTeamId ?? 0,
                                             WinningTeamId = match.WinningTeamId ?? 0,// Handle nullable
                                             FirstTeam = match.FirstTeam != null ? match.FirstTeam.TeamName : string.Empty, // Handle nullable navigation property
                                             SecondTeam = match.SecondTeam != null ? match.SecondTeam.TeamName : string.Empty,
                                             WinningTeam = match.WinningTeam != null ? match.WinningTeam.TeamName : string.Empty,// Handle nullable navigation property
                                             WinConditionId = match.WinConditionId ?? 0,
                                             WinCondition = match.WinCondition != null ? match.WinCondition.ConditionName : string.Empty,
                                             RoundId = round.Id,
                                             RoundOrder = round.RoundOrder,
                                             RoundGroupId = roundGroup.Id,
                                             MatchOrder = match.MatchOrder,
                                             MatchDate = match.MatchDate,
                                             MatchStatus = match.MatchStatus.ToString(),
                                             NumberOfSet = match.Sets.Count()
                                         }).ToListAsync();*/
            var matchViewModels = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.WinningTeam)
                .Include(m => m.WinCondition)
                .Include(m => m.Sets)
                .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                .Include(m => m.Court)
                .Where(m => m.RoundGroup.Round.TournamentId == tournamentId)
                //&& m.RoundGroup.Round.RoundOrder > 1)
                .OrderBy(m => m.RoundGroup.Round.RoundOrder)
                .ThenBy(m => m.MatchOrder)
                .Select(m => new PickleballMatchViewModel
                {
                    MatchId = m.Id,
                    TournamentId = m.RoundGroup.Round.TournamentId,
                    Court = m.Court != null ? m.Court.CourtName : null,
                    CourtId = m.CourtId.HasValue ? m.CourtId.Value : null,
                    FirstTeamId = m.FirstTeamId ?? 0, // Handle nullable
                    SecondTeamId = m.SecondTeamId ?? 0,
                    WinningTeamId = m.WinningTeamId ?? 0, // Handle nullable
                    FirstTeam = m.FirstTeam != null ? m.FirstTeam.TeamName : string.Empty, // Handle nullable navigation property
                    SecondTeam = m.SecondTeam != null ? m.SecondTeam.TeamName : string.Empty,
                    WinningTeam = m.WinningTeam != null ? m.WinningTeam.TeamName : string.Empty, // Handle nullable navigation property
                    WinConditionId = m.WinConditionId ?? 0,
                    WinCondition = m.WinCondition != null ? m.WinCondition.ConditionName : string.Empty,
                    RoundId = m.RoundGroup.RoundId,
                    RoundOrder = m.RoundGroup.Round.RoundOrder,
                    RoundGroupId = (uint)m.RoundGroupId,
                    MatchOrder = m.MatchOrder,
                    MatchDate = m.MatchDate,
                    MatchStatus = m.MatchStatus.ToString(),
                    NumberOfSet = m.Sets.Count()

                }).ToListAsync();
            return matchViewModels;
        }
        public async Task<PickleballMatch> GetMatchByIdAsync(uint matchId)
        {
            var match = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.Court)
                .Include(m => m.WinningTeam)
            .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                    .ThenInclude(r => r.Tournament)
                    .Include(m => m.Sets)
                    .FirstOrDefaultAsync(m => m.Id == matchId);
            return match;
        }

        public async Task<List<PickleballMatch>> GetPickleballMatchInTournament(uint tournamentId)
        {
            var matches = await (from match in _context.PickleballMatch
                                 join roundGroup in _context.RoundGroup on match.RoundGroupId equals roundGroup.Id
                                 join round in _context.Round on roundGroup.RoundId equals round.Id
                                 join tournament in _context.Tournament on round.TournamentId equals tournament.Id
                                 join set in _context.Set on match.Id equals set.PickleballMatchId

                                 //join winCondition in _context.WinCondition on match.WinConditionId equals winCondition.Id
                                 where tournament.Id == tournamentId
                                 orderby match.MatchDate
                                 select match).ToListAsync();

            return matches;
        }

        public async Task<PickleballMatch> GetMatchWithTeamByIdAsync(uint matchId)
        {
            var match = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.Sets)
                .Where(m => m.Id == matchId)
                .FirstOrDefaultAsync();
            return match;
        }
        /*public async Task<List<PickleballMatch>> GetMatchesByRoundIdAsync(uint roundId)
        {
            var matches = await _context.PickleballMatch
                .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                .Where(m => m.RoundGroup.RoundId == roundId)
                .OrderBy(m => m.MatchOrder)
                .ToListAsync();

            return matches;
        }*/

        public async Task<PickleballMatch> GetMatchByOrderAsync(int matchOrder, uint tournamentId)
        {
            var match = await (from m in _context.PickleballMatch
                               join rg in _context.RoundGroup on m.RoundGroupId equals rg.Id
                               join r in _context.Round on rg.RoundId equals r.Id
                               join s in _context.Set on m.Id equals s.PickleballMatchId                               
                               where r.TournamentId == tournamentId && m.MatchOrder == matchOrder
                               select m)
                               .Include(m => m.Court)
                               .FirstOrDefaultAsync();
            return match;
        }

        public async Task<List<PickleballMatch>> GetMatchesByRoundGroupIdAsync(uint roundGroupId)
        {
            var matches = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.WinningTeam)
                .Include(m => m.WinCondition)
                .Include(m => m.Court)
            .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                    .ThenInclude(r => r.Tournament)
                    .Include(m => m.Sets)
                .Where(m => m.RoundGroupId == roundGroupId)
                .OrderBy(m => m.MatchDate)
                .ToListAsync();

            return matches;
        }

        public async Task<List<PickleballMatch>> GetMatchesByIdsAsync(List<uint> matchIds)
        {
            var matches = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.WinningTeam)
                .Include(m => m.WinCondition)
                .Include(m => m.Court)
            .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                    .ThenInclude(r => r.Tournament)
                    .Include(m => m.Sets)
                .Where(t => matchIds.Contains(t.Id))
                .OrderBy(t => t.MatchOrder)
                .ToListAsync();

            return matches;
        }

        public async Task<List<TeamRoundGroup>> GetTeamsRoundGroup(uint matchId)
        {
            var match = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .ThenInclude(t => t.TeamRoundGroups)
                .Include(m => m.SecondTeam)
                .ThenInclude(t => t.TeamRoundGroups)
                .FirstOrDefaultAsync(m => m.Id == matchId);


            var teamRoundGroups = match.FirstTeam.TeamRoundGroups
                .Where(f => f.RoundGroupId == match.RoundGroupId)
                .Concat(match.SecondTeam.TeamRoundGroups)
                .Where(s => s.RoundGroupId == match.RoundGroupId)
                .ToList();

            return teamRoundGroups;
        }
        public async Task<List<PickleballMatch>> GetMatchesByRoundIdAsync(uint roundId)
        {
            var matches = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.WinningTeam)
                .Include(m => m.WinCondition)
                .Include(m => m.Court)
            .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                    .ThenInclude(r => r.Tournament)
                    .Include(m => m.Sets)
                .Where(m => m.RoundGroup.RoundId == roundId)
                .OrderBy(m => m.MatchDate)
                .ToListAsync();

            return matches;
        }

        public async Task<bool> CheckMatchesInRoundCompleted(uint roundId)
        {
            var isCompleted = await _context.PickleballMatch
                .Include(m => m.RoundGroup)                
                .Where(m => m.RoundGroup.RoundId == roundId)
                .AllAsync(m => m.MatchStatus == Domain.Enums.MatchStatus.Completed
                || m.MatchStatus == Domain.Enums.MatchStatus.Canceled);

            return isCompleted;
        }

        public async Task<List<PickleballMatchViewModel>> GetNextRounsMatchesByTournamentIdAsync(uint tournamentId)
        {
            /*var matchViewModels = await (from match in _context.PickleballMatch
                                         join roundGroup in _context.RoundGroup on match.RoundGroupId equals roundGroup.Id
                                         join round in _context.Round on roundGroup.RoundId equals round.Id
                                         join tournament in _context.Tournament on round.TournamentId equals tournament.Id
                                         join set in _context.Set on match.Id equals set.Id
                                         where tournament.Id == tournamentId && round.RoundOrder > 1
                                         orderby match.MatchDate// Add your condition here
                                         select new PickleballMatchViewModel
                                         {
                                             MatchId = match.Id,
                                             TournamentId = tournament.Id,
                                             FirstTeamId = match.FirstTeamId ?? 0, // Handle nullable
                                             SecondTeamId = match.SecondTeamId ?? 0,
                                             WinningTeamId = match.WinningTeamId ?? 0, // Handle nullable
                                             FirstTeam = match.FirstTeam != null ? match.FirstTeam.TeamName : string.Empty, // Handle nullable navigation property
                                             SecondTeam = match.SecondTeam != null ? match.SecondTeam.TeamName : string.Empty,
                                             WinningTeam = match.WinningTeam != null ? match.WinningTeam.TeamName : string.Empty, // Handle nullable navigation property
                                             WinConditionId = match.WinConditionId ?? 0,
                                             WinCondition = match.WinCondition != null ? match.WinCondition.ConditionName : string.Empty,
                                             RoundId = round.Id,
                                             RoundOrder = round.RoundOrder,
                                             RoundGroupId = roundGroup.Id,
                                             MatchOrder = match.MatchOrder,
                                             MatchDate = match.MatchDate,
                                             MatchStatus = match.MatchStatus.ToString(),
                                             NumberOfSet = match.Sets.Count()
                                
                                         }).ToListAsync();*/
            var matchViewModels = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.WinningTeam)
                .Include(m => m.WinCondition)
                .Include(m => m.Sets)
                .Include(m => m.Court)
                .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                .Where(m => m.RoundGroup.Round.TournamentId == tournamentId
                && m.RoundGroup.Round.RoundOrder > 1)
                .OrderBy(m => m.MatchOrder)
                .Select(m => new PickleballMatchViewModel
                {
                    MatchId = m.Id,
                    TournamentId = m.RoundGroup.Round.TournamentId,
                    FirstTeamId = m.FirstTeamId ?? 0, // Handle nullable
                    SecondTeamId = m.SecondTeamId ?? 0,
                    WinningTeamId = m.WinningTeamId ?? 0, // Handle nullable
                    FirstTeam = m.FirstTeam != null ? m.FirstTeam.TeamName : string.Empty, // Handle nullable navigation property
                    SecondTeam = m.SecondTeam != null ? m.SecondTeam.TeamName : string.Empty,
                    WinningTeam = m.WinningTeam != null ? m.WinningTeam.TeamName : string.Empty, // Handle nullable navigation property
                    WinConditionId = m.WinConditionId ?? 0,
                    WinCondition = m.WinCondition != null ? m.WinCondition.ConditionName : string.Empty,
                    RoundId = m.RoundGroup.RoundId,
                    RoundOrder = m.RoundGroup.Round.RoundOrder,
                    RoundGroupId = (uint)m.RoundGroupId,
                    MatchOrder = m.MatchOrder,
                    MatchDate = m.MatchDate,
                    MatchStatus = m.MatchStatus.ToString(),
                    NumberOfSet = m.Sets.Count(),
                    Court = m.Court != null ? m.Court.CourtName : null,
                    CourtId = m.CourtId.HasValue ? m.CourtId.Value : null

                }).ToListAsync();
            return matchViewModels;
        }

        public async Task<List<PickleballMatch>> GetNextRounsMatchesInTournamentAsync(uint tournamentId)
        {
            var matches = await _context.PickleballMatch
                .Include(m => m.FirstTeam)
                .Include(m => m.SecondTeam)
                .Include(m => m.WinningTeam)
                .Include(m => m.WinCondition)
                .Include(m => m.Sets)
                .Include(m => m.Court)
                .Include(m => m.RoundGroup)
                .ThenInclude(rg => rg.Round)
                .Where(m => m.RoundGroup.Round.TournamentId == tournamentId
                && m.RoundGroup.Round.RoundOrder > 1)
                .OrderBy(m => m.MatchOrder)
                .ToListAsync();

            return matches;
        }
    }
}
