﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TournamentCampaignRepository : GenericRepository<TournamentCampaign>, ITournamentCampaignRepository
    {
        private readonly AppDbContext _dbContext;

        public TournamentCampaignRepository(AppDbContext dbContext,
            ICurrentTime currentTime,
            IClaimsService claimsService)
            : base(dbContext, currentTime, claimsService)
        {
            _dbContext = dbContext;
            
        }
        
        public async Task<List<TournamentCampaign>> GetTournamentCampaignsAsync()
        {
            var tournamentCampaigns = await _dbContext.TournamentCampaign
                .Include(t => t.Tournaments)
                .Include(t => t.CourtGroupTournamentCampaigns)
                .ThenInclude(t => t.CourtGroup)
                .Include(t => t.Athletes)
                .OrderByDescending(t => t.CreationDate)
                .ToListAsync();

            return tournamentCampaigns;
        }

        public async Task<TournamentCampaign> GetTournamentCampaignByIdAsync(uint campaignId)
        {
            var tournamentCampaign = await _dbContext.TournamentCampaign
                .Include(t => t.Tournaments)
                .Include(t => t.CourtGroupTournamentCampaigns)
                .ThenInclude(t => t.CourtGroup)
                .Include(t => t.Athletes)
                .FirstOrDefaultAsync(t => t.Id == campaignId);

            return tournamentCampaign;
        }

        public async Task<bool> CheckAllTournamentCompleted(uint campaignId)
        {
            var isCompleted = await _dbContext.TournamentCampaign
                .Include(c => c.Tournaments)
                .Where(c => c.Id == campaignId)
                .AllAsync(c => c.Tournaments.All(t => t.TournamentStatus == Domain.Enums.MatchStatus.Completed));

            return isCompleted;
        }
    }
}
