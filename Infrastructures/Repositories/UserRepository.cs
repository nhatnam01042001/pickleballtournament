﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;

        public UserRepository(AppDbContext dbContext,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(dbContext, timeService, claimsService)
        {
            _dbContext = dbContext;
        }

        public Task<bool> CheckUserNameExisted(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<User> GetUserByUserName(string userName)
        {
            throw new NotImplementedException();
        }

        public Task<User> GetUserNameAndPasswordHash(string userName, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public async Task<User> GetUserByAccountId(uint accountId)
        {
            var user = _dbContext.User.Find(accountId);
            return user;
        }

        public async Task<List<string>> GetListEmail()
        {
            var user = await _dbContext.User.ToListAsync();
            var emails = user.Select(user => user.Email).ToList();
            return emails;
        }
        public async Task<List<User>> GetUserWithRank(double rank)
        {
            var users = await _dbContext.User.ToListAsync();
            var usersWithRank = users.Where(users => users.Rank == rank).ToList();
            return usersWithRank;
        }

        public async Task<List<User>> GetAthleteUserAsync()
        {
            var users = await _dbContext.User.Where(u => u.Role == Domain.Enums.Role.Athlete).ToListAsync();
            return users;
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await _dbContext.User.ToListAsync();
        }
        public async Task<User> GetUserByIdAsync(uint id)
        {
            return await _dbContext.User.FindAsync(id); // Ensure your DbContext is properly configured
        }

        public async Task<List<User>> GetUsersAsync(int pageIndex, int pageSize)
        {
            return await _dbContext.User
                .Skip(pageIndex * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }

        public async Task<int> GetTotalUsersCountAsync()
        {
            return await _dbContext.User.CountAsync();
        }
        public async Task AddUserAsync(User user)
        {
            await _dbContext.User.AddAsync(user);
        }
        // public async Task AddUserAsync(uint id)
        // {
        //     await _dbContext.User.Where(x => x.Account.UserId == id).Include(x => x.Account).ToListAsync();
        // }

        public void UpdateUser(User user)
        {
            _dbContext.User.Update(user);
        }
        
        public override async Task<List<User>> GetAllAsync()
        {
            return await _dbContext.User.Where(u => u.Status != UserStatus.Deleted).ToListAsync();
        }

        public override async Task<User?> GetByIdAsync(uint id)
        {
            var result = await _dbSet.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted != true);
            return result;
        }
        public async Task<User>GetUserWithAccountAsync(uint userId)
        {
            var user = await _dbContext.User.Include(u => u.Account)
                .FirstOrDefaultAsync(u => u.Id == userId);
            return user;
        }

        public async Task<List<User>> GetUserNotParticipateInCampaignAsync(uint campaignId)
        {
            var users = await _dbContext.User
                .Include(u => u.Athletes)
                .ThenInclude(a => a.TournamentCampaign)
                .Where(u => u.Role == Role.Athlete)
                .ToListAsync();

            var athletes = await _dbContext.Athlete
                .Include(a => a.User)
                .Include(a => a.TournamentCampaign)
                .Where(a => a.TournamentCampaignId == campaignId)
                .Select(a => a.UserId)
                .ToListAsync();

            var usersNotInCampaign = users
                .Where(u => !athletes.Contains(u.Id))
                .ToList();

            return usersNotInCampaign;
        }

        public async Task<List<User>> GetUsersByIdAsync(List<uint> ids)
        {
            var users = await _dbContext.User
                .Where(a => ids.Contains(a.Id))
                .ToListAsync();

            return users;
        }
        /*public async Task<User> GetUserByAccountIdAsync(uint accountId)
        {

        }*/
    }
}
