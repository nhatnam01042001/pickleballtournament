﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class RoundGroupRepository : GenericRepository<RoundGroup>, IRoundGroupRepository
    {
        private readonly AppDbContext _context;

        public RoundGroupRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService) 
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<RoundGroup>> GetRoundGroupByRoundIdAsync(uint roundId)
        {
            var roundGroup = await _context.RoundGroup.Where(rg => rg.RoundId == roundId).ToListAsync();
            return roundGroup;
        }

        public async Task<RoundGroup> GetRoundGroupWithRoundByIdAsync(uint roundGroupId)
        {
            var roundGroup = await _context.RoundGroup.Include(rg => rg.Round)
                .Where(rg => rg.Id == roundGroupId).FirstOrDefaultAsync();
            return roundGroup;
        }

        public async Task<RoundGroup> GetRoundGroupByIdAsync(uint roundGroupId)
        {
            var roundGroup = await _context.RoundGroup
                .Include(rg => rg.Round)
                .Where(rg => rg.Id == roundGroupId)
                .FirstOrDefaultAsync();
            return roundGroup;
        }

        public async Task<RoundGroup> GetRoundGroupByRoundIdElimination(uint roundId)
        {
            var roundGroup = await _context.RoundGroup
                .Include(rg => rg.Round)
                .Where(rg => rg.Round.Id == roundId)
                .FirstOrDefaultAsync();

            return roundGroup;
        }
    }
}
