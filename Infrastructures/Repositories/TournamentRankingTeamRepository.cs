﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TournamentRankingTeamRepository : GenericRepository<TournamentRankingTeam>, ITournamentRankingTeamRepository
    {
        private readonly AppDbContext _context;

        public TournamentRankingTeamRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService) 
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<TournamentRankingTeam>> GetTournamentRankingTeamsByTournamentIdAsync(uint tournamentId)
        {
            var rankingTeams = await _context.TournamentRankingTeam
                .Include(rt => rt.Tournament)                
                .Include(rt => rt.Team)
                .ThenInclude(t => t.TeamRoundGroups)                
                .Where(rt => rt.TournamentId == tournamentId)
                .OrderBy(rt => rt.Rank)
                .ToListAsync();

            return rankingTeams;
        }
        
        public async Task<TournamentRankingTeam> GetSpecificRankingTeam(uint tournamentId, uint teamId)
        {
            var rankingTeam = await _context.TournamentRankingTeam
                .Include(t => t.Team)
                .Include(t => t.Tournament)
                .FirstOrDefaultAsync(t => t.TournamentId == tournamentId
                && t.TeamId == teamId);

            return rankingTeam;
        }
    }
}
