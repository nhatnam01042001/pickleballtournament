﻿using Application;
using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CampaignRegistRepository : GenericRepository<CampaignRegistration>, ICampaignRegistRepository
    {
        private readonly AppDbContext _context;

        public CampaignRegistRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }
        public Task<bool> CheckRegistrationExisted(string email, uint id)
        {
            var isExisted = _context.CampaignRegistration
                .AnyAsync(t => t.Email == email && t.TournamentCampaignId == id);
            return isExisted;
        }

        public async Task<List<CampaignRegistration>> GetApproveRegistration()
        {
            var registrationList = await _context.CampaignRegistration
                .Where(t => t.RegistrationStatus == TournamentRegistrationStatus.Approved)
                .ToListAsync();
            return registrationList;
        }

        public async Task<List<CampaignRegistration>> GetRegistrationByCampaignId(uint campaignId)
        {
            var listRegist = await _context.CampaignRegistration               
                .Where(tr => tr.TournamentCampaignId == campaignId)
                .ToListAsync();
            return listRegist;
        }
    }

}
