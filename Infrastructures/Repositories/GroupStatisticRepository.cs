﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class GroupStatisticRepository : GenericRepository<TeamRoundGroupStatistics>,IGroupStatisticRepository
    {
        private readonly AppDbContext _context;

        public GroupStatisticRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context, timeService, claimsService)
        {
            _context = context;
        }
        public async Task<TeamRoundGroupStatistics> GetTeamStatisticsByTeamRoundGroupIdAsync(uint teamRoundGroupId)
        {
            var statistic = await _context.TeamRoundGroupStatistic
                .Where(s => s.TeamRoundGroupId == teamRoundGroupId)
                .FirstOrDefaultAsync();
            return statistic;
        }

        public async Task<TeamRoundGroupStatistics> GetSpecificTeamStatisticsAsync(uint roundGroupId, uint teamId)
        {
            var statistic = await _context.TeamRoundGroupStatistic
                .Include(s => s.TeamRoundGroup)
                .Where(s => s.TeamRoundGroup.RoundGroupId == roundGroupId
                && s.TeamRoundGroup.TeamId == teamId).FirstOrDefaultAsync();

            return statistic;
        }

        public async Task<List<TeamRoundGroupStatistics>> GetTeamStatisticByRoundIdAsync(uint roundId)
        {
            var teamStats = await _context.TeamRoundGroupStatistic
                .Include(t => t.TeamRoundGroup)
                .ThenInclude(trg => trg.RoundGroup)
                .Where(t => t.TeamRoundGroup.RoundGroup.RoundId == roundId)
                .ToListAsync();

            return teamStats;
        }

        public async Task<List<TeamRoundGroupStatistics>> GetTeamStatisticByTournamentIdAsync(uint tournamentId, uint teamId)
        {
            var teamStats = await _context.TeamRoundGroupStatistic
                .Include(t => t.TeamRoundGroup)
                .ThenInclude(trg => trg.RoundGroup)
                .ThenInclude(rg => rg.Round)
                .Where(t => t.TeamRoundGroup.RoundGroup.Round.TournamentId == tournamentId
                && t.TeamRoundGroup.TeamId == teamId)
                .ToListAsync();

            return teamStats;
        }
    }
}
