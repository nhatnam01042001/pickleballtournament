﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.CommentViewModel;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories;

public class CommentRepository : GenericRepository<Comment>, ICommentRepository
{
    private readonly AppDbContext _dbContext;
    public CommentRepository(AppDbContext dbContext, ICurrentTime timeService, IClaimsService claimsService)
        : base(dbContext, timeService, claimsService)
    {
        _dbContext = dbContext;
    }
    public async Task<IEnumerable<Comment>> GetByTournamentIdAsync(uint tournamentId)
    {
        // Chỉ lấy các bình luận không bị xóa mềm
        return await _dbSet
            .Include(c => c.Account)
            .ThenInclude(a => a.User)
            .Where(x => x.TournamentId == tournamentId && (x.IsDeleted == false || x.IsDeleted == null))
            .ToListAsync();
    }

    public async Task<bool> TournamentExistsAsync(uint tournamentId)
    {
        // Chỉ kiểm tra các tournament có bình luận không bị xóa mềm
        //return await _dbSet
        //    .Include(c => c.Account)
        //    .ThenInclude(a => a.User)
        //    .AnyAsync(x => x.TournamentId == tournamentId && (x.IsDeleted == false || x.IsDeleted == null));

        return await _dbContext.TournamentCampaign.AnyAsync(x => x.Id == tournamentId);

    }

    public async Task<Comment?> GetByIdAsync(uint id)
    {
        // Chỉ lấy bình luận không bị xóa mềm
        return await _dbSet
            .Include(c => c.Account)
            .ThenInclude(a => a.User)
            .FirstOrDefaultAsync(x => x.Id == id && (x.IsDeleted == false || x.IsDeleted == null));
    }

}