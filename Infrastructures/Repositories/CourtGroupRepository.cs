﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class CourtGroupRepository : GenericRepository<CourtGroup>, ICourtGroupRepository
    {
        private readonly AppDbContext _context;
        public CourtGroupRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService) : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<CourtGroup>> GetCourtGroupsByCampaignIdAsync(uint campaignId)
        {
            var courtGroups = await _context.CourtGroupTournamentCampaign
                .Include(c => c.TournamentCampaign)
                .Include(c => c.CourtGroup)
                .ThenInclude(cg => cg.Courts)
                .Where(c => c.TournamentCampaignId == campaignId)
                .Select(c => c.CourtGroup)
                .ToListAsync();

            return courtGroups;
        }

        public async Task<List<CourtGroup>> GetCourtGroupsByIdAsync(IEnumerable<uint> courtGroupIds)
        {
            var courtGroups = await _context.CourtGroup
                .Where(cg => courtGroupIds.Contains(cg.Id))
                .ToListAsync();

            return courtGroups;
        }

        public async Task<List<CourtGroup>> GetCourtGroupNotExistedinCampaign(uint campaignId)
        {
            var courtGroups = await _context.CourtGroup
                .Include(cg => cg.CourtGroupTournamentCampaigns)
                .ToListAsync();

            var campaignCourtGroups = await _context.CourtGroupTournamentCampaign
                .Include(c => c.TournamentCampaign)
                .Include(c => c.CourtGroup)
                .Where(c => c.TournamentCampaignId == campaignId)
                .ToListAsync();

            var courtGroupNotExistedCampaign = courtGroups
                .Where(c => !campaignCourtGroups.Any(cg => cg.CourtGroupId == c.Id))
                .ToList();

            return courtGroupNotExistedCampaign;
        }
    }
}
