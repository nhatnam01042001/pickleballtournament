﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class TournamentAthleteRepository : GenericRepository<TournamentAthlete>, ITournamentAthleteRepository
    {
        private readonly AppDbContext _context;

        public TournamentAthleteRepository
            (AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService) 
            : base(context, timeService, claimsService)
        {
            _context = context;
        }

        public async Task<List<Athlete>> GetAthleteByTournamentIdAsync(uint tournamentId)
        {
            var tournamentAthletes = await _context.TournamentAthlete
                .Include(t => t.Athlete)
                .ThenInclude(a => a.User)
                .Include(t => t.Athlete.GuestPlayer)
                .Where(t => t.TournamentId == tournamentId)
                .Select(t => t.Athlete)
                .ToListAsync();

            var sortedAthletes = tournamentAthletes
        .OrderBy(a => a.User != null ? a.User.FullName : a.GuestPlayer.FullName)
        .ToList();
            return sortedAthletes;
        }

        public async Task<Athlete> GetSpecificTournamentAthlete(uint athleteId, uint tournamentId)
        {
            var athlete = await _context.TournamentAthlete
                .Include(t => t.Tournament)
                .Include(t => t.Athlete)
                .Where(t => t.AthleteId == athleteId && t.TournamentId == tournamentId)
                .Select(t => t.Athlete)
                .FirstOrDefaultAsync();
            return athlete;
        }
        public async Task<List<Athlete>> GetAthletesWithNonTeamAsync(uint tournamentId)
        {
            var tournamentAthletes = await _context.TournamentAthlete
                .Include(t => t.Athlete)
                .ThenInclude(a => a.User)
                .Include(t => t.Athlete.GuestPlayer)
                .Where(t => t.TournamentId == tournamentId)
                .Select(t => t.Athlete)
                .ToListAsync();

            var nonTeamAthletes = tournamentAthletes
                .Where(a => a.Teams == null || !a.Teams.Any())
                .ToList();
            var sortedAthletes = nonTeamAthletes
        .OrderBy(a => a.User != null ? a.User.FullName : a.GuestPlayer.FullName)
        .ToList();

            return sortedAthletes;
            //return nonTeamAthletes;
        }

        public async Task<Athlete> GetAthleteInTournamentAsync(uint athleteId, uint tournamentId)
        {
            var athlete = await _context.TournamentAthlete
                .Include(ta => ta.Athlete)
                .ThenInclude(a => a.User)
                .Include(ta => ta.Athlete)
                .ThenInclude(a => a.GuestPlayer)
                .Where(ta => ta.AthleteId == athleteId && ta.TournamentId == tournamentId)
                .Select(ta => ta.Athlete)
                .FirstOrDefaultAsync();
            return athlete;
        }

        public async Task<List<Athlete>> GetAthletesNotInTournamentAsync(uint tournamentId)
        {
            var athletes = await _context.Athlete
                .Include(a => a.TournamentCampaign)
                .ThenInclude(a => a.Tournaments)
                .Include(a => a.GuestPlayer)
                .Include(a => a.User)
                .Include(a => a.TournamentAthletes)
                .Where(a => a.TournamentCampaign.Tournaments.Any(t => t.Id == tournamentId))
                .ToListAsync();

            var tournamentAthletes = await _context.TournamentAthlete
                .Include(t => t.Tournament)
                .Include(t => t.Athlete)
                .Where(t => t.TournamentId == tournamentId)
                .Select(t => t.AthleteId)
                .ToListAsync();

            var athletesNotInTournament = athletes
                .Where(a => !tournamentAthletes.Contains(a.Id)).ToList();

            return athletesNotInTournament;
        }
    }
}
