﻿using Application;
using Application.Repositories;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Infrastructures.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly IUserRepository _userRepository;
        private readonly ITournamentCampaignRepository _tounamentCampaignRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IAthleteRepository _athleteRepository;
        private readonly IPickleballMatchRepository _pickleballMatchRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly ICampaignRegistRepository _campaignRegistRepository;
        private readonly ITournamentRepository _tournamentRepository;
        private readonly IRoundRepository _roundRepository;
        private readonly IRoundGroupRepository _roundGroupRepository;
        private IDbContextTransaction _transaction;
        private readonly ICourtRepository _courtRepository;
        private readonly ITeamRoundGroupRepository _teamRoundGroupRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly INewsArticleRepository _newsArticleRepository;
        private readonly ICourtGroupRepository _courtGroupRepository;
        private readonly IWinConditionRepository _winConditionRepository;
        private readonly IGroupStatisticRepository _groupStatisticRepository;
        private readonly ITournamentRegistrationRepository _tournamentRegistrationRepository;
        private readonly ITournamentAthleteRepository _tournamentAthleteRepository;
        private readonly ISetRepository _setRepository;
        private readonly IUserRegistrationRepository _userRegistrationRepository;
        private readonly ICourtGroupCampaignRepository _courtGroupCampaignRepository;
        private readonly ITournamentRankingTeamRepository _tournamentRankingTeamRepository;

        public UnitOfWork(AppDbContext dbContext,
            IUserRepository userRepository,
            IAccountRepository accountRepository,
            ITournamentCampaignRepository tournamentCampaignRepository,
            ITeamRepository teamRepository,
            IAthleteRepository athleteRepository,
            IPickleballMatchRepository pickleballMatchRepository,
            ICampaignRegistRepository campaignRegistRepository,
            ITournamentRepository tournamentRepository,
            IRoundRepository roundRepository,
            IRoundGroupRepository roundGroupRepository,
            ICourtRepository courtRepository,
            ITeamRoundGroupRepository teamRoundGroupRepository,
            ICommentRepository commentRepository,
            INewsArticleRepository newsArticleRepository,
            ICourtGroupRepository courtGroupRepository,
            IWinConditionRepository winConditionRepository,
            IGroupStatisticRepository groupStatisticRepository,
            ITournamentRegistrationRepository tournamentRegistrationRepository,
            ITournamentAthleteRepository tournamentAthleteRepository,
            ISetRepository setRepository,
            IUserRegistrationRepository userRegistrationRepository,
            ICourtGroupCampaignRepository courtGroupCampaignRepository,
            ITournamentRankingTeamRepository tournamentRankingTeamRepository)
        {
            _dbContext = dbContext;
            _userRepository = userRepository;
            _accountRepository = accountRepository;
            _tounamentCampaignRepository = tournamentCampaignRepository;
            _teamRepository = teamRepository;
            _pickleballMatchRepository = pickleballMatchRepository;
            _athleteRepository = athleteRepository;
            _campaignRegistRepository = campaignRegistRepository;
            _tournamentRepository = tournamentRepository;
            _roundRepository = roundRepository;
            _roundGroupRepository = roundGroupRepository;
            _courtRepository = courtRepository;
            _teamRoundGroupRepository = teamRoundGroupRepository;
            _commentRepository = commentRepository;
            _newsArticleRepository = newsArticleRepository;
            _courtGroupRepository = courtGroupRepository;
            _winConditionRepository = winConditionRepository;
            _groupStatisticRepository = groupStatisticRepository;
            _tournamentRegistrationRepository = tournamentRegistrationRepository;
            _tournamentAthleteRepository = tournamentAthleteRepository;
            _setRepository = setRepository;
            _userRegistrationRepository = userRegistrationRepository;
            _courtGroupCampaignRepository = courtGroupCampaignRepository;
            _tournamentRankingTeamRepository = tournamentRankingTeamRepository;
        }

        public IUserRepository UserRepository => _userRepository;
        public ITournamentCampaignRepository TournamentCampaignRepository => _tounamentCampaignRepository;
        public IAccountRepository AccountRepository => _accountRepository;

        public IAthleteRepository AthleteRepository => _athleteRepository;

        public IPickleballMatchRepository PickleballMatchRepository => _pickleballMatchRepository;

        public ITeamRepository TeamRepository => _teamRepository;

        public ICampaignRegistRepository CampaignRegistRepository => _campaignRegistRepository;

        public ITournamentRepository TournamentRepository => _tournamentRepository;

        public IRoundRepository RoundRepository => _roundRepository;

        public IRoundGroupRepository RoundGroupRepository => _roundGroupRepository;

        public ICourtRepository CourtRepository => _courtRepository;

        public ITeamRoundGroupRepository TeamRoundGroupRepository => _teamRoundGroupRepository;
        public ICommentRepository CommentRepository => _commentRepository;
        public INewsArticleRepository NewsArticleRepository => _newsArticleRepository;
        public ICourtGroupRepository CourtGroupRepository => _courtGroupRepository;

        public IWinConditionRepository WinConditionRepository => _winConditionRepository;

        public IGroupStatisticRepository GroupStatisticRepository => _groupStatisticRepository;

        public ITournamentAthleteRepository TournamentAthleteRepository => _tournamentAthleteRepository;

        public ITournamentRegistrationRepository TournamentRegistrationRepository => _tournamentRegistrationRepository;

        public ISetRepository SetRepository => _setRepository;

        public IUserRegistrationRepository UserRegistrationRepository => _userRegistrationRepository;
        public ICourtGroupCampaignRepository CourtGroupCampaignRepository => _courtGroupCampaignRepository;
        public ITournamentRankingTeamRepository TournamentRankingTeamRepository => _tournamentRankingTeamRepository;

        public async Task<int> SaveChangeAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            _transaction = await _dbContext.Database.BeginTransactionAsync();
            return _transaction;
        }

        public async Task CommitAsync()
        {
            await _transaction.CommitAsync();
        }

        public async Task RollbackAsync()
        {
            await _transaction.RollbackAsync();
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }
        
    }
}
