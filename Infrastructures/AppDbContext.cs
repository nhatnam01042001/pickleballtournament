﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<CourtGroup> CourtGroup { get; set; }
        public DbSet<Court> Court { get; set; }
        public DbSet<CourtGroupTournamentCampaign> CourtGroupTournamentCampaign { get; set; }
        public DbSet<PickleballMatch> PickleballMatch { get; set; }        
        public DbSet<User> User { get; set; }
        public DbSet<Athlete> Athlete { get; set; }
        public DbSet<TournamentCampaign> TournamentCampaign { get; set; }
        public DbSet<CampaignRegistration> CampaignRegistration { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<Tournament> Tournament { get; set; }
        public DbSet<WinCondition> WinCondition { get; set; }
        public DbSet<Set> Set { get; set; }
        public DbSet<NewsArticle> NewsArticle { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<TeamRoundGroupStatistics> TeamRoundGroupStatistic { get; set; }
        public DbSet<RoundGroup> RoundGroup { get; set; }
        public DbSet<Round> Round { get; set; }
        public DbSet<TeamRoundGroup> TeamRoundGroup { get; set; }
        public DbSet<TournamentRegistration> TournamentRegistration { get; set; }
        public DbSet<TournamentAthlete> TournamentAthlete { get; set; }
        public DbSet<UserRegistration> UserRegistration { get; set; }
        public DbSet<TournamentRankingTeam> TournamentRankingTeam { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /*modelBuilder.Entity<Participant>()
            .HasOne(p => p.Users)
            .WithMany(u => u.Participants)
            .HasForeignKey(p => p.UserId);

            modelBuilder.Entity<Participant>()
                .HasOne(p => p.TournamentRegistration)
                .WithMany(t => t.Participants)
                .HasForeignKey(p => p.TournamentRegistrationId);*/

            modelBuilder.Entity<Team>()
                .HasOne(t => t.FirstAthlete)
                .WithMany()
                .HasForeignKey(t => t.FirstAthleteId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Team>()
                .HasOne(t => t.SecondAthlete)
                .WithMany()
                .HasForeignKey(t => t.SecondAthleteId)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<Tournament>()
            .HasOne(t => t.TournamentCampaign)
            .WithMany(tc => tc.Tournaments)
            .HasForeignKey(t => t.TournamentCampaignId) 
            .OnDelete(DeleteBehavior.Restrict);

            /*modelBuilder.Entity<TournamentRegistration>()
                .HasOne(tr => tr.Participant)
                .WithOne(p => p.GuestPlayers)
                .HasForeignKey<Participant>(p => p.TournamentRegistrationId);*/

            modelBuilder.Entity<User>()
            .HasOne(u => u.Account)
            .WithOne(a => a.User)
            .HasForeignKey<Account>(a => a.UserId)
            .OnDelete(DeleteBehavior.Cascade);

            /*modelBuilder.Entity<User>()
                .HasOne(u => u.Participant)
                .WithOne(p => p.User)
                .HasForeignKey<Participant>(p => p.UserId)
                .OnDelete(DeleteBehavior.Restrict);*/
            modelBuilder.Entity<User>()
                .HasOne(u => u.Registration)
                .WithOne(r => r.User)
                .HasForeignKey<UserRegistration>(r => r.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Athlete>()
            .HasOne(p => p.User)
            .WithMany(u => u.Athletes)
            .HasForeignKey(p => p.UserId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CampaignRegistration>()
                .HasOne(tr => tr.Athlete)
                .WithOne(p => p.GuestPlayer)
                .HasForeignKey<Athlete>(p => p.GuestPlayerId)
                .OnDelete(DeleteBehavior.Restrict);

            /*modelBuilder.Entity<User>()
            .HasOne(u => u.Participant)
            .WithOne(p => p.User)
            .HasForeignKey<Participant>(p => p.UserId)
            .OnDelete(DeleteBehavior.Cascade);*/
            modelBuilder.Entity<Tournament>()
            .HasMany(t => t.Rounds)
            .WithOne(r => r.Tournament)
            .HasForeignKey(r => r.TournamentId);

            // Round -> Matches
            /*modelBuilder.Entity<Round>()
                .HasMany(r => r.Matches)
                .WithOne(m => m.Round)
                .HasForeignKey(m => m.RoundId);*/

            // Round -> RoundGroups
            modelBuilder.Entity<Round>()
                .HasMany(r => r.RoundGroups)
                .WithOne(rg => rg.Round)
                .HasForeignKey(rg => rg.RoundId);

            // RoundGroup -> Matches
            modelBuilder.Entity<RoundGroup>()
                .HasMany(rg => rg.Matches)
                .WithOne(m => m.RoundGroup)
                .HasForeignKey(m => m.RoundGroupId);

            // Team -> Matches as Team1
            modelBuilder.Entity<Team>()
                .HasMany(t => t.FirstTeamMatches)
                .WithOne(m => m.FirstTeam)
                .HasForeignKey(m => m.FirstTeamId)
                .OnDelete(DeleteBehavior.Restrict);

            // Team -> Matches as Team2
            modelBuilder.Entity<Team>()
                .HasMany(t => t.SecondTeamMatches)
                .WithOne(m => m.SecondTeam)
                .HasForeignKey(m => m.SecondTeamId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.SetsWon)
                .WithOne(s => s.WinningTeam)
                .HasForeignKey(s => s.WinningTeamId)
                .OnDelete(DeleteBehavior.Restrict);


            // Configure many-to-many relationship
            modelBuilder.Entity<TeamRoundGroup>()
                .HasKey(trg => new { trg.TeamId, trg.RoundGroupId });

            modelBuilder.Entity<TeamRoundGroup>()
                .HasOne(trg => trg.Team)
                .WithMany(t => t.TeamRoundGroups)
                .HasForeignKey(trg => trg.TeamId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TeamRoundGroup>()
                .HasOne(trg => trg.RoundGroup)
                .WithMany(rg => rg.TeamRoundGroups)
                .HasForeignKey(trg => trg.RoundGroupId)
                .OnDelete(DeleteBehavior.Restrict);

            /*modelBuilder.Entity<PickleballMatch>()
            .HasOne(m => m.WinningTeam)
            .WithMany(t => t.MatchesWon)
            .HasForeignKey(m => m.WinningTeamId)
            .OnDelete(DeleteBehavior.Restrict);*/

            modelBuilder.Entity<TeamRoundGroupStatistics>()
            .HasKey(trgs => trgs.Id);

            /*modelBuilder.Entity<TeamRoundGroupStatistics>()
                .HasOne(trgs => trgs.Team)
                .WithMany(t => t.Statistics)
                .HasForeignKey(trgs => trgs.TeamId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TeamRoundGroupStatistics>()
                .HasOne(trgs => trgs.RoundGroup)
                .WithMany(rg => rg.Statistics)
                .HasForeignKey(trgs => trgs.RoundGroupId)
                .OnDelete(DeleteBehavior.Restrict);*/

            modelBuilder.Entity<TeamRoundGroup>()
                .HasOne(trg => trg.TeamRoundGroupStatistics)
                .WithOne(trgs => trgs.TeamRoundGroup)
                .HasForeignKey<TeamRoundGroupStatistics>
                (trgs => trgs.TeamRoundGroupId)
                .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<Team>()
                .HasMany(t => t.MatchesWon)
                .WithOne(m => m.WinningTeam)
                .HasForeignKey(m => m.WinningTeamId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.SetsWon)
                .WithOne(m => m.WinningTeam)
                .HasForeignKey(m => m.WinningTeamId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WinCondition>()
                .HasMany(wc => wc.PickleballMatches)
                .WithOne(pm => pm.WinCondition)
                .HasForeignKey(pm => pm.WinConditionId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CourtGroupTournamentCampaign>()
            .HasKey(cgtc => new { cgtc.CourtGroupId, cgtc.TournamentCampaignId });

            modelBuilder.Entity<CourtGroupTournamentCampaign>()
                .HasOne(cgtc => cgtc.CourtGroup)
                .WithMany(cg => cg.CourtGroupTournamentCampaigns)
                .HasForeignKey(cgtc => cgtc.CourtGroupId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CourtGroupTournamentCampaign>()
                .HasOne(cgtc => cgtc.TournamentCampaign)
                .WithMany(tc => tc.CourtGroupTournamentCampaigns)
                .HasForeignKey(cgtc => cgtc.TournamentCampaignId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TournamentRegistration>()
            .HasOne(tr => tr.Tournament)
            .WithMany(t => t.TournamentRegistrations)
            .HasForeignKey(tr => tr.TournamentId)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TournamentRegistration>()
                .HasOne(tr => tr.Athlete)
                .WithMany(a => a.TournamentRegistrations)
                .HasForeignKey(tr => tr.AthleteId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TournamentAthlete>()
           .HasKey(ta => new { ta.TournamentId, ta.AthleteId });

            modelBuilder.Entity<TournamentAthlete>()
                .HasOne(ta => ta.Tournament)
                .WithMany(t => t.TournamentAthletes)
                .HasForeignKey(ta => ta.TournamentId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TournamentAthlete>()
                .HasOne(ta => ta.Athlete)
                .WithMany(a => a.TournamentAthletes)
                .HasForeignKey(ta => ta.AthleteId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TournamentCampaign>()
                .HasMany(t => t.Athletes)
                .WithOne(a => a.TournamentCampaign)
                .HasForeignKey(a => a.TournamentCampaignId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Tournament>()
           .HasMany(t => t.TournamentRankingTeams)
           .WithOne(tr => tr.Tournament)
           .HasForeignKey(tr => tr.TournamentId)
           .OnDelete(DeleteBehavior.Restrict);

            // Configure Team
            modelBuilder.Entity<Team>()
                .HasOne(t => t.TournamentRankingTeam)
                .WithOne(tr => tr.Team)
                .HasForeignKey<TournamentRankingTeam>(tr => tr.TeamId)
                .OnDelete(DeleteBehavior.Restrict);// Each team has one ranking record

            // Configure TournamentRankingTeams
            modelBuilder.Entity<TournamentRankingTeam>()
                .HasKey(tr => new { tr.TournamentId, tr.TeamId });

            modelBuilder.Entity<TournamentRankingTeam>(entity =>
            {
                entity.HasKey(tr => tr.Id);
                entity.Property(tr => tr.Id)
                .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<UserRegistration>(entity =>
            {
                entity.HasKey(u => u.Id);
                entity.Property(u => u.Id)
                .ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<PickleballMatch>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<CourtGroup>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });


            modelBuilder.Entity<Court>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<CourtGroupTournamentCampaign>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<CampaignRegistration>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Round>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<RoundGroup>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TeamRoundGroup>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TeamRoundGroupStatistics>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Tournament>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Athlete>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Set>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<WinCondition>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<TournamentCampaign>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                    .ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                .ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<TournamentRegistration>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                .ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<TournamentAthlete>(entity =>
            {
                entity.HasKey(t => t.Id);
                entity.Property(t => t.Id)
                .ValueGeneratedOnAdd();
            });
        }
    }
}
