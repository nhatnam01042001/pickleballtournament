﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.TournamentCampaignViewModels;
using Application.ViewModels.TournamentsViewModels;
using Application.ViewModels.TournamentViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/tournament-campaign")]
    public class TournamentCampaignController : BaseController
    {
        private readonly ITournamentCampaignService _tournamentService;

        public TournamentCampaignController(ITournamentCampaignService tournamentService)
        {
            _tournamentService = tournamentService;
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTournamentByIdAsync(uint id)
        {            
            var tournament = await _tournamentService.GetTournamentCampaignByIdAsync(id);
            ApiResponseModel<TournamentCampaignViewModel> response = new ApiResponseModel<TournamentCampaignViewModel>();
            if (tournament == null)
            {
                response.Success = false;
                response.Message = "Tournament does not existed";
                return BadRequest(response.Message);
            }
            response.Success = true;
            response.Message = "Success";
            response.Data = tournament;
            return Ok(response.Data);
        }
        [HttpGet]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTournamentsAsync()
        {
            var tournaments = await _tournamentService.GetTournamentCampaignsAsync();
            ApiResponseModel<List<TournamentCampaignViewModel>> response = new ApiResponseModel<List<TournamentCampaignViewModel>>();
            if (tournaments == null)
            {
                response.Success = false;
                response.Message = "Tournament does not existed";
                return BadRequest(response.Message);
            }
            response.Success = true;
            response.Message = "Success";
            response.Data = tournaments;
            return Ok(response.Data);
        }
        [HttpGet("paging")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<Pagination<TournamentCampaignViewModel>> GetTournamentPagingsion(int pageIndex = 0, int pageSize = 10)
        {
            return await _tournamentService.GetTournamentCampaignPagingAsync(pageIndex, pageSize);
        }

        [HttpPost]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> CreateTournament(CreateTournamentCampaignDto creatCampaignDto)
        {
            var response = await _tournamentService.CreateTournamentCampaignAsync(creatCampaignDto);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("status{campaignId}")]        
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateCampaignStatus(uint campaignId,UpdateCampaignStatusDto campaignStatusDto)
        {
            var response = await _tournamentService.UpdateCampaignStatus(campaignId, campaignStatusDto);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("date{campaignId}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateCampaignDate(uint campaignId, UpdateCampaignDateDto campaignDateDto)
        {
            var response = await _tournamentService.UpdateCampaignDate(campaignId, campaignDateDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
