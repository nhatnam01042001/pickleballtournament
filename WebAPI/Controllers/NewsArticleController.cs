﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.CourtGroupViewModels;
using Application.ViewModels.NewsArticleViewModels;
using Application.ViewModels.TournamentViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers;

[Route("api/newarticle")]
[ApiController]
public class NewsArticleController : BaseController
{
    private readonly INewsArticleService _newsArticleService;

    public NewsArticleController(INewsArticleService newsArticleService)
    {
        _newsArticleService = newsArticleService;
    }

    [HttpGet]
    public async Task<IActionResult> GetNewsArticle()
    {
        var responseModel = await _newsArticleService.GetNewsArticles();
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpGet("paging")]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<Pagination<NewsArticleViewModel>> GetNewsArticlesPaging(int pageIndex = 0, int pageSize = 10)
    {
        return await _newsArticleService.GetNewsArticlesPaging(pageIndex, pageSize);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<NewsArticleViewModel>> GetNewsArticleById(uint id)
    {
        var newsArticle = await _newsArticleService.GetNewsArticlesById(id);
        if (newsArticle == null)
        {
            return NotFound();
        }
        return Ok(newsArticle);
    }

    [HttpPost]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> AddNewsArticle([FromBody] AddNewsArticleViewModel model)
    {
        var responseModel = await _newsArticleService.AddNewsArticle(model);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpPut("{id}")]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> UpdateNewsArticle(uint id, [FromBody] UpdateNewsArticleViewModel model)
    {
        var responseModel = await _newsArticleService.UpdateNewsAritcle(id, model);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpDelete("{id}")]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> DeleteNewsArticle(uint id)
    {
        var response = await _newsArticleService.DeleteNewsArticle(id);
        if (!response.Success)
        {
            return BadRequest(response.Message);
        }
        return Ok(response.Data);
    }
}
    