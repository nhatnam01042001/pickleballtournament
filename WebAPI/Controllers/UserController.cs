﻿using Application.Interfaces;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System.Net.Mail;
using System.Net;
using WebAPI.ApiResponseModels;
using Application;
using Infrastructures;
using Application.Utils;

namespace WebAPI.Controllers
{
    [Route("api/users")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task RegisterAsync(UserRegisterDto loginObject) => await _userService.RegisterAsync(loginObject);

      

        [HttpPut("{id}")]
        //[Authorize(Roles = "Athletes")]        
        [Authorize(Policy = "ManagersOnly")]
        public async Task<UserViewModel> UpdateUserAsync(uint id, UpdatedUserDTO updatedUserDTO)
        {
            var userViewModel = await _userService.UpdateUserProfile(id, updatedUserDTO);
            return userViewModel;
        }
        
        [HttpGet]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetUsers()
        {
            var response = await _userService.GetUsers();
            return Ok(response);
        }

        [HttpGet("user-not-in-campaign/{campaignId}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetUsersNotParticipateInCampaign(uint campaignId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _userService.GetUserNotInCampaignAsync(campaignId,pageIndex,pageSize);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpGet("current-user")]
        [EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCurrentUser()
        {
            var response = await _userService.GetCurrentUser();
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpGet("paged")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetUsersPaged(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _userService.GetUsersPaged(pageIndex, pageSize);
            return Ok(response);
        }

        [HttpGet("{id}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<ActionResult<GetUserDTO>> GetUser(uint id)
        {
            try
            {
                var user = await _userService.GetUserById(id);
                return Ok(user);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost("import-users")]
        [EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> ImportUsers(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return BadRequest("Invalid File");

            List<CreateUserDTO> users = new List<CreateUserDTO>();

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                using (var package = new ExcelPackage(stream))
                {
                    var worksheet = package.Workbook.Worksheets[0]; 
                    int rowCount = worksheet.Dimension.Rows;

                    for (int row = 2; row <= rowCount; row++) 
                    {
                        var userDto = new CreateUserDTO
                        {
                            UserName = worksheet.Cells[row, 2].Text.Trim(),       
                            Password = worksheet.Cells[row, 3].Text.Trim(),       
                            FullName = worksheet.Cells[row, 4].Text.Trim(),       
                            Email = worksheet.Cells[row, 5].Text.Trim(),         
                            PhoneNumber = worksheet.Cells[row, 6].Text.Trim(),    
                            Rank = double.TryParse(worksheet.Cells[row, 7].Text.Trim(), out var rank) ? rank : (double?)null, 
                            Gender = worksheet.Cells[row, 8].Text.Trim()          
                        };
                        users.Add(userDto);
                    }
                }
            }

            foreach (var user in users)
            {
                await _userService.CreateUser(user);
            }

            return Ok("Import success!");
        }

        [HttpPost]
        [EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<ActionResult> CreateUser([FromBody] CreateUserDTO createUserDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _userService.CreateUser(createUserDTO);
            return Ok("User created successfully.");
        }

        /*[HttpPut("{id}")]
        public async Task<ActionResult> UpdateUser(uint id, [FromForm] UpdatedUserDTO updateUserDTO)
        {
            await _userService.UpdateUser(id, updateUserDTO);
            return NoContent();
        }*/

        [HttpDelete("{id}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<ActionResult> DeleteUser(uint id)
        {
            await _userService.DeleteUser(id);
            return NoContent();
        }
        
        [HttpPut("update-manager/{id}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateManager(uint id, [FromBody] UpdateManagerDTO updateManagerDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _userService.UpdateManagerAsync(id, updateManagerDTO);
            return NoContent();
        }

        [HttpPost("reset-password")]
        public async Task<ApiResponseModel<EmailResponseViewModel>> ResetPasswordAsync([FromBody] ResetPasswordDto resetPasswordDto)
        {
            var response = await _userService.ResetPasswordAsync(resetPasswordDto);
            return response;
        }
        
    }
}

