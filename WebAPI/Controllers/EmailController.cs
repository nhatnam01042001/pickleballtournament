﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.AccountViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/email")]
    public class EmailController : BaseController
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("send")]
        public async Task<IActionResult> SendEmail([FromBody] EmailDto emailDto)
        {
            await _emailService.SendEmailAsync(emailDto);
            return Ok();
        }

        [HttpPost("account-detail")]
        public async Task<IActionResult> SendAccountDetail(AccountSendingDetails accountDetail)
        {
            var response = await _emailService.SendAccountDetailsAsync(accountDetail);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response.Message);
        }
    }
}
