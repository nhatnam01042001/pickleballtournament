﻿using Application.Interfaces;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.CourtGroupViewModels;
using Application.ViewModels.ParticipantViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/courtGroups")]
    public class CourtGroupController : BaseController
    {
        private readonly ICourtGroupService _courtGroupService;

        public CourtGroupController(ICourtGroupService courtGroupService)
        {
            _courtGroupService = courtGroupService;
        }

        [HttpGet]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCourtGroups()
        {
            var responseModel = await _courtGroupService.GetCourtGroups();
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }

        [HttpGet("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCourtGroupbyID(uint id)
        {
            var responseModel = await _courtGroupService.GetCourtGroupByID(id);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }

        [HttpGet("non-existed-campaign/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCourtNotExistedInCamapign(uint campaignId)
        {
            var response = await _courtGroupService.GetCourtGroupsNotExistedInCampaign(campaignId);
            return Ok(response.Data);
        }

        [HttpPost]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AddCourtGroup([FromBody] AddCourtGroupViewModel model)
        {
            var responseModel = await _courtGroupService.AddCourtGroup(model);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }

        [HttpPost("{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AddCourtGroupToCampaign(uint campaignId, List<CourtGroupCampaignDto> courtGroupCampaignDtos)
        {
            var response = await _courtGroupService.AddCourtGroupToCampaignAsync(campaignId, courtGroupCampaignDtos);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateCourtGroup(uint id, [FromBody] UpdateCourtGroupViewModel model)
        {
            var responseModel = await _courtGroupService.UpdateCourtGroup(id, model);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }

        [HttpDelete("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> DeleteCourtGroup(uint id)
        {
            var response = await _courtGroupService.DeleteCourtGroup(id);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }               
    }
}
