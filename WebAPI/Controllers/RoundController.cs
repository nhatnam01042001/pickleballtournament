﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.RoundViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/round")]
    public class RoundController : BaseController
    {
        private readonly IRoundService _roundService;

        public RoundController(IRoundService roundService)
        {
            _roundService = roundService;
        }

        [HttpGet("{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetRoundByTournamentIdAsync(uint tournamentId)
        {
            var response = await _roundService.GetRoundByTournamentIdAsync(tournamentId);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response.Message);
        }

        [HttpPost("next-rounds")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> CreateNextRoundsInTournament(CreateRoundsDto createRoundsDto)
        {
            var response = await _roundService.GenerateRoundsTournament(createRoundsDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
