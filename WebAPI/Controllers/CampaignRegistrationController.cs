﻿using Application.Interfaces;
using Application.ViewModels.TournamentCampaignViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/campaign-registration")]
    public class CampaignRegistrationController : BaseController
    {
        private readonly ICampaignRegistService _campaignRegistService;

        public CampaignRegistrationController(ICampaignRegistService campaignRegistService)
        {
            _campaignRegistService = campaignRegistService;
        }
        [HttpGet("campaign/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetListRegistrationByCampaignId(uint campaignId)
        {
            var response = await _campaignRegistService.GetListRegistrationByCampaignId(campaignId);
            return Ok(response);
        }
        [HttpGet("campaign/paging/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetListRegistrationByCampaignIdPagingsions(uint campaignId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _campaignRegistService.GetCampaignRegistrationPagingsions(campaignId,pageIndex,pageSize);
            return Ok(response);
        }

        [HttpPost("user/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "AthletesOnly")]
        //[Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> ParticipateCampaignForUser(uint campaignId)
        {
            var response = await _campaignRegistService.ParticipateCampaignForUser(campaignId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        [HttpPost("guest/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        
        public async Task<IActionResult> RegistToTournament(uint campaignId,CampaignRegistrationDTO campaignRegistrationDTO)
        {
            var responseModel = await _campaignRegistService.RegistToTournament(campaignId,campaignRegistrationDTO);
            if(responseModel.Success)
            {                
                return Ok(responseModel.Message);
            }
            return BadRequest(responseModel.Message);
        }
        [HttpPut("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateRegistrationStatus(uint id, UpdateCampaignRegistDto updateCampaignRegist)
        {
            var response = await _campaignRegistService.UpdateRegistrationStatus(id, updateCampaignRegist.Status);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
