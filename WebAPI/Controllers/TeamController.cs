﻿using Application.Interfaces;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/teams")]
    public class TeamController : BaseController
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetSingleTeams(uint tournamentId)
        {
            var response = await _teamService.GetSingleTeams(tournamentId);
            if (response.Data == null)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }

        [HttpPost]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> AddSingleTeamList(uint tournamentId)
        {
            var responseModel = await _teamService.AddSingleTeamList(tournamentId);
            if (responseModel.Data == null)
            {
                return BadRequest(responseModel.Message);
            }
            return Ok(responseModel.Data);
        }
        [HttpPost("single-team/{roundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> AddSingleTeamToGroup(uint roundGroupId,CreateSingleTeamDto createSingleTeamDto)
        {
            var response = await _teamService.AddSingleTeamToGroup(roundGroupId, createSingleTeamDto.AthleteId);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }


        [HttpPost("double-team/{roundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> AddDoubleTeamToGroup(uint roundGroupId, AssignPlayerViewModel assignPlayerDto)
        {
            var response = await _teamService.AddDoubleTeamToGroup(roundGroupId, assignPlayerDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("single-team/{teamRoundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> UpdateSingleTeam(uint teamRoundGroupId, UpdateSingleTeamDto updateSingleTeamDto)
        {
            var response = await _teamService.UpdateSingleTeam(teamRoundGroupId, updateSingleTeamDto.TeamId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        
        [HttpPut("doube-team/{teamRoundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> UpdateDoubleTeamToGroup(uint teamRoundGroupId, AssignPlayerViewModel assignPlayerDto)
        {
            var response = await _teamService.UpdateDoubleTeamToGroup(teamRoundGroupId, assignPlayerDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        [HttpGet("non-match-teams/{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTeamsWithoutMatch(uint roundId)
        {
            var response = await _teamService.GetTeamsWithoutMatch(roundId);
            return Ok(response.Data);
        }
    }
}
