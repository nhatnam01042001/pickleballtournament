﻿using Application.Interfaces;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/tournament-registration")]
    public class TournamentRegistrationController : BaseController
    {
        private readonly ITournamentRegistrationService _service;

        public TournamentRegistrationController(ITournamentRegistrationService service)
        {
            _service = service;
        }
        [HttpPost("guest")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> RegistToTournament(uint tournamentId, TournamentRegistDto tournamentRegistDto)
        {
            var response = await _service.RegistToTournament(tournamentId, tournamentRegistDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Data);
        }
        [HttpGet("{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetRegistrationByTournamentIdAsync(uint tournamentId)
        {
            var response = await _service.GetRegistrationByTournamentIdAsync(tournamentId);
            return Ok(response.Data);
        }

        [HttpGet("paging/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetRegistrationByTournamentIdPagingsions(uint tournamentId,int pageIndex = 0, int pageSize = 10)
        {
            var response = await _service.GetRegistrationByTournamentIdPagingsions(tournamentId,pageIndex,pageSize);
            return Ok(response.Data);
        }
        
        [HttpPost("user")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "AthletesOnly")]
        public async Task<IActionResult> ParticipateTournamentForUser(uint tournamentId)
        {

            var response = await _service.ParticipateTournamentForUser(tournamentId);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        [HttpPut("{registId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateRegistrationStatus(uint registId, UpdateTournamentRegistDto updateTournamentRegistDto)
        {
            var response = await _service.UpdateRegistrationStatus(registId, updateTournamentRegistDto.Status);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
