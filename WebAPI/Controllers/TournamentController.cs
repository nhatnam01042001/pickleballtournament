﻿using Application.Interfaces;
using Application.ViewModels.TournamentViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/tournament")]
    public class TournamentController : BaseController
    {
        private readonly ITournamentService _tournamentService;

        public TournamentController(ITournamentService tournamentService)
        {
            _tournamentService = tournamentService;
        }

        [HttpGet("{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTournamentByIdAsync(uint tournamentId)
        {
            var response = await _tournamentService.GetTournamentByIdAsync(tournamentId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        [HttpGet("campaign/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTournamentsAsync(uint campaignId)
        {
            var response = await _tournamentService.GettAllTournamentAsync(campaignId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response?.Message);
        }

        [HttpPost]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> CreateTournament(CreateTournamentDTO createTournamentDTO)
        {
            var response = await _tournamentService.CreateTournament(createTournamentDTO);
            if(response.Data != null)
            {
                return Ok(response.Message);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("status/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateTournamentStatus(uint tournamentId, UpdateTournamentStatusDto tournamentStatusDto)
        {
            var response = await _tournamentService.UpdateTournamentStatus(tournamentId, tournamentStatusDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
