﻿using Application.Interfaces;
using Application.ViewModels.AccountViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace WebAPI.Controllers
{
    [Route("api/accounts")]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        [HttpPost("manager-account")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> CreateManagerAccount()
        {
            var managerAccount = await _accountService.CreateManagerAccount();
            if (managerAccount == null)
            {
                return BadRequest("Manager Account is existed");
            }
            return Ok("Create Manager Account success !");
        }

        [HttpPost("account/{userId}")]
        //[EnableCors("AllowSpecificOrigin")]
        //[Authorize(Roles = "manager")]
        public async Task<IActionResult> CreateAccount(uint userId,CreateAccountDTO accountObject)
        {
            var response = await _accountService.CreateAccountAsync(userId,accountObject);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        [HttpPost("user-login")]
        //[EnableCors("AllowSpecificOrigin")]
        [AllowAnonymous]
        public async Task<IActionResult> UserLogin(UserLoginDTO userLoginDTO)
        {
            var response = await _accountService.LoginAsync(userLoginDTO);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPost("manager-login")]
        //[EnableCors("AllowSpecificOrigin")]
        [AllowAnonymous]
        public async Task<IActionResult> LoginManagerAccountAsync(UserLoginDTO accountObject)
        {
            var response = await _accountService.LoginManagerAccountAsync(accountObject);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPost("change-password")]
        [Authorize] // Chỉ cho phép người dùng đã đăng nhập
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO changePasswordDTO)
        {
            // Lấy thông tin userId từ Claims trong token
            var userId = User.FindFirstValue("UserId");
            if (string.IsNullOrEmpty(userId))
            {
                return Unauthorized(new { message = "Unauthorized" });
            }

            // Gọi service để thay đổi mật khẩu
            var response = await _accountService.ChangePasswordAsync(uint.Parse(userId), changePasswordDTO);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
