﻿using Application.Commons;
using Application.Interfaces;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.ParticipantViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/athletes")]
    public class AthleteController : BaseController
    {
        private readonly IAthleteService _athleteService;

        public AthleteController(IAthleteService participantService)
        {
            _athleteService = participantService;
        }
        /*[HttpGet]
        public async Task<IActionResult> GetTournamentParticipants(uint campaignId)
        {
            var responseModel = await _athleteService.GetTournamentParticipantsAsync(campaignId);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }
        [HttpPost("user-athletes")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> AddUserParticipant(uint campaignId)
        {
            var responseModel = await _athleteService.AddUserPlayer(campaignId);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }
        [HttpPost("guest-athletes")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> AddGuestParticipant(uint campaignId)
        {
            var responseModel = await _athleteService.AddGuestPlayer(campaignId);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }*/

        [HttpGet("non-teams/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetNonTeamAthletes(uint tournamentId)
        {
            var response = await _athleteService.GetNonTeamAthletes(tournamentId);
            if (!response.Success)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }
        [HttpGet("campaign/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetAthleteByCampaignIdAsync(uint campaignId)
        {
            var response = await _athleteService.GetAthletesByCampaignIdAsync(campaignId);
            return Ok(response.Data);
        }

        [HttpGet("{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetAthleteByTournamentIdAsync(uint tournamentId)
        {
            var response = await _athleteService.GetAthleteByTournamentIdAsync(tournamentId);
            return Ok(response.Data);
        }
        [HttpGet("tournament/paging/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetAthletesInTourmamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _athleteService.GetAthletesInTourmamentPagingsions(tournamentId,pageIndex,pageSize);
            return Ok(response.Data);
        }

        [HttpGet("campaign/paging/{campaignId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTournamentParticipantsPagingsions(uint campaignId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _athleteService.GetTournamentParticipantsPagingsions(campaignId, pageIndex, pageSize);
            return Ok(response.Data);
        }

        [HttpGet("athletes-not-in-tournament/paging/{tournamentId}")]
        [EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetAthletesNotInTournament(uint tournamentId,int pageIndex = 0, int pageSize = 10)
        {
            var response = await _athleteService.GetAthletesNotInTournamentPagingsions(tournamentId,pageIndex,pageSize);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpGet("athletes-not-in-tournament/{tournamentId}")]
        [EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetAthletesNotInTournament(uint tournamentId)
        {
            var response = await _athleteService.GetAthletesNotInTournamentPagingsions(tournamentId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPost("user-athletes/{campaignId}")]
        [EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AddUserParticipant(uint campaignId,List<UserIdViewModel> userIds)
        {
            var responseModel = await _athleteService.AddUserToCampaign(campaignId,userIds);
            if (responseModel.Success)
            {
                return Ok(responseModel.Data);
            }
            return BadRequest(responseModel.Message);
        }

        [HttpPost("athletes/{tournamentId}")]
        [EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AddAthletesToTournament(uint tournamentId, List<AthleteIdViewModel> athleteIds)
        {
            var response = await _athleteService.AddAthletesToTournament(tournamentId, athleteIds);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
