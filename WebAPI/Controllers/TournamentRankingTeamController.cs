﻿using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/tournament-ranked-team")]
    [ApiController]
    public class TournamentRankingTeamController : BaseController
    {
        private readonly ITournamentRankingTeamService _rankingTeamService;

        public TournamentRankingTeamController(ITournamentRankingTeamService rankingTeamService) 
        {
            _rankingTeamService = rankingTeamService;
        }

        [HttpGet("/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTournamentRankingTeamParticipantsPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _rankingTeamService.GetTournamentRankingTeamPaging(tournamentId, pageIndex, pageSize);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
