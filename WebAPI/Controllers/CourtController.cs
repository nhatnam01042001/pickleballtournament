﻿using Application.Interfaces;
using Application.ViewModels.CourtViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebAPI.Controllers
{
    [Route("api/courts")]
    [ApiController]
    public class CourtController : ControllerBase
    {
        private readonly ICourtService _courtService;

        public CourtController(ICourtService courtService)
        {
            _courtService = courtService;
        }
        [HttpGet]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCourts()
        {
            var response = await _courtService.GetCourts();
            return Ok(response.Data);
        }

        [HttpGet("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCourt(uint id)
        {
            var response = await _courtService.GetCourtById(id);
            return Ok(response.Data);
        }

        [HttpGet("court-group/{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetCourtsByCourtGroupId(uint id)
        {
            var response = await _courtService.GetCourtsByCourtGroupId(id);
            return Ok(response.Data);
        }
        [HttpGet("court-available/{matchId}")]
        public async Task<IActionResult> GetAvailableCourts(uint matchId)
        {
            var response = await _courtService.GetAvailableCourts(matchId);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPost("court/{courtGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> CreateCourt(uint courtGroupId, IEnumerable<CreateCourtDTO> courtDTOs)
        {
            var response = await _courtService.AddCourtToCourtGroup(courtGroupId,courtDTOs);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateCourt(uint id, [FromBody] UpdateCourtDTO updateCourtDTO)
        {
            var response = await _courtService.UpdateCourt(id,updateCourtDTO);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpDelete("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<ActionResult> DeleteCourt(uint id)
        {
            var response = await _courtService.DeleteCourt(id);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
