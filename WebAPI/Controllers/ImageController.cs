﻿using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/image")]
    [ApiController]
    public class ImagesController : BaseController
    {
        private readonly IImageService _imageService;

            public ImagesController(IImageService imageService)
            {
                _imageService = imageService;
            }

            [HttpPost("upload")]
            public async Task<IActionResult> UploadImage([FromForm] IFormFile file)
            {
                if (file == null)
                {
                    return BadRequest("No file uploaded.");
                }

                var imageUrl = await _imageService.UploadImageAsync(file);

                return Ok(new { Url = imageUrl });
        }
        }
    }

