﻿using Application.Interfaces;
using Application.ViewModels.SetViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/set")]
    public class SetController : BaseController
    {
        private readonly ISetService _setService;

        public SetController(ISetService setService)
        {
            _setService = setService;
        }

        /*[HttpPost("{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> CreateSetsForMatch(uint matchId, IEnumerable<UpdateSetDto> createSetDtos)
        {
            var response = await _setService.CreateSetsForMatch(matchId, createSetDtos);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);   
        }*/

        [HttpGet("{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetListSetOfMatch(uint matchId)
        {
            var response = await _setService.GetSetsOfMatchAsync(matchId);
            if(response.Data is null)
            {
                return BadRequest(response.Message);
            }
            return Ok(response.Data);
        }

        [HttpPut("{setId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateSet(uint setId, UpdateSetDto updateSetDto)
        {
            var response = await _setService.UpdateSet(setId, updateSetDto);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
