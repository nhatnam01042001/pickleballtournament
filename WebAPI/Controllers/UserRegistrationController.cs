﻿using Application.Interfaces;
using Application.ViewModels.UserRegistrationViewModels;
using Application.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/user-registration")]
    public class UserRegistrationController : BaseController
    {
        private readonly IUserRegistrationService _registrationService;

        public UserRegistrationController(IUserRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }

        [HttpGet("{registId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetUserRegistrationById(uint registId)
        {
            var response = await _registrationService.GetRegistrationById(registId);
            if(response.Data is null)
            {
                return NotFound(response.Message);
            }
            return Ok(response.Data);
        }

        [HttpGet("paging")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GetUserRegistrationPagingsions(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _registrationService.GetUserRegistrationPagingsion(pageIndex, pageSize);
            return Ok(response.Data);
        }

        [HttpPost]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> RegistUser(RegistUserDto userRegisterDto)
        {
            var response = await _registrationService.RegistUser(userRegisterDto);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("{registId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateRegistrationStatus (uint registId,UpdateUserReigstDto userReigstDto)
        {
            var response = await _registrationService.UpdateRegistration(registId, userReigstDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
