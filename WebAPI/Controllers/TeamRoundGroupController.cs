﻿using Application.Interfaces;
using Application.ViewModels.TeamRoundGroupViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/team-group")]
    public class TeamRoundGroupController : BaseController
    {
        private readonly ITeamRoundGroupService _teamRoundGroupService;

        public TeamRoundGroupController(ITeamRoundGroupService teamRoundGroupService)
        {
            _teamRoundGroupService = teamRoundGroupService;
        }

        [HttpGet("{roundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTeamRoundGroupByRoundGroupId(uint roundGroupId)
        {
            var response = await _teamRoundGroupService.GetTeamRoundGroupByGroupId(roundGroupId);
            if(response.Data is null)
            {
                return Ok(response.Message);
            }
            return Ok(response.Data);
        }
        [HttpGet("advanced-team/{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetNumberOfAdvanceTeams(uint roundId)
        {
            var response = await _teamRoundGroupService.GetNumberOfAdvanceTeamsInRound(roundId);
            return Ok(response.Data);
        }

        [HttpGet("ranked-team/{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetRankedGroupTeam(uint roundId)
        {
            var response = await _teamRoundGroupService.GetRankedGroupTeam(roundId);
            return Ok(response.Data);
        }
    }
}
