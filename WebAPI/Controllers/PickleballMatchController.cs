﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.CourtViewModels;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/pickleball-match")]
    public class PickleballMatchController : BaseController
    {
        private readonly IPickleballMatchService _pickleballMatchService;

        public PickleballMatchController(IPickleballMatchService pickleballMatchService)
        {
            _pickleballMatchService = pickleballMatchService;
        }
        [HttpGet("current-match/{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetMatchByIdAsync(uint matchId)
        {
            var response = await _pickleballMatchService.GetMatchByIdAsync(matchId);
            return Ok(response.Data);
        }

        [HttpGet("group/{roundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetMatchesByRoundGroupId(uint roundGroupId)
        {
            var response = await _pickleballMatchService.GetMatchesByRoundGroupId(roundGroupId);
            return Ok(response.Data);
        }

        [HttpGet("round/{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetMatchesByRoundId(uint roundId)
        {
            var response = await _pickleballMatchService.GetMatchesByRoundId(roundId);
            if(response.Data is null)
            {
                return Ok(response.Message);
            }
            return Ok(response.Data);
        }
        [HttpGet("{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetPickleballMatchesByTournamentIdAsync(uint tournamentId)
        {
            var response = await _pickleballMatchService.GetMatchesByTournamentIdAsync(tournamentId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpGet("paging/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetPickleballMatchesInTournamentPaging(uint tournamentId,int pageIndex = 0, int pageSize = 10)
        {
            var response = await _pickleballMatchService.GetMatchesByTournamentPagingsions(tournamentId,pageIndex,pageSize);
            if (response.Data is not null)
            {
                return Ok(response.Data);
            }
            return Ok(response.Message);
        }

        [HttpGet("advanced-match/{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetTeamsForAdvancingMatches(uint matchId)
        {
            var response = await _pickleballMatchService.GetTeamsForAdvancingMatches(matchId);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        [HttpGet("next-rounds-match/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetNextRoundsMatchesByTournamentId(uint tournamentId)
        {
            var response = await _pickleballMatchService.GetNextRoundsMatchesByTournamentId(tournamentId);
            return Ok(response.Data);
        }
        [HttpGet("next-rounds-match/paging/{tournamentId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetNextRoundMatchesPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _pickleballMatchService.GetNextRoundMatchesInTournamentPagingsions(tournamentId, pageIndex, pageSize);
            return Ok(response.Data);
        }
        [HttpGet("round/paging/{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetNextRoundsMatchesInTournamentPaging(uint roundId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _pickleballMatchService.GetMatchesByRoundIdPagingsions(roundId,pageIndex,pageSize);
            return Ok(response.Data);
        }
        [HttpPut("assign-court/{matchId}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AssignCourtToMatch(uint matchId, CourtIdViewModel courtId)
        {
            var response = await _pickleballMatchService.AddCourtToMatch(matchId,courtId);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("match-status/{matchId}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateMatchResult(uint matchId, UpdateMatchStatusDto matchStatusDto)
        {
            var response = await _pickleballMatchService.UpdateMatchStatus(matchId,matchStatusDto);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("match-date/{matchId}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateMatchDate(uint matchId, UpdateMatchDateDto matchDateDto)
        {
            var response = await _pickleballMatchService.UpdateMatchDate(matchId,matchDateDto);
            if(response.Success )
            {
                return Ok(response.Data );
            }
            return BadRequest(response.Message);
        }

        [HttpPut("match-detail/{matchId}")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateMatchDetails(uint matchId, UpdateMatchDetailDto matchDetailDto)
        {
            var response = await _pickleballMatchService.UpdateMatchDetails(matchId, matchDetailDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("assign-single-team/{matchId}")]
        ////[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AddSingleTeamToMatch(uint matchId, AssignPlayerViewModel viewModel)
        {
            var response = await _pickleballMatchService.AddSingleTeamToMatch(matchId, viewModel);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("assign-double-team/{matchId}")]
        ////[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AssignDoubleTeamToMatch(uint matchId, AssignTeamsViewModel teamDTO)
        {
            var response = await _pickleballMatchService.AssignDoubleTeamToMatch(matchId, teamDTO);
            if(response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("match-result/{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateMatchResult(uint matchId, UpdateMatchResultDTO matchDTO)
        {
            var response = await _pickleballMatchService.UpdateMatchResult(matchId, matchDTO);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }
        [HttpPut("next-match/{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateNextRoundMatch(uint matchId, AssignTeamsNextRoundMatch teamDTO)
        {
            var response = await _pickleballMatchService.AssignTeamsForNextRoundMatch1(matchId,teamDTO);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPost("{roundGroupId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> GenerateMatchesForRoundGroup(uint roundGroupId)
        {
            var response = await _pickleballMatchService.GenerateMatchesForRoundGroup(roundGroupId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("group-match/{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> UpdateMatchGroupResult(uint matchId, UpdateMatchResultDTO updateMatchDto)
        {
            var response = await _pickleballMatchService.UpdateRoundGroupMatchResult(matchId, updateMatchDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }

        [HttpPut("assign-teams/{matchId}")]
        //[EnableCors("AllowSpecificOrigin")]
        [Authorize(Policy = "ManagersOnly")]
        public async Task<IActionResult> AssignTeamsToMacth(uint matchId, AssignTeamsDto teamsDto)
        {
            var response = await _pickleballMatchService.AssignTeamToMatch(matchId, teamsDto);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
    }
}
