﻿using Application.Interfaces;
using Application.ViewModels.WinConditionViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/win-condition")]
    public class WinConditionController : BaseController
    {
        private readonly IWinCondtionService _winCondtionService;

        public WinConditionController(IWinCondtionService winCondtionService)
        {
            _winCondtionService = winCondtionService;
        }

        [HttpGet]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetAllWinCondition()
        {
            var response = await _winCondtionService.GetAllWinConditionAsync();
            return Ok(response);
        }
        [HttpGet("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetWinConditionById(uint id)
        {
            var response = await _winCondtionService.GetWinConditionById(id);
            return Ok(response);
        }
        [HttpPost]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> CreateWinCondition(InsertOrUpdateWinConditionDTO winConditionDTO)
        {
            var response = await _winCondtionService.CreateWinConditionAsync(winConditionDTO);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }
        [HttpPut("{id}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> UpdateWinCondition(uint id, InsertOrUpdateWinConditionDTO winConditionDTO)
        {
            var response = await _winCondtionService.UpdateWinConditionAsync(id, winConditionDTO);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }
    }
}
