﻿using Application.Interfaces;
using Application.ViewModels.RoundGroupViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.ApiResponseModels;

namespace WebAPI.Controllers
{
    [Route("api/round-group")]
    public class RoundGroupController : BaseController
    {
        private readonly IRoundGroupService _roundGroupService;

        public RoundGroupController(IRoundGroupService roundGroupService)
        {
            _roundGroupService = roundGroupService;
        }
        [HttpPost("{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> CreateRoundGroup(uint roundId, CreateRoundGroupDto createRoundGroupDto )
        {
            var response = await _roundGroupService.CreateRoundGroup(roundId, createRoundGroupDto.RoundGroupName);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Message);
        }
        
        [HttpGet("{roundId}")]
        //[EnableCors("AllowSpecificOrigin")]
        public async Task<IActionResult> GetRoundGroupByRoundIdAsync(uint roundId)
        {
            var response = await _roundGroupService.GetRoundGroupByRoundIdAsync(roundId);
            if (response.Success)
            {
                return Ok(response.Data);
            }
            return BadRequest(response.Data);
        }
    }
}
