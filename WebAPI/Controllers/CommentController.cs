﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels.CommentViewModel;
using Application.ViewModels.CourtGroupViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers;

[Route("api/comment")]
[ApiController]
public class CommentController : BaseController
{
    private readonly ICommentService _commentService;

    public CommentController(ICommentService commentService)
    {
        _commentService = commentService;
    }

    [HttpGet]
    public async Task<IActionResult> GetComments()
    {
        var responseModel = await _commentService.GetComments();
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetCommentById(uint id)
    {
        var responseModel = await _commentService.GetCommentById(id);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpGet("tournament/{tournamentId}")]
    public async Task<IActionResult> GetCommentByTournamentId(uint tournamentId)
    {
        var responseModel = await _commentService.GetCommentByTournamentId(tournamentId);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpGet("paging/{tournamentId}")]
    public async Task<IActionResult> GetCommentByTournamentIdPaging(uint tournamentId,int pageIndex = 0, int pageSize = 10)
    {
        var responseModel = await _commentService.GetCommentByTournamentIdPaging(tournamentId, pageIndex, pageSize);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpPost]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> AddCComment([FromBody] AddCommentViewModel model)
    {
        var responseModel = await _commentService.AddComment(model);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpPut("{id}")]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> UpdateComment(uint id, [FromBody] UpdateCommentViewModel model)
    {
        var responseModel = await _commentService.UpdateComment(id, model);
        if (responseModel.Success)
        {
            return Ok(responseModel.Data);
        }
        return BadRequest(responseModel.Message);
    }

    [HttpPut("softdelete/{id}")]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> SoftDeleteComment(uint id)
    {
        var response = await _commentService.SoftDeleteComment(id);
        if (!response.Success)
        {
            return BadRequest(response.Message);
        }
        return Ok(response.Data);
    }

    [HttpDelete("permanent/{id}")]
    //[EnableCors("AllowSpecificOrigin")]
    public async Task<IActionResult> DeleteComment(uint id)
    {
        var response = await _commentService.DeleteComment(id);
        if (!response.Success)
        {
            return BadRequest(response.Message);
        }
        return Ok(response.Data);
    }
}

