﻿using Application.Interfaces;
using Infrastructures;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace WebAPI.Services
{
    public class ClaimsService : IClaimsService
    {
        private readonly AppDbContext _dbContext;
        public ClaimsService(IHttpContextAccessor httpContextAccessor)
        {
            // todo implementation to get the current userId
            var Id = httpContextAccessor.HttpContext?.User?.FindFirstValue("UserId");
            GetCurrentUserId = string.IsNullOrEmpty(Id) ? 0  : uint.Parse(Id);
        }

        public uint GetCurrentUserId
        {
            get;
        }
    }
}
