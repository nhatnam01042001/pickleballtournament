﻿using Application.Commons;
using Infrastructures;
using Microsoft.EntityFrameworkCore;
using Hangfire;
using Hangfire.MemoryStorage;
using WebAPI.Middelwares;
using WebAPI;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using WebAPI.Validations;
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

// parse the configuration in appsettings
var configuration = builder.Configuration.Get<AppConfiguration>();
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        builder => builder
            .AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod());
});

builder.Services.AddInfrastructuresService(builder.Configuration, configuration.DatabaseConnection);
builder.Services.AddWebAPIService(builder.Configuration);
builder.Services.AddSingleton(configuration);
builder.Services.AddHangfire(config => config.UseMemoryStorage()); // Configure Hangfire services https://docs.hangfire.io/
builder.Services.AddHangfireServer();

// Add authentication services
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration.JWTSection.Issuer,
            ValidAudience = configuration.JWTSection.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.JWTSection.SecretKey)),
            RoleClaimType = "Role"
        };
    });

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("AthletesOnly", policy => policy.RequireRole("Athlete"));
    options.AddPolicy("CoachesOnly", policy => policy.RequireRole("Coach"));
    options.AddPolicy("RefereesOnly", policy => policy.RequireRole("Referee"));
    options.AddPolicy("AdminsOnly", policy => policy.RequireRole("Admin"));
    options.AddPolicy("ManagersOnly", policy => policy.RequireRole("Manager"));
});

/*
    register with singleton life time
    now we can use dependency injection for AppConfiguration
*/
builder.Services.AddControllers();

// Config swagger với jwt
builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new OpenApiInfo { Title = "Demo API", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
    option.OperationFilter<SwaggerFileOperationFilter>();
});

builder.Services.AddSingleton(configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Your API v1"));
}

//Https
/*if (!app.Environment.IsDevelopment())
{
    app.UseStaticFiles(new StaticFileOptions
    {
        FileProvider = new PhysicalFileProvider(
       Path.Combine(builder.Environment.ContentRootPath, ".well-known")),
        RequestPath = "/.well-known",
    });
*/



//app.UseMiddleware<GlobalExceptionMiddleware>();
app.UseMiddleware<PerformanceMiddleware>();
app.MapHealthChecks("/healthchecks");
app.UseHttpsRedirection();
app.UseCors("AllowSpecificOrigin");
app.UseAuthentication(); // validate token
// todo authentication
app.UseAuthorization();

app.MapControllers();
app.Run();
