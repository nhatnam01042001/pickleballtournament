﻿using System.Net;

namespace WebAPI.Middelwares
{
    public class GlobalExceptionMiddleware : IMiddleware
    {
        private readonly ILogger<GlobalExceptionMiddleware> _logger;   
        public GlobalExceptionMiddleware(ILogger<GlobalExceptionMiddleware> logger)
        {

            _logger = logger;

        }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                // log the exception
                _logger.LogError(ex, "An unhandled exception has occurred");

                // Placeholder for push notification logic
                // Add your notification logic here

                // Write the log to the console for debugging (optional)
                Console.WriteLine("GlobalExceptionMiddleware");
                Console.WriteLine(ex.Message);

                // Return a generic error response
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";

                var errorResponse = new { message = "An unexpected error occurred. Please try again later." };
                var errorResponseJson = System.Text.Json.JsonSerializer.Serialize(errorResponse);

                await context.Response.WriteAsync(errorResponseJson);
            }
        }
    }
}
