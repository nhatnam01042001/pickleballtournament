﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITournamentRegistrationRepository : IGenericRepository<TournamentRegistration>
    {
        Task<bool> CheckRegistrationExisted(uint tournamentId, string email, string phone);
        Task<List<TournamentRegistration>> GetTournamentRegistrationsAsync(uint tournamentId);
    }
}
