﻿using Application.ViewModels.CommentViewModel;
using Domain.Entities;
using WebAPI.ApiResponseModels;

namespace Application.Repositories;

public interface ICommentRepository : IGenericRepository<Comment>
{
    Task<IEnumerable<Comment>> GetByTournamentIdAsync(uint tournamentId);
    Task<bool> TournamentExistsAsync(uint tournamentId);

}