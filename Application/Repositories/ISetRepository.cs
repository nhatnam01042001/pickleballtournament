﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ISetRepository : IGenericRepository<Set>
    {
        public Task<List<Set>> GetSetsByMatchIdAsync(uint matchId);
        public Task<Set> GetSetByIdAsync(uint setId);
        public Task<Set> GetLatestSetByMatchIdAsync(uint matchId);
    }
}
