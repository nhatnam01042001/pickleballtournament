﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITeamRepository : IGenericRepository<Team>
    {
        Task<List<Team>> GetSingleTeamsAsync(uint id);
        Task<List<Team>> GetTeamsByTournamentIdAsync(uint tournamentId);
        Task<bool> CheckTeamMemberExistedInTournament(uint tournamentId, uint athleteId);
        Task<List<Team>> GetTeamsByRoundGroupIdAsync(uint roundGroupId);
        Task<List<Team>> GetListTeamByIdAsync(List<uint> teamIds);
    }
}
