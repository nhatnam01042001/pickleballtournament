﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ICampaignRegistRepository : IGenericRepository<CampaignRegistration>
    {
        Task<bool> CheckRegistrationExisted(string email, uint id);
        Task<List<CampaignRegistration>> GetApproveRegistration();
        Task<List<CampaignRegistration>> GetRegistrationByCampaignId(uint campaignId);
    }
}
