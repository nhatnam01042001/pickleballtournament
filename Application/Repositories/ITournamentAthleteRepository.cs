﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITournamentAthleteRepository : IGenericRepository<TournamentAthlete>
    {
        Task<Athlete> GetSpecificTournamentAthlete(uint athleteId, uint tournamentId);
        Task<List<Athlete>> GetAthleteByTournamentIdAsync(uint tournamentId);
        Task<List<Athlete>> GetAthletesWithNonTeamAsync(uint tournamentId);
        Task<Athlete> GetAthleteInTournamentAsync(uint athleteId, uint tournamentId);
        Task<List<Athlete>> GetAthletesNotInTournamentAsync(uint tournamentId);
    }
}
