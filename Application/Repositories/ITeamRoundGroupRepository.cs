﻿using Application.ViewModels.TeamRoundGroupViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITeamRoundGroupRepository : IGenericRepository<TeamRoundGroup>
    {
        public Task<TeamRoundGroup> GetSpecificTeamRoundGroupAsync(uint teamId, uint roundGroupId);
        public Task<TeamRoundGroup> GetTeamRoundGroupByIdAsync(uint teamRoundGroupId);
        public Task<List<TeamRoundGroup>> GetTeamRoundGroupsByRoundGroupIdAsync(uint roundGroupId);
        public Task<TeamRoundGroupStatistics> GetGroupTeamStatisticAsync(uint teamId, uint roundGroupId);
        public Task<int> GetNumberOfTeamInRound(uint roundId);
        public Task<Dictionary<uint, List<TeamRoundGroupViewModel>>> GetListRankedTeamStatistic(uint roundId);
        public Task<Dictionary<uint, List<TeamRoundGroupViewModel>>> GetListRankedTeam(uint roundId);
        public Task<List<TeamRoundGroup>> GetTeamRoundGroupByRoundId(uint roundId);
        public Task<List<Team>> GetTeamWithoutMatchAsync(uint roundId);
    }
}
