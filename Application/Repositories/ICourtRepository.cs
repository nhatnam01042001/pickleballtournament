﻿using Application.Repositories;
using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICourtRepository : IGenericRepository<Court>
    {
        Task<List<Court>> GetCourtsAsync();
        Task<Court?> GetCourtByIdAsync(uint id);
        Task<List<Court>> GetCourtsByCourtGroupIdAsync(uint courtGroupId);
        Task AddCourtAsync(Court court);
        void UpdateCourt(Court court);
        void DeleteCourt(Court court);
        Task<List<Court>> GetAvailableCourtsAsync(uint matchId);
    }
}