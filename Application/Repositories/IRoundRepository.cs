﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IRoundRepository : IGenericRepository<Round>
    {
        public Task<Round> GetNextRoundByRoundIdAsync(uint roundId);
        public Task<Round> GetPreviousRoundByRoundIdAsync(uint roundId);
        Task<List<Round>> GetRoundByTournamentIdAsync(uint tournamentId);
        public Task<Round> GetRoundByRoundOrderAsync(uint tournamentId, int roundOrder);
    }
}
