﻿using Domain.Entities;

namespace Application.Repositories;

public interface INewsArticleRepository : IGenericRepository<NewsArticle>
{
}