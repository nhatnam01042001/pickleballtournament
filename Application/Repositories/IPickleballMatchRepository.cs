﻿using Application.Commons;
using Application.ViewModels.PickleballMatchViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IPickleballMatchRepository : IGenericRepository<PickleballMatch>
    {
        Task<List<PickleballMatchViewModel>> GetMatchesByTournamentIdAsync(uint tournamentId);
        Task<PickleballMatch> GetMatchByIdAsync(uint matchId);
        Task<List<PickleballMatch>> GetMatchesByRoundIdAsync(uint roundId);
        Task<PickleballMatch> GetMatchByOrderAsync(int matchOrder, uint tournamentId);
        Task<PickleballMatch> GetMatchWithTeamByIdAsync(uint matchId);
        Task<List<PickleballMatch>> GetMatchesByIdsAsync(List<uint> matchIds);
        Task<List<TeamRoundGroup>> GetTeamsRoundGroup(uint matchId);
        Task<List<PickleballMatch>> GetMatchesByRoundGroupIdAsync(uint roundGroupId);
        Task<bool> CheckMatchesInRoundCompleted(uint roundId);
        Task<List<PickleballMatchViewModel>> GetNextRounsMatchesByTournamentIdAsync(uint tournamentId);
        Task<List<PickleballMatch>> GetNextRounsMatchesInTournamentAsync(uint tournamentId);
    }
}
