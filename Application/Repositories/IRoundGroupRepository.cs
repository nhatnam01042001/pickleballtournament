﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IRoundGroupRepository : IGenericRepository<RoundGroup>
    {
        Task<List<RoundGroup>> GetRoundGroupByRoundIdAsync(uint roundId);
        Task<RoundGroup> GetRoundGroupWithRoundByIdAsync(uint roundGroupId);
        Task<RoundGroup> GetRoundGroupByIdAsync(uint roundGroupId);
        Task<RoundGroup> GetRoundGroupByRoundIdElimination(uint roundId);
    }
}
