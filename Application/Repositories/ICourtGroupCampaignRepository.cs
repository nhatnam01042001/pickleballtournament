﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ICourtGroupCampaignRepository : IGenericRepository<CourtGroupTournamentCampaign>
    {
        Task<List<CourtGroup>> GetCourtGroupByCampaignIdAsync(uint campaignId);
        Task<List<CourtGroup>> GetCourtGroupsNotExistedInCampaign(uint campaignId);
    }
}
