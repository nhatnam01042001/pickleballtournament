﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IGroupStatisticRepository : IGenericRepository<TeamRoundGroupStatistics>
    {
        public Task<TeamRoundGroupStatistics> GetTeamStatisticsByTeamRoundGroupIdAsync(uint teamRoundGroupId);
        public Task<TeamRoundGroupStatistics> GetSpecificTeamStatisticsAsync(uint roundGroupId, uint teamId);
        public Task<List<TeamRoundGroupStatistics>> GetTeamStatisticByRoundIdAsync(uint roundId);
        public Task<List<TeamRoundGroupStatistics>> GetTeamStatisticByTournamentIdAsync(uint tournamentId, uint teamId);
    }
}
