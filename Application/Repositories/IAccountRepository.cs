﻿using Application.ViewModels.UserViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        Task<bool> CheckUserNameExited(string username);
        Task<Account> GetAccountByUserNameAsync(string username);
        Task<Account> GetAccountByEmailAsync(string username);
        Task<Account>? GetManagerAccountAsync();
        Task<bool> ExistsAsync(uint id);
        Task UpdateAccountAsync(Account account);


    }
}
