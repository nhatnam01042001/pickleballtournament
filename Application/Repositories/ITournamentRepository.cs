﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITournamentRepository : IGenericRepository<Tournament>
    {
        Task<List<Tournament>> GetTournamentWithType(uint campaignId);
        Task<Tournament> GetTournamentByIdAndCampaignId(uint id, uint campaignId);
        Task<Tournament> GetTournamentByIdAsync(uint tournamentId);
        Task<bool> CheckFormatTypeExisted(uint campaignId, Domain.Enums.FormatType formatType);
        Task<List<Tournament>> GetTournamentsAsync();
    }
}
