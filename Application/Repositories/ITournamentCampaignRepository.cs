﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITournamentCampaignRepository : IGenericRepository<TournamentCampaign>
    {
        Task<List<TournamentCampaign>> GetTournamentCampaignsAsync();
        Task<TournamentCampaign> GetTournamentCampaignByIdAsync(uint campaignId);
        Task<bool> CheckAllTournamentCompleted(uint campaignId);
    }
}
