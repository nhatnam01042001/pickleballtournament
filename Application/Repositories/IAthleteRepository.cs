﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IAthleteRepository : IGenericRepository<Athlete>
    {
        Task<List<Athlete>> GetParticipantsInTournamentAsync(uint tournamentCampaignId);
        Task<List<Athlete>> GetAthletesWithNonTeamAsync(uint tournamentId);
        Task<List<Athlete>> GetAthletesWithTeam(uint tournamentId);
        Task<Athlete> GetAthleteByIdAsync(uint athleteId);
        Task<bool> CheckAthleteExisted(uint tournamentId, uint userId);
        Task<Athlete> GetAthleteByUserIdAsync(uint userId);
        Task<List<Athlete>> GetAthletesByCampaignIdAsync(uint campaignId);
        Task<Athlete> GetAthleteByEmailAndPhoneNumber(uint campaignId, string email, string phoneNumber);
    }
}
