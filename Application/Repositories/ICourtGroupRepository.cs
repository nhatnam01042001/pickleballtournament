﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ICourtGroupRepository : IGenericRepository<CourtGroup>
    {
        Task<List<CourtGroup>> GetCourtGroupsByIdAsync(IEnumerable<uint> courtGroupIds);
        Task<List<CourtGroup>> GetCourtGroupNotExistedinCampaign(uint campaignId);
    }
}
