﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface ITournamentRankingTeamRepository : IGenericRepository<TournamentRankingTeam>
    {
        Task<List<TournamentRankingTeam>> GetTournamentRankingTeamsByTournamentIdAsync(uint tournamentId);
        Task<TournamentRankingTeam> GetSpecificRankingTeam(uint tournamentId, uint teamId);
    }
}
