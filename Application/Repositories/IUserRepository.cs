﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetUserNameAndPasswordHash(string userName, string passwordHash);
        Task<bool> CheckUserNameExisted(string userName);
        Task<User> GetUserByUserName(string userName);
        Task<User> GetUserByAccountId(uint accountId);
        Task<List<User>> GetUserWithRank(double rank);
        Task<List<User>> GetAthleteUserAsync();
        Task<List<User>> GetUsersAsync();
        Task<List<User>> GetUsersAsync(int pageIndex, int pageSize);
        Task<int> GetTotalUsersCountAsync();
        Task<User> GetUserByIdAsync(uint id);
        Task AddUserAsync(User user);
        void UpdateUser(User user);
        Task<List<User>> GetAllAsync();
        Task<User?> GetByIdAsync(uint id);
        Task<User> GetUserWithAccountAsync(uint userId);
        Task<List<User>> GetUserNotParticipateInCampaignAsync(uint campaignId);
        Task<List<User>> GetUsersByIdAsync(List<uint> ids);

    }
}
