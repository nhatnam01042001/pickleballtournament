﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.WinConditionViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class WinConditionService : IWinCondtionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public WinConditionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<List<WinConditionViewModel>>> GetAllWinConditionAsync()
        {
            var response = new ApiResponseModel<List<WinConditionViewModel>>()
            {
                Success = false,
                Data = null
            };            
            var winConditions = await _unitOfWork.WinConditionRepository.GetAllAsync();
            if(winConditions is null)
            {
                response.Message = "List is empty !";
                return response;
            }
            var listViewModel = new List<WinConditionViewModel>();
            foreach (var winCondition in winConditions)
            {
                WinConditionViewModel viewModel = winCondition.ToViewModel();
                listViewModel.Add(viewModel);
            }
            response.Data = listViewModel;
            response.Success = true;
            response.Message = "List of Win Condition !";
            return response;
        }

        public async Task<ApiResponseModel<WinConditionViewModel>> CreateWinConditionAsync(InsertOrUpdateWinConditionDTO winConditionDTO)
        {
            var response = new ApiResponseModel<WinConditionViewModel>()
            {
                Success = false,
                Data = null
            };
            if(winConditionDTO.ConditionName == null && winConditionDTO.Description == null)
            {
                response.Message = "All field is required !";
                return response;
            }
            var winCondition = winConditionDTO.ToWinCondition();
            await _unitOfWork.WinConditionRepository.AddAsync(winCondition);
            try
            {
                await _unitOfWork.SaveChangeAsync();
                response.Data = winCondition.ToViewModel();
                response.Message = "Create Win Condition success !";
                response.Success = true;
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }            
        }
        public async Task<ApiResponseModel<WinConditionViewModel>> GetWinConditionById(uint id)
        {
            var response = new ApiResponseModel<WinConditionViewModel>()
            {
                Success = false,
                Data = null
            };
            var winCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync(id);
            if(winCondition == null)
            {
                response.Message = "Win Condition does not existed !";
                return response;
            }
            WinConditionViewModel viewModel = winCondition.ToViewModel();
            response.Success = true;
            response.Data = viewModel;
            response.Message = "Win Condition Detail !";
            return response;
        }

        public async Task<ApiResponseModel<WinConditionViewModel>> UpdateWinConditionAsync(uint id ,InsertOrUpdateWinConditionDTO winConditionDTO)
        {
            var response = new ApiResponseModel<WinConditionViewModel>()
            {
                Success = false,
                Data = null
            };
            var existedWinCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync(id);
            if (existedWinCondition == null)
            {
                response.Message = "Win Condition does not existed !";
                return response;
            }
            if (winConditionDTO.ConditionName == null && winConditionDTO.Description == null)
            {
                response.Message = "Nothing Change !";
                response.Success = true;
                return response;
            }
            existedWinCondition.UpdateWinCondition(winConditionDTO);
            try
            {
                _unitOfWork.WinConditionRepository.Update(existedWinCondition);
                await _unitOfWork.SaveChangeAsync();
                response.Data = existedWinCondition.ToViewModel();
                response.Message = "Update Win Condition success !";
                response.Success = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }
    }  
}
