﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.TournamentViewModels;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class TournamentService : ITournamentService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TournamentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /*public async Task<ApiResponseModel<TournamentViewModel>> CreateTournament(CreateTournamentDTO createTournamentDTO)
        {
            var response = new ApiResponseModel<TournamentViewModel>
            {
                Success = false,
                Data = null
            };
            var tournamentCampaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(createTournamentDTO.TournamentCampaignId);
            if (tournamentCampaign is null)
            {
                response.Message = "Tournament Campaign does not existed !";
                return response;
            }
            /*if (createTournamentDTO.StartDate < DateTime.Now)
            {
                response.Message = "Start Date must greater than Present Date !";
                return response;
            }
            if (createTournamentDTO.StartDate > createTournamentDTO.EndDate)
            {
                response.Message = "Start Date must smaller than End Date";
                return response;
            }
            if (createTournamentDTO.StartDate < tournamentCampaign.StartDate || createTournamentDTO.EndDate > tournamentCampaign.EndDate)
            {
                response.Message = "Start Date and End Date must in range of Tournament Campaign Start-End Date";
                return response;
            }
            var tournament = createTournamentDTO.ToTournament()
            tournament.FormatType = createTournamentDTO.FormatType.ToFormatType();
            tournament.Rounds = GenerateRounds(tournament);
            List<Round> listRound = tournament.Rounds.ToList();
            List<RoundGroup> roundGroups = GenerateRoundGroups(listRound);
            int matchOrderCounter = 1;
            var listMatch = GenerateMatchesForRounds(roundGroups, createTournamentDTO.NumberOfTeams, ref matchOrderCounter);
            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    await _unitOfWork.TournamentRepository.AddAsync(tournament);
                    await _unitOfWork.RoundRepository.AddRangeAsync(listRound);
                    await _unitOfWork.RoundGroupRepository.AddRangeAsync(roundGroups);
                    await _unitOfWork.PickleballMatchRepository.AddRangeAsync(listMatch);
                    await _unitOfWork.SaveChangeAsync();
                    await transaction.CommitAsync();
                }
                catch
                {
                    // Rollback the transaction if any error occurs+
                    await transaction.RollbackAsync();
                    response.Message = "Can not create tournament !";
                    return response;
                }
            }
            var tournamentViewModel = tournament.ToTournamentViewModel();
            tournamentViewModel.FormatType = tournament.FormatType.ToString();
            //tournamentViewModel.TournamentCampaign = tournamentCampaign.TournamentName;
            response.Success = true;
            response.Message = "Create Tournament Success !";
            response.Data = tournamentViewModel;
            return response;
        }*/

        private List<Round> GenerateRounds(Tournament tournament)
        {
            List<Round> rounds = new List<Round>();
            int numTeams = tournament.NumberOfTeams;
            int totalRounds = (int)Math.Ceiling(Math.Log(numTeams, 2));

            for (int roundNumber = 1; roundNumber <= totalRounds; roundNumber++)
            {
                Round round = new Round
                {
                    TournamentId = tournament.Id,
                    RoundName = GetRoundName(roundNumber, totalRounds),
                    RoundOrder = roundNumber,
                    Tournament = tournament,
                    RoundGroups = new List<RoundGroup>()
                };
                rounds.Add(round);
            }
            return rounds;
        }

        private List<RoundGroup> GenerateRoundGroups(List<Round> rounds)
        {
            List<RoundGroup> roundGroups = new List<RoundGroup>();

            foreach (var round in rounds)
            {
                RoundGroup roundGroup = new RoundGroup
                {
                    RoundId = round.Id,
                    RoundGroupName = round.RoundName,
                    Matches = new List<PickleballMatch>(), // Ensure Matches is initialized
                    TeamRoundGroups = new List<TeamRoundGroup>(), // Initialize if necessary
                    Statistics = new List<TeamRoundGroupStatistics>() // Initialize if necessary
                };

                round.RoundGroups.Add(roundGroup);
                roundGroups.Add(roundGroup);
            }

            return roundGroups;
        }

        private string GetRoundName(int roundNumber, int totalRounds)
        {
            switch (roundNumber)
            {
                case var n when totalRounds >= 4 && n == totalRounds - 2: // Quarter Finals
                    return "Quarter Final";
                case var n when totalRounds >= 3 && n == totalRounds - 1: // Semi Finals
                    return "Semi Final";
                case var n when n == totalRounds: // Final
                    return "Final";
                default:
                    return $"Round {roundNumber}";
            }
        }

        private List<PickleballMatch> GenerateMatchesForRounds(List<RoundGroup> roundGroups, int numTeams, ref int matchOrderCounter)
        {
            var matches = new List<PickleballMatch>();
            //int totalRounds = (int)Math.Ceiling(Math.Log2(numTeams));
            int matchesInCurrentRound = numTeams / 2;
            foreach (var roundGroup in roundGroups)
            {
                for (int i = 0; i < matchesInCurrentRound; i++)
                {
                    PickleballMatch match = new PickleballMatch
                    {
                        RoundGroup = roundGroup,
                        RoundGroupId = roundGroup.Id, // Assuming RoundId is the Id of the Round entity
                        MatchStatus = Domain.Enums.MatchStatus.Scheduling,
                        MatchOrder = matchOrderCounter++,
                        MatchDate = DateTime.Now,
                        FirstTeamId = null,
                        SecondTeamId = null,
                    };
                    matches.Add(match);

                    if (roundGroup.Matches == null)
                    {
                        roundGroup.Matches = new List<PickleballMatch>();
                    }
                    roundGroup.Matches.Add(match);
                }
                matchesInCurrentRound /= 2;
            }
            return matches;
        }
        public async Task<ApiResponseModel<TournamentViewModel>> GetTournamentByIdAsync(uint tournamentId)
        {
            var response = new ApiResponseModel<TournamentViewModel>
            {
                Success = false,
                Data = null
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            TournamentViewModel viewModel = tournament.ToTournamentViewModel();            
            response.Success = true;
            response.Message = "Tournament Detail !";
            response.Data = viewModel;
            return response;
        }

        public async Task<ApiResponseModel<List<TournamentViewModel>>> GettAllTournamentAsync(uint campaignId)
        {
            var response = new ApiResponseModel<List<TournamentViewModel>>
            {
                Success = false,
                Data = null
            };
            var listTournament = await _unitOfWork.TournamentRepository.GetTournamentWithType(campaignId);
            if (listTournament is null)
            {
                response.Message = "List of Tournament is empty";
                return response;
            }
            var listViewModel = new List<TournamentViewModel>();
            foreach (var tournament in listTournament)
            {
                TournamentViewModel viewModel = tournament.ToTournamentViewModel();
                viewModel.FormatType = tournament.FormatType.ToString();
                viewModel.TournamentType = tournament.TournamentType.ToString();
                listViewModel.Add(viewModel);
            }
            response.Success = true;
            response.Message = "List of Tournament !";
            response.Data = listViewModel;
            return response;
        }

        public async Task<ApiResponseModel<TournamentViewModel>> CreateTournament(CreateTournamentDTO createTournamentDTO)
        {
            var response = new ApiResponseModel<TournamentViewModel>
            {
                Success = false,
                Data = null
            };

            var tournamentCampaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(createTournamentDTO.TournamentCampaignId);
            if (tournamentCampaign is null)
            {
                response.Message = "Tournament Campaign does not exist!";
                return response;
            }
            if(await _unitOfWork.TournamentRepository.CheckFormatTypeExisted(createTournamentDTO.TournamentCampaignId, createTournamentDTO.FormatType.ToFormatType()))
            {
                response.Message = $"Tournament with format type {createTournamentDTO.FormatType} has already been existed in Campaign !";
                return response;
            }
            var tournament = createTournamentDTO.ToTournament();
            tournament.FormatType = createTournamentDTO.FormatType.ToFormatType();
            tournament.TournamentType = createTournamentDTO.TournamentType.ToTournamentType();

            if (tournament.TournamentType == TournamentType.Elimination)
            {
               // tournament.Rounds = GenerateRounds(tournament);
                List<Round> listRound = GenerateRounds(tournament);
                List<RoundGroup> roundGroups = GenerateRoundGroups(listRound);
                int matchOrderCounter = 1;
                var listMatch = GenerateMatchesForRounds(roundGroups, createTournamentDTO.NumberOfTeams, ref matchOrderCounter);
                List<Set> sets = GenerateSetsForMatch(listMatch, createTournamentDTO.NumberOfSets);

                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        await _unitOfWork.TournamentRepository.AddAsync(tournament);
                        //await _unitOfWork.SaveChangeAsync();

                        await _unitOfWork.RoundRepository.AddRangeAsync(listRound);
                        //await _unitOfWork.SaveChangeAsync();
                        await _unitOfWork.RoundGroupRepository.AddRangeAsync(roundGroups);
                        //await _unitOfWork.SaveChangeAsync();
                        await _unitOfWork.PickleballMatchRepository.AddRangeAsync(listMatch);
                        //await _unitOfWork.SaveChangeAsync();

                        await _unitOfWork.SetRepository.AddRangeAsync(sets);
                        await _unitOfWork.SaveChangeAsync();
                        await transaction.CommitAsync();
                    }
                    catch(Exception ex)
                    {
                        await transaction.RollbackAsync();
                        response.Message = ex.Message;
                        return response;
                    }
                }
            }
            else if (tournament.TournamentType == TournamentType.GroupStage)
            {
                var firstRound = new Round
                {
                    TournamentId = tournament.Id,
                    RoundName = "Round 1",
                    RoundOrder = 1,
                    Tournament = tournament,
                    RoundGroups = new List<RoundGroup>()
                };

                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        await _unitOfWork.TournamentRepository.AddAsync(tournament);
                        await _unitOfWork.RoundRepository.AddAsync(firstRound);
                        tournament.Rounds.Add(firstRound);
                        await _unitOfWork.SaveChangeAsync();
                        await transaction.CommitAsync();
                    }
                    catch(Exception ex)
                    {
                        await transaction.RollbackAsync();
                        response.Message = ex.Message;
                        return response;
                    }
                }
            }
            else
            {
                response.Message = "Invalid Tournament Type!";
                return response;
            }

            var tournamentViewModel = tournament.ToTournamentViewModel();
            tournamentViewModel.FormatType = tournament.FormatType.ToString();
            response.Success = true;
            response.Message = "Tournament created successfully!";
            response.Data = tournamentViewModel;

            return response;
        }

        private List<Set> GenerateSetsForMatch(List<PickleballMatch> matches, int numberOfSets)
        {
            List<Set> sets = new List<Set>();
            foreach (var match in matches)
            {
                for(int i = 1; i <= numberOfSets; i++)
                {
                    Set set = new Set
                    {
                        PickleballMatchId = match.Id,
                        FirstTeamScore = 0,
                        SecondTeamScore = 0,
                        SetOrder = i,
                        SetStatus = MatchStatus.Scheduling,
                        PickleballMatch = match,
                        WinningTeamId = null
                    };
                    sets.Add(set);
                }
            }
            return sets;
        }

        public async Task<ApiResponseModel<TournamentViewModel>> UpdateTournamentStatus(uint tournamentId, UpdateTournamentStatusDto tournamentStatusDto)
        {
            var response = new ApiResponseModel<TournamentViewModel>()
            {
                Success = false,
                Data = null
            };

            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if(tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            if(tournamentStatusDto.TournamentStatus is null)
            {
                response.Message = "Nothing changed !";
                return response;
            }
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(tournament.TournamentCampaignId);
            var status = tournamentStatusDto.TournamentStatus.ToCampaignStatus();
            if(status == MatchStatus.Completed)
            {
                response.Message = "Can not set the tournament completed !";
                return response;
            }
            var tournamentTeams = await _unitOfWork.TournamentRankingTeamRepository.GetTournamentRankingTeamsByTournamentIdAsync(tournamentId);
            
            switch (tournament.TournamentStatus)
            {
                case Domain.Enums.MatchStatus.Scheduling:
                    if (status == Domain.Enums.MatchStatus.InProgress && campaign.StartDate < DateTime.Now)
                    {
                        response.Message = $"Can not set start Tournament Campaign before {campaign.StartDate.Date} !";
                        return response;
                    }
                    if (status == Domain.Enums.MatchStatus.InProgress && tournamentTeams.Count != tournament.NumberOfTeams)
                    {
                        response.Message = "Tournament does not have enough team required !";
                        return response;
                    }
                    if (tournamentTeams.Count != tournament.NumberOfTeams)
            {
                response.Message = "Tournament does not have enough team required !";
                return response;
            }
                    tournament.TournamentStatus = status;
                    break;
                case Domain.Enums.MatchStatus.InProgress:
                    if (status != Domain.Enums.MatchStatus.Postponed && status != Domain.Enums.MatchStatus.Canceled)
                    {
                        response.Message = "The Tournament is in progress ! Only select Postponed or Canceled !"; ;
                        return response;
                    }
                    
                    tournament.TournamentStatus = status;
                    break;
                case Domain.Enums.MatchStatus.Completed:
                    response.Message = "The Tournament Campaign has alreaady completed ! Can not update Tournament Campaign status !";
                    return response;
                case Domain.Enums.MatchStatus.Postponed:
                    if (status == Domain.Enums.MatchStatus.InProgress && campaign.EndDate < DateTime.Now)
                    {
                        response.Message = $"Can not continue Tournament after {campaign.EndDate.Date} ! Please update Tournament Campaign End Date !";
                        return response;
                    }
                    if (status == Domain.Enums.MatchStatus.Completed)
                    {
                        response.Message = $"Can not set tournament completed !";
                        return response;
                    }
                    tournament.TournamentStatus = status;
                    break;
                case Domain.Enums.MatchStatus.Canceled:
                    response.Message = "The Tournament Campaign has alreaady been canceled ! Can not update Tournament Campaign status !";
                    return response;
            }
            try
            {
                _unitOfWork.TournamentRepository.Update(tournament);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Data = tournament.ToTournamentViewModel();
                response.Message = "Update Tournament status success !";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
                return response;
            }
            return response;
        }
    }
}

