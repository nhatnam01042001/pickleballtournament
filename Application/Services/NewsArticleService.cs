﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.CourtGroupViewModels;
using Application.ViewModels.NewsArticleViewModels;
using Application.ViewModels.TournamentViewModels;
using AutoMapper;
using Domain.Entities;
using WebAPI.ApiResponseModels;

namespace Application.Services;

public class NewsArticleService : INewsArticleService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public NewsArticleService(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }
    /*public async Task<Pagination<TournamentCampaignViewModel>> GetTournamentCampaignPagingAsync(int pageIndex = 0, int pageSize = 10)
    {
        var tournaments = await _unitOfWork.TournamentCampaignRepository.ToPagination(pageIndex, pageSize);
        var result = tournaments.ToTournamentViewModel();
        return result;
    }*/
    public async Task<ApiResponseModel<List<NewsArticleViewModel>>> GetNewsArticles()
    {
        var response = new ApiResponseModel<List<NewsArticleViewModel>>()
        {
            Data = null,
            Success = false
        };
        var newsArticles = await _unitOfWork.NewsArticleRepository.GetAllAsync();
        if (newsArticles is null)
        {
            response.Message = "List of News Article is empty";
            return response;
        }

        response.Success = true;
        response.Message = "List of News Article !";
        response.Data = _mapper.Map<List<NewsArticleViewModel>>(newsArticles);
        return response;
    }

    public async Task<Pagination<NewsArticleViewModel>> GetNewsArticlesPaging(int pageIndex = 0, int pageSize = 10)
    {
        var news = await _unitOfWork.NewsArticleRepository.ToPagination(pageIndex, pageSize);
        var result = news.ToNewsArticleViewModel();
        return result;
    }
    public async Task<ApiResponseModel<NewsArticleViewModel>> GetNewsArticlesById(uint id)
    {
        var response = new ApiResponseModel<NewsArticleViewModel>()
        {
            Data = null,
            Success = false
        };

        var newsArticles = await _unitOfWork.NewsArticleRepository.GetByIdAsync(id);
        if (newsArticles is null)
        {
            response.Message = "News Article is not found";
            return response;
        }

        response.Success = true;
        response.Message = "News Article Data !";
        response.Data = _mapper.Map<NewsArticleViewModel>(newsArticles);
        return response;
    }
    public async Task<ApiResponseModel<NewsArticleViewModel>> AddNewsArticle(AddNewsArticleViewModel model)
    {
        var response = new ApiResponseModel<NewsArticleViewModel>()
        {
            Data = null,
            Success = false
        };

        var newsArticle = _mapper.Map<NewsArticle>(model);
        await _unitOfWork.NewsArticleRepository.AddAsync(newsArticle);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "News Article Created !";
        response.Data = _mapper.Map<NewsArticleViewModel>(newsArticle);
        return response;
    }

    public async Task<ApiResponseModel<NewsArticleViewModel>> UpdateNewsAritcle(uint id, UpdateNewsArticleViewModel model)
    {
        var response = new ApiResponseModel<NewsArticleViewModel>()
        {
            Data = null,
            Success = false
        };

        var existingNewsArticle = _unitOfWork.NewsArticleRepository.GetByIdAsync(id).Result;
        if (existingNewsArticle is null)
        {
            response.Message = "News is not found";
            return response;
        }

        var updateNewsArticle = _mapper.Map(model, existingNewsArticle);
        _unitOfWork.NewsArticleRepository.Update(updateNewsArticle);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "News Article Updated !";
        response.Data = _mapper.Map<NewsArticleViewModel>(updateNewsArticle);
        return response;
    }

    public async Task<ApiResponseModel<NewsArticleViewModel>> DeleteNewsArticle(uint id)
    {
        var response = new ApiResponseModel<NewsArticleViewModel>()
        {
            Data = null,
            Success = false
        };

        var existingNewsArticle = _unitOfWork.NewsArticleRepository.GetByIdAsync(id).Result;
        if (existingNewsArticle is null)
        {
            response.Message = "News Article is not found";
            return response;
        }

        _unitOfWork.NewsArticleRepository.Remove(existingNewsArticle);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "News Article Deleted Successfully!";
        return response;
    }
}