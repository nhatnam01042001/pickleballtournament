﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AthleteViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class TournamentAthleteService 
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClaimsService _claimService;

        public TournamentAthleteService(IUnitOfWork unitOfWork, IClaimsService claimService)
        {
            _unitOfWork = unitOfWork;
            _claimService = claimService;
        }

        public async Task<ApiResponseModel<List<AthleteViewModel>>> GetAthleteByTournamentIdAsync(uint tournamentId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Success = false,
                Data = null
            };
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            if (tournament == null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var tournamentAthletes = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tournamentId);
            List<AthleteViewModel> athleteViewModels = new List<AthleteViewModel>();
            foreach (var athlete in tournamentAthletes)
            {
                AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                athleteViewModels.Add(athleteViewModel);
            }
            response.Success = true;
            response.Data = athleteViewModels;
            response.Message = "";
            return response;
        }

        public async Task<ApiResponseModel<Athlete>> ParticipateTournamentForUser(uint tournamentId)
        {
            var response = new ApiResponseModel<Athlete>
            {
                Success = false,
                Data = null
            };

            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            if(tournament == null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            if(tournament.TournamentStatus == Domain.Enums.MatchStatus.InProgress)
            {
                response.Message = "Tournament is in progress !";
                return response;
            }
            var userId = _claimService.GetCurrentUserId;
            if(userId == 0)
            {
                response.Message = "Required user login action !";
                return response;
            }
            var athlete = await _unitOfWork.AthleteRepository.GetAthleteByUserIdAsync(userId);
            if(athlete == null)
            {
                response.Message = "You have not regist to Tournament Campaign !";
                return response;
            }
            if(athlete is not null)
            {
                var existedAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete(athlete.Id,tournamentId);
                if(existedAthlete is not null)
                {
                    response.Message = "Athlete existed ! ";
                    return response;
                }
                if(!BooleanUtils.CheckValidateGender(athlete.GetAthleteGender(),tournament.FormatType))
                {
                    response.Message = "Can not participate this tournament ! Please check tournament format type !";
                    return response;
                }
                var tournamentAthlete = new TournamentAthlete
                {
                    AthleteId = athlete.Id,
                    Athlete = athlete,
                    TournamentId = tournament.Id,
                    Tournament = tournament,                    
                };
                try
                {
                    await _unitOfWork.TournamentAthleteRepository.AddAsync(tournamentAthlete);
                    await _unitOfWork.SaveChangeAsync();
                    response.Success = true;
                    response.Message = "Participate to tournament success !";
                }
                catch(Exception ex)
                {
                    response.Message = ex.Message;
                    return response;
                }
            }
            return response;
        }

        public async Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetAthletesInTourmamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<AthleteViewModel>>
            {
                Data = null,
                Success = true
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if(tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var athletes = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tournamentId);
            if(athletes is null)
            {
                response.Message = "Tournament does not have any athlete !";
                return response;
            }
            List<AthleteViewModel> athleteViewModels = new List<AthleteViewModel>();
            foreach(var athlete in athletes)
            {
                AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                athleteViewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                athleteViewModel.TournamentId = tournament.Id.ToString();
                athleteViewModels.Add(athleteViewModel);
            }
            response.Message = "List of athletes in Tournament !";
            response.Data = athleteViewModels.ToPagination(pageIndex, pageSize);
            return response;
        }
    }
}
