﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.CourtViewModels;
using Application.ViewModels.PickleballMatchViewModels;
using AutoMapper;
using Domain.Entities;
using Microsoft.AspNetCore.ResponseCompression;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class CourtService : ICourtService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CourtService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ApiResponseModel<List<CourtViewModel>>> GetCourts()
        {
            var response = new ApiResponseModel<List<CourtViewModel>>
            {
                Data = null,
                Success = true
            };
            var courts = await _unitOfWork.CourtRepository.GetCourtsAsync();
            if(courts is null)
            {
                response.Message = "List court is empty !";
                return response;
            }
            response.Message = "List of courts !";
            response.Data = courts.Select(c => c.ToCourtViewModel()).ToList();
            return response;
        }

        public async Task<ApiResponseModel<List<CourtViewModel>>> GetCourtsByCourtGroupId(uint courtGroupId)
        {
            var response = new ApiResponseModel<List<CourtViewModel>>
            {
                Data = null,
                Success = true
            };
            var courts = await _unitOfWork.CourtRepository.GetCourtsByCourtGroupIdAsync(courtGroupId);
            if (courts is null)
            {
                response.Message = "List court is empty or Court group does not existed !";
                return response;
            }
            response.Message = $"List of courts in Court {courts.Select(c => c.CourtGroup.CourtGroupName)}!";
            response.Data = courts.Select(c => c.ToCourtViewModel()).ToList();
            return response;
        }
        public async Task<ApiResponseModel<CourtViewModel>> GetCourtById(uint id)
        {
            var response = new ApiResponseModel<CourtViewModel>
            {
                Success = true,
                Data = null
            };
            var court = await _unitOfWork.CourtRepository.GetCourtByIdAsync(id);
            if (court == null)
            {
                response.Message = $"Court with ID {id} not found !";
                return response;
            }
            response.Message = $"Court {court.CourtName} detail !";
            response.Data = court.ToCourtViewModel();
            return response;
        }

        /*public async Task<ApiResponseModel<CourtViewModel>> CreateCourt(CreateCourtDTO createCourtDTO)
        {
            var response = new ApiResponseModel<CourtViewModel>
            {
                Success = true,
                Data = null
            };
            try
            {
                var court = _mapper.Map<Court>(createCourtDTO);
                await _unitOfWork.CourtRepository.AddCourtAsync(court);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Create new court successfully !";
                response.Data = court.ToCourtViewModel() ;
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }*/

        public async Task<ApiResponseModel<CourtViewModel>> UpdateCourt(uint id, UpdateCourtDTO updateCourtDTO)
        {
            var response = new ApiResponseModel<CourtViewModel>()
            {
                Data = null,
                Success = false
            };
            var court = await _unitOfWork.CourtRepository.GetCourtByIdAsync(id);
            if (court == null)
            {
                response.Message = $"Court with ID {id} not found !";
            }
            try
            {
                _mapper.Map(updateCourtDTO, court);
                _unitOfWork.CourtRepository.UpdateCourt(court);
                await _unitOfWork.SaveChangeAsync();
                response.Message = $"Update court with ID {id} successfully !";
                response.Success = true;
                response.Data = court.ToCourtViewModel();
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<CourtViewModel>> DeleteCourt(uint id)
        {
            var response = new ApiResponseModel<CourtViewModel>
            {
                Success = false,
                Data = null
            };
            var court = await _unitOfWork.CourtRepository.GetCourtByIdAsync(id);
            if (court == null)
            {
                response.Message = $"Court with ID {id} not found !";
                return response;
            }
            try
            {
                _unitOfWork.CourtRepository.DeleteCourt(court);
                await _unitOfWork.SaveChangeAsync();
                response.Message = $"Delete Court with ID {id} successfully";
                response.Success = true;
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }
        public async Task<ApiResponseModel<List<CourtViewModel>>> AddCourtToCourtGroup(uint courtGroupId,IEnumerable<CreateCourtDTO> courtDTOs)
        {
            var response = new ApiResponseModel<List<CourtViewModel>>
            {
                Success = false,
                Data = null
            };
            var courtGroup = await _unitOfWork.CourtGroupRepository.GetByIdAsync(courtGroupId);
            if(courtGroup is null)
            {
                response.Message = "Court Group dớ not existed !";
                return response;
            }
            List<Court> listCourt = new List<Court>();
            foreach(var courtDto in courtDTOs )
            {
                Court court = new Court
                {
                    CourtGroup = courtGroup,
                    CourtGroupId = courtGroup.Id,
                    CourtName = courtDto.CourtName,                    
                };
                listCourt.Add(court);
            }
            try
            {
                await _unitOfWork.CourtRepository.AddRangeAsync(listCourt);
                await _unitOfWork.SaveChangeAsync();
                response.Message = $"Add {listCourt.Count} courts successfully !";
                response.Success = true;
                response.Data = listCourt.Select(c => c.ToCourtViewModel()).ToList();
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<List<CourtViewModel>>> GetAvailableCourts(uint matchId)
        {
            var response = new ApiResponseModel<List<CourtViewModel>>()
            {
                Success = true,
                Data = null
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if(match is null)
            {
                response.Success = false;
                response.Message = "Match does not existed !";
                return response;
            }
            if(match.MatchStatus != Domain.Enums.MatchStatus.Scheduling)
            {                
                response.Message = "Match has been scheduled ! Can not change the court !";
                return response;
            }
            var courts = await _unitOfWork.CourtRepository.GetAvailableCourtsAsync(match.Id);
            if(courts is null)
            {
                response.Message = "No court available !";
                return response;
            }
            response.Message = "List available Courts !";
            response.Data = courts.Select(c => c.ToCourtViewModel()).ToList();
            return response;
        }       
    }
}
