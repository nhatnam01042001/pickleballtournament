﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Application.ViewModels.CourtViewModels;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class PickleballMatchService : IPickleballMatchService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PickleballMatchService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public async Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetMatchesByTournamentIdAsync(uint tournamentId)
        {
            var response = new ApiResponseModel<List<PickleballMatchViewModel>>()
            {
                Success = false,
                Data = null,
            };
            var listMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByTournamentIdAsync(tournamentId);
            if (listMatches is null)
            {
                response.Message = "List of matches is empty";
                return response;
            }
            response.Success = true;
            response.Message = "List of Pickleball Matches !";
            response.Data = listMatches;
            return response;
        }

        public async Task<ApiResponseModel<Pagination<PickleballMatchViewModel>>> GetMatchesByTournamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<PickleballMatchViewModel>>
            {
                Data = null,
                Success = true
            };
            var matches = await _unitOfWork.PickleballMatchRepository.GetMatchesByTournamentIdAsync(tournamentId);
            if (matches is null)
            {
                response.Message = "Tournament does not have any match !";
                return response;
            }
            response.Success = true;
            response.Data = matches.ToPagination(pageIndex, pageSize);
            return response;
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> GetMatchByIdAsync(uint matchId)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>()
            {
                Success = false,
                Data = null,
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            var matchViewModel = match.ToMatchesViewModel();
            /*var winCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync((uint)match.WinConditionId);
            var winningTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.WinningTeamId);
            if(winCondition is not null)
            {
                matchViewModel.WinCondition = winCondition.ConditionName;
            }
            if(winningTeam is not null)
            {
                matchViewModel.WinningTeam = winningTeam.TeamName;
            }*/
            response.Message = "Pickleball match details !";
            response.Success = true;
            response.Data = matchViewModel;
            return response;
        }
        public async Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchResult(uint matchId, UpdateMatchResultDTO matchDTO)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>
            {
                Success = false,
                Message = string.Empty
            };
            if (matchDTO.WinConditionId == null && matchDTO.WinningTeamId == null)
            {
                response.Message = "All fields are required !";
                return response;
            }


            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match == null)
            {
                response.Message = "Match not found!";
                return response;
            }
            if (match.FirstTeamId != matchDTO.WinningTeamId && match.SecondTeamId != matchDTO.WinningTeamId)
            {
                response.Message = "The winning team must compete in the match!";
                return response;
            }

            var winningTeam = await _unitOfWork.TeamRepository.GetByIdAsync(matchDTO.WinningTeamId);
            if (winningTeam == null)
            {
                response.Message = "Winning team not found!";
                return response;
            }

            var winCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync(matchDTO.WinConditionId);
            if (winCondition == null)
            {
                response.Message = "Win condition not found !";
                return response;
            }
            match.WinningTeamId = matchDTO.WinningTeamId;
            match.WinningTeam = winningTeam;
            match.WinConditionId = matchDTO.WinConditionId;
            match.WinCondition = winCondition;
            _unitOfWork.PickleballMatchRepository.Update(match);

            var currentRoundId = match.RoundGroup.RoundId;
            var nextRound = await _unitOfWork.RoundRepository.GetNextRoundByRoundIdAsync(currentRoundId);

            // If there is no next round, finalize the update and return the current match's view model
            if (nextRound == null)
            {
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Match result updated successfully!";
                var finalMatch = match.ToMatchesViewModel();
                finalMatch.WinningTeam = winningTeam.TeamName;
                finalMatch.WinCondition = winCondition.ConditionName;
                response.Data = finalMatch;
                return response;
            }

            var nextRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(nextRound.Id);
            if (nextRoundMatches == null || !nextRoundMatches.Any())
            {
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Match result updated successfully!";
                response.Data = match.ToMatchesViewModel();
                return response;
            }

            nextRoundMatches = nextRoundMatches.OrderBy(m => m.MatchOrder).ToList();
            var currentMatchOrder = match.MatchOrder;
            var targetMatchOrder = (currentMatchOrder + 1) / 2 + nextRoundMatches.First().MatchOrder - 1;
            var targetMatch = nextRoundMatches.FirstOrDefault(m => m.MatchOrder == targetMatchOrder);

            if (targetMatch == null)
            {
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Match result updated successfully!";
                response.Data = match.ToMatchesViewModel();
                return response;
            }

            if (match.MatchOrder % 2 == 1)
            {
                targetMatch.FirstTeamId = matchDTO.WinningTeamId;
                targetMatch.FirstTeam = winningTeam;
            }
            else
            {
                targetMatch.SecondTeamId = matchDTO.WinningTeamId;
                targetMatch.SecondTeam = winningTeam;
            }

            TeamRoundGroup teamRoundGroup = new TeamRoundGroup
            {
                TeamId = matchDTO.WinningTeamId,
                RoundGroupId = (uint)targetMatch.RoundGroupId,
            };

            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    _unitOfWork.PickleballMatchRepository.Update(targetMatch);
                    await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                    await _unitOfWork.SaveChangeAsync();
                    await transaction.CommitAsync();

                    response.Success = true;
                    response.Message = "Match result updated successfully!";
                    response.Data = targetMatch.ToMatchesViewModel();
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = "An error occurred while updating the match result.";
                    return response;
                }
            }

            return response;
        }

        public async Task<ApiResponseModel<NextRoundMatchViewModel>> GetTeamsForAdvancingMatches(uint matchId)
        {
            var response = new ApiResponseModel<NextRoundMatchViewModel>
            {
                Success = false,
                Message = string.Empty,
                Data = null
            };

            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            var currentRoundId = match.RoundGroup.RoundId;
            var previousRound = await _unitOfWork.RoundRepository.GetPreviousRoundByRoundIdAsync(currentRoundId);

            if (previousRound is null)
            {
                response.Message = "No previous round found!";
                return response;
            }

            var previousRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(previousRound.Id);
            if (previousRoundMatches is null || !previousRoundMatches.Any())
            {
                response.Message = "No matches found in the previous round";
            }
            var currentRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(currentRoundId);
            var currentRoundFirstMatchOrder = currentRoundMatches.First().MatchOrder;
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
            var advancingMatchOrders = IntegerUtils.GetPreviousMatchOrders(match.MatchOrder, currentRoundFirstMatchOrder, tournament.NumberOfTeams - 1);
            if (advancingMatchOrders == null || !advancingMatchOrders.Any())
            {
                response.Message = "No advancing matches found!";
                return response;
            }
            var advancingMatches = new List<PickleballMatch>();
            foreach (var matchOrder in advancingMatchOrders)
            {
                var advancingMatch = await _unitOfWork.PickleballMatchRepository.GetMatchByOrderAsync(matchOrder, match.RoundGroup.Round.TournamentId);
                if (advancingMatch != null)
                {
                    advancingMatches.Add(advancingMatch);
                }
            }

            var nextRoundMatchViewModel = new NextRoundMatchViewModel()
            {
                Id = match.Id,
                Round = match.RoundGroup.RoundGroupName,
                RoundGroupId = (uint)match.RoundGroupId,
                RoundId = match.RoundGroup.RoundId,
                MatchOrder = match.MatchOrder,
                FirstTeams = new List<TeamViewModel>(),
                SecondTeams = new List<TeamViewModel>()
            };

            foreach (var advancingMatch in advancingMatches)
            {
                TeamViewModel? firstTeamViewModel = null;
                TeamViewModel? secondTeamViewModel = null;

                if (advancingMatch.FirstTeamId.HasValue)
                {
                    var firstTeam = await _unitOfWork.TeamRepository.GetByIdAsync(advancingMatch.FirstTeamId.Value);
                    if (firstTeam != null)
                    {
                        firstTeamViewModel = new TeamViewModel
                        {
                            TeamId = firstTeam.Id,
                            TeamName = firstTeam.TeamName
                        };
                    }
                }

                if (advancingMatch.SecondTeamId.HasValue)
                {
                    var secondTeam = await _unitOfWork.TeamRepository.GetByIdAsync(advancingMatch.SecondTeamId.Value);
                    if (secondTeam != null)
                    {
                        secondTeamViewModel = new TeamViewModel
                        {
                            TeamId = secondTeam.Id,
                            TeamName = secondTeam.TeamName
                        };
                    }
                }

                if (advancingMatch.MatchOrder % 2 == 1)
                {
                    if (firstTeamViewModel != null)
                    {
                        nextRoundMatchViewModel.FirstTeams.Add(firstTeamViewModel);
                    }
                    if (secondTeamViewModel != null)
                    {
                        nextRoundMatchViewModel.FirstTeams.Add(secondTeamViewModel);
                    }
                }
                else
                {
                    if (firstTeamViewModel != null)
                    {
                        nextRoundMatchViewModel.SecondTeams.Add(firstTeamViewModel);
                    }
                    if (secondTeamViewModel != null)
                    {
                        nextRoundMatchViewModel.SecondTeams.Add(secondTeamViewModel);
                    }
                }
            }

            response.Success = true;
            response.Message = "Advancing matches retrieved successfully!";
            response.Data = nextRoundMatchViewModel;

            return response;
        }

        public async Task<ApiResponseModel<NextRoundMatchViewModel>> AssignTeamsForNextRoundMatch(uint matchId, AssignTeamsNextRoundMatch teamsDTO)
        {
            var response = new ApiResponseModel<NextRoundMatchViewModel>
            {
                Success = false,
                Data = null
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
            if (match.MatchOrder < tournament.NumberOfTeams / 2)
            {
                response.Message = "First round match does not have any advanced Team"!;
                return response;
            }
            if (teamsDTO.FirstTeamId == 0 && teamsDTO.SecondTeamId == 0)
            {
                response.Message = "Nothing change !";
                return response;
            }
            var currentRoundId = match.RoundGroup.RoundId;
            var previousRound = await _unitOfWork.RoundRepository.GetPreviousRoundByRoundIdAsync(currentRoundId);

            if (previousRound is null)
            {
                response.Message = "No previous round found!";
                return response;
            }

            var previousRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(previousRound.Id);
            if (previousRoundMatches is null || !previousRoundMatches.Any())
            {
                response.Message = "No matches found in the previous round";
            }
            var currentRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(currentRoundId);
            var currentRoundFirstMatchOrder = currentRoundMatches.First().MatchOrder;
            var advancingMatchOrders = IntegerUtils.GetPreviousMatchOrders(match.MatchOrder, currentRoundFirstMatchOrder, tournament.NumberOfTeams - 1);
            if (advancingMatchOrders == null || !advancingMatchOrders.Any())
            {
                response.Message = "No advancing matches found!";
                return response;
            }
            var advancingMatches = new List<PickleballMatch>();
            foreach (var matchOrder in advancingMatchOrders)
            {
                var advancingMatch = await _unitOfWork.PickleballMatchRepository.GetMatchByOrderAsync(matchOrder, match.RoundGroup.Round.TournamentId);
                if (advancingMatch is not null)
                {
                    advancingMatches.Add(advancingMatch);
                }
            }

            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    foreach (var advancingMatch in advancingMatches)
                    {
                        if (teamsDTO.FirstTeamId != 0 && (teamsDTO.FirstTeamId == advancingMatch.FirstTeamId || teamsDTO.FirstTeamId == advancingMatch.SecondTeamId))
                        {
                            advancingMatch.WinningTeamId = teamsDTO.FirstTeamId;
                            advancingMatch.WinningTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)teamsDTO.FirstTeamId);
                            match.FirstTeamId = teamsDTO.FirstTeamId;
                            match.FirstTeam = advancingMatch.WinningTeam;

                            var teamRoundGroup = new TeamRoundGroup
                            {
                                RoundGroupId = (uint)match.RoundGroupId,
                                TeamId = (uint)teamsDTO.FirstTeamId
                            };

                            await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                            _unitOfWork.PickleballMatchRepository.Update(advancingMatch);
                            _unitOfWork.PickleballMatchRepository.Update(match);
                        }

                        if (teamsDTO.SecondTeamId != 0 && (teamsDTO.SecondTeamId == advancingMatch.FirstTeamId || teamsDTO.SecondTeamId == advancingMatch.SecondTeamId))
                        {
                            advancingMatch.WinningTeamId = teamsDTO.SecondTeamId;
                            advancingMatch.WinningTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)teamsDTO.SecondTeamId);
                            match.SecondTeamId = teamsDTO.SecondTeamId;
                            match.SecondTeam = advancingMatch.WinningTeam;

                            var teamRoundGroup = new TeamRoundGroup
                            {
                                RoundGroupId = (uint)match.RoundGroupId,
                                TeamId = (uint)teamsDTO.SecondTeamId
                            };

                            await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                            _unitOfWork.PickleballMatchRepository.Update(advancingMatch);
                            _unitOfWork.PickleballMatchRepository.Update(match);
                        }
                    }

                    await transaction.CommitAsync();

                    response.Success = true;
                    response.Message = "Teams assigned to next round match successfully!";
                    response.Data = new NextRoundMatchViewModel
                    {
                        Id = match.Id,
                        MatchOrder = match.MatchOrder,
                        Round = match.RoundGroup.Round.RoundName,
                        RoundGroupId = (uint)match.RoundGroupId,
                        RoundId = match.RoundGroup.RoundId,
                        FirstTeams = new List<TeamViewModel>
                {
                    match.FirstTeam != null ? new TeamViewModel { TeamId = match.FirstTeam.Id, TeamName = match.FirstTeam.TeamName } : null
                }.Where(t => t != null).ToList(),
                        SecondTeams = new List<TeamViewModel>
                {
                    match.SecondTeam != null ? new TeamViewModel { TeamId = match.SecondTeam.Id, TeamName = match.SecondTeam.TeamName } : null
                }.Where(t => t != null).ToList()
                    };
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = $"An error occurred while assigning teams: {ex.Message}";
                }
            }

            return response;
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> AddSingleTeamToMatch(uint matchId, AssignPlayerViewModel viewModel)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>
            {
                Success = false,
                Data = null
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not exited !";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
            if (match.MatchOrder > tournament.NumberOfTeams)
            {
                response.Message = "Assign player step can only perform for first roound match !";
                return response;
            }
            if (viewModel.FirstAthleteId == 0 && viewModel.SecondAthleteId == 0)
            {
                response.Message = "Nothing change !";
                return response;
            }
            ;
            if (viewModel.FirstAthleteId == 0)
            {
                if (match.FirstTeamId == null)
                {
                    response.Message = "FirstTeam must be assigned to the match!";
                    return response;
                }
            }
            if (viewModel.FirstAthleteId == viewModel.SecondAthleteId)
            {
                response.Message = "Can not assign same athlete to the match !";
                return response;
            }
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(match.RoundGroup.Round.TournamentId);
            if (viewModel.FirstAthleteId != 0)
            {
                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)viewModel.FirstAthleteId, tournament.Id);
                        if (firstAthlete == null)
                        {
                            response.Message = "FirstAthlete does not existed!";
                            return response;
                        }

                        if (!athletes.Any(a => a.Id == firstAthlete.Id))
                        {
                            response.Message = "FirstAthlete already has a team!";
                            return response;
                        }

                        Team firstTeam = firstAthlete.ToTeam();
                        firstTeam.TeamType = tournament.FormatType;
                        firstTeam.TeamName = firstTeam.GetTeamName();
                        firstTeam.AverageRank = firstTeam.GetTeamAverageRank();
                        firstTeam.TournamentId = tournament.Id;
                        firstTeam.Tournament = tournament;

                        if (!firstTeam.CheckValidateTeam(firstTeam.TeamType))
                        {
                            response.Message = "First team is not valid! Please check gender and average rank!";
                            return response;
                        }
                        if (match.FirstTeamId == null)
                        {
                            await _unitOfWork.TeamRepository.AddAsync(firstTeam);
                            await _unitOfWork.SaveChangeAsync();

                            TeamRoundGroup firstTeamRoundGroup = new TeamRoundGroup()
                            {
                                TeamId = firstTeam.Id,
                                RoundGroupId = (uint)match.RoundGroupId
                            };
                            match.FirstTeamId = firstTeam.Id;
                            match.FirstTeam = firstTeam;

                            await _unitOfWork.TeamRoundGroupRepository.AddAsync(firstTeamRoundGroup);
                            await _unitOfWork.GroupStatisticRepository.AddAsync(new TeamRoundGroupStatistics
                            {
                                TeamRoundGroupId = firstTeamRoundGroup.Id,
                                TeamRoundGroup = firstTeamRoundGroup,
                                Wins = 0,
                                Losses = 0,
                                Points = 0,
                                SetDifference = 0,
                                PointsDifference = 0,
                            });
                            TournamentRankingTeam rankingTeam = tournament.CreateTournamentRankingTeam(firstTeam);
                            await _unitOfWork.TournamentRankingTeamRepository.AddAsync(rankingTeam);
                        }
                        else
                        {
                            var existedTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.FirstTeamId);
                            existedTeam.UpdateTeam(firstTeam);
                            _unitOfWork.TeamRepository.Update(existedTeam);

                        }

                        await _unitOfWork.SaveChangeAsync();
                        await transaction.CommitAsync();
                    }
                    catch (Exception)
                    {
                        await transaction.RollbackAsync();
                        response.Message = "An error occurred while assigning the first team to the match!";
                        return response;
                    }
                }
            }

            if (viewModel.SecondAthleteId != 0)
            {
                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {

                        var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)viewModel.SecondAthleteId, tournament.Id);

                        if (secondAthlete == null)
                        {
                            response.Message = "SecondAthlete does not existed in Tournament !";
                            return response;
                        }

                        if (!athletes.Any(a => a.Id == secondAthlete.Id))
                        {
                            response.Message = "SecondAthlete already has a team!";
                            return response;
                        }

                        Team secondTeam = secondAthlete.ToTeam();
                        secondTeam.TeamType = tournament.FormatType;
                        secondTeam.TeamName = secondTeam.GetTeamName();
                        secondTeam.AverageRank = secondTeam.GetTeamAverageRank();
                        secondTeam.TournamentId = tournament.Id;
                        secondTeam.Tournament = tournament;
                        secondTeam.TeamType = tournament.FormatType;

                        if (!secondTeam.CheckValidateTeam(secondTeam.TeamType))
                        {
                            response.Message = "Second team is not valid! Please check gender and average rank!";
                            return response;
                        }

                        if (match.SecondTeamId == null)
                        {
                            await _unitOfWork.TeamRepository.AddAsync(secondTeam);
                            await _unitOfWork.SaveChangeAsync(); // Ensure the Id is generated

                            match.SecondTeamId = secondTeam.Id;
                            match.SecondTeam = secondTeam;

                            TeamRoundGroup secondTeamRoundGroup = new TeamRoundGroup
                            {
                                TeamId = secondTeam.Id,
                                RoundGroupId = (uint)match.RoundGroupId
                            };
                            await _unitOfWork.TeamRoundGroupRepository.AddAsync(secondTeamRoundGroup);
                            await _unitOfWork.GroupStatisticRepository.AddAsync(new TeamRoundGroupStatistics
                            {
                                TeamRoundGroupId = secondTeamRoundGroup.Id,
                                TeamRoundGroup = secondTeamRoundGroup,
                                Wins = 0,
                                Losses = 0,
                                Points = 0,
                                PointsDifference = 0,
                                SetDifference = 0
                            });
                            TournamentRankingTeam rankingTeam = tournament.CreateTournamentRankingTeam(secondTeam);
                            await _unitOfWork.TournamentRankingTeamRepository.AddAsync(rankingTeam);
                        }
                        else
                        {
                            var existingTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.SecondTeamId);
                            if (existingTeam == null)
                            {
                                response.Message = "Existing team not found!";
                                return response;
                            }
                            existingTeam.UpdateTeam(secondTeam);
                            _unitOfWork.TeamRepository.Update(existingTeam);
                        }
                        _unitOfWork.PickleballMatchRepository.Update(match);
                        await _unitOfWork.SaveChangeAsync();
                        await transaction.CommitAsync();
                    }
                    catch (Exception)
                    {
                        await transaction.RollbackAsync();
                        response.Message = "An error occurred while assigning the second team to the match!";
                        return response;
                    }
                }
            }
            else if (viewModel.SecondAthleteId == 0 && match.SecondTeamId != null)
            {
                // Skip execution related to secondTeam
                // No action required here
            }
            if (viewModel.FirstAthleteId == 0 && match.FirstTeamId != 0)
            {

            }
            response.Success = true;
            response.Message = "Teams assigned to the match successfully!";
            response.Data = match.ToMatchesViewModel();
            return response;
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> AssignDoubleTeamToMatch(uint matchId, AssignTeamsViewModel teamDTO)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>()
            {
                Success = false,
                Data = null,
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
            if (match.MatchOrder > tournament.NumberOfTeams / 2)
            {
                response.Message = "Assign player step can only perform for first roound match !";
                return response;
            }
            if ((teamDTO.FirstTeam == null || (teamDTO.FirstTeam.FirstAthleteId == 0 && teamDTO.FirstTeam.SecondAthleteId == 0)) &&
                (teamDTO.SecondTeam == null || (teamDTO.SecondTeam.FirstAthleteId == 0 && teamDTO.SecondTeam.SecondAthleteId == 0)))
            {
                response.Message = "Nothing change";
                return response;
            }
            if (teamDTO.FirstTeam.FirstAthleteId != 0 && teamDTO.FirstTeam.SecondAthleteId == 0)
                if (teamDTO.FirstTeam == null || (teamDTO.FirstTeam.FirstAthleteId == null && teamDTO.FirstTeam.SecondAthleteId == null))
                {
                    if (match.FirstTeamId == null)
                    {
                        response.Message = "FirstTeam must be assigned to the match!";
                        return response;
                    }
                }
            if (teamDTO.FirstTeam != null)
            {
                bool isFirstTeamInvalid = (teamDTO.FirstTeam.FirstAthleteId == 0 && teamDTO.FirstTeam.SecondAthleteId != 0) ||
                                          (teamDTO.FirstTeam.FirstAthleteId != 0 && teamDTO.FirstTeam.SecondAthleteId == 0);

                if (isFirstTeamInvalid)
                {
                    response.Message = "FirstTeam has invalid athlete ID configuration. Both IDs should be 0 or both should have values.";
                    return response;
                }
            }

            if (teamDTO.SecondTeam != null)
            {
                bool isSecondTeamInvalid = (teamDTO.SecondTeam.FirstAthleteId == 0 && teamDTO.SecondTeam.SecondAthleteId != 0) ||
                                           (teamDTO.SecondTeam.FirstAthleteId != 0 && teamDTO.SecondTeam.SecondAthleteId == 0);

                if (isSecondTeamInvalid)
                {
                    response.Message = "SecondTeam has invalid athlete ID configuration. Both IDs should be 0 or both should have values.";
                    return response;
                }
            }
            if ((teamDTO.FirstTeam.FirstAthleteId != 0 && teamDTO.FirstTeam.SecondAthleteId != 0) && (teamDTO.FirstTeam.FirstAthleteId == teamDTO.FirstTeam.SecondAthleteId))
            {
                response.Message = "Can not assign same athlete to First team !";
                return response;
            }
            if ((teamDTO.SecondTeam.FirstAthleteId != 0 && teamDTO.SecondTeam.SecondAthleteId != 0) && (teamDTO.SecondTeam.FirstAthleteId == teamDTO.SecondTeam.SecondAthleteId))
            {
                response.Message = "Can not assign same athlete to Second team !";
                return response;
            }
            if (teamDTO.FirstTeam != null && teamDTO.SecondTeam != null)
            {
                bool areFirstAthleteIdsEqualToAny = teamDTO.FirstTeam.FirstAthleteId == teamDTO.SecondTeam.FirstAthleteId ||
                                                    teamDTO.FirstTeam.FirstAthleteId == teamDTO.SecondTeam.SecondAthleteId;

                bool areSecondAthleteIdsEqualToAny = teamDTO.FirstTeam.SecondAthleteId == teamDTO.SecondTeam.FirstAthleteId ||
                                                     teamDTO.FirstTeam.SecondAthleteId == teamDTO.SecondTeam.SecondAthleteId;

                if (areFirstAthleteIdsEqualToAny || areSecondAthleteIdsEqualToAny)
                {
                    response.Message = "Athlete IDs of FirstTeam must not match any Athlete IDs of SecondTeam.";
                    return response;
                }
            }
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(match.RoundGroup.Round.TournamentId);
            if (teamDTO.FirstTeam != null)
            {
                if (teamDTO.FirstTeam.FirstAthleteId != 0 && teamDTO.FirstTeam.SecondAthleteId != 0)
                {
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.FirstTeam.FirstAthleteId, tournament.Id);
                            if (firstAthlete == null)
                            {
                                response.Message = "First Athlete does not exist!";
                                return response;
                            }
                            var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.FirstTeam.SecondAthleteId, tournament.Id);
                            if (secondAthlete == null)
                            {
                                response.Message = "Second Athlete does not exist!";
                                return response;
                            }
                            if (athletes.FirstOrDefault(a => a.Id == firstAthlete.Id) == null)
                            {
                                response.Message = "FirstAthlete already has a team!";
                                return response;
                            }
                            if (athletes.FirstOrDefault(a => a.Id == secondAthlete.Id) == null)
                            {
                                response.Message = "SecondAthlete already has a team!";
                                return response;
                            }

                            Team firstTeam;
                            if (match.FirstTeamId == null)
                            {
                                firstTeam = TeamViewModelExtensions.CreateDoubleTeam(firstAthlete, secondAthlete);
                                firstTeam.TeamName = firstTeam.GetTeamName();
                                firstTeam.TeamType = tournament.FormatType;
                                firstTeam.TournamentId = tournament.Id;
                                firstTeam.Tournament = tournament;
                                firstTeam.AverageRank = firstTeam.GetTeamAverageRank();

                                if (!firstTeam.CheckValidateTeam(firstTeam.TeamType))
                                {
                                    response.Message = "First team is not valid! Please check gender and average rank!";
                                    return response;
                                }
                                await _unitOfWork.TeamRepository.AddAsync(firstTeam);
                                await _unitOfWork.SaveChangeAsync();
                                match.FirstTeamId = firstTeam.Id;
                                match.FirstTeam = firstTeam;

                                var firstTeamRoundGroup = new TeamRoundGroup
                                {
                                    TeamId = firstTeam.Id,
                                    RoundGroupId = (uint)match.RoundGroupId
                                };
                                await _unitOfWork.TeamRoundGroupRepository.AddAsync(firstTeamRoundGroup);
                                await _unitOfWork.GroupStatisticRepository.AddAsync(new TeamRoundGroupStatistics
                                {
                                    TeamRoundGroupId = firstTeamRoundGroup.Id,
                                    TeamRoundGroup = firstTeamRoundGroup,
                                    Wins = 0,
                                    Losses = 0,
                                    Points = 0,
                                    SetDifference = 0,
                                    PointsDifference = 0
                                });
                                TournamentRankingTeam rankingTeam = tournament.CreateTournamentRankingTeam(firstTeam);
                                await _unitOfWork.TournamentRankingTeamRepository.AddAsync(rankingTeam);
                            }
                            else
                            {
                                firstTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.FirstTeamId);
                                firstTeam.FirstAthleteId = firstAthlete.Id;
                                firstTeam.FirstAthlete = firstAthlete;
                                firstTeam.SecondAthleteId = secondAthlete.Id;
                                firstTeam.SecondAthlete = secondAthlete;
                                firstTeam.AverageRank = firstTeam.GetTeamAverageRank();
                                if (!firstTeam.CheckValidateTeam(firstTeam.TeamType))
                                {
                                    response.Message = "Invalid Team ! Please check gender and average rank !";
                                    return response;
                                }
                                _unitOfWork.TeamRepository.Update(firstTeam);
                            }
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = "An error occurred while assigning the first team to the match!";
                            return response;
                        }
                    }
                }
                else if (teamDTO.FirstTeam.FirstAthleteId == 0 && teamDTO.FirstTeam.SecondAthleteId != 0)
                {
                    if (match.FirstTeamId == null)
                    {
                        response.Message = "Match does not has First Team ! Please input two athletes !";
                        return response;
                    }
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.FirstTeam.SecondAthleteId, tournament.Id);
                            if (secondAthlete == null)
                            {
                                response.Message = "Second Athlete does not exist!";
                                return response;
                            }

                            if (athletes.FirstOrDefault(a => a.Id == secondAthlete.Id) == null)
                            {
                                response.Message = "SecondAthlete already has a team!";
                                return response;
                            }
                            var firstTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.FirstTeamId);
                            firstTeam.SecondAthleteId = secondAthlete.Id;
                            firstTeam.SecondAthlete = secondAthlete;
                            if (!firstTeam.CheckValidateTeam(firstTeam.TeamType))
                            {
                                response.Message = "Invalid Team ! Please check gender and average rank !";
                                return response;
                            }
                            _unitOfWork.TeamRepository.Update(firstTeam);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = "An error occurred while updating the first team!";
                            return response;
                        }
                    }
                }
                else if (teamDTO.FirstTeam.FirstAthleteId != null && teamDTO.FirstTeam.SecondAthleteId == null)
                {
                    if (match.FirstTeamId == null)
                    {
                        response.Message = "Match does not has First Team ! Please input two athletes !";
                        return response;
                    }
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.FirstTeam.FirstAthleteId, tournament.Id);
                            if (firstAthlete == null)
                            {
                                response.Message = "First Athlete does not exist!";
                                return response;
                            }

                            if (athletes.FirstOrDefault(a => a.Id == firstAthlete.Id) == null)
                            {
                                response.Message = "FirstAthlete already has a team!";
                                return response;
                            }

                            var firstTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.FirstTeamId);
                            firstTeam.FirstAthleteId = firstAthlete.Id;
                            firstTeam.FirstAthlete = firstAthlete;
                            if (!firstTeam.CheckValidateTeam(firstTeam.TeamType))
                            {
                                response.Message = "Invalid Team ! Please check gender and average rank !";
                                return response;
                            }
                            _unitOfWork.TeamRepository.Update(firstTeam);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = "An error occurred while updating the first team!";
                            return response;
                        }
                    }
                }
            }
            if (teamDTO.FirstTeam == null && match.FirstTeamId != null)
            {
                // Skipping execution for match.FirstTeam
            }

            if (teamDTO.SecondTeam != null)
            {
                if (teamDTO.SecondTeam.FirstAthleteId != 0 && teamDTO.SecondTeam.SecondAthleteId != 0)
                {
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.SecondTeam.FirstAthleteId, tournament.Id);
                            if (firstAthlete == null)
                            {
                                response.Message = "First Athlete does not exist!";
                                return response;
                            }
                            var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.SecondTeam.SecondAthleteId, tournament.Id);
                            if (secondAthlete == null)
                            {
                                response.Message = "Second Athlete does not exist!";
                                return response;
                            }
                            if (athletes.FirstOrDefault(a => a.Id == firstAthlete.Id) == null)
                            {
                                response.Message = "FirstAthlete already has a team!";
                                return response;
                            }
                            if (athletes.FirstOrDefault(a => a.Id == secondAthlete.Id) == null)
                            {
                                response.Message = "SecondAthlete already has a team!";
                                return response;
                            }

                            Team secondTeam;
                            if (match.SecondTeamId == null)
                            {
                                secondTeam = TeamViewModelExtensions.CreateDoubleTeam(firstAthlete, secondAthlete);
                                secondTeam.TeamName = secondTeam.GetTeamName();
                                secondTeam.TeamType = tournament.FormatType;
                                secondTeam.TournamentId = tournament.Id;
                                secondTeam.Tournament = tournament;
                                secondTeam.AverageRank = secondTeam.GetTeamAverageRank();

                                if (!secondTeam.CheckValidateTeam(secondTeam.TeamType))
                                {
                                    response.Message = "Second team is not valid! Please check gender and average rank!";
                                    return response;
                                }
                                await _unitOfWork.TeamRepository.AddAsync(secondTeam);
                                await _unitOfWork.SaveChangeAsync();
                                match.SecondTeamId = secondTeam.Id;
                                match.SecondTeam = secondTeam;

                                var secondTeamRoundGroup = new TeamRoundGroup
                                {
                                    TeamId = secondTeam.Id,
                                    RoundGroupId = (uint)match.RoundGroupId
                                };
                                await _unitOfWork.TeamRoundGroupRepository.AddAsync(secondTeamRoundGroup);
                                await _unitOfWork.GroupStatisticRepository.AddAsync(new TeamRoundGroupStatistics
                                {
                                    TeamRoundGroupId = secondTeamRoundGroup.Id,
                                    TeamRoundGroup = secondTeamRoundGroup,
                                    Wins = 0,
                                    Losses = 0,
                                    Points = 0,
                                    SetDifference = 0,
                                    PointsDifference = 0
                                });
                                TournamentRankingTeam rankingTeam = tournament.CreateTournamentRankingTeam(secondTeam);
                                await _unitOfWork.TournamentRankingTeamRepository.AddAsync(rankingTeam);
                            }
                            else
                            {
                                secondTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.SecondTeamId);
                                secondTeam.FirstAthleteId = firstAthlete.Id;
                                secondTeam.FirstAthlete = firstAthlete;
                                secondTeam.SecondAthleteId = secondAthlete.Id;
                                secondTeam.SecondAthlete = secondAthlete;
                                secondTeam.AverageRank = secondTeam.GetTeamAverageRank();
                                if (!secondTeam.CheckValidateTeam(secondTeam.TeamType))
                                {
                                    response.Message = "Second team is not valid! Please check gender and average rank!";
                                    return response;
                                }
                                _unitOfWork.TeamRepository.Update(secondTeam);
                            }
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = "An error occurred while assigning the second team to the match!";
                            return response;
                        }
                    }
                }
                else if (teamDTO.SecondTeam.FirstAthleteId == null && teamDTO.SecondTeam.SecondAthleteId != null)
                {
                    if (match.SecondTeamId != null)
                    {
                        response.Message = "Match does not has Second Team ! Please input two athletes !";
                        return response;
                    }
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {

                            var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.SecondTeam.SecondAthleteId, tournament.Id);
                            if (secondAthlete == null)
                            {
                                response.Message = "Second Athlete does not exist!";
                                return response;
                            }
                            var secondTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.SecondTeamId);
                            secondTeam.SecondAthleteId = secondAthlete.Id;
                            secondTeam.SecondAthlete = secondAthlete;
                            secondTeam.AverageRank = secondTeam.GetTeamAverageRank();
                            if (!secondTeam.CheckValidateTeam(secondTeam.TeamType))
                            {
                                response.Message = "Second team is not valid! Please check gender and average rank!";
                                return response;
                            }
                            _unitOfWork.TeamRepository.Update(secondTeam);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = "An error occurred while updating the second team!";
                            return response;
                        }
                    }
                }
                else if (teamDTO.SecondTeam.FirstAthleteId != null && teamDTO.SecondTeam.SecondAthleteId == null)
                {
                    if (match.SecondTeamId != null)
                    {
                        response.Message = "Match does not has Second Team ! Please input two athletes !";
                        return response;
                    }
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete((uint)teamDTO.SecondTeam.FirstAthleteId, tournament.Id);
                            if (firstAthlete == null)
                            {
                                response.Message = "First Athlete does not exist!";
                                return response;
                            }

                            var secondTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)match.SecondTeamId);
                            secondTeam.FirstAthleteId = firstAthlete.Id;
                            secondTeam.FirstAthlete = firstAthlete;
                            secondTeam.AverageRank = secondTeam.GetTeamAverageRank();
                            if (!secondTeam.CheckValidateTeam(secondTeam.TeamType))
                            {
                                response.Message = "Second team is not valid! Please check gender and average rank!";
                                return response;
                            }
                            _unitOfWork.TeamRepository.Update(secondTeam);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = "An error occurred while updating the second team!";
                            return response;
                        }
                    }
                }
            }
            if (teamDTO.SecondTeam == null || (teamDTO.SecondTeam.FirstAthleteId == null && teamDTO.SecondTeam.SecondAthleteId == null))
            {
                // Skipping execution for match.SecondTeam
            }
            response.Success = true;
            response.Message = "Teams assigned to the match successfully!";
            var updatedMatch = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            response.Data = updatedMatch.ToMatchesViewModel();
            return response;
        }

        public async Task<ApiResponseModel<NextRoundMatchViewModel>> AssignTeamsForNextRoundMatch1(uint matchId, AssignTeamsNextRoundMatch teamsDTO)
        {
            var response = new ApiResponseModel<NextRoundMatchViewModel>
            {
                Success = false,
                Data = null
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
            if (match.MatchOrder < tournament.NumberOfTeams / 2)
            {
                response.Message = "First round match does not have any advanced Team"!;
                return response;
            }
            if (teamsDTO.FirstTeamId == 0 && teamsDTO.SecondTeamId == 0)
            {
                response.Message = "Nothing change !";
                return response;
            }
            var winCondition = await _unitOfWork.WinConditionRepository.GetAllAsync();
            if (!winCondition.Any(w => w.Id == teamsDTO.FirstMatchWinConditionId) && teamsDTO.FirstMatchWinConditionId != 0)
            {
                response.Message = "First Match win condition does not existed !";
                return response;
            }
            if (!winCondition.Any(w => w.Id == teamsDTO.SecondMatchWinConditionId) && teamsDTO.SecondMatchWinConditionId != 0)
            {
                response.Message = "Second Match win condition does not existed !";
                return response;
            }
            var currentRoundId = match.RoundGroup.RoundId;
            var previousRound = await _unitOfWork.RoundRepository.GetPreviousRoundByRoundIdAsync(currentRoundId);

            if (previousRound is null)
            {
                response.Message = "No previous round found!";
                return response;
            }

            var previousRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(previousRound.Id);
            if (previousRoundMatches is null || !previousRoundMatches.Any())
            {
                response.Message = "No matches found in the previous round";
            }
            var currentRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(currentRoundId);
            var currentRoundFirstMatchOrder = currentRoundMatches.First().MatchOrder;
            var advancingMatchOrders = IntegerUtils.GetPreviousMatchOrders(match.MatchOrder, currentRoundFirstMatchOrder, tournament.NumberOfTeams - 1);
            if (advancingMatchOrders == null || !advancingMatchOrders.Any())
            {
                response.Message = "No advancing matches found!";
                return response;
            }
            var advancingMatches = new List<PickleballMatch>();
            foreach (var matchOrder in advancingMatchOrders)
            {
                var advancingMatch = await _unitOfWork.PickleballMatchRepository.GetMatchByOrderAsync(matchOrder, match.RoundGroup.Round.TournamentId);
                if (advancingMatch is not null)
                {
                    advancingMatches.Add(advancingMatch);
                }
            }

            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    /*foreach (var pickleballMatch in advancingMatches)
                    {
                        if (teamsDTO.FirstTeamId != 0 && (teamsDTO.FirstTeamId != pickleballMatch.FirstTeamId && teamsDTO.FirstTeamId != pickleballMatch.SecondTeamId))
                        {
                            response.Message = "This team is advanced to another match";
                            return response;
                        }
                        else
                        {
                            break;
                        }
                    }*/
                    var FirstAdvancingMatch = advancingMatches.Where(m => m.MatchOrder % 2 == 1).FirstOrDefault();
                    if (teamsDTO.FirstTeamId != 0 && (teamsDTO.FirstTeamId != FirstAdvancingMatch.FirstTeamId && teamsDTO.FirstTeamId != FirstAdvancingMatch.SecondTeamId))
                    {
                        response.Message = "This team is advanced to another match";
                        return response;
                    }
                    if (teamsDTO.FirstTeamId != 0 && (teamsDTO.FirstTeamId == FirstAdvancingMatch.FirstTeamId || teamsDTO.FirstTeamId == FirstAdvancingMatch.SecondTeamId))
                    {
                        var firstTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)teamsDTO.FirstTeamId);
                        FirstAdvancingMatch.WinningTeamId = teamsDTO.FirstTeamId;
                        FirstAdvancingMatch.WinningTeam = firstTeam;

                        //match.FirstTeamId = teamsDTO.FirstTeamId;
                        //match.FirstTeam = advancingMatch.WinningTeam;
                        if (match.FirstTeamId == null)
                        {
                            if (teamsDTO.FirstMatchWinConditionId == 0 && FirstAdvancingMatch.WinConditionId == null)
                            {
                                response.Message = "First Match win condition is empty ! Please select a win condition !";
                                return response;
                            }
                            else
                            {
                                FirstAdvancingMatch.WinConditionId = teamsDTO.FirstMatchWinConditionId;
                                FirstAdvancingMatch.WinCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync((uint)teamsDTO.FirstMatchWinConditionId);
                            }
                            match.FirstTeamId = teamsDTO.FirstTeamId;
                            match.FirstTeam = FirstAdvancingMatch.WinningTeam;
                            var teamRoundGroup = new TeamRoundGroup
                            {
                                RoundGroupId = (uint)match.RoundGroupId,
                                TeamId = (uint)teamsDTO.FirstTeamId,
                                Team = firstTeam,
                                RoundGroup = await _unitOfWork.RoundGroupRepository.GetByIdAsync((uint)match.RoundGroupId)
                            };
                            await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                        }
                        else
                        {
                            if (teamsDTO.FirstMatchWinConditionId != 0 && FirstAdvancingMatch.WinConditionId != null)
                            {
                                FirstAdvancingMatch.WinConditionId = teamsDTO.FirstMatchWinConditionId;
                                FirstAdvancingMatch.WinCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync((uint)teamsDTO.FirstMatchWinConditionId);
                            }
                            else if (teamsDTO.FirstMatchWinConditionId == 0 && FirstAdvancingMatch.WinConditionId != null)
                            {

                            }
                            match.FirstTeamId = teamsDTO.FirstTeamId;
                            match.FirstTeam = FirstAdvancingMatch.WinningTeam;
                            match.FirstTeamId = teamsDTO.FirstTeamId;
                            match.FirstTeam = FirstAdvancingMatch.WinningTeam;
                            //update existed TeamRoundGroup

                            var teamRoundGroup = await _unitOfWork.TeamRoundGroupRepository.GetSpecificTeamRoundGroupAsync((uint)match.FirstTeamId, (uint)match.RoundGroupId);
                            teamRoundGroup.TeamId = (uint)teamsDTO.FirstTeamId;
                            _unitOfWork.TeamRoundGroupRepository.Update(teamRoundGroup);
                        }
                        _unitOfWork.PickleballMatchRepository.Update(FirstAdvancingMatch);
                        _unitOfWork.PickleballMatchRepository.Update(match);
                        await _unitOfWork.SaveChangeAsync();
                    }

                    if (teamsDTO.FirstTeamId == 0 && match.FirstTeamId != null)
                    {

                    }

                    /*foreach (var pickleballMatch in advancingMatches)
                    {
                        // Validate and assign Second Team
                        if (teamsDTO.SecondTeamId != 0 && (teamsDTO.SecondTeamId != pickleballMatch.FirstTeamId && teamsDTO.SecondTeamId != pickleballMatch.SecondTeamId))
                        {
                            response.Message = "This team is advanced to another match";
                            return response;
                        }
                        else
                        {
                            break;
                        }
                    }*/
                    var secondAdvancingMatch = advancingMatches.Where(m => m.MatchOrder % 2 == 0).FirstOrDefault();
                    if (teamsDTO.SecondTeamId != 0 && (teamsDTO.SecondTeamId != secondAdvancingMatch.FirstTeamId && teamsDTO.SecondTeamId != secondAdvancingMatch.SecondTeamId))
                    {
                        response.Message = "This team is advanced to another match";
                        return response;
                    }
                    if (teamsDTO.SecondTeamId != 0 &&
                        (teamsDTO.SecondTeamId == secondAdvancingMatch.FirstTeamId || teamsDTO.SecondTeamId == secondAdvancingMatch.SecondTeamId))
                    {
                        var secondTeam = await _unitOfWork.TeamRepository.GetByIdAsync((uint)teamsDTO.SecondTeamId);
                        secondAdvancingMatch.WinningTeamId = teamsDTO.SecondTeamId;
                        secondAdvancingMatch.WinningTeam = secondTeam;
                        //secondAdvancingMatch.WinConditionId = teamsDTO.SecondMatchWinConditionId;
                        //secondAdvancingMatch.WinCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync((uint)teamsDTO.SecondMatchWinConditionId);
                        //match.SecondTeamId = teamsDTO.SecondTeamId;
                        //match.SecondTeam = advancingMatch.WinningTeam;

                        if (match.SecondTeamId == null)
                        {
                            if (teamsDTO.SecondMatchWinConditionId == 0 && secondAdvancingMatch.WinConditionId == null)
                            {
                                response.Message = "Second Match does not have win condition ! Please select win condition !";
                            }
                            else
                            {
                                secondAdvancingMatch.WinConditionId = teamsDTO.SecondMatchWinConditionId;
                                secondAdvancingMatch.WinCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync((uint)teamsDTO.SecondMatchWinConditionId);
                            }

                            match.SecondTeamId = teamsDTO.SecondTeamId;
                            match.SecondTeam = secondAdvancingMatch.WinningTeam;
                            var teamRoundGroup = new TeamRoundGroup
                            {
                                RoundGroupId = (uint)match.RoundGroupId,
                                TeamId = (uint)teamsDTO.SecondTeamId,
                                Team = secondTeam,
                                RoundGroup = await _unitOfWork.RoundGroupRepository.GetByIdAsync((uint)match.RoundGroupId)
                            };
                            await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                        }
                        else
                        {
                            if (teamsDTO.SecondMatchWinConditionId != 0 && secondAdvancingMatch.WinConditionId != null)
                            {
                                secondAdvancingMatch.WinConditionId = teamsDTO.SecondMatchWinConditionId;
                                secondAdvancingMatch.WinCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync((uint)teamsDTO.SecondMatchWinConditionId);
                            }
                            else if (teamsDTO.SecondMatchWinConditionId == 0 && secondAdvancingMatch.WinConditionId != null)
                            {

                            }
                            match.SecondTeamId = teamsDTO.SecondTeamId;
                            match.SecondTeam = secondAdvancingMatch.WinningTeam;
                            var teamRoundGroup = await _unitOfWork.TeamRoundGroupRepository
                                .GetSpecificTeamRoundGroupAsync((uint)match.SecondTeamId, (uint)match.RoundGroupId);
                            teamRoundGroup.TeamId = (uint)teamsDTO.SecondTeamId;
                            _unitOfWork.TeamRoundGroupRepository.Update(teamRoundGroup);
                        }

                        _unitOfWork.PickleballMatchRepository.Update(secondAdvancingMatch);
                        _unitOfWork.PickleballMatchRepository.Update(match);
                        await _unitOfWork.SaveChangeAsync();
                    }
                    if (teamsDTO.SecondTeamId == null && match.SecondTeamId == null)
                    {

                    }


                    await transaction.CommitAsync();

                    response.Success = true;
                    response.Message = "Teams assigned to next round match successfully!";
                    response.Data = new NextRoundMatchViewModel
                    {
                        Id = match.Id,
                        MatchOrder = match.MatchOrder,
                        Round = match.RoundGroup.Round.RoundName,
                        RoundGroupId = (uint)match.RoundGroupId,
                        RoundId = match.RoundGroup.RoundId,
                        FirstTeams = new List<TeamViewModel>
                {
                    match.FirstTeam != null ? new TeamViewModel { TeamId = match.FirstTeam.Id, TeamName = match.FirstTeam.TeamName } : null
                }.Where(t => t != null).ToList(),
                        SecondTeams = new List<TeamViewModel>
                {
                    match.SecondTeam != null ? new TeamViewModel { TeamId = match.SecondTeam.Id, TeamName = match.SecondTeam.TeamName } : null
                }.Where(t => t != null).ToList()
                    };
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = $"An error occurred while assigning teams: {ex.Message}";
                }
            }
            return response;
        }

        public async Task<ApiResponseModel<List<PickleballMatchViewModel>>> GenerateMatchesForRoundGroup(uint roundGroupId)
        {
            var response = new ApiResponseModel<List<PickleballMatchViewModel>>()
            {
                Success = false,
                Data = null
            };
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupByIdAsync(roundGroupId);
            if (roundGroup is null)
            {
                response.Message = "Group does not existed in round !";
                return response;
            }
            if (roundGroup.GroupStatus != MatchStatus.Scheduling)
            {
                response.Message = "The Group's matches have been scheduled !";
                return response;
            }
            var listTeam = await _unitOfWork.TeamRepository.GetTeamsByRoundGroupIdAsync(roundGroupId);
            if (listTeam is null)
            {
                response.Message = "Group does not have any Team !";
                return response;
            }
            if (listTeam.Count == 1)
            {
                response.Message = "Group only has 1 team ! Can not create macth for group !";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(roundGroup.Round.TournamentId);
            List<PickleballMatch> listMatches = new List<PickleballMatch>();
            for (int i = 0; i < listTeam.Count; i++)
            {
                for (int j = i + 1; j < listTeam.Count; j++)
                {
                    PickleballMatch match = new PickleballMatch
                    {
                        FirstTeamId = listTeam[i].Id,
                        FirstTeam = listTeam[i],
                        SecondTeamId = listTeam[j].Id,
                        SecondTeam = listTeam[j],
                        MatchDate = DateTime.Now,
                        MatchStatus = MatchStatus.Scheduling,
                        MatchOrder = 0,
                        RoundGroupId = roundGroup.Id,
                        RoundGroup = roundGroup
                    };
                    listMatches.Add(match);
                }
            }
            List<Set> sets = GenerateSetsForMatch(listMatches, tournament.NumberOfSets);
            roundGroup.GroupStatus = MatchStatus.Scheduled;
            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    //await _unitOfWork.TeamRepository.AddRangeAsync(listTeam);
                    //await _unitOfWork.SaveChangeAsync();

                    await _unitOfWork.PickleballMatchRepository.AddRangeAsync(listMatches);
                    //await _unitOfWork.SaveChangeAsync();

                    await _unitOfWork.SetRepository.AddRangeAsync(sets);
                    _unitOfWork.RoundGroupRepository.Update(roundGroup);
                    await _unitOfWork.SaveChangeAsync();
                    await transaction.CommitAsync();

                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = ex.Message;
                    return response;
                }
            }
            List<PickleballMatchViewModel> listMatchesViewModel = new List<PickleballMatchViewModel>();
            foreach (var match in listMatches)
            {
                PickleballMatchViewModel viewModel = match.ToMatchesViewModel();
                listMatchesViewModel.Add(viewModel);
            }
            response.Success = true;
            response.Message = "Generate Matches for Round success !";
            response.Data = listMatchesViewModel;
            return response;
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> UpdateRoundGroupMatchResult(uint matchId, UpdateMatchResultDTO matchDTO)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>
            {
                Data = null,
                Success = false,
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchWithTeamByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            if (match.MatchStatus != MatchStatus.WaitingForResult)
            {
                response.Message = "Can not update match's result ! Please check the match status !";
                return response;
            }
            if (matchDTO.WinningTeamId == 0)
            {
                response.Message = "Please select winning Team !";
            }
            if (matchDTO.WinConditionId == 0)
            {
                response.Message = "Please select Win condition !";
                return response;
            }
            if (matchDTO.WinningTeamId != match.FirstTeamId && matchDTO.WinningTeamId != match.SecondTeamId)
            {
                response.Message = "The team does not participate match !";
                return response;
            }
            var winCondition = await _unitOfWork.WinConditionRepository.GetByIdAsync(matchDTO.WinConditionId);
            if (winCondition is null)
            {
                response.Message = "Win condition is not valid !";
                return response;
            }
            match.WinConditionId = winCondition.Id;
            match.WinCondition = winCondition;
            TeamRoundGroupStatistics firstTeamStatistic =
                    await _unitOfWork.TeamRoundGroupRepository.GetGroupTeamStatisticAsync((uint)match.FirstTeamId, (uint)match.RoundGroupId);
            TeamRoundGroupStatistics secondTeamStatistic =
                await _unitOfWork.TeamRoundGroupRepository.GetGroupTeamStatisticAsync((uint)match.SecondTeamId, (uint)match.RoundGroupId);
            List<TeamRoundGroupStatistics> listStatistic = new List<TeamRoundGroupStatistics>
                { firstTeamStatistic,
                  secondTeamStatistic
                };
            match.MatchStatus = MatchStatus.Completed;
            if (matchDTO.WinningTeamId == match.FirstTeamId)
            {
                match.WinningTeamId = match.FirstTeamId;
                match.WinningTeam = match.FirstTeam;

                firstTeamStatistic.Wins = 1;
                firstTeamStatistic.Points = 1;
                secondTeamStatistic.Losses = 1;
                secondTeamStatistic.Points = 0;

                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        _unitOfWork.PickleballMatchRepository.Update(match);
                        _unitOfWork.GroupStatisticRepository.UpdateRange(listStatistic);
                        await _unitOfWork.SaveChangeAsync();
                    }
                    catch (Exception ex)
                    {
                        await transaction.CommitAsync();
                        response.Message = ex.Message;
                        return response;
                    }
                }
            }
            else
            {
                match.WinningTeamId = match.SecondTeamId;
                match.WinningTeam = match.SecondTeam;

                secondTeamStatistic.Wins = 1;
                secondTeamStatistic.Points = 1;
                firstTeamStatistic.Losses = 1;
                firstTeamStatistic.Points = 0;

                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        _unitOfWork.PickleballMatchRepository.Update(match);
                        _unitOfWork.GroupStatisticRepository.UpdateRange(listStatistic);
                        await _unitOfWork.SaveChangeAsync();
                    }
                    catch (Exception ex)
                    {
                        await transaction.CommitAsync();
                        response.Message = ex.Message;
                        return response;
                    }
                }
            }
            response.Success = true;
            response.Data = match.ToMatchesViewModel();
            response.Message = "Update Match result success !";
            return response;
        }
        public List<Set> GenerateSetsForMatch(List<PickleballMatch> matches, int numberOfSets)
        {
            List<Set> sets = new List<Set>();
            foreach (var match in matches)
            {
                for (int i = 1; i <= numberOfSets; i++)
                {
                    Set set = new Set
                    {
                        PickleballMatchId = match.Id,
                        FirstTeamScore = 0,
                        SecondTeamScore = 0,
                        SetOrder = i,
                        SetStatus = MatchStatus.Scheduling,
                        PickleballMatch = match,
                        WinningTeamId = null,
                    };
                    sets.Add(set);
                }
            }
            return sets;
        }

        public async Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetMatchesByRoundGroupId(uint roundGroupId)
        {
            var response = new ApiResponseModel<List<PickleballMatchViewModel>>()
            {
                Data = null,
                Success = true
            };
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetByIdAsync(roundGroupId);
            if (roundGroup == null)
            {
                response.Message = "Round group does not existed !";
                return response;
            }
            var matches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundGroupIdAsync(roundGroupId);
            if (matches is null)
            {
                response.Message = "List matches is empty !";
                return response;
            }
            List<PickleballMatchViewModel> matchViewModels = new List<PickleballMatchViewModel>();
            foreach (var match in matches)
            {
                PickleballMatchViewModel matchViewModel = match.ToMatchesViewModel();
                matchViewModels.Add(matchViewModel);
            }
            response.Data = matchViewModels;
            response.Message = $"List match of {roundGroup.RoundGroupName}";
            return response;
        }

        public async Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetMatchesByRoundId(uint roundId)
        {
            var response = new ApiResponseModel<List<PickleballMatchViewModel>>()
            {
                Data = null,
                Success = true
            };
            var round = await _unitOfWork.RoundRepository.GetByIdAsync(roundId);
            if (round == null)
            {
                response.Message = "Round does not existed !";
                return response;
            }
            var matches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(roundId);
            if (matches is null)
            {
                response.Message = "List matches is empty !";
                return response;
            }
            List<PickleballMatchViewModel> matchViewModels = new List<PickleballMatchViewModel>();
            foreach (var match in matches)
            {
                PickleballMatchViewModel matchViewModel = match.ToMatchesViewModel();
                matchViewModels.Add(matchViewModel);
            }
            response.Data = matchViewModels;
            response.Message = $"List match of {round.RoundName}";
            return response;
        }

        public async Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetNextRoundsMatchesByTournamentId(uint tournamentId)
        {
            var response = new ApiResponseModel<List<PickleballMatchViewModel>>
            {
                Data = null,
                Success = true
            };
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed";
                return response;
            }
            var matches = await _unitOfWork.PickleballMatchRepository.GetNextRounsMatchesByTournamentIdAsync(tournamentId);
            if (matches is null)
            {
                response.Message = "List matches is empty !";
                return response;
            }
            response.Data = matches;
            response.Message = "List of next rounds matches !";
            return response;
        }

        public async Task<ApiResponseModel<Pagination<PickleballMatchViewModel>>> GetMatchesByRoundIdPagingsions(uint roundId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<PickleballMatchViewModel>>
            {
                Data = null,
                Success = true
            };
            var round = await _unitOfWork.RoundRepository.GetByIdAsync(roundId);
            if (round == null)
            {
                response.Message = "Round does not existed !";
                return response;
            }
            var matches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(roundId);
            if (matches is null)
            {
                response.Message = "List matches is empty !";
                return response;
            }

            response.Data = matches.Select(m => m.ToMatchesViewModel()).ToList().ToPagination(pageIndex, pageSize);
            response.Message = $"List match of {round.RoundName}";
            return response;
        }
        public async Task<ApiResponseModel<PickleballMatchViewModel>> AssignTeamToMatch(uint matchId, AssignTeamsDto teamsDto)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>
            {
                Data = null,
                Success = false
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            var round = await _unitOfWork.RoundRepository.GetByIdAsync(match.RoundGroup.RoundId);
            if (round.RoundOrder != 2)
            {
                response.Message = "Assign Team apply for second Round !";
                return response;
            }
            if (teamsDto.FirstTeamId == 0 && teamsDto.SecondTeamId == 0)
            {
                response.Message = "Nothing changes !";
                return response;
            }
            if (teamsDto.FirstTeamId == teamsDto.SecondTeamId)
            {
                response.Message = "Can not assign same team in match !";
                return response;
            }
            if (teamsDto.FirstTeamId == 0 && match.FirstTeam is null)
            {
                response.Message = "First team must be assigned !";
                return response;
            }
            var teamsWithoutMatch = await _unitOfWork.TeamRoundGroupRepository.GetTeamWithoutMatchAsync(match.RoundGroup.RoundId);
            if (teamsWithoutMatch is null)
            {
                response.Message = "All teams in round have already had match !";
                return response;
            }

            var firstTeam = teamsWithoutMatch.FirstOrDefault(t => t.Id == teamsDto.FirstTeamId);
            var secondTeam = teamsWithoutMatch.FirstOrDefault(t => t.Id == teamsDto.SecondTeamId);
            if (teamsDto.FirstTeamId != 0 && teamsDto.SecondTeamId != 0)
            {
                if (!teamsWithoutMatch.Any(t => t.Id == teamsDto.FirstTeamId))
                {
                    response.Message = "First team already compete in match !";
                    return response;
                }
                if (!teamsWithoutMatch.Any(t => t.Id == teamsDto.SecondTeamId))
                {
                    response.Message = "Second team already compete in match !";
                    return response;
                }
                match.FirstTeamId = firstTeam?.Id;
                match.FirstTeam = firstTeam;
                match.SecondTeamId = secondTeam?.Id;
                match.SecondTeam = secondTeam;
            }
            if (teamsDto.FirstTeamId != 0 && teamsDto.SecondTeamId == 0)
            {
                if (!teamsWithoutMatch.Any(t => t.Id == teamsDto.FirstTeamId))
                {
                    response.Message = "First team already compete in match !";
                    return response;
                }
                match.FirstTeamId = firstTeam?.Id;
                match.FirstTeam = firstTeam;
            }
            if (teamsDto.FirstTeamId == 0 && teamsDto.SecondTeamId != 0)
            {
                if (!teamsWithoutMatch.Any(t => t.Id == teamsDto.SecondTeamId))
                {
                    response.Message = "Second team already compete in match !";
                    return response;
                }
                match.SecondTeamId = secondTeam?.Id;
                match.SecondTeam = secondTeam;
            }
            try
            {
                _unitOfWork.PickleballMatchRepository.Update(match);
                await _unitOfWork.SaveChangeAsync();
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
            response.Success = true;
            response.Data = match.ToMatchesViewModel();
            response.Message = "Assign Team to Match success !";
            return response;
        }

        public async Task<ApiResponseModel<Pagination<PickleballMatchViewModel>>> GetNextRoundMatchesInTournamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<PickleballMatchViewModel>>
            {
                Data = null,
                Success = true
            };

            var matches = await _unitOfWork.PickleballMatchRepository.GetNextRounsMatchesByTournamentIdAsync(tournamentId);
            if (matches is null)
            {
                response.Message = "Next Round does not have any match !";
                return response;
            }
            response.Message = "List of Pickleball matches !";
            response.Data = matches.ToPagination(pageIndex, pageSize);
            return response;
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> AddCourtToMatch(uint matchId, CourtIdViewModel courtIdViewModel)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>
            {
                Data = null,
                Success = false
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }

            if (match.MatchStatus != Domain.Enums.MatchStatus.Scheduling)
            {
                response.Message = "Match has been scheduled ! Can not change the court !";
                return response;
            }

            var courts = await _unitOfWork.CourtRepository.GetAvailableCourtsAsync(match.Id);
            if (courts is null)
            {
                response.Message = "No court available !";
                return response;
            }
            if (!courts.Any(c => c.Id == courtIdViewModel.CourtId))
            {
                response.Message = "Court is not available !";
                return response;
            }
            var court = courts.FirstOrDefault(c => c.Id == courtIdViewModel.CourtId);
            match.Court = court;
            match.CourtId = court.Id;
            try
            {
                _unitOfWork.PickleballMatchRepository.Update(match);
                await _unitOfWork.SaveChangeAsync();
                response.Message = $"Add court {court.CourtName} to Match {match.MatchOrder} successfully !";
                response.Success = true;
                response.Data = match.ToMatchesViewModel();
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchDate(uint matchId, UpdateMatchDateDto updateMatchDateDto)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>()
            {
                Data = null,
                Success = false,
            };

            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            if (updateMatchDateDto.MatchDate < DateTime.Now)
            {
                response.Message = "Match Date must not smaller than Present Date Time !";
                return response;
            }
            match.MatchDate = (DateTime)updateMatchDateDto.MatchDate;
            try
            {
                _unitOfWork.PickleballMatchRepository.Update(match);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Update Match Date success !";
                response.Data = match.ToMatchesViewModel();
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchStatus(uint matchId,UpdateMatchStatusDto matchStatusDto)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>
            {
                Data = null,
                Success = false
            };
            if(matchStatusDto == null)
            {
                response.Message = "Nothing change !";
                return response;
            }
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(match.RoundGroup.Round.TournamentId);            
            try
            {
                var matchStatus = matchStatusDto.MatchStatus.ToMatchStatus();
                string message;
                if(matchStatus == MatchStatus.InProgress && !match.ValidateMatch(tournament.FormatType,out message))
                {
                    response.Message = message;
                    return response;
                }
                if(matchStatus == MatchStatus.Completed)
                {
                    response.Message = "Can not update match status match complete !";
                    return response;
                }
                switch (match.MatchStatus)
                {
                    case MatchStatus.Scheduling:
                        if(matchStatus == MatchStatus.Completed)
                        {
                            response.Message = "Can not set macth completed !";
                            return response;
                        }
                        match.MatchStatus = matchStatus;
                        break;                   
                    case MatchStatus.Postponed:
                        if (matchStatus == MatchStatus.InProgress && match.MatchDate != DateTime.Now)
                        {
                            response.Message = "Match Date has passed ! Can not start the match !";
                            return response;
                        }
                        match.MatchStatus = matchStatus;
                        break;
                    case MatchStatus.InProgress:
                        if(matchStatus != MatchStatus.Postponed || matchStatus!= MatchStatus.Canceled)
                        {
                            response.Message = "The match is in progress now ! Can only postponed or canceled the match !";
                            return response;
                        }
                        match.MatchStatus = matchStatus;
                        break;
                    case MatchStatus.Completed:
                        response.Message = "The match has already completed ! Can not change match status !";
                        return response;
                    case MatchStatus.Canceled:
                        response.Message = "The match has been canceled ! Can not change match status !";
                        return response;
                }
                _unitOfWork.PickleballMatchRepository.Update(match);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Update match status success !";
                response.Data = match.ToMatchesViewModel();
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
            return response;
        }

        public async Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchDetails(uint matchId, UpdateMatchDetailDto matchDetailDto)
        {
            var response = new ApiResponseModel<PickleballMatchViewModel>()
            {
                Data = null,
                Success = false,
            };

            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match is null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            if (match.MatchStatus != Domain.Enums.MatchStatus.Scheduling 
                && match.MatchStatus != Domain.Enums.MatchStatus.Postponed)
            {
                response.Message = $"Match has been {match.MatchStatus.ToString()} ! Can not update match details !";
                return response;
            }
            if (matchDetailDto.MatchDate == null && matchDetailDto.CourtId == 0)
            {
                response.Message = "Nothing change !";
                return response;
            }
            if(matchDetailDto.MatchDate is not null)
            {
                if (matchDetailDto.MatchDate < DateTime.Now)
                {
                    response.Message = "Match Date must not smaller than Present Date Time !";
                    return response;
                }
                match.MatchDate = (DateTime)matchDetailDto.MatchDate;                
            }
            if(matchDetailDto.CourtId != 0)
            {
                var courts = await _unitOfWork.CourtRepository.GetAvailableCourtsAsync(match.Id);
                if (courts is null)
                {
                    response.Message = "No court available !";
                    return response;
                }
                if (!courts.Any(c => c.Id == matchDetailDto.CourtId))
                {
                    response.Message = "Court is not available !";
                    return response;
                }
                var court = courts.FirstOrDefault(c => c.Id == matchDetailDto.CourtId);
                match.Court = court;
                match.CourtId = court.Id;
            }
            try
            {
                _unitOfWork.PickleballMatchRepository.Update(match);
                await _unitOfWork.SaveChangeAsync();
                response.Message = $"Update match details successfully !";
                response.Success = true;
                response.Data = match.ToMatchesViewModel();
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }
    }
}

