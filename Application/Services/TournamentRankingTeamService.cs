﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Application.ViewModels.TournamentRankingViewModel;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class TournamentRankingTeamService : ITournamentRankingTeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TournamentRankingTeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<Pagination<RankingTeamModel>>> GetTournamentRankingTeamPaging(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<RankingTeamModel>>()
            {
                Data = null,
                Success = true
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if(tournament is null)
            {
                response.Message = "Tournament does not existed !";
                response.Success = false;
                return response;
            }
            var tournamentRankingTeams = await _unitOfWork.TournamentRankingTeamRepository.GetTournamentRankingTeamsByTournamentIdAsync(tournamentId);
            if(tournamentRankingTeams is null)
            {
                response.Message = "List team is empty !";
                return response;
            }
            response.Data = tournamentRankingTeams.ToPagination(pageIndex, pageSize);
            response.Message = "Ranking Team in Tournament !";
            return response;
        }
    }
}
