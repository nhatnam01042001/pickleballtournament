﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly IMapper _mapper;
        private readonly AppConfiguration _appConfiguration;
        private readonly IEmailService _emailService;
        private readonly IClaimsService _claimsService;

        public UserService(IUnitOfWork unitOfWork, ICurrentTime currentTime, IMapper mapper, AppConfiguration configuration, IEmailService emailService, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _mapper = mapper;
            _appConfiguration = configuration;
            _emailService = emailService;
            _claimsService = claimsService;
        }



        public Task RegisterAsync(UserRegisterDto userObject)
        {
            throw new NotImplementedException();
        }
        public async Task<UserViewModel> UpdateUserProfile(uint id, UpdatedUserDTO updatedUser)
        {
            var existedUser = await _unitOfWork.UserRepository.GetByIdAsync(id);
            if (existedUser is null)
            {
                throw new Exception("User is not existed ! Please try again");
            }
            string dateFormat = "dd-MM-yyyy";
            try
            {
                if (DateTime.TryParseExact(updatedUser.DateOfBirth, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDate))
                {
                    existedUser.DateOfBirth = parsedDate;
                }
                else
                {
                    // Parsing failed, handle the error condition
                    Console.WriteLine("Invalid date format.");
                    // Optionally, throw an exception, log an error, or take appropriate action
                }
            }
            catch (FormatException)
            {
                // Exception occurred during parsing, handle it if needed
                Console.WriteLine("Error occurred while parsing the date.");
                // Optionally, throw the exception, log an error, or take appropriate action
            }
            existedUser.UpdatedUserViewModel(updatedUser);
            _unitOfWork.UserRepository.Update(existedUser);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                UserViewModel userViewModel = existedUser.ToUserViewModel();
                return userViewModel;
            }
            return null;
        }

        public async Task<List<GetUserDTO>> GetUsers()
        {
            var users = await _unitOfWork.UserRepository.GetUsersAsync();
            return _mapper.Map<List<GetUserDTO>>(users);
        }

        public async Task<Pagination<GetUserDTO>> GetUsersPaged(int pageIndex, int pageSize)
        {
            var totalItemsCount = await _unitOfWork.UserRepository.GetTotalUsersCountAsync();
            var users = await _unitOfWork.UserRepository.GetUsersAsync(pageIndex, pageSize);
            var userDtos = _mapper.Map<List<GetUserDTO>>(users);
            return new Pagination<GetUserDTO>
            {
                TotalItemsCount = totalItemsCount,
                PageSize = pageSize,
                PageIndex = pageIndex,
                Items = userDtos
            };
        }

        public async Task<GetUserDTO> GetUserById(uint id)
        {
            var user = await _unitOfWork.UserRepository.GetUserByIdAsync(id);
            if (user == null || user.Status == UserStatus.Deleted)
            {
                // Handle the case where the user is not found
                throw new KeyNotFoundException($"User with ID {id} not found");
            }
            return _mapper.Map<GetUserDTO>(user);
        }

        public async Task CreateUser(CreateUserDTO createUserDTO)
        {

            var user = _mapper.Map<User>(createUserDTO);
            var salt = GenerateSalt();
            var account = new Account
            {

                UserName = createUserDTO.UserName,
                PasswordHash = HashPassword(createUserDTO.Password, salt),
                Salt = salt,
                User = user
            };

            user.Account = account;
            user.Role = 0;
            await _unitOfWork.UserRepository.AddUserAsync(user);
            await _unitOfWork.SaveChangeAsync();
        }

        private string HashPassword(string password, string salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var combinedPassword = password + salt;
                var bytes = Encoding.UTF8.GetBytes(combinedPassword);
                var hash = sha256.ComputeHash(bytes);
                IEnumerable<string> hashHex = hash.Select(x => x.ToString("x2"));
                return string.Join("", hashHex);
            }
        }

        private string GenerateSalt()
        {
            byte[] randomBytes = RandomNumberGenerator.GetBytes(15);
            IEnumerable<string> saltHex = randomBytes.Select(x => x.ToString("x2"));
            return string.Join("", saltHex);
        }

        /*public async Task UpdateUser(uint id, UpdatedUserDTO updateUserDTO)
        {
            var user = await _unitOfWork.UserRepository.GetUserByIdAsync(id);
            if (user == null || user.Status == UserStatus.Deleted)
            {
                // Handle the case where the user is not found
                throw new KeyNotFoundException($"User with ID {id} not found");
            }
            _mapper.Map(updateUserDTO, user);
            _unitOfWork.UserRepository.UpdateUser(user);
            await _unitOfWork.SaveChangeAsync();
        }*/

        public async Task DeleteUser(uint id)
        {
            var user = await _unitOfWork.UserRepository.GetUserByIdAsync(id);
            if (user == null)
            {
                // Handle the case where the user is not found
                throw new KeyNotFoundException($"User with ID {id} not found");
            }
            user.IsDeleted = true;
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task UpdateManagerAsync(uint id, UpdateManagerDTO updateManagerDTO)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(id);
            if (user != null)
            {
                _mapper.Map(updateManagerDTO, user);
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        public async Task<ApiResponseModel<EmailResponseViewModel>> ResetPasswordAsync(ResetPasswordDto resetPasswordDto)
        {
            var response = new ApiResponseModel<EmailResponseViewModel> { Success = false };

            var account = await _unitOfWork.AccountRepository.GetAccountByEmailAsync(resetPasswordDto.Email);
            if (account == null)
            {
                response.Message = "Email does not exist!";
                return response;
            }

            if (string.IsNullOrEmpty(account.User.Email))
            {
                response.Message = "Account does not have an email!";
                return response;
            }

            var newPassword = ResetPasswordViewModelExtension.GenerateRandomPassword(8);

            var salt = GenerateSalt();
            account.PasswordHash = HashPassword(newPassword, salt);
            account.Salt = salt;

            await _unitOfWork.AccountRepository.UpdateAccountAsync(account);
            await _unitOfWork.SaveChangeAsync();

            await _emailService.SendPasswordResetEmail(account.User.Email, account.UserName, newPassword);

            response.Success = true;
            response.Message = "Password reset successfully! The new password has been sent to the user's email.";
            return response;
        }

        public async Task<ApiResponseModel<Pagination<UserViewModel>>> GetUserNotInCampaignAsync(uint campaignId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<UserViewModel>>
            {
                Success = true,
                Data = null
            };
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Success = false;
                response.Message = "Tournament Campaign does not existed !";
                return response;
            }
            var users = await _unitOfWork.UserRepository.GetUserNotParticipateInCampaignAsync(campaignId);
            if (users is null)
            {
                response.Message = "All users have participated in this campaign !";
                return response;
            }
            response.Message = "List Users not participate in Campaign !";
            response.Data = users.ToPagination(pageIndex, pageSize);
            return response;
        }

        public async Task<ApiResponseModel<UserViewModel>> GetCurrentUser()
        {
            var response = new ApiResponseModel<UserViewModel>
            {
                Success = false,
                Data = null
            };
            var userId = _claimsService.GetCurrentUserId;
            if (userId == default)
            {
                response.Message = "Action required login !";
                return response;
            }
            var user = await _unitOfWork.UserRepository.GetUserByIdAsync(userId);
            if (user is null)
            {
                response.Message = "User does not existed !";
                return response;
            }
            response.Success = true;
            response.Message = "User Profile !";
            response.Data = user.ToUserViewModel();
            return response;
        }

        

    }
}
