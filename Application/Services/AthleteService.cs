﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.ParticipantViewModels;
using Application.ViewModels.UserViewModels;
using CloudinaryDotNet.Actions;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class AthleteService : IAthleteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClaimsService _claimsService;
        public AthleteService(IUnitOfWork unitOfWork, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _claimsService = claimsService;
        }

        /*public async Task<ApiResponseModel<List<AthleteViewModel>>> AddUserPlayer(uint tournamentCampaignId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Data = null,
                Success = false,
            };

            List<Athlete> participants = new List<Athlete>();
            var users = await _unitOfWork.UserRepository.GetAthleteUserAsync();
            if (users is null)
            {
                response.Message = "List of User is empty !";
                return response;
            }
            var existedParticipants = await _unitOfWork.AthleteRepository.GetParticipantsInTournamentAsync(tournamentCampaignId);
            foreach (var user in users)
            {
                bool checkExisted = existedParticipants.Any(p => p.TournamentCampaignId == tournamentCampaignId && p.UserId == user.Id);
                if (!checkExisted)
                {
                    var participant = new Athlete
                    {
                        TournamentCampaignId = tournamentCampaignId,
                        UserId = user.Id,
                        User = user,
                        ParticipantType = 0,
                    };
                    participants.Add(participant);
                }
            }
            await _unitOfWork.AthleteRepository.AddRangeAsync(participants);
            bool addSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (!addSuccess)
            {
                response.Message = "There is any new athlete !";
                return response;
            }
            List<AthleteViewModel> participantViewModels = new List<AthleteViewModel>();
            foreach (var participant in participants)
            {
                AthleteViewModel viewModel = participant.ToAthleteViewModel();
                participantViewModels.Add(viewModel);
            }
            response.Success = true;
            response.Message = "List of athletes !";
            response.Data = participantViewModels;
            return response;
        }

        public async Task<ApiResponseModel<List<AthleteViewModel>>> AddGuestPlayer(uint tournamentCampaignId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>()
            {
                Success = false,
                Data = null
            };
            List<Athlete> participants = new List<Athlete>();
            var guestPlayers = await _unitOfWork.CampaignRegistRepository.GetApproveRegistration();
            if (guestPlayers is null)
            {
                response.Message = "There is any new athlete !";
                return response;
            }
            var existedParticipants = await _unitOfWork.AthleteRepository.GetParticipantsInTournamentAsync(tournamentCampaignId);
            foreach (var guestPlayer in guestPlayers)
            {
                bool checkExisted = existedParticipants.Any(p => p.TournamentCampaignId == tournamentCampaignId && p.GuestPlayerId == guestPlayer.Id);
                if (!checkExisted)
                {
                    var participant = new Athlete
                    {
                        TournamentCampaignId = tournamentCampaignId,
                        GuestPlayerId = guestPlayer.Id,
                        GuestPlayer = guestPlayer,
                        ParticipantType = ParticipantType.GuestPlayer
                    };
                    participants.Add(participant);
                }
            }
            await _unitOfWork.AthleteRepository.AddRangeAsync(participants);
            bool addSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (!addSuccess)
            {
                response.Message = "There is any new athlete !";
                return response;
            }
            List<AthleteViewModel> participantViewModels = new List<AthleteViewModel>();
            foreach (var participant in participants)
            {
                AthleteViewModel viewModel = participant.ToAthleteViewModel();
                participantViewModels.Add(viewModel);
            }
            response.Success = true;
            response.Message = "List of athlete !";
            response.Data = participantViewModels;
            return response;
        }*/

        public async Task<ApiResponseModel<List<AthleteViewModel>>>? GetTournamentParticipantsAsync(uint tournamentId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>()
            {
                Data = null,
                Success = false
            };
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(tournamentId);
            if (campaign is null)
            {
                response.Message = "Tournament Campaign does not existed !";
                return response;
            }
            var participants = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(tournamentId);
            if (participants is null)
            {
                response.Message = "List of Athletes is empty";
                return response;
            }
            List<AthleteViewModel> listParticipants = new List<AthleteViewModel>();
            foreach (var participant in participants)
            {
                AthleteViewModel viewModel = participant.ToAthleteViewModel();
                viewModel.TournamentCampaignId = campaign.Id.ToString();
                listParticipants.Add(viewModel);
            }
            response.Success = true;
            response.Message = "List of Athletes !";
            response.Data = listParticipants;
            return response;
        }

        public async Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetTournamentParticipantsPagingsions(uint campaignId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<AthleteViewModel>>
            {
                Data = null,
                Success = true
            };
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(campaignId);
            if (athletes is null)
            {
                response.Message = "List of athletes is empty !";
                return response;
            }
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Message = "Tournament Campaign does not existed !";
                return response;
            }
            List<AthleteViewModel> athleteViewModels = new List<AthleteViewModel>();
            foreach (var athlete in athletes)
            {
                AthleteViewModel viewModel = athlete.ToAthleteViewModel();
                viewModel.TournamentCampaignId = campaign.Id.ToString();
                athleteViewModels.Add(viewModel);
            }
            response.Message = "List of athletes !";
            response.Data = athleteViewModels.ToPagination(pageIndex, pageSize);
            return response;
        }
        public async Task<ApiResponseModel<List<AthleteViewModel>>>? GetNonTeamAthletes(uint tournamentId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Success = false,
                Data = null
            };
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(tournamentId);
            if (athletes is null)
            {
                response.Message = "List is empty !";
                return response;
            }
            List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
            foreach (var athlete in athletes)
            {
                AthleteViewModel viewModel = athlete.ToAthleteViewModel();
                viewModel.TournamentId = tournament.Id.ToString();
                viewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                listAthletes.Add(viewModel);
            }
            response.Success = true;
            response.Message = "List Non Team Athletes ";
            if (tournament.FormatType == FormatType.MenSingles || tournament.FormatType == FormatType.MenDual)
            {
                response.Data = listAthletes.Where(a => a.Gender == "Male").ToList();
            }
            else if (tournament.FormatType == FormatType.WomenDual || tournament.FormatType == FormatType.WomenSingles)
            {
                response.Data = listAthletes.Where(a => a.Gender == "Female").ToList();
            }
            else
            {
                response.Data = listAthletes;
            }
            return response;
        }

        public async Task<ApiResponseModel<List<AthleteViewModel>>> GetAthletesByCampaignIdAsync(uint campaignId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Success = true,
                Data = null
            };
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(campaignId);
            if (athletes is null)
            {
                response.Message = "List is empty !";
                return response;
            }
            /*List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
            foreach(var athlete in athletes)
            {
                AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                athleteViewModel.TournamentCampaignId = campaignId.ToString();
                listAthletes.Add(athleteViewModel);
            }*/
            response.Data = athletes.Select(a => a.ToAthleteViewModel()).ToList();
            return response;
        }

        public async Task<ApiResponseModel<List<AthleteViewModel>>> GetAthleteByTournamentIdAsync(uint tounamentId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Success = true,
                Data = null
            };
            var athletes = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tounamentId);
            if (athletes is null)
            {
                response.Message = "List athletes is empty !";
                return response;
            }
            /*List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
            foreach (var athlete in athletes)
            {
                AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                listAthletes.Add(athleteViewModel);
            }*/
            response.Data = athletes.Select(a => a.ToAthleteViewModel()).ToList();
            return response;
        }

        public async Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetAthletesInTourmamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<AthleteViewModel>>
            {
                Data = null,
                Success = true
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var athletes = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tournamentId);
            if (athletes is null)
            {
                response.Message = "Tournament does not have any athlete !";
                return response;
            }
            List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
            foreach (var athlete in athletes)
            {
                AthleteViewModel viewModel = athlete.ToAthleteViewModel();
                viewModel.TournamentId = tournament.Id.ToString();
                viewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                listAthletes.Add(viewModel);
            }
            response.Message = "List of athletes in Tournament !";
            response.Data = listAthletes.ToPagination(pageIndex, pageSize);
            return response;
        }

        public async Task<ApiResponseModel<List<AthleteViewModel>>> AddUserToCampaign(uint campaignId, List<UserIdViewModel> users)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Data = null,
                Success = false
            };

            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Message = "Tournament Campaign does not exist!";
                return response;
            }

            // Get the users who are not participating in the campaign
            var usersNotInCampaign = await _unitOfWork.UserRepository.GetUserNotParticipateInCampaignAsync(campaignId);

            // Get the list of user IDs to be added
            var userIdsToAdd = users.Select(u => u.UserId).ToList();

            // Filter out users who are already in the campaign
            var eligibleUsersToAdd = usersNotInCampaign
                .Where(u => userIdsToAdd.Contains(u.Id))
                .ToList();

            if (!eligibleUsersToAdd.Any())
            {
                response.Message = "No eligible users to add to the campaign.";
                return response;
            }
            //var usersAdded = await _unitOfWork.UserRepository.GetUsersByIdAsync(eligibleUsersToAdd.Select(u => u.Id).ToList());
            List<Athlete> athletes = new List<Athlete>();
            foreach (var user in eligibleUsersToAdd)
            {
                var athlete = user.ToAthlete();
                athlete.TournamentCampaignId = campaignId;
                athlete.TournamentCampaign = campaign;
                athletes.Add(athlete);
            }
            try
            {
                await _unitOfWork.AthleteRepository.AddRangeAsync(athletes);
                await _unitOfWork.SaveChangeAsync();
                response.Data = athletes.Select(a => a.ToAthleteViewModel()).ToList();
                response.Message = "List User Added to Tournament Campaign !";
                response.Success = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<List<AthleteViewModel>>> AddAthletesToTournament(uint tournamentId, List<AthleteIdViewModel> athleteIds)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Success = false,
                Data = null
            };

            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            ;
            int maxAthletes = tournament.FormatType switch
            {
                FormatType.MenSingles or FormatType.WomenSingles => tournament.NumberOfTeams,
                FormatType.MenDual or FormatType.WomenDual or FormatType.DualMixed => tournament.NumberOfTeams * 2,
                _ => 0
            };

            var athletes = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tournamentId);
            // Check if the tournament is full
            if (athletes.Count >= maxAthletes)
            {
                response.Message = "The number of athletes in this tournament is full!";
                return response;
            }
            var athletesNotInTournament = await _unitOfWork.TournamentAthleteRepository.GetAthletesNotInTournamentAsync(tournamentId);
            var athleteIdsToAdd = athleteIds.Select(a => a.AthleteId).ToList();
            var eligibleAthletesToAdd = athletesNotInTournament
                .Where(u => athleteIdsToAdd.Contains(u.Id)
                && u.User.Gender.CheckValidateGender(tournament.FormatType))
                .ToList();

            if (!eligibleAthletesToAdd.Any())
            {
                response.Message = "No eligible athletes to add to the Tournament !";
                return response;
            }
            List<TournamentAthlete> tournamentAthletes = new List<TournamentAthlete>();
            foreach (var athlete in eligibleAthletesToAdd)
            {
                TournamentAthlete tournamentAthlete = new TournamentAthlete()
                {
                    AthleteId = athlete.Id,
                    Athlete = athlete,
                    TournamentId = tournament.Id,
                    Tournament = tournament,
                };
                tournamentAthletes.Add(tournamentAthlete);
            }
            try
            {
                await _unitOfWork.TournamentAthleteRepository.AddRangeAsync(tournamentAthletes);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "List of Athletes participate in Tournament !";
                List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
                foreach (var athlete in eligibleAthletesToAdd)
                {
                    AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                    athleteViewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                    athleteViewModel.TournamentId = tournament.Id.ToString();
                    listAthletes.Add(athleteViewModel);
                }
                response.Data = listAthletes;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetAthletesNotInTournamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<AthleteViewModel>>
            {
                Data = null,
                Success = false,
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var athletes = await _unitOfWork.TournamentAthleteRepository.GetAthletesNotInTournamentAsync(tournamentId);
            if (athletes is null)
            {
                response.Success = true;
                response.Message = "All athletes have participated in Tournament !";
                return response;
            }            
            List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
            foreach (var athlete in athletes)
            {
                AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                athleteViewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                athleteViewModel.TournamentId = tournament.Id.ToString();
                listAthletes.Add(athleteViewModel);
            }
            response.Success = true;
            response.Message = "List of Athletes not participate in Tournament !";
            if (tournament.FormatType == FormatType.MenSingles || tournament.FormatType == FormatType.MenDual)
            {
                response.Data = listAthletes.Where(a => a.Gender == "Male").ToList().ToPagination(pageIndex, pageSize);
            }
            else if (tournament.FormatType == FormatType.WomenDual || tournament.FormatType == FormatType.WomenSingles)
            {
                response.Data = listAthletes.Where(a => a.Gender == "Female").ToList().ToPagination(pageIndex, pageSize);
            }
            else
            {
                response.Data = listAthletes.ToPagination(pageIndex, pageSize);
            }
            return response;
        }


        public async Task<ApiResponseModel<List<AthleteViewModel>>> GetAthletesNotInTournament(uint tournamentId)
        {
            var response = new ApiResponseModel<List<AthleteViewModel>>
            {
                Data = null,
                Success = false,
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var athletes = await _unitOfWork.TournamentAthleteRepository.GetAthletesNotInTournamentAsync(tournamentId);
            if (athletes is null)
            {
                response.Success = true;
                response.Message = "All athletes have participated in Tournament !";
                return response;
            }
            List<AthleteViewModel> listAthletes = new List<AthleteViewModel>();
            foreach (var athlete in athletes)
            {
                AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                athleteViewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                athleteViewModel.TournamentId = tournament.Id.ToString();
                listAthletes.Add(athleteViewModel);
            }
            response.Success = true;
            response.Message = "List of Athletes not participate in Tournament !";
            if (tournament.FormatType == FormatType.MenSingles || tournament.FormatType == FormatType.MenDual)
            {
                response.Data = listAthletes.Where(a => a.Gender == "Male").ToList();
            }
            else if (tournament.FormatType == FormatType.WomenDual || tournament.FormatType == FormatType.WomenSingles)
            {
                response.Data = listAthletes.Where(a => a.Gender == "Female").ToList();
            }
            else
            {
                response.Data = listAthletes;
            }
            return response;
        }
    }
}

