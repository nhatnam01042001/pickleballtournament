﻿using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Application.ViewModels.TeamRoundGroupViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class TeamRoundGroupService : ITeamRoundGroupService
    {
        private readonly IUnitOfWork _unitOfWork;
        public TeamRoundGroupService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<List<TeamRoundGroupViewModel>>> GetTeamRoundGroupByGroupId(uint roundGroupId)
        {
            var response = new ApiResponseModel<List<TeamRoundGroupViewModel>>
            {
                Success = true,
                Data = null
            };
            var listTeams = await _unitOfWork.TeamRoundGroupRepository.GetTeamRoundGroupsByRoundGroupIdAsync(roundGroupId);
            if (listTeams == null)
            {
                response.Message = "Group does not contain any team !";
                return response;
            }
            List<TeamRoundGroupViewModel> listTeamsViewModel = new List<TeamRoundGroupViewModel>(); ;
            
            foreach(var teamRoundGroup in listTeams)
            {
                TeamRoundGroupViewModel teamViewModel = new TeamRoundGroupViewModel()
                {
                    TeamId = teamRoundGroup.TeamId,
                    TeamName = teamRoundGroup.Team.TeamName,
                    Wins = teamRoundGroup.TeamRoundGroupStatistics.Wins,
                    Losses = teamRoundGroup.TeamRoundGroupStatistics.Losses,
                    Points = teamRoundGroup.TeamRoundGroupStatistics.Points,
                    TeamStatus = teamRoundGroup.TeamStatus.ToString(),
                    TeamRoundGroupId = teamRoundGroup.Id,
                    //oundGroupId = teamRoundGroup.RoundGroupId
                };
                listTeamsViewModel.Add(teamViewModel);
            }
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetByIdAsync(roundGroupId);
            response.Message = $"List Teams in Group {roundGroup.RoundGroupName} !";
            response.Data = listTeamsViewModel;
            return response;
        }

        public async Task<ApiResponseModel<List<int>>> GetNumberOfAdvanceTeamsInRound(uint roundId)
        {
            var response = new ApiResponseModel<List<int>>()
            {
                Success = false,
                Data = null
            };
            var numberOfTeams = await _unitOfWork.TeamRoundGroupRepository.GetNumberOfTeamInRound(roundId);
            if(numberOfTeams == 0)
            {
                response.Message = "Round does not have any team !";
                return response;
            }
            if(numberOfTeams == 1)
            {
                response.Message = "Round only has 1 team !";
                return response;
            }
            var numberOfAdvancedTeam = numberOfTeams.GetPowersOfTwo();
            response.Data = numberOfAdvancedTeam;
            response.Success = true;
            return response;
        }

        public async Task<ApiResponseModel<Dictionary<uint, List<TeamRoundGroupViewModel>>>> GetRankedGroupTeam(uint roundId)
        {
            var response = new ApiResponseModel<Dictionary<uint, List<TeamRoundGroupViewModel>>>
            {
                Success = true,
                Data = null
            };
            var listRankedTeams = await _unitOfWork.TeamRoundGroupRepository.GetListRankedTeamStatistic(roundId);
            if(listRankedTeams is null)
            {
                response.Message = "List of team in round is null !";
                return response;
            }
            response.Data = listRankedTeams;
            response.Message = "List Ranked Team !";
            return response;
        }
    }
}
