﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.TournamentCampaignViewModels;
using Application.ViewModels.TournamentsViewModels;
using Application.ViewModels.TournamentViewModels;
using AutoMapper;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class TournamentCampaignService : ITournamentCampaignService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimsService _claimService;

        public TournamentCampaignService(IUnitOfWork unitOfWork, IMapper mapper, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _claimService = claimsService;
        }
        public async Task<ApiResponseModel<TournamentCampaignViewModel>> CreateTournamentCampaignAsync(CreateTournamentCampaignDto creatCampaignDto)
        {
            var response = new ApiResponseModel<TournamentCampaignViewModel>()
            {
                Data = null,
                Success = false,
            };
            var campaign = creatCampaignDto.ToTournamentCampaign();
            if (campaign.StartDate < DateTime.Now)
            {
                response.Message = "Start Date must greater or equal than Present Time !";
                return response;
            }
            if (campaign.EndDate < DateTime.Now)
            {
                response.Message = "End Date must greater than or equal Present Time !";
                return response;
            }
            if (campaign.RegisterExpiredDate < DateTime.Now)
            {
                response.Message = "Register Expired Date must greater than or equal Present Time !";
                return response;
            }
            if (campaign.StartDate > campaign.EndDate)
            {
                response.Message = "Start Date must smaller than or equal End Date !";
                return response;
            }
            if (campaign.RegisterExpiredDate > campaign.EndDate)
            {
                response.Message = "Register Expired Date must smaller than or equal End Date!";
                return response;
            }
            if (campaign.RegisterExpiredDate > campaign.StartDate)
            {
                response.Message = "Register Expired Date must smaller than or equal Start Date!";
                return response;
            }

            var courtGroupIds = creatCampaignDto.CourtGroups.Select(c => c.CourtGroupId).ToList();
            var courtGroups = await _unitOfWork.CourtGroupRepository.GetCourtGroupsByIdAsync(courtGroupIds);
            if (creatCampaignDto.CourtGroups != null)
            {
                using (var transaction = await _unitOfWork.BeginTransactionAsync())
                {
                    try
                    {
                        await _unitOfWork.TournamentCampaignRepository.AddAsync(campaign);
                        await _unitOfWork.SaveChangeAsync();
                        List<CourtGroupTournamentCampaign> courtGroupsCampaign = new List<CourtGroupTournamentCampaign>();
                        foreach (var courtGroup in courtGroups)
                        {
                            CourtGroupTournamentCampaign courtGroupCampaign = new CourtGroupTournamentCampaign
                            {
                                CourtGroupId = courtGroup.Id,
                                CourtGroup = courtGroup,
                                TournamentCampaignId = campaign.Id,
                                TournamentCampaign = campaign,
                            };
                            courtGroupsCampaign.Add(courtGroupCampaign);
                        }
                        await _unitOfWork.CourtGroupCampaignRepository.AddRangeAsync(courtGroupsCampaign);
                        await _unitOfWork.SaveChangeAsync();
                        await _unitOfWork.CommitAsync();
                    }
                    catch (Exception ex)
                    {
                        await _unitOfWork.RollbackAsync();
                        response.Message = ex.Message;
                        return response;
                    }
                }
            }
            else
            {
                try
                {
                    await _unitOfWork.TournamentCampaignRepository.AddAsync(campaign);
                    await _unitOfWork.SaveChangeAsync();
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                    return response;
                }
            }
            TournamentCampaignViewModel campaignViewModel = campaign.ToTournamentViewModel();
            response.Success = true;
            response.Message = "Create Tournament Campaign Success !";
            response.Data = campaignViewModel;
            return response;
        }

        public async Task<Pagination<TournamentCampaignViewModel>> GetTournamentCampaignPagingAsync(int pageIndex = 0, int pageSize = 10)
        {
            var tournaments = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignsAsync();
            var result = tournaments.ToPagination(pageIndex, pageSize);
            return result;
        }

        public async Task<List<TournamentCampaignViewModel>> GetTournamentCampaignsAsync()
        {
            var tournaments = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignsAsync();
            List<TournamentCampaignViewModel> tournamentViewModels = new List<TournamentCampaignViewModel>();
            foreach (var tournament in tournaments)
            {
                TournamentCampaignViewModel tournamentViewModel = tournament.ToTournamentViewModel();
                tournamentViewModels.Add(tournamentViewModel);
            }
            return tournamentViewModels;
        }

        public async Task<TournamentCampaignViewModel>? GetTournamentCampaignByIdAsync(uint id)
        {
            var tournament = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(id);
            TournamentCampaignViewModel tournamentViewModel = tournament.ToTournamentViewModel();
            return tournamentViewModel;
        }

        public async Task<ApiResponseModel<TournamentCampaignViewModel>> UpdateCampaignStatus(uint campaignId, UpdateCampaignStatusDto campaignStatusDto)
        {
            var response = new ApiResponseModel<TournamentCampaignViewModel>
            {
                Success = false,
                Data = null
            };

            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Message = "Tournament Campaign does not existed !";
                return response;
            }
            if (campaignStatusDto.CampaignStatus is null)
            {
                response.Message = "Nothing change !";
                return response;
            }
            var campaignStatus = campaignStatusDto.CampaignStatus.ToCampaignStatus();
            var isTournamentCompleted = await _unitOfWork.TournamentCampaignRepository.CheckAllTournamentCompleted(campaign.Id);
            switch (campaign.CampaignStatus)
            {
                case Domain.Enums.MatchStatus.Scheduled:
                    if (campaignStatus == Domain.Enums.MatchStatus.InProgress && campaign.StartDate < DateTime.Now)
                    {
                        response.Message = $"Can not set start Tournament Campaign before {campaign.StartDate.Date} !";
                        return response;
                    }
                    if (campaignStatus == Domain.Enums.MatchStatus.Completed && !isTournamentCompleted)
                    {
                        response.Message = $"All tournament in Campaign has not finished yet !";
                        return response;
                    }
                    campaign.CampaignStatus = campaignStatus;
                    break;
                case Domain.Enums.MatchStatus.Scheduling:
                    if (campaignStatus == Domain.Enums.MatchStatus.InProgress && campaign.StartDate < DateTime.Now)
                    {
                        response.Message = $"Can not set start Tournament Campaign before {campaign.StartDate.Date} !";
                        return response;
                    }
                    if (campaignStatus == Domain.Enums.MatchStatus.Completed && !isTournamentCompleted)
                    {
                        response.Message = $"All tournament in Campaign has not finished yet !";
                        return response;
                    }
                    campaign.CampaignStatus = campaignStatus;
                    break;
                case Domain.Enums.MatchStatus.InProgress:
                    if (campaignStatus == Domain.Enums.MatchStatus.Scheduled)
                    {
                        response.Message = "The Tournament Campaign is in progress ! Can not re-scheduled Tournament Campaign !"; ;
                        return response;
                    }
                    if (campaignStatus == Domain.Enums.MatchStatus.Completed && !isTournamentCompleted)
                    {
                        response.Message = $"All tournament in Campaign has not finished yet !";
                        return response;
                    }
                    campaign.CampaignStatus = campaignStatus;
                    break;
                case Domain.Enums.MatchStatus.Completed:
                    response.Message = "The Tournament Campaign has alreaady completed ! Can not update Tournament Campaign status !";
                    return response;
                case Domain.Enums.MatchStatus.Postponed:
                    if (campaignStatus == Domain.Enums.MatchStatus.InProgress && campaign.EndDate < DateTime.Now)
                    {
                        response.Message = $"Can not continue Tournament Campain after End date {campaign.EndDate.Date}! Please update End Date !";
                        return response;
                    }
                    if (campaignStatus == Domain.Enums.MatchStatus.Completed && !isTournamentCompleted)
                    {
                        response.Message = $"All tournament in Campaign has not finished yet !";
                        return response;
                    }
                    campaign.CampaignStatus = campaignStatus;
                    break;
                case Domain.Enums.MatchStatus.Canceled:
                    response.Message = "The Tournament Campaign has alreaady been canceled ! Can not update Tournament Campaign status !";
                    return response;
            }
            try
            {
                _unitOfWork.TournamentCampaignRepository.Update(campaign);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Data = campaign.ToTournamentViewModel();
                response.Message = "Update Tournament Campaign status success !";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
                return response;
            }
            return response;
        }


        public async Task<ApiResponseModel<TournamentCampaignViewModel>> UpdateCampaignDate(uint campaignId, UpdateCampaignDateDto campaignDateDto)
        {
            var response = new ApiResponseModel<TournamentCampaignViewModel>
            {
                Success = false,
                Data = null
            };

            // Retrieve campaign
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetTournamentCampaignByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Message = "Tournament Campaign does not exist!";
                return response;
            }

            // If all fields are null, return nothing to change
            if (campaignDateDto.RegistrationExpiredDate == null &&
                campaignDateDto.StartDate == null &&
                campaignDateDto.EndDate == null)
            {
                response.Message = "Nothing to change!";
                return response;
            }

            // Validation: RegistrationExpiredDate must be smaller than StartDate
            DateTime effectiveRegistrationExpiredDate = (DateTime)(campaignDateDto.RegistrationExpiredDate ?? campaign.RegisterExpiredDate);
            DateTime effectiveStartDate = campaignDateDto.StartDate ?? campaign.StartDate;

            if (effectiveRegistrationExpiredDate >= effectiveStartDate)
            {
                response.Message = "Registration expiration date must be earlier than the start date.";
                return response;
            }

            // Validation: StartDate must be smaller than EndDate
            DateTime effectiveEndDate = campaignDateDto.EndDate ?? campaign.EndDate;

            if (effectiveStartDate >= effectiveEndDate)
            {
                response.Message = "Start date must be earlier than the end date.";
                return response;
            }

            // Update only the fields that are not null
            switch (campaign.CampaignStatus)
            {
                case Domain.Enums.MatchStatus.InProgress:
                    response.Message = "Tournament Campaign is in progress ! Can not update Tournament Campaign status !";
                    return response;
                case Domain.Enums.MatchStatus.Completed:
                    response.Message = "Tournament Campaign has already completed ! Can not update Tournament Campaign status !";
                    return response;
                case Domain.Enums.MatchStatus.Canceled:
                    response.Message = "Tournament Campaign has been canceled ! Can not update Tournament Campaign status !";
                    return response;
                default:
                    if (campaignDateDto.RegistrationExpiredDate.HasValue)
                    {
                        campaign.RegisterExpiredDate = campaignDateDto.RegistrationExpiredDate.Value;
                    }

                    if (campaignDateDto.StartDate.HasValue)
                    {
                        campaign.StartDate = campaignDateDto.StartDate.Value;
                    }

                    if (campaignDateDto.EndDate.HasValue)
                    {
                        campaign.EndDate = campaignDateDto.EndDate.Value;
                    }

                    // Update campaign in the repository
                    _unitOfWork.TournamentCampaignRepository.Update(campaign);
                    await _unitOfWork.SaveChangeAsync();

                    // Prepare success response
                    response.Success = true;
                    response.Data = campaign.ToTournamentViewModel();
                    response.Message = "Campaign dates updated successfully!";
                    break;
            }
            return response;
        }
    }
}

