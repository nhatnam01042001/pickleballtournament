﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.CommentViewModel;
using Application.ViewModels.CourtGroupViewModels;
using AutoMapper;
using Domain.Entities;
using WebAPI.ApiResponseModels;

namespace Application.Services;

public class CommentService : ICommentService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IClaimsService _claimsService;


    public CommentService(IUnitOfWork unitOfWork, IMapper mapper, IClaimsService claimsService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _claimsService = claimsService;
    }
    public async Task<ApiResponseModel<List<CommentViewModel>>> GetComments()
    {
        var response = new ApiResponseModel<List<CommentViewModel>>()
        {
            Data = null,
            Success = false
        };
        var comments = await _unitOfWork.CommentRepository.GetAllAsync();
        if (comments is null)
        {
            response.Message = "List of Comment is empty";
            return response;
        }

        response.Success = true;
        response.Message = "List of Comment !";
        response.Data = _mapper.Map<List<CommentViewModel>>(comments);
        return response;
    }

    public async Task<ApiResponseModel<CommentViewModel>> GetCommentById(uint id)
    {
        var response = new ApiResponseModel<CommentViewModel>()
        {
            Data = null,
            Success = false
        };

        var comment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
        if (comment is null)
        {
            response.Message = "Comment is not found";
            return response;
        }

        response.Success = true;
        response.Message = "Comment Data !";
        response.Data = _mapper.Map<CommentViewModel>(comment);
        return response;
    }

    public async Task<ApiResponseModel<List<CommentViewModel>>> GetCommentByTournamentId(uint tournamentId)
    {
        var response = new ApiResponseModel<List<CommentViewModel>>()
        {
            Data = null,
            Success = false
        };

        var tournamentExists = await _unitOfWork.CommentRepository.TournamentExistsAsync(tournamentId);
        if (!tournamentExists)
        {
            response.Message = "Tournament Campaign doesn't exist!";
            return response;
        }

        var comments = await _unitOfWork.CommentRepository.GetByTournamentIdAsync(tournamentId);
        if (comments == null || !comments.Any())
        {
            response.Message = "Comments are not found for the given tournament";
            return response;
        }

        var commentViewModels = comments.Select(comment => new CommentViewModel
        {
            Id = comment.Id,
            UserId = comment.Account?.UserId ?? 0,
            FullName = comment.Account?.User?.FullName,
            ImageUrl = comment.Account?.User?.ImageUrl,
            CommentText = comment.CommentText,
            TournamentId = comment.TournamentId,
            CreatedBy = comment.CreatedBy,
            CreateDate = comment.CreateDate,
            IsDeleted = comment.IsDeleted,
            DeleteBy = comment.DeleteBy
        }).ToList();

        response.Success = true;
        response.Message = "Comments Data!";
        response.Data = commentViewModels;
        return response;
    }

    public async Task<ApiResponseModel<CommentViewModel>> AddComment(AddCommentViewModel model)
    {
        var response = new ApiResponseModel<CommentViewModel>()
        {
            Data = null,
            Success = false
        };

        var tournamentExists = await _unitOfWork.CommentRepository.TournamentExistsAsync(model.TournamentId);
        if (!tournamentExists)
        {
            response.Message = "Tournament Campaign doesn't exist!";
            return response;
        }

        var userId = _claimsService.GetCurrentUserId;
        var accountExists = await _unitOfWork.AccountRepository.ExistsAsync(userId);
        if (!accountExists)
        {
            response.Message = "Account doesn't exist!";
            return response;
        }

        if (model.AccountId != userId)
        {
            response.Message = "You are not authorized to add comment for this account!";
            return response;
        }

        var comment = _mapper.Map<Comment>(model);
        comment.AccountId = userId;
        comment.CreateDate = DateTime.Now;

        await _unitOfWork.CommentRepository.AddAsync(comment);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "Comment Created!";
        response.Data = _mapper.Map<CommentViewModel>(comment);
        return response;
    }

    public async Task<ApiResponseModel<CommentViewModel>> UpdateComment(uint id, UpdateCommentViewModel model)
    {
        var response = new ApiResponseModel<CommentViewModel>()
        {
            Data = null,
            Success = false
        };

        var existingComment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
        if (existingComment is null)
        {
            response.Message = "Comment is not found";
            return response;
        }

        var currentUserId = _claimsService.GetCurrentUserId;

        if (existingComment.CreatedBy != currentUserId)
        {
            response.Message = "You are not authorized to update this comment!";
            return response;
        }

        var updateComment = _mapper.Map(model, existingComment);
        _unitOfWork.CommentRepository.Update(updateComment);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "Comment Updated!";
        response.Data = _mapper.Map<CommentViewModel>(updateComment);
        return response;
    }

    public async Task<ApiResponseModel<bool>> SoftDeleteComment(uint id)
    {
        var response = new ApiResponseModel<bool>()
        {
            Data = false,
            Success = false
        };

        var existingComment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
        if (existingComment is null)
        {
            response.Message = "Comment is not found";
            return response;
        }

        var currentUserId = _claimsService.GetCurrentUserId;

        existingComment.IsDeleted = true;
        existingComment.DeleteBy = currentUserId;
        _unitOfWork.CommentRepository.Update(existingComment);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "Comment Soft Deleted!";
        response.Data = true;
        return response;
    }


    public async Task<ApiResponseModel<bool>> DeleteComment(uint id)
    {
        var response = new ApiResponseModel<bool>()
        {
            Data = false,
            Success = false
        };

        var existingComment = await _unitOfWork.CommentRepository.GetByIdAsync(id);
        if (existingComment is null)
        {
            response.Message = "Comment is not found";
            return response;
        }


        _unitOfWork.CommentRepository.Remove(existingComment);
        await _unitOfWork.SaveChangeAsync();

        response.Success = true;
        response.Message = "Comment Deleted!";
        response.Data = true;
        return response;
    }
    public async Task<ApiResponseModel<Pagination<CommentViewModel>>> GetCommentByTournamentIdPaging(uint tournamentId, int pageIndex = 0, int pageSize = 10)
    {
        var response = new ApiResponseModel<Pagination<CommentViewModel>>
        {
            Data = null,
            Success = true
        };

        var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
        if (tournament is null)
        {
            response.Success = false;
            response.Message = "Tournament doesn't exist!";
            return response;
        }

        var comments = await _unitOfWork.CommentRepository.GetByTournamentIdAsync(tournamentId);
        if (comments == null || !comments.Any())
        {
            response.Message = "There is no comment !";
            return response;
        }
        response.Message = $"List comment in Tournament{tournament.TournamentName} !";
        var listComment = comments.ToList();
        response.Data = listComment.ToPagination(pageIndex, pageSize);
        return response;
    }
}
    