﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AccountViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;
using Application.ViewModels.EmailViewModels;
using Microsoft.EntityFrameworkCore;
using Application.Repositories;

namespace Application.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;
        private readonly AppConfiguration _configuration;
        private readonly IClaimsService _claimsService;
        public AccountService(IUnitOfWork unitOfWork, ICurrentTime currentTime, AppConfiguration configuration, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsService = claimsService;
        }

        public async Task<Account> CreateManagerAccount()
        {
            var manager = await _unitOfWork.AccountRepository.GetManagerAccountAsync();
            if (manager is not null)
            {
                return null;
            }
            var managerAccount = _configuration.ManagerAccount;
            Account account = managerAccount.ToAccount();
            string inputPassword = account.PasswordHash + account.Salt;
            account.PasswordHash = inputPassword.Hash();
            User user = new User
            {
                FullName = managerAccount.AccountName,
                Email = managerAccount.AccountEmail,
                Role = Role.Manager
            };
            await _unitOfWork.UserRepository.AddAsync(user);
            account.UserId = user.Id;
            account.User = user;
            await _unitOfWork.AccountRepository.AddAsync(account);
            bool createAccountSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (!createAccountSuccess)
            {
                throw new Exception("Create Account Failed !");
            }
            return account;
        }
        public async Task<ApiResponseModel<string?>> LoginAsync(UserLoginDTO userObject)
        {
            var response = new ApiResponseModel<string?>()
            {
                Data = null,
                Success = false
            };
            var account = await _unitOfWork.AccountRepository.GetAccountByUserNameAsync(userObject.UserName);
            if(account is null)
            {
                response.Message = "Username does not correct ! Please try again";
                return response;
            }
            string password = userObject.Password + account.Salt;
            if (account.PasswordHash != password.Hash())
            {
                response.Message = "Password is incorrect ! Please try again !";
                return response;
            }            
            var user = await _unitOfWork.UserRepository.GetUserWithAccountAsync(account.UserId);
            response.Data = user.GenerateJsonWebToken(_configuration.JWTSection, _currentTime.GetCurrentTime());
            response.Success = true;
            response.Message = "Login success !";
            return response;
        }

        public async Task<ApiResponseModel<string?>> LoginManagerAccountAsync(UserLoginDTO accountObject)
        {
            var response = new ApiResponseModel<string?>()
            {
                Data = null,
                Success = false
            };
            var adminAccount = _configuration.ManagerAccount;
            if (accountObject.UserName == adminAccount.UserName && accountObject.Password == adminAccount.AccountPassword)
            {
                response.Data = adminAccount.GenerateJsonWebToken(_configuration.JWTSection, _currentTime.GetCurrentTime());
                response.Success = true;
                response.Message = "Login success !";
                return response;
            }
            response.Message = "Password is incorrect ! Please try again !";
            return response;
        }

        public async Task<ApiResponseModel<EmailResponseViewModel>> CreateAccountAsync(uint userId, CreateAccountDTO accountObject)
        {
            var response = new ApiResponseModel<EmailResponseViewModel>
            {
                Success = false,
                Data = null
            };
            var user = await _unitOfWork.UserRepository.GetUserWithAccountAsync(userId);
            if (user is null || user.IsDeleted)
            {
                response.Message = "User does not existed !";
                return response;
            }
            if(user.Account is not null)
            {
                response.Message = "User has already had account !";
                return response;
            }
            var checkExistedUserName = await _unitOfWork.AccountRepository.CheckUserNameExited(accountObject.UserName);
            if (checkExistedUserName)
            {
                response.Message = "UserName is existed ! Please try again !";
                return response;
            }
            Account account = accountObject.ToAccount();
            account.Salt = StringUtils.GenerateSalt();
            string inputPassword = accountObject.Password + account.Salt;
            account.PasswordHash = inputPassword.Hash();
            account.UserId = user.Id;
            account.User = user;
            try
            {
                await _unitOfWork.AccountRepository.AddAsync(account);
                await _unitOfWork.SaveChangeAsync();

                var managerEmail = _configuration.ManagerEmailForSend;
                var smtpClient = new SmtpClient
                {
                    Port = 587,
                    EnableSsl = true,
                    Host = "smtp.gmail.com",
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(managerEmail.Email, managerEmail.Password),
                };
                var message = new MailMessage
                {
                    Subject = "Account Information!",
                    Body = $"Username: {account.UserName}\n" +
           $"Password: {accountObject.Password}",
                    From = new MailAddress(managerEmail.Email),
                };
                message.To.Add(new MailAddress(account.User.Email));
                await smtpClient.SendMailAsync(message);
                response.Success = true;
                response.Message = "Email sent successfully!";
                response.Data = new EmailResponseViewModel
                {
                    SenderEmail = managerEmail.Email,
                    ReceiverEmail = account.User.Email,
                    Message = message.Body
                };
            }
            catch (SmtpException smtpEx)
            {
                response.Message = $"Failed to send email: {smtpEx.Message}";
            }
            catch (Exception ex)
            {
                response.Message = $"An error occurred: {ex.Message}";
            }
            return response;
        }


        public async Task<ApiResponseModel<string>> ChangePasswordAsync(uint userId, ChangePasswordDTO changePasswordDTO)
        {
            var response = new ApiResponseModel<string>();

            var account = await _unitOfWork.AccountRepository.GetByIdAsync(userId);
            if (account == null)
            {
                response.Success = false;
                response.Message = "Account not found.";
                return response;
            }

            string oldPasswordHash = (changePasswordDTO.OldPassword + account.Salt).Hash();
            if (oldPasswordHash != account.PasswordHash)
            {
                response.Success = false;
                response.Message = "Old password is incorrect.";
                return response;
            }

            string newSalt = StringUtils.GenerateSalt();
            string newPasswordHash = (changePasswordDTO.NewPassword + newSalt).Hash();

            account.PasswordHash = newPasswordHash;
            account.Salt = newSalt;
            await _unitOfWork.AccountRepository.UpdateAccountAsync(account);

            response.Success = true;
            response.Data = "Password changed successfully.";
            return response;
        }

    }
}
