﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.SetViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.FileSystemGlobbing;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class SetService : ISetService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SetService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<List<SetViewModel>>> GetSetsOfMatchAsync(uint matchId)
        {
            var response = new ApiResponseModel<List<SetViewModel>>
            {
                Success = true,
                Data = null
            };
            var sets = await _unitOfWork.SetRepository.GetSetsByMatchIdAsync(matchId);
            if(sets is null)
            {
                response.Message = "Match does not have any set !";
                return response;
            }
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            response.Data = sets.Select(s => new SetViewModel
            {
                SetId = s.Id,
                FirstTeamId = match.FirstTeamId.HasValue ? match.FirstTeamId.Value : (uint?)null,
                FirstTeamName = match.FirstTeam != null ? match.FirstTeam.TeamName : null,
                SecondTeamId = match.SecondTeamId.HasValue ? match.SecondTeamId.Value : (uint?)null,
                SecondTeamName = match.SecondTeam != null ? match.SecondTeam.TeamName : null,
                FirstTeamScore = s.FirstTeamScore,
                SecondTeamScore = s.SecondTeamScore,
                MatchId = match.Id,
                SetStatus = s.SetStatus.ToString(),
                WinningTeamId = match.WinningTeamId.HasValue ? match.WinningTeamId.Value : (uint?)null,
                WinningTeamName = match.WinningTeam != null ? match.WinningTeam.TeamName : null,
                SetOrder = s.SetOrder
            }).ToList();
            response.Message = "List of Sets in Match !";
            return response;
        }

        /*public async Task<ApiResponseModel<List<SetViewModel>>> CreateSetsForMatch(uint matchId, IEnumerable<UpdateSetDto> createSetDtos)
        {
            var response = new ApiResponseModel<List<SetViewModel>>()
            {
                Success = false,
                Data = null
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match == null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            if(match.MatchStatus == MatchStatus.Completed)
            {
                response.Message = "Match has already had reuslt !";
                return response;
            }
            foreach (var setDto in createSetDtos)
            {
                if (setDto.FirstTeamScore < 0 || setDto.FirstTeamScore > 11 ||
                    setDto.SecondTeamScore < 0 || setDto.SecondTeamScore > 11)
                {
                    response.Message = "Scores must be between 0 and 11!";
                    return response;
                }

                if (setDto.FirstTeamScore != 11 && setDto.SecondTeamScore != 11)
                {
                    response.Message = "One of the scores must be 11!";
                    return response;
                }
                if (setDto.FirstTeamScore == setDto.SecondTeamScore)
                {
                    response.Message = "Set does not receive draw result";
                    return response;
                }
            }
            var firstTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.FirstTeamId);
            var secondTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.SecondTeamId);
            List<Set> sets = new List<Set>();
            int currentOrder = match.Sets.Count + 1;
            int firstTeamSetWins = 0;
            int secondTeamSetWins = 0;

            foreach (var setDto in createSetDtos)
            {
                if (setDto.FirstTeamScore < 0 || setDto.FirstTeamScore > 11 ||
                    setDto.SecondTeamScore < 0 || setDto.SecondTeamScore > 11)
                {
                    response.Message = "Scores must be between 0 and 11!";
                    return response;
                }

                if (setDto.FirstTeamScore != 11 && setDto.SecondTeamScore != 11)
                {
                    response.Message = "One of the scores must be 11!";
                    return response;
                }

                if (setDto.FirstTeamScore == setDto.SecondTeamScore)
                {
                    response.Message = "A set cannot end in a draw!";
                    return response;
                }

                Set set = setDto.ToSet();
                set.SetOrder = currentOrder;

                bool isFirstTeamWinner = setDto.FirstTeamScore > setDto.SecondTeamScore;
                set.WinningTeamId = isFirstTeamWinner ? (uint)match.FirstTeamId : (uint)match.SecondTeamId;

                if (isFirstTeamWinner)
                {
                    firstTeamSetWins++;
                    firstTeamStats.Wins++;
                    firstTeamStats.SetDifference += 1;

                    secondTeamStats.Losses++;
                    secondTeamStats.SetDifference -= 1;
                }
                else
                {
                    secondTeamSetWins++;
                    secondTeamStats.Wins++;
                    secondTeamStats.SetDifference += 1;

                    firstTeamStats.Losses++;
                    firstTeamStats.SetDifference -= 1;
                }

                firstTeamStats.PointsDifference += (setDto.FirstTeamScore - setDto.SecondTeamScore);
                secondTeamStats.PointsDifference += (setDto.SecondTeamScore - setDto.FirstTeamScore);
                set.SetStatus = MatchStatus.Completed;
                sets.Add(set);
                currentOrder++;
            }
            match.WinningTeamId = firstTeamSetWins > secondTeamSetWins ? match.FirstTeamId : match.SecondTeamId;
            match.MatchStatus = MatchStatus.Completed;
            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    await _unitOfWork.SetRepository.AddRangeAsync(sets);
                    _unitOfWork.PickleballMatchRepository.Update(match);
                    _unitOfWork.GroupStatisticRepository.Update(firstTeamStats);
                    _unitOfWork.GroupStatisticRepository.Update(secondTeamStats);
                    await _unitOfWork.SaveChangeAsync();

                    var roundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(match.RoundGroup.RoundId);
                    if(roundMatches.All(m => m.MatchStatus == MatchStatus.Completed))
                    {
                        var round = match.RoundGroup.Round;
                        round.RoundStatus = MatchStatus.Completed;
                        _unitOfWork.RoundRepository.Update(round);
                        await _unitOfWork.SaveChangeAsync();
                    }

                    await transaction.CommitAsync();

                    response.Success = true;
                    response.Data = sets.Select(s => new SetViewModel
                    {
                        SetId = s.Id,
                        FirstTeamId = (uint)match.FirstTeamId,
                        FirstTeamName = match.FirstTeam.TeamName,
                        SecondTeamId = (uint)match.SecondTeamId,
                        SecondTeamName = match.SecondTeam.TeamName,
                        FirstTeamScore = s.FirstTeamScore,
                        SecondTeamScore = s.SecondTeamScore,
                        MatchId = match.Id,
                        SetStatus = s.SetStatus.ToString(),
                        WinningTeamId = (uint)match.WinningTeamId,
                        WinningTeamName = match.WinningTeam.TeamName

                    }).ToList();
                }
                catch (Exception ex) 
                {
                    await transaction.RollbackAsync();
                    response.Message = $"An error occurred: {ex.Message}";
                }
            }
            return response;
        }*/

        /*public async Task<ApiResponseModel<List<SetViewModel>>> CreateSetForEliminationMatch(uint matchId, IEnumerable<CreateSetDto> createSetDtos)
        {
            var response = new ApiResponseModel<List<SetViewModel>>()
            {
                Success = false,
                Data = null
            };
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(matchId);
            if (match == null)
            {
                response.Message = "Match does not existed !";
                return response;
            }
            if (match.MatchStatus == MatchStatus.Completed)
            {
                response.Message = "Match has already had reuslt !";
                return response;
            }
            foreach (var setDto in createSetDtos)
            {
                if (setDto.FirstTeamScore < 0 || setDto.FirstTeamScore > 11 ||
                    setDto.SecondTeamScore < 0 || setDto.SecondTeamScore > 11)
                {
                    response.Message = "Scores must be between 0 and 11!";
                    return response;
                }

                if (setDto.FirstTeamScore != 11 && setDto.SecondTeamScore != 11)
                {
                    response.Message = "One of the scores must be 11!";
                    return response;
                }
                if (setDto.FirstTeamScore == setDto.SecondTeamScore)
                {
                    response.Message = "Set does not receive draw result";
                    return response;
                }
            }
            var firstTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.FirstTeamId);
            var secondTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.SecondTeamId);
            List<Set> sets = new List<Set>();
            int currentOrder = match.Sets.Count + 1;
            int firstTeamSetWins = 0;
            int secondTeamSetWins = 0;

            foreach (var setDto in createSetDtos)
            {
                if (setDto.FirstTeamScore < 0 || setDto.FirstTeamScore > 11 ||
                    setDto.SecondTeamScore < 0 || setDto.SecondTeamScore > 11)
                {
                    response.Message = "Scores must be between 0 and 11!";
                    return response;
                }

                if (setDto.FirstTeamScore != 11 && setDto.SecondTeamScore != 11)
                {
                    response.Message = "One of the scores must be 11!";
                    return response;
                }

                if (setDto.FirstTeamScore == setDto.SecondTeamScore)
                {
                    response.Message = "A set cannot end in a draw!";
                    return response;
                }

                Set set = setDto.ToSet();
                set.SetOrder = currentOrder;

                bool isFirstTeamWinner = setDto.FirstTeamScore > setDto.SecondTeamScore;
                set.WinningTeamId = isFirstTeamWinner ? (uint)match.FirstTeamId : (uint)match.SecondTeamId;

                if (isFirstTeamWinner)
                {
                    firstTeamSetWins++;
                    firstTeamStats.Wins++;
                    firstTeamStats.SetDifference += 1;

                    secondTeamStats.Losses++;
                    secondTeamStats.SetDifference -= 1;
                }
                else
                {
                    secondTeamSetWins++;
                    secondTeamStats.Wins++;
                    secondTeamStats.SetDifference += 1;

                    firstTeamStats.Losses++;
                    firstTeamStats.SetDifference -= 1;
                }

                firstTeamStats.PointsDifference += (setDto.FirstTeamScore - setDto.SecondTeamScore);
                secondTeamStats.PointsDifference += (setDto.SecondTeamScore - setDto.FirstTeamScore);
                set.SetStatus = MatchStatus.Completed;
                sets.Add(set);
                currentOrder++;
            }
            match.WinningTeamId = firstTeamSetWins > secondTeamSetWins ? match.FirstTeamId : match.SecondTeamId;
            match.MatchStatus = MatchStatus.Completed;



            return response;
        }*/

        public async Task<ApiResponseModel<SetViewModel>> CreateSetForMatch(CreateSetDto createSetDto)
        {
            var response = new ApiResponseModel<SetViewModel>
            {
                Data = null,
                Success = false
            };

            // Fetch the match
            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(createSetDto.MatchId);
            if (match == null)
            {
                response.Message = "Match does not exist!";
                return response;
            }

            // Check if the match is already completed
            if (match.MatchStatus == MatchStatus.Completed)
            {
                response.Message = "Match has already had result!";
                return response;
            }

            // Validate the set scores
            if (createSetDto.FirstTeamScore < 0 || createSetDto.FirstTeamScore > 11 ||
                createSetDto.SecondTeamScore < 0 || createSetDto.SecondTeamScore > 11)
            {
                response.Message = "Scores must be between 0 and 11!";
                return response;
            }

            if (createSetDto.FirstTeamScore != 11 && createSetDto.SecondTeamScore != 11)
            {
                response.Message = "One of the scores must be 11!";
                return response;
            }

            if (createSetDto.FirstTeamScore == createSetDto.SecondTeamScore)
            {
                response.Message = "A set cannot end in a draw!";
                return response;
            }

            // Fetch the tournament and match details
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
            if(tournament.TournamentStatus != MatchStatus.InProgress)
            {
                response.Message = "Tournament does not In Progress yet ! Please update Tournament team and status !";
                return response;
            }
            var setsInMatch = await _unitOfWork.SetRepository.GetSetsByMatchIdAsync(match.Id);

            // Get statistics for both teams
            var firstTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.FirstTeamId);
            var secondTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.SecondTeamId);

            // Create the set
            Set set = createSetDto.ToSet();
            set.SetOrder = setsInMatch == null ? 1 : setsInMatch.Last().SetOrder + 1;

            // Determine the winning team for the set
            set.WinningTeamId = createSetDto.FirstTeamScore > createSetDto.SecondTeamScore
                ? (uint)match.FirstTeamId
                : (uint)match.SecondTeamId;

            // Calculate the total sets to win based on the tournament's set format
            int totalSets = tournament.NumberOfSets;
            int setsToWin = (totalSets / 2) + 1;

            // Save the set
            try
            {
                await _unitOfWork.SetRepository.AddAsync(set);
                await _unitOfWork.SaveChangeAsync();
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }

            // Update the statistics for the teams
            var firstTeamSetWins = setsInMatch.Count(s => s.WinningTeamId == match.FirstTeamId);
            var secondTeamSetWins = setsInMatch.Count(s => s.WinningTeamId == match.SecondTeamId);

            firstTeamSetWins += set.WinningTeamId == match.FirstTeamId ? 1 : 0;
            secondTeamSetWins += set.WinningTeamId == match.SecondTeamId ? 1 : 0;

            // Check if a team has already won the required number of sets
            if (firstTeamSetWins == setsToWin || secondTeamSetWins == setsToWin)
            {
                // Update the match status and winning team
                match.MatchStatus = MatchStatus.Completed;
                match.WinningTeamId = firstTeamSetWins > secondTeamSetWins ? match.FirstTeamId : match.SecondTeamId;
                match.WinningTeam = firstTeamSetWins > secondTeamSetWins ? match.FirstTeam : match.SecondTeam;

                // Update team statistics
                firstTeamStats.Wins += firstTeamSetWins > secondTeamSetWins ? 1 : 0;
                firstTeamStats.Losses += firstTeamSetWins < secondTeamSetWins ? 1 : 0;
                firstTeamStats.Points += firstTeamSetWins > secondTeamSetWins ? 1 : 0;
                firstTeamStats.SetDifference += firstTeamSetWins - secondTeamSetWins;
                firstTeamStats.PointsDifference += createSetDto.FirstTeamScore - createSetDto.SecondTeamScore;

                secondTeamStats.Wins += secondTeamSetWins > firstTeamSetWins ? 1 : 0;
                secondTeamStats.Losses += secondTeamSetWins < firstTeamSetWins ? 1 : 0;
                secondTeamStats.Points += secondTeamSetWins > firstTeamSetWins ? 1 : 0;
                secondTeamStats.SetDifference += secondTeamSetWins - firstTeamSetWins;
                secondTeamStats.PointsDifference += createSetDto.SecondTeamScore - createSetDto.FirstTeamScore;

                // Call method to update current match and team statistics
                try
                {
                    await UpdateCurrentMatchAndStatistics(match, firstTeamStats, secondTeamStats);
                }
                catch(Exception ex)
                {
                    response.Message = ex.Message;
                    return response;
                }

                // Check if there's a next round
                var currentRoundId = match.RoundGroup.RoundId;
                var nextRound = await _unitOfWork.RoundRepository.GetNextRoundByRoundIdAsync(currentRoundId);

                if (nextRound != null)
                {
                    var nextRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(nextRound.Id);
                    if (nextRoundMatches != null && nextRoundMatches.Any())
                    {
                        //var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(match.RoundGroup.Round.TournamentId);
                        await UpdateNextRoundMatch(match, nextRoundMatches,tournament);
                    }
                }

                response.Message = "Match completed and set added successfully!";
            }
            else
            {
                // Save statistics if the match is not yet completed
                try
                {
                    await UpdateCurrentMatchAndStatistics(match, firstTeamStats, secondTeamStats);
                    response.Message = "Set added successfully!";
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                    return response;
                }
            }

            // Return the response with the created set details
            response.Data = new SetViewModel
            {
                SetId = set.Id,
                FirstTeamId = (uint)match.FirstTeamId,
                FirstTeamName = match.FirstTeam.TeamName,
                SecondTeamId = (uint)match.SecondTeamId,
                SecondTeamName = match.SecondTeam.TeamName,
                FirstTeamScore = set.FirstTeamScore,
                SecondTeamScore = set.SecondTeamScore,
                MatchId = match.Id,
                SetStatus = set.SetStatus.ToString(),
                WinningTeamId = (uint)set.WinningTeamId,
                WinningTeamName = set.WinningTeam.TeamName,
                SetOrder = set.SetOrder
            };

            response.Success = true;
            return response;
        }

        public async Task<ApiResponseModel<SetViewModel>> UpdateSet(uint setId, UpdateSetDto setDto)
        {
            var response = new ApiResponseModel<SetViewModel>
            {
                Success = false,
                Data = null
            };

            var set = await _unitOfWork.SetRepository.GetSetByIdAsync(setId);
            if (set.SetStatus == MatchStatus.Completed)
            {
                response.Message = "Set has already had result! Cannot update set!";
                return response;
            }

            var match = await _unitOfWork.PickleballMatchRepository.GetMatchByIdAsync(set.PickleballMatchId);
            if (match.MatchStatus == MatchStatus.Completed)
            {
                response.Message = "Match has already had result! Cannot update set!";
                return response;
            }

            if (match.MatchStatus != MatchStatus.InProgress)
            {
                response.Message = "Match has not started yet! Please update match status!";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(match.RoundGroup.Round.TournamentId);
            var setsInMatch = await _unitOfWork.SetRepository.GetSetsByMatchIdAsync(set.PickleballMatchId);
            var previousSet = setsInMatch.FirstOrDefault(s => s.SetOrder == set.SetOrder - 1);

            if (previousSet != null && previousSet.SetStatus != MatchStatus.Completed)
            {
                response.Message = $"The set {previousSet.SetOrder} must be completed before updating the current set.";
                return response;
            }

            if (setDto.FirstTeamScore < 0 || setDto.FirstTeamScore > 11 || setDto.SecondTeamScore < 0 || setDto.SecondTeamScore > 11)
            {
                response.Message = "Scores must be between 0 and 11!";
                return response;
            }

            if (setDto.FirstTeamScore != 11 && setDto.SecondTeamScore != 11)
            {
                response.Message = "One of the scores must be 11!";
                return response;
            }

            if (setDto.FirstTeamScore == setDto.SecondTeamScore)
            {
                response.Message = "A set cannot end in a draw!";
                return response;
            }            
            set.ToSet(setDto);
            // Determine the winning team for the current set based on the score
            set.WinningTeamId = setDto.FirstTeamScore > setDto.SecondTeamScore
                ? (uint)match.FirstTeamId
                : (uint)match.SecondTeamId;

            set.WinningTeam = setDto.FirstTeamScore > setDto.SecondTeamScore
                ? match.FirstTeam
                : match.SecondTeam;

            int totalSets = setsInMatch.Count;
            int setsToWin = (totalSets / 2) + 1;

            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    _unitOfWork.SetRepository.Update(set);
                    await _unitOfWork.SaveChangeAsync();
                    response.Message = "Set updated successfully!";

                    var firstTeamSetWins = setsInMatch.Count(s => s.WinningTeamId == match.FirstTeamId);
                    var secondTeamSetWins = setsInMatch.Count(s => s.WinningTeamId == match.SecondTeamId);

                    // If a team has won the required number of sets, update match status and statistics
                    if (firstTeamSetWins == setsToWin || secondTeamSetWins == setsToWin)
                    {
                        match.WinningTeamId = firstTeamSetWins > secondTeamSetWins ? match.FirstTeamId : match.SecondTeamId;
                        match.MatchStatus = MatchStatus.Completed;

                        var firstTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.FirstTeamId);
                        var secondTeamStats = await _unitOfWork.GroupStatisticRepository.GetSpecificTeamStatisticsAsync((uint)match.RoundGroupId, (uint)match.SecondTeamId);

                        firstTeamStats.Wins += firstTeamSetWins > secondTeamSetWins ? 1 : 0;
                        firstTeamStats.Losses += firstTeamSetWins < secondTeamSetWins ? 1 : 0;
                        firstTeamStats.Points += firstTeamSetWins > secondTeamSetWins ? 1 : 0;
                        firstTeamStats.SetDifference += firstTeamSetWins - secondTeamSetWins;
                        firstTeamStats.PointsDifference += setsInMatch.Sum(s => s.FirstTeamScore) - setsInMatch.Sum(s => s.SecondTeamScore);

                        secondTeamStats.Wins += secondTeamSetWins > firstTeamSetWins ? 1 : 0;
                        secondTeamStats.Losses += secondTeamSetWins < firstTeamSetWins ? 1 : 0;
                        secondTeamStats.Points += secondTeamSetWins > firstTeamSetWins ? 1 : 0;
                        secondTeamStats.SetDifference += secondTeamSetWins - firstTeamSetWins;
                        secondTeamStats.PointsDifference += setsInMatch.Sum(s => s.SecondTeamScore) - setsInMatch.Sum(s => s.FirstTeamScore);

                        await UpdateCurrentMatchAndStatistics(match, firstTeamStats, secondTeamStats);
                    }
                    if (match.RoundGroup.Round.RoundName == "Final")
                    {
                        await UpdateTournamentRankingTeamsForFinal(match, match.RoundGroup.Round.RoundName);
                    }
                    var currentRound = await _unitOfWork.RoundRepository.GetByIdAsync(match.RoundGroup.RoundId);
                    var nextRound = await _unitOfWork.RoundRepository.GetNextRoundByRoundIdAsync(match.RoundGroup.RoundId);
                    if (nextRound is not null)
                    {


                        var nextRoundMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(nextRound.Id);
                        if (nextRoundMatches != null && nextRoundMatches.Count > 0 && match.MatchStatus == MatchStatus.Completed)
                        {
                            await UpdateNextRoundMatch(match, nextRoundMatches, tournament);
                            // Update the tournament ranking team for both teams
                            await UpdateTournamentRankingTeams((uint)match.FirstTeamId, match.RoundGroup.Round.TournamentId, currentRound.RoundName, firstTeamSetWins > secondTeamSetWins ? nextRound.RoundName : currentRound.RoundName);
                            await UpdateTournamentRankingTeams((uint)match.SecondTeamId, match.RoundGroup.Round.TournamentId, currentRound.RoundName, secondTeamSetWins > firstTeamSetWins ? nextRound.RoundName : currentRound.RoundName);
                        }

                    }
                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = ex.Message;
                    return response;
                }
            }

            // Check if the current round is completed
            var isRoundCompleted = await _unitOfWork.PickleballMatchRepository.CheckMatchesInRoundCompleted(match.RoundGroup.RoundId);
            if (isRoundCompleted)
            {
                var currentRound = await _unitOfWork.RoundRepository.GetByIdAsync(match.RoundGroup.RoundId);
                currentRound.RoundStatus = MatchStatus.Completed;
                _unitOfWork.RoundRepository.Update(currentRound);
            }

            // Check if all matches in the tournament are completed
            var totalMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByTournamentIdAsync(match.RoundGroup.Round.TournamentId);
            var completedMatches = totalMatches.Count(m => m.MatchStatus.ToMatchStatus() == MatchStatus.Completed || m.MatchStatus.ToMatchStatus() == MatchStatus.Canceled);

            if (completedMatches == totalMatches.Count)
            {
                
                tournament.TournamentStatus = MatchStatus.Completed;
                _unitOfWork.TournamentRepository.Update(tournament);
            }

            response.Data = new SetViewModel
            {
                SetId = set.Id,
                FirstTeamId = (uint)match.FirstTeamId,
                FirstTeamName = match.FirstTeam.TeamName,
                SecondTeamId = (uint)match.SecondTeamId,
                SecondTeamName = match.SecondTeam.TeamName,
                FirstTeamScore = set.FirstTeamScore,
                SecondTeamScore = set.SecondTeamScore,
                MatchId = match.Id,
                SetStatus = set.SetStatus.ToString(),
                WinningTeamId = (uint)set.WinningTeamId,
                WinningTeamName = set.WinningTeam.TeamName,
                SetOrder = set.SetOrder
            };  

            response.Success = true;
            return response;
        }

        private async Task UpdateCurrentMatchAndStatistics(PickleballMatch match, TeamRoundGroupStatistics firstTeamStats, TeamRoundGroupStatistics secondTeamStats)
        {
            _unitOfWork.PickleballMatchRepository.Update(match);
            _unitOfWork.GroupStatisticRepository.Update(firstTeamStats);
            _unitOfWork.GroupStatisticRepository.Update(secondTeamStats);            
            await _unitOfWork.SaveChangeAsync();
        }

        private async Task UpdateNextRoundMatch(PickleballMatch match, IEnumerable<PickleballMatch> nextRoundMatches,Tournament tournament)
        {
            nextRoundMatches = nextRoundMatches.OrderBy(m => m.MatchOrder).ToList();
            var currentMatchOrder = match.MatchOrder;
            var targetMatchOrder = (currentMatchOrder + 1) / 2 + nextRoundMatches.First().MatchOrder - 1;
            var targetMatch = nextRoundMatches.FirstOrDefault(m => m.MatchOrder == targetMatchOrder);
            var totalMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByTournamentIdAsync(match.RoundGroup.Round.TournamentId);

            if (match.RoundGroup.Round.RoundName == "Semi Final" && tournament.TournamentType == TournamentType.GroupStage)
            {
                targetMatch = nextRoundMatches.FirstOrDefault(m => m.RoundGroup.Round.RoundName == "Final");
            }

            if (nextRoundMatches.First().MatchOrder == totalMatches.Count)
            {
                targetMatch = nextRoundMatches.First();
            }
            
            

            if (match.MatchOrder % 2 == 1)
            {
                targetMatch.FirstTeamId = match.WinningTeamId;
                targetMatch.FirstTeam = match.WinningTeam;
            }
            else
            {
                targetMatch.SecondTeamId = match.WinningTeamId;
                targetMatch.SecondTeam = match.WinningTeam;
            }

            TeamRoundGroup teamRoundGroup = new TeamRoundGroup
            {
                RoundGroupId = (uint)targetMatch.RoundGroupId,
                RoundGroup = targetMatch.RoundGroup,
                TeamId = (uint)match.WinningTeamId,
                Team = match.WinningTeam
            };

            _unitOfWork.PickleballMatchRepository.Update(targetMatch);
            await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
            await _unitOfWork.SaveChangeAsync();

            // Create the statistic with the auto-generated TeamRoundGroup ID
            TeamRoundGroupStatistics statistic = new TeamRoundGroupStatistics
            {
                TeamRoundGroupId = teamRoundGroup.Id,
                TeamRoundGroup = teamRoundGroup,
                Wins = 0,
                Losses = 0,
                Points = 0,
                SetDifference = 0,
                PointsDifference = 0
            };

            var teamsRoundGroup = await _unitOfWork.PickleballMatchRepository.GetTeamsRoundGroup(match.Id);
            foreach (var matchTeamRoundGroup in teamsRoundGroup)
            {
                if (matchTeamRoundGroup.TeamId == match.WinningTeamId)
                {
                    matchTeamRoundGroup.TeamStatus = TeamStatus.Advanced;

                    // Update the advanced team's ranking
                    await UpdateTournamentRankingTeams(matchTeamRoundGroup.TeamId, match.RoundGroup.Round.TournamentId,
                        match.RoundGroup.Round.RoundName, nextRoundMatches.First().RoundGroup.Round.RoundName);
                }
                else
                {
                    matchTeamRoundGroup.TeamStatus = TeamStatus.Disqualified;

                    // Rank the disqualified team based on the round and existing ranks
                    await RankDisqualifiedTeam(matchTeamRoundGroup.TeamId, match.RoundGroup.Round.TournamentId, match.RoundGroup.Round.RoundName);
                }
            }

            _unitOfWork.TeamRoundGroupRepository.UpdateRange(teamsRoundGroup);
            await _unitOfWork.GroupStatisticRepository.AddAsync(statistic);
            await _unitOfWork.SaveChangeAsync();
        }

        private async Task UpdateTournamentRankingTeams(uint teamId, uint tournamentId, string currentRoundName, string nextRoundName)
        {
            // Retrieve all TeamRoundGroupStatistics for the team in the tournament
            var teamStatsInTournament = await _unitOfWork.GroupStatisticRepository.GetTeamStatisticByTournamentIdAsync(tournamentId, teamId);

            // Aggregate the statistics
            var totalWins = teamStatsInTournament.Sum(s => s.Wins);
            var totalLosses = teamStatsInTournament.Sum(s => s.Losses);
            var totalPoints = teamStatsInTournament.Sum(s => s.Points);
            var totalSetDifference = teamStatsInTournament.Sum(s => s.SetDifference);
            var totalPointsDifference = teamStatsInTournament.Sum(s => s.PointsDifference);

            // Update TournamentRankingTeams entity
            var tournamentRankingTeam = await _unitOfWork.TournamentRankingTeamRepository.GetSpecificRankingTeam(tournamentId, teamId);
            tournamentRankingTeam.MatchWins = totalWins;
            tournamentRankingTeam.MatchLosses = totalLosses;
            tournamentRankingTeam.TotalPoints = totalPoints;
            tournamentRankingTeam.SetDifference = totalSetDifference;
            tournamentRankingTeam.ScoresDifference = totalPointsDifference;

            // Update achieved round
            tournamentRankingTeam.AchievedRound = teamStatsInTournament.First().TeamRoundGroup.TeamId == teamId ? nextRoundName : currentRoundName;

            _unitOfWork.TournamentRankingTeamRepository.Update(tournamentRankingTeam);
            await _unitOfWork.SaveChangeAsync();
        }

        private async Task RankDisqualifiedTeam(uint teamId, uint tournamentId, string currentRoundName)
        {
            // Retrieve all the ranking teams in the tournament
            var tournamentRankingTeams = await _unitOfWork.TournamentRankingTeamRepository.GetTournamentRankingTeamsByTournamentIdAsync(tournamentId);

            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);

            // Find the total number of teams in the tournament
            var totalTeams = tournament.NumberOfTeams;

            // Get the list of already ranked teams (i.e., those with a rank greater than 0)
            var rankedTeams = tournamentRankingTeams.Where(t => t.Rank > 0).OrderByDescending(t => t.Rank).ToList();

            // Determine the starting rank for disqualified teams (starting from the bottom)
            int nextRank = rankedTeams.Any() ? rankedTeams.Min(r => r.Rank) - 1 : totalTeams;

            // Check if the team has already been ranked (if not, assign the rank)
            var tournamentRankingTeam = tournamentRankingTeams.FirstOrDefault(t => t.TeamId == teamId);
            if (tournamentRankingTeam != null && tournamentRankingTeam.Rank == 0)
            {
                // Rank the disqualified team based on the round they were disqualified
                tournamentRankingTeam.AchievedRound = currentRoundName;
                tournamentRankingTeam.Rank = nextRank;

                _unitOfWork.TournamentRankingTeamRepository.Update(tournamentRankingTeam);
                await _unitOfWork.SaveChangeAsync();
            }
        }

        private async Task UpdateTournamentRankingTeamsForFinal(PickleballMatch match, string finalRoundName)
        {
            // Update ranking for FirstTeam (e.g., the winner)
            await UpdateTournamentRankingTeams((uint)match.FirstTeamId, match.RoundGroup.Round.TournamentId, finalRoundName, finalRoundName);

            // Update ranking for SecondTeam (e.g., the runner-up)
            await UpdateTournamentRankingTeams((uint)match.SecondTeamId, match.RoundGroup.Round.TournamentId, finalRoundName, finalRoundName);

            // Optionally, update additional fields for winner/runner-up if needed
            var tournamentRankingFirstTeam = await _unitOfWork.TournamentRankingTeamRepository.GetSpecificRankingTeam(match.RoundGroup.Round.TournamentId, (uint)match.FirstTeamId);
            var tournamentRankingSecondTeam = await _unitOfWork.TournamentRankingTeamRepository.GetSpecificRankingTeam(match.RoundGroup.Round.TournamentId, (uint)match.SecondTeamId);

            // If needed, update rank or specific fields for the winner and runner-up
            tournamentRankingFirstTeam.Rank = 1; // Winner
            tournamentRankingSecondTeam.Rank = 2; // Runner-up

            // Optionally, set final round achievement
            tournamentRankingFirstTeam.AchievedRound = finalRoundName;
            tournamentRankingSecondTeam.AchievedRound = finalRoundName;

            var teamsRoundGroup = await _unitOfWork.PickleballMatchRepository.GetTeamsRoundGroup(match.Id);
            foreach (var matchTeamRoundGroup in teamsRoundGroup)
            {
                if (matchTeamRoundGroup.TeamId == match.WinningTeamId)
                {
                    matchTeamRoundGroup.TeamStatus = TeamStatus.Advanced;

                }
                else
                {
                    matchTeamRoundGroup.TeamStatus = TeamStatus.Disqualified;
                }
            }
            _unitOfWork.TournamentRankingTeamRepository.Update(tournamentRankingFirstTeam);
            _unitOfWork.TournamentRankingTeamRepository.Update(tournamentRankingSecondTeam);
            _unitOfWork.TeamRoundGroupRepository.UpdateRange(teamsRoundGroup);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
