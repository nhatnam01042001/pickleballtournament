﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.ParticipantViewModels;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task<ApiResponseModel<List<SingleTeamViewModel>>> AddSingleTeamList(uint tournamentId)
        {
            throw new NotImplementedException();
        }

        public Task<ApiResponseModel<List<SingleTeamViewModel>>> GetSingleTeams(uint tournamentId)
        {
            throw new NotImplementedException();
        }

        public async Task<ApiResponseModel<TeamGroupViewModel>> AddSingleTeamToGroup(uint roundGroupId, uint athleteId)
        {
            var response = new ApiResponseModel<TeamGroupViewModel>()
            {
                Success = false,
                Data = null
            };
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupWithRoundByIdAsync(roundGroupId);
            if (roundGroup == null)
            {
                response.Message = "Round Group does not existed !";
                return response;
            }
            if(roundGroup.GroupStatus != Domain.Enums.MatchStatus.Scheduling)
            {
                response.Message = "Group has been scheduled ! Can not assign team to group !";
                return response;
            }
            var nonTeamAthlete = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(roundGroup.Round.TournamentId); ;
            var athlete = await _unitOfWork.TournamentAthleteRepository.GetAthleteInTournamentAsync(athleteId, roundGroup.Round.TournamentId);
            if (athlete == null)
            {
                response.Message = "Athlete does not existed in Tournament !"!;
                return response;
            }
            if (!nonTeamAthlete.Any(a => a.Id == athlete.Id))
            {
                response.Message = "Athlete has already had team !";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(roundGroup.Round.TournamentId);
            var team = athlete.ToTeam();
            team.TeamName = team.GetTeamName();
            team.TeamType = tournament.FormatType;
            team.TournamentId = tournament.Id;
            team.Tournament = tournament;
            team.AverageRank = tournament.Rank;
            if (!team.CheckValidateTeam(team.TeamType))
            {
                response.Message = "Team is not valid ! Please check gender and average rank !";
                return response;
            }
            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    await _unitOfWork.TeamRepository.AddAsync(team);
                    await _unitOfWork.SaveChangeAsync();
                    TournamentRankingTeam rankingTeam = tournament.CreateTournamentRankingTeam(team);
                    await _unitOfWork.TournamentRankingTeamRepository.AddAsync(rankingTeam);

                    TeamRoundGroup teamRoundGroup = new TeamRoundGroup
                    {
                        RoundGroupId = roundGroup.Id,
                        TeamId = team.Id,
                    };
                    await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                    await _unitOfWork.SaveChangeAsync();
                    TeamRoundGroupStatistics statistics = new TeamRoundGroupStatistics
                    {
                        TeamRoundGroupId = teamRoundGroup.Id,
                        Points = 0,
                        Wins = 0,
                        Draws = 0,
                        Losses = 0,
                    };

                    await _unitOfWork.GroupStatisticRepository.AddAsync(statistics);
                    await _unitOfWork.SaveChangeAsync();
                    await _unitOfWork.CommitAsync();
                    TeamGroupViewModel teamGroup = new TeamGroupViewModel
                    {
                        TeamId = team.Id,
                        TeamName = team.TeamName,
                        RoundGroupId = roundGroup.Id,
                        RoundGroupName = roundGroup.RoundGroupName,
                        MatchWin = statistics.Wins,
                        MatchDraw = statistics.Draws,
                        MatchLose = statistics.Losses,
                        Point = statistics.Points,
                        TeamRoundGroupId = teamRoundGroup.Id
                    };
                    response.Success = true;
                    response.Message = "Team Group Detail !";
                    response.Data = teamGroup;
                }
                catch (Exception ex)
                {
                    await _unitOfWork.RollbackAsync();
                    response.Message = ex.Message;
                    return response;
                }                
            }            
            return response;
        }

        public async Task<ApiResponseModel<TeamGroupViewModel>> AddDoubleTeamToGroup(uint roundGroupId, AssignPlayerViewModel assignPlayerDto)
        {
            var response = new ApiResponseModel<TeamGroupViewModel>
            {
                Success = false,
                Data = null
            };

            // Validate athlete IDs
            if (assignPlayerDto.FirstAthleteId == 0 || assignPlayerDto.SecondAthleteId == 0)
            {
                response.Message = "Double Team requires two athletes! Please input two athletes!";
                return response;
            }

            if (assignPlayerDto.FirstAthleteId == assignPlayerDto.SecondAthleteId)
            {
                response.Message = "Cannot assign the same athlete to a Double Team! Please input two different athletes!";
                return response;
            }

            // Retrieve the RoundGroup
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupWithRoundByIdAsync(roundGroupId);
            if (roundGroup == null)
            {
                response.Message = "Round Group does not exist!";
                return response;
            }

            if (roundGroup.GroupStatus != Domain.Enums.MatchStatus.Scheduling)
            {
                response.Message = "Group has been scheduled! Cannot assign team to group!";
                return response;
            }

            // Retrieve non-team athletes
            var nonTeamAthlete = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(roundGroup.Round.TournamentId);

            // Retrieve first athlete
            var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetAthleteInTournamentAsync((uint)assignPlayerDto.FirstAthleteId, roundGroup.Round.TournamentId);
            if (firstAthlete == null)
            {
                response.Message = "First Athlete does not exist in Tournament!";
                return response;
            }

            if (!nonTeamAthlete.Any(a => a.Id == firstAthlete.Id))
            {
                response.Message = "First Athlete already has a team!";
                return response;
            }

            // Retrieve second athlete
            var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetAthleteInTournamentAsync((uint)assignPlayerDto.SecondAthleteId, roundGroup.Round.TournamentId);
            if (secondAthlete == null)
            {
                response.Message = "Second Athlete does not exist!";
                return response;
            }

            if (!nonTeamAthlete.Any(a => a.Id == secondAthlete.Id))
            {
                response.Message = "Second Athlete already has a team!";
                return response;
            }

            // Retrieve the tournament
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(roundGroup.Round.TournamentId);

            // Create new team
            Team newTeam = TeamViewModelExtensions.CreateDoubleTeam(firstAthlete, secondAthlete);
            newTeam.TeamName = newTeam.GetTeamName();
            newTeam.TeamType = tournament.FormatType;
            newTeam.TournamentId = tournament.Id;
            newTeam.Tournament = tournament;
            newTeam.AverageRank = newTeam.GetTeamAverageRank();

            // Validate the team
            if (!newTeam.CheckValidateTeam(newTeam.TeamType))
            {
                response.Message = "Team is not valid! Please check gender and average rank.";
                return response;
            }

            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    // Add the new team
                    await _unitOfWork.TeamRepository.AddAsync(newTeam);
                    await _unitOfWork.SaveChangeAsync(); // Save changes to persist the teamId

                    TournamentRankingTeam rankingTeam = tournament.CreateTournamentRankingTeam(newTeam);
                    await _unitOfWork.TournamentRankingTeamRepository.AddAsync(rankingTeam);
                    // Create and add TeamRoundGroup
                    TeamRoundGroup teamRoundGroup = new TeamRoundGroup
                    {
                        RoundGroupId = roundGroup.Id,
                        TeamId = newTeam.Id,
                    };
                    await _unitOfWork.TeamRoundGroupRepository.AddAsync(teamRoundGroup);
                    await _unitOfWork.SaveChangeAsync(); // Save to persist TeamRoundGroupId

                    // Create and add TeamRoundGroupStatistics
                    TeamRoundGroupStatistics statistics = new TeamRoundGroupStatistics
                    {
                        TeamRoundGroupId = teamRoundGroup.Id,
                        Points = 0,
                        Wins = 0,
                        Draws = 0,
                        Losses = 0,
                    };
                    await _unitOfWork.GroupStatisticRepository.AddAsync(statistics);
                    await _unitOfWork.SaveChangeAsync(); // Save statistics

                    // Commit the transaction
                    await transaction.CommitAsync();

                    // Prepare the response data
                    TeamGroupViewModel teamGroup = new TeamGroupViewModel
                    {
                        TeamId = newTeam.Id,
                        TeamName = newTeam.TeamName,
                        RoundGroupId = roundGroup.Id,
                        RoundGroupName = roundGroup.RoundGroupName,
                        MatchWin = statistics.Wins,
                        MatchDraw = statistics.Draws,
                        MatchLose = statistics.Losses,
                        Point = statistics.Points,
                        TeamRoundGroupId = teamRoundGroup.Id
                    };

                    response.Success = true;
                    response.Message = "Team Group Detail!";
                    response.Data = teamGroup;
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync(); // Rollback if an error occurs
                    response.Message = ex.Message;
                    return response;
                }
            }

            return response; // Return the response
        }


        public async Task<ApiResponseModel<TeamGroupViewModel>> UpdateSingleTeam(uint teamRoundGroupId, uint athleteId)
        {
            var response = new ApiResponseModel<TeamGroupViewModel>
            {
                Success = false,
                Data = null
            };
            var teamRoundGroup = await _unitOfWork.TeamRoundGroupRepository.GetTeamRoundGroupByIdAsync(teamRoundGroupId);
            if(teamRoundGroup is null)
            {
                response.Message = "The team in group does not existed !";
                return response;
            }
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupWithRoundByIdAsync(teamRoundGroup.RoundGroup.Id);
            if (roundGroup == null)
            {
                response.Message = "Round Group does not existed !";
                return response;
            }
            if (roundGroup.GroupStatus != Domain.Enums.MatchStatus.Scheduling)
            {
                response.Message = "Group has been scheduled ! Can not assign team to group !";
                return response;
            }
            if (athleteId == 0)
            {
                response.Message = "Everything up to date ! Nothing change !";
                return response;
            }
            var athlete = await _unitOfWork.TournamentAthleteRepository.GetAthleteInTournamentAsync(athleteId, roundGroup.Round.TournamentId);
            if(athlete is null)
            {
                response.Message = "Athlete does not existed in tournament !";
                return response;
            }
            var nonTeamAthlete = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(roundGroup.Round.TournamentId); ;
            if(nonTeamAthlete.Any(a => a.Id == athleteId))
            {
                response.Message = "Athlete has already had team !";
                return response;
            }
            var team = await _unitOfWork.TeamRepository.GetByIdAsync(teamRoundGroup.TeamId);
            team.FirstAthleteId = athleteId;
            team.TeamName = team.GetTeamName();
            _unitOfWork.TeamRepository.Update(team);
            await _unitOfWork.SaveChangeAsync();
            response.Message = "Update Team success !";
            var statistics = await _unitOfWork.GroupStatisticRepository.GetTeamStatisticsByTeamRoundGroupIdAsync(teamRoundGroupId);
            response.Data = new TeamGroupViewModel
            {
                TeamId = team.Id,
                TeamName = team.TeamName,
                RoundGroupId = roundGroup.Id,
                RoundGroupName = roundGroup.RoundGroupName,
                MatchWin = statistics.Wins,
                MatchDraw = statistics.Draws,
                MatchLose = statistics.Losses,
                Point = statistics.Points,
                TeamRoundGroupId = teamRoundGroupId
            };
            return response;
        }

        public async Task<ApiResponseModel<TeamGroupViewModel>> UpdateDoubleTeamToGroup(uint teamRoundGroupId, AssignPlayerViewModel assignPlayerDto)
        {
            var response = new ApiResponseModel<TeamGroupViewModel>
            {
                Success = false,
                Data = null
            };
            var teamRoundGroup = await _unitOfWork.TeamRoundGroupRepository.GetTeamRoundGroupByIdAsync(teamRoundGroupId);
            if (teamRoundGroup is null)
            {
                response.Message = "The team in group does not existed !";
                return response;
            }
            var roundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupWithRoundByIdAsync(teamRoundGroup.RoundGroup.Id);
            if (roundGroup == null)
            {
                response.Message = "Round Group does not existed !";
                return response;
            }
            if (roundGroup.GroupStatus != Domain.Enums.MatchStatus.Scheduling)
            {
                response.Message = "Group has been scheduled ! Can not assign team to group !";
                return response;
            }
            if (assignPlayerDto.FirstAthleteId == 0 && assignPlayerDto.SecondAthleteId == 0)
            {
                response.Message = "Everything up to date ! Nothing change !";
                return response;
            }
            if ((assignPlayerDto.FirstAthleteId == 0 && assignPlayerDto.SecondAthleteId == 0)
                || assignPlayerDto.FirstAthleteId == assignPlayerDto.SecondAthleteId)
            {
                response.Message = "Can not assign same athlete to Double Team ! Please input two different athletes !";
                return response;
            }
            var firstAthlete = await _unitOfWork.TournamentAthleteRepository.GetAthleteInTournamentAsync((uint)assignPlayerDto.FirstAthleteId, roundGroup.Round.TournamentId);
            if (firstAthlete is null)
            {
                response.Message = "First Athlete does not existed in tournament !";
                return response;
            }
            var secondAthlete = await _unitOfWork.TournamentAthleteRepository.GetAthleteInTournamentAsync((uint)assignPlayerDto.SecondAthleteId, roundGroup.Round.TournamentId);
            if(secondAthlete is null)
            {
                response.Message = "Second Athlete does not existed in tournament !";
                return response;
            }
            var nonTeamAthlete = await _unitOfWork.AthleteRepository.GetAthletesWithNonTeamAsync(roundGroup.Round.TournamentId); ;
            if (nonTeamAthlete.Any(a => a.Id == assignPlayerDto.FirstAthleteId))
            {
                response.Message = "First Athlete has already had team !";
                return response;
            }
            if (nonTeamAthlete.Any(a => a.Id == assignPlayerDto.SecondAthleteId))
            {
                response.Message = "Second Athlete has already had team !";
                return response;
            }
            var updatedTeam = await _unitOfWork.TeamRepository.GetByIdAsync(teamRoundGroup.TeamId);
            if(updatedTeam is null)
            {
                response.Message = "Team in this group does not existed !";
                return response;
            }
            if(assignPlayerDto.FirstAthleteId != 0 && assignPlayerDto.SecondAthleteId != 0)
            {
                
                updatedTeam.FirstAthleteId = (uint)assignPlayerDto.FirstAthleteId;
                updatedTeam.FirstAthlete = firstAthlete;
                updatedTeam.SecondAthleteId = (uint)assignPlayerDto.SecondAthleteId;
                updatedTeam.SecondAthlete = secondAthlete;
                updatedTeam.AverageRank = updatedTeam.GetTeamAverageRank();
                if (!updatedTeam.CheckValidateTeam(updatedTeam.TeamType))
                {
                    response.Message = "Invalid Team ! Please check athletes gender and Rank !";
                    return response;
                }                
            }
            if(assignPlayerDto.FirstAthleteId != 0 && assignPlayerDto.SecondAthleteId == 0)
            {
                
                updatedTeam.FirstAthleteId = (uint)assignPlayerDto.FirstAthleteId;
                updatedTeam.FirstAthlete = firstAthlete;
                updatedTeam.AverageRank = updatedTeam.GetTeamAverageRank();
                if (!updatedTeam.CheckValidateTeam(updatedTeam.TeamType))
                {
                    response.Message = "Invalid Team ! Please check athletes gender and Rank !";
                    return response;
                }
            }
            if(assignPlayerDto.FirstAthleteId == 0 && assignPlayerDto.SecondAthleteId != 0)
            {
                
                updatedTeam.SecondAthleteId = (uint)assignPlayerDto.SecondAthleteId;
                updatedTeam.SecondAthlete = secondAthlete;
                updatedTeam.AverageRank = updatedTeam.GetTeamAverageRank();
                if (!updatedTeam.CheckValidateTeam(updatedTeam.TeamType))
                {
                    response.Message = "Invalid Team ! Please check athletes gender and Rank !";
                    return response;
                }
            }
            try
            {
                _unitOfWork.TeamRepository.Update(updatedTeam);
                await _unitOfWork.SaveChangeAsync();
                var statistics = await _unitOfWork.GroupStatisticRepository.GetTeamStatisticsByTeamRoundGroupIdAsync(teamRoundGroupId);
                response.Message = "Update Team success !";
                response.Success = true;
                response.Data = new TeamGroupViewModel
                {
                    TeamId = updatedTeam.Id,
                    TeamName = updatedTeam.TeamName,
                    RoundGroupId = roundGroup.Id,
                    RoundGroupName = roundGroup.RoundGroupName,
                    MatchWin = statistics.Wins,
                    MatchDraw = statistics.Draws,
                    MatchLose = statistics.Losses,
                    Point = statistics.Points,
                    TeamRoundGroupId = teamRoundGroupId
                };
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<List<TeamViewModel>>> GetAdvancedTeams(uint roundId, int numberOfTeam)
        {
            var response = new ApiResponseModel<List<TeamViewModel>>()
            {
                Success = true,
                Data = null
            };
            return response;
        }
        public async Task<ApiResponseModel<List<int>>> GetListNumberOfAdvancedTeam(uint roundId)
        {
            var response = new ApiResponseModel<List<int>>
            {
                Success = true,
                Data = null
            };
            var listRankedTeam = await _unitOfWork.TeamRoundGroupRepository.GetListRankedTeam(roundId);
            if (listRankedTeam is null)
            {
                response.Message = "Tournament does not have any team !";
                return response;
            }
            int numberOfFirstRankedTeam = listRankedTeam.Count();
            int totalTeams = listRankedTeam.Sum(group => group.Value.Count());

            int minPowerOfTwo = 1;
            while (minPowerOfTwo < numberOfFirstRankedTeam)
            {
                minPowerOfTwo *= 2;
            }

            int maxPowerOfTwo = 1;
            while (maxPowerOfTwo * 2 <= totalTeams)
            {
                maxPowerOfTwo *= 2;
            }

            // If maxPowerOfTwo exceeds the totalTeams, adjust it
            if (maxPowerOfTwo > totalTeams)
            {
                maxPowerOfTwo = totalTeams;
            }
            List<int> powerOfTwoList = new List<int>();
            for (int i = minPowerOfTwo; i <= maxPowerOfTwo; i *= 2)
            {
                powerOfTwoList.Add(i);
            }
            response.Data = powerOfTwoList;
            response.Message = "List number of advanced team !";
            return response;
        }

        public async Task<ApiResponseModel<List<TeamViewModel>>> GetTeamsWithoutMatch(uint roundId)
        {
            var response = new ApiResponseModel<List<TeamViewModel>>
            {
                Success = true,
                Data = null
            };

            var teams = await _unitOfWork.TeamRoundGroupRepository.GetTeamWithoutMatchAsync(roundId);
            if(teams is null)
            {
                response.Message = "All teams have already competed in match !";
                return response;
            }
            List<TeamViewModel> listTeams = new List<TeamViewModel>();
            foreach (var team in teams)
            {
                TeamViewModel teamViewModel = team.ToTeamViewModel();
                listTeams.Add(teamViewModel);
            }
            response.Data = listTeams;
            response.Message = "List Team without match !";
            return response;
        }
    }
}
/*public async Task<ApiResponseModel<List<SingleTeamViewModel>>> GetSingleTeams(uint tournamentId)
{
    var response = new ApiResponseModel<List<SingleTeamViewModel>>();
    var teams = await _unitOfWork.TeamRepository.GetSingleTeamsAsync(tournamentId);
    if(teams == null)
    {
        response.Success = false;
        response.Message = "The list of team is empty";
        return response;
    }
    var listTeams = new List<SingleTeamViewModel>();
    foreach(var team in teams)
    {
        var teamViewModel = team.ToTeamViewModel();
        listTeams.Add(teamViewModel);
    }
    response.Success = true;
    response.Message = "List Single Team !";
    response.Data = listTeams;
    return response;
}

public async Task<ApiResponseModel<List<SingleTeamViewModel>>> AddSingleTeamList(uint tournamentId)
{
    var response = new ApiResponseModel<List<SingleTeamViewModel>>();
    List<Team> teams = new List<Team>();
    var participants = await _unitOfWork.ParticipantRepository.GetParticipantsInTournamentAsync(tournamentId);
    if (participants is null)
    {
        response.Success = false;
        response.Message = "The participants List is empty or Tournament does not existed !";
        return response;
    }
    foreach (var participant in participants)
    {
        if (participant.User is not null)
        {
            var team = new Team
            {
                TeamName = participant.User.FullName,
                FirstParticipantId = participant.Id,                        
            };
            if(participant.User.Gender == "Male")
            {
                team.TeamType = Domain.Enums.TeamType.MenSingle;
            }
            else
            {
                team.TeamType = Domain.Enums.TeamType.WomenSingle;
            }
            teams.Add(team);
        }
        else if (participant.GuestPlayer is not null)
        {
            var team = new Team
            {
                TeamName = participant.GuestPlayer.FullName,
                FirstParticipantId = participant.Id,                        
            };
            if (participant.GuestPlayer.Gender == "Male")
            {
                team.TeamType = Domain.Enums.TeamType.MenSingle;
            }
            else
            {
                team.TeamType = Domain.Enums.TeamType.WomenSingle;
            }
            teams.Add(team) ;
        }
    }
    if (!teams.Any())
    {
        throw new Exception("No valid participants to create teams from.");
    }

    await _unitOfWork.TeamRepository.AddRangeAsync(teams);
    bool addSuccess = await _unitOfWork.SaveChangeAsync() == teams.Count;

    if (!addSuccess)
    {
        throw new Exception("Cannot add teams to the tournament!");
    }
    List<SingleTeamViewModel> teamViewModels = new List<SingleTeamViewModel>();
    foreach (var team in teams)
    {
        SingleTeamViewModel viewModel = team.ToTeamViewModel();
        teamViewModels.Add(viewModel);
    }
    response.Success = true;
    response.Message = "The List of Single Team !";
    response.Data = teamViewModels;
    return response;
}*/


