﻿using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using CloudinaryDotNet;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class CampaignRegistrationService : ICampaignRegistService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClaimsService _claimsService;
        private readonly AppConfiguration _configuration;
        public CampaignRegistrationService(IUnitOfWork unitOfWork, IClaimsService claimsService, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _claimsService = claimsService;
            _configuration = configuration;
        }

        public async Task<ApiResponseModel<List<CampaignRegistViewModel>>> GetListRegistrationByCampaignId(uint campaignId)
        {
            var response = new ApiResponseModel<List<CampaignRegistViewModel>>()
            {
                Success = false,
                Data = null
            };
            var listRegist = await _unitOfWork.CampaignRegistRepository.GetRegistrationByCampaignId(campaignId);
            if (listRegist == null)
            {
                response.Message = "List empty !";
                return response;
            }
            List<CampaignRegistViewModel> listViewModel = new List<CampaignRegistViewModel>();
            foreach (var regist in listRegist)
            {
                CampaignRegistViewModel viewModel = regist.ToRegistrationViewModel();
                listViewModel.Add(viewModel);
            }
            response.Success = true;
            response.Message = "List Registration !";
            response.Data = listViewModel;
            return response;
        }

        public async Task<ApiResponseModel<Pagination<CampaignRegistViewModel>>> GetCampaignRegistrationPagingsions(uint campaignId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<CampaignRegistViewModel>>()
            {
                Success = true,
                Data = null
            };
            var listRegist = await _unitOfWork.CampaignRegistRepository.GetRegistrationByCampaignId(campaignId);
            if (listRegist == null)
            {
                response.Message = "List empty !";
                return response;
            }
            response.Message = "List Registration !";
            response.Data = listRegist.ToPagination(pageIndex,pageSize);
            return response;
        }

        public async Task<ApiResponseModel<CampaignRegistViewModel>> RegistToTournament(uint campaignId, CampaignRegistrationDTO campaignRegistDTO)
        {
            var response = new ApiResponseModel<CampaignRegistViewModel>
            {
                Success = false,
                Data = null
            };
            if (!BooleanUtils.ValidatePhone(campaignRegistDTO.PhoneNumber))
            {
                response.Message = "Phone number must contain exactly 10 positive digits !";
                return response;
            }
            if (!BooleanUtils.ValidateEmail(campaignRegistDTO.Email))
            {
                response.Message = "Email must be in the format: 'name@gmail.com' where 'name' does not start with a digit !";
                return response;
            }
            // Check if the tournament campaign exists
            var tournamentCampaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(campaignId);
            if (tournamentCampaign is null)
            {
                response.Message = "Tournament Campaign does not exist!";
                return response;
            }
            if(tournamentCampaign.RegisterExpiredDate <= DateTime.Now)
            {
                response.Message = "Tournament Registration Date has been expired !";
                return response;
            }
            // Check if the user is already registered for the tournament
            var existedRegistration = await _unitOfWork.CampaignRegistRepository.CheckRegistrationExisted(campaignRegistDTO.Email, tournamentCampaign.Id);
            if (existedRegistration)
            {
                response.Message = "You have already registered for this Tournament! Please check your Registration status!";
                return response;
            }

            // Proceed with registration
            campaignRegistDTO.TournamentCampaignId = tournamentCampaign.Id;
            var tournamentRegistration = campaignRegistDTO.ToTournamentRegistration();
            tournamentRegistration.RegistrationStatus = 0;

            await _unitOfWork.CampaignRegistRepository.AddAsync(tournamentRegistration);
            bool registSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (!registSuccess)
            {
                response.Message = "Registration to Tournament failed! Please try again.";
                return response;
            }

            // Successful registration
            var registrationViewModel = tournamentRegistration.ToRegistrationViewModel();
            response.Success = true;
            response.Message = "Your registration is sent successfully! Please wait for approval!";
            response.Data = registrationViewModel;

            return response;
        }

        public async Task<ApiResponseModel<EmailResponseViewModel>> UpdateRegistrationStatus(uint id, string status)
        {
            var response = new ApiResponseModel<EmailResponseViewModel>()
            {
                Success = false,
                Data = null
            };
            var regist = await _unitOfWork.CampaignRegistRepository.GetByIdAsync(id);
            if (regist == null)
            {
                response.Message = "Registration does not existed !";
                return response;
            }
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(regist.TournamentCampaignId);
            var managerEmail = _configuration.ManagerEmailForSend;
            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(managerEmail.Email, managerEmail.Password),
            };
            var message = new MailMessage();
            switch (status)
            {
                case "Approved":
                    regist.RegistrationStatus = TournamentRegistrationStatus.Approved;
                    var athlete = new Athlete
                    {
                        GuestPlayerId = id,
                        GuestPlayer = regist,
                        TournamentCampaignId = regist.TournamentCampaignId,
                        TournamentCampaign = campaign,
                        ParticipantType = ParticipantType.GuestPlayer,
                    };
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            _unitOfWork.CampaignRegistRepository.Update(regist);
                            await _unitOfWork.AthleteRepository.AddAsync(athlete);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                            var gender = athlete.GetAthleteGender();
                            message.Subject = "Athlete Information !";
                            message.Body = $"AthleteId: {athlete.Id}\n" +
                   $"Full Name: {athlete.GuestPlayer.FullName}\n" +
                   $"Tournament Campaign : {campaign.TournamentName}\n" +
                   $"Athlete Type : Guest Player\n" +
                   $"Gender : {gender}";
                            message.From = new MailAddress(managerEmail.Email);

                            message.To.Add(new MailAddress(regist.Email));
                            await smtpClient.SendMailAsync(message);
                            response.Message = "Registration has been approved !";                            
                        }
                        catch (SmtpException smtpEx)
                        {
                            response.Message = $"Failed to send email: {smtpEx.Message}";
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = ex.Message;
                            return response;
                        }
                    }
                    break;
                case "Denied":
                    regist.RegistrationStatus = TournamentRegistrationStatus.Denied;
                    regist.IsDeleted = true;
                    try
                    {
                        _unitOfWork.CampaignRegistRepository.Update(regist);
                        await _unitOfWork.SaveChangeAsync();
                        message.Subject = $"{campaign.TournamentName} campaign registration announcement !";
                        message.Body = $"Your {campaign.TournamentName} campaign registration has been denied by the tournament manager !";
                        message.From = new MailAddress(managerEmail.Email);
                        
                        message.To.Add(new MailAddress(regist.Email));
                        await smtpClient.SendMailAsync(message);
                        response.Message = "Registration has been denied !";
                        
                    }
                    catch (SmtpException smtpEx)
                    {
                        response.Message = $"Failed to send email: {smtpEx.Message}";
                    }
                    catch (Exception ex)
                    {
                        response.Message = ex.Message;
                        return response;
                    }
                    break;
                default:
                    response.Message = "Invalid status provied !";
                    return response;
            }
            response.Success = true;
            response.Data = new EmailResponseViewModel
            {
                SenderEmail = managerEmail.Email,
                ReceiverEmail = regist.Email,
                Message = message.Body
            };
            return response;
        }
        public async Task<ApiResponseModel<AthleteViewModel>> ParticipateCampaignForUser(uint campaignId)
        {
            var response = new ApiResponseModel<AthleteViewModel>
            {
                Success = false,
                Data = null
            };
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Message = "Campaign does not existed !";
                return response;
            }
            if (campaign.RegisterExpiredDate <= DateTime.Now)
            {
                response.Message = "Tournament Registration Date has been expired !";
                return response;
            }
            var userId = _claimsService.GetCurrentUserId;
            if (userId == 0)
            {
                response.Message = "This function required user login action !";
                return response;
            }
            var user = await _unitOfWork.UserRepository.GetByIdAsync(userId);
            if (user == null)
            {
                response.Message = "User does not existed !";
                return response;
            }
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(campaignId);
            if (athletes.Any(a => a.UserId == userId))
            {
                response.Message = "Athlete has already existed in Campaign !";
                return response;
            }
            var athlete = user.ToAthlete();
            athlete.TournamentCampaignId = campaign.Id;
            athlete.TournamentCampaign = campaign;
            try
            {
                await _unitOfWork.AthleteRepository.AddAsync(athlete);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Participate in Campaign success !";
                var athleteViewModel = athlete.ToAthleteViewModel();
                athleteViewModel.TournamentCampaignId = campaign.Id.ToString();
                response.Data = athleteViewModel;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

    }
}
