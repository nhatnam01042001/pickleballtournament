﻿using Application.Interfaces;
using Application.Repositories;
using Application.Utils;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.RoundViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class RoundService : IRoundService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoundService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<List<RoundViewModel>>> GetRoundByTournamentIdAsync(uint tournamentId)
        {
            var response = new ApiResponseModel<List<RoundViewModel>>
            {
                Success = false,
                Data = null
            };
            var rounds = await _unitOfWork.RoundRepository.GetRoundByTournamentIdAsync(tournamentId);
            if(rounds == null)
            {
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            List<RoundViewModel> listRounds = new List<RoundViewModel>();
            foreach (var round in rounds)
            {
                RoundViewModel roundViewModel = new RoundViewModel
                {
                    RoundId = round.Id,
                    RoundName = round.RoundName,
                    RoundOrder = round.RoundOrder,
                    RoundStatus = round.RoundStatus.ToString(),
                    TournamentId = round.TournamentId,
                    TournamentName = tournament.TournamentName
                };
                listRounds.Add(roundViewModel);
            }
            response.Data = listRounds;
            response.Message = "List Round !";
            response.Success = true;
            return response;
        }

        public async Task<ApiResponseModel<List<PickleballMatchViewModel>>> GenerateRoundsTournament(CreateRoundsDto createRoundsDto)
        {
            var response = new ApiResponseModel<List<PickleballMatchViewModel>>
            {
                Data = null,
                Success = false,
            };

            var firstRound = await _unitOfWork.RoundRepository.GetRoundByRoundOrderAsync(createRoundsDto.TournamentId, 1);
            if(firstRound is null)
            {
                response.Message = "Tournament does not have first round or Tournament does not existed !";
                return response;
            };            
            if(!await _unitOfWork.PickleballMatchRepository.CheckMatchesInRoundCompleted(firstRound.Id))
            {
                response.Message = "First Round does not completed !";
                return response;
            }
            var secondRound = await _unitOfWork.RoundRepository.GetRoundByRoundOrderAsync(createRoundsDto.TournamentId, 2);
            if(secondRound is not null)
            {
                response.Message = "Next Round is exsited ! Can not generate Next Round !";
                return response;
            }
            firstRound.RoundStatus = Domain.Enums.MatchStatus.Completed;
            var listRankedTeam = await GetTopTeamsByPowerOfTwoAsync(firstRound.Id, createRoundsDto.NumberOfTeams);
            if(listRankedTeam is null)
            {
                response.Message = "Tournament does not have any team !";
                return response;
            }
            List<Round> rounds = new List<Round>();
            int totalRounds = (int)Math.Ceiling((Math.Log(createRoundsDto.NumberOfTeams,2)));
            for(int roundNumber = 2; roundNumber < 2 + totalRounds; roundNumber++)
            {
                Round round = new Round
                {
                    TournamentId = firstRound.TournamentId,
                    Tournament = firstRound.Tournament,
                    RoundName = GetRoundName(roundNumber, totalRounds),
                    RoundOrder = roundNumber,
                    RoundStatus = Domain.Enums.MatchStatus.Scheduling,
                    RoundGroups = new List<RoundGroup>()
                };
                rounds.Add(round);
            }
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(firstRound.TournamentId);
            //var secondRoundOrder = rounds.FirstOrDefault(r => r.RoundOrder == 2);
            List<RoundGroup> roundGroups = GenerateRoundGroups(rounds);
            //var secondRoundGroups = roundGroups.Where(r => r.Round.RoundOrder == 2).ToList();
            
            //List<TeamRoundGroup> teamRoundGroups = GenerateTeamRoundGroup(secondRoundGroup, listRankedTeam);
            var firstTeamRoundGroups = await _unitOfWork.TeamRoundGroupRepository.GetTeamRoundGroupByRoundId(firstRound.Id);
            
            foreach(var teamRoundGroup in firstTeamRoundGroups)
            {
                if(listRankedTeam.Any(t => t.Id == teamRoundGroup.TeamId))
                {
                    teamRoundGroup.TeamStatus = Domain.Enums.TeamStatus.Advanced;
                }
                else
                {
                    teamRoundGroup.TeamStatus = Domain.Enums.TeamStatus.Disqualified;                    
                }
            }
            var tournamentRankingTeams = await _unitOfWork.TournamentRankingTeamRepository.GetTournamentRankingTeamsByTournamentIdAsync(firstRound.TournamentId);
            var teamStatistics = await _unitOfWork.GroupStatisticRepository.GetTeamStatisticByRoundIdAsync(firstRound.Id);

            foreach (var teamRoundGroupStat in teamStatistics)
            {
                var rankingTeam = tournamentRankingTeams.FirstOrDefault(t => t.TeamId == teamRoundGroupStat.TeamRoundGroup.TeamId);
                if (rankingTeam != null)
                {
                    rankingTeam.MatchWins += teamRoundGroupStat.Wins;
                    rankingTeam.MatchLosses += teamRoundGroupStat.Losses;
                    rankingTeam.SetDifference += teamRoundGroupStat.SetDifference;
                    rankingTeam.TotalPoints += teamRoundGroupStat.Points;
                    rankingTeam.ScoresDifference += teamRoundGroupStat.PointsDifference;

                    // Update AchievedRound based on team status
                    var nextRound = rounds.FirstOrDefault(r => r.RoundOrder == firstRound.RoundOrder + 1);
                    if (teamRoundGroupStat.TeamRoundGroup.TeamStatus == Domain.Enums.TeamStatus.Advanced)
                    {
                        rankingTeam.AchievedRound = nextRound?.RoundName;
                    }
                    else
                    {
                        rankingTeam.AchievedRound = firstRound.RoundName;
                    }
                }
            }

            // Update disqualified teams rank
            var disqualifiedTeams = firstTeamRoundGroups.Where(t => t.TeamStatus == Domain.Enums.TeamStatus.Disqualified).ToList();
            if (disqualifiedTeams.Any())
            {
                // Get total number of teams in the tournament and start rank from the bottom
                int totalTeams = tournamentRankingTeams.Count;
                int nextRank = totalTeams;

                // Get the disqualified team IDs and corresponding TournamentRankingTeams
                var disqualifiedTeamIds = disqualifiedTeams.Select(t => t.TeamId).ToList();
                var disqualifiedRankingTeams = tournamentRankingTeams.Where(t => disqualifiedTeamIds.Contains(t.TeamId)).ToList();

                // Rank disqualified teams from bottom to top
                foreach (var disqualifiedRankingTeam in disqualifiedRankingTeams)
                {
                    disqualifiedRankingTeam.Rank = nextRank--; // Assign rank from bottom and decrement for each team
                }
            }
            var matches = await _unitOfWork.PickleballMatchRepository.GetMatchesByRoundIdAsync(firstRound.Id);
            int matchOrderCounter = matches.Max(m => m.MatchOrder) + 1;
            List<PickleballMatch> pickleballMatches = GenerateMatches(roundGroups, createRoundsDto.NumberOfTeams, ref matchOrderCounter);
            List<Set> sets = GenerateSetsForMatch(pickleballMatches, firstRound.Tournament.NumberOfSets);
            //List<TeamRoundGroupStatistics> statistics = GenerateTeamStatistics(teamRoundGroups);
            using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                    _unitOfWork.RoundRepository.Update(firstRound);

                    // Save rounds and round groups before creating any dependent entities
                    await _unitOfWork.RoundRepository.AddRangeAsync(rounds);
                    //await _unitOfWork.SaveChangeAsync(); // Save to ensure IDs are generated

                    await _unitOfWork.RoundGroupRepository.AddRangeAsync(roundGroups);
                   //await _unitOfWork.SaveChangeAsync(); // Save to ensure RoundGroup IDs are generated

                    // Retrieve the second round by its order after saving
                    

                    // Save matches and sets
                    await _unitOfWork.PickleballMatchRepository.AddRangeAsync(pickleballMatches);
                    //await _unitOfWork.SaveChangeAsync();
                    await _unitOfWork.SetRepository.AddRangeAsync(sets);
                    _unitOfWork.TournamentRankingTeamRepository.UpdateRange(tournamentRankingTeams);
                    await _unitOfWork.SaveChangeAsync(); // Final save
                    var secondRoundOrder = rounds.FirstOrDefault(r => r.RoundOrder == 2);

                    if (secondRoundOrder == null)
                    {
                        throw new Exception("Second round could not be found.");
                    }

                    var roundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupByRoundIdElimination(secondRoundOrder.Id);

                    // Generate team round groups and add them to the repository
                    var teamRoundGroups = GenerateTeamRoundGroup(roundGroup, listRankedTeam);
                    await _unitOfWork.TeamRoundGroupRepository.AddRangeAsync(teamRoundGroups);

                    // Generate statistics and save them
                    var statistics = GenerateTeamStatistics(teamRoundGroups);
                    await _unitOfWork.GroupStatisticRepository.AddRangeAsync(statistics);
                    _unitOfWork.TeamRoundGroupRepository.UpdateRange(firstTeamRoundGroups);
                    await _unitOfWork.SaveChangeAsync();
                    //Commit the transaction if everything was successful
                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = ex.Message;
                }
            }
            /*using (var transaction = await _unitOfWork.BeginTransactionAsync())
            {
                try
                {
                   
                }
                catch(Exception ex)
                {
                    await transaction.RollbackAsync();
                    response.Message = ex.Message;
                    return response;
                }
            }*/
            var matchIds =pickleballMatches.Select(m => m.Id).ToList();
            var sortedMatches = await _unitOfWork.PickleballMatchRepository.GetMatchesByIdsAsync(matchIds);
            List<PickleballMatchViewModel> matchesViewModel = new List<PickleballMatchViewModel>();
            foreach(var  match in sortedMatches)
            {
                PickleballMatchViewModel matchViewModel = match.ToMatchesViewModel();
                matchViewModel.NumberOfSet = firstRound.Tournament.NumberOfSets;               
                matchesViewModel.Add(matchViewModel);
            }
            response.Data = matchesViewModel;
            response.Success = true;
            response.Message = "List Pickleball Matches of remaining Rounds !";
            return response;
        }

        private string GetRoundName(int roundNumber, int totalRounds)
        {
            if (totalRounds == 1)
            {
                return "Final"; // Only one round, it's the Final
            }
            if (totalRounds == 2)
            {
                return roundNumber == 2 ? "Semi Final" : "Final"; // 2 rounds: Semi Final and Final
            }
            if (totalRounds == 3)
            {
                switch (roundNumber)
                {
                    case 2: return "Quarter Final";  // First round is Quarter Final
                    case 3: return "Semi Final";     // Second round is Semi Final
                    case 4: return "Final";          // Third round is Final
                    default: return $"Round {roundNumber}"; // Fallback (unlikely)
                }
            }

            // Logic for more than 3 rounds (handle the last three as Quarters, Semis, Final)
            int remainingRounds = totalRounds - roundNumber + 1;
            switch (remainingRounds)
            {
                case 3: return "Quarter Final";
                case 2: return "Semi Final";
                case 1: return "Final";
                default: return $"Round {roundNumber - 1}"; // For earlier rounds, just label as "Round X"
            }
        }
        public async Task<List<Team>> GetTopTeamsByPowerOfTwoAsync(uint roundId, int selectedPowerOfTwo)
        {
            var groupedRankedTeams = await _unitOfWork.TeamRoundGroupRepository.GetListRankedTeamStatistic(roundId);

            // Step 1: Sort the teams based on the statistics
            var sortedTeams = groupedRankedTeams.SelectMany(g => g.Value.OrderByDescending(t => t.Points)
                                                                        .ThenByDescending(t => t.SetDifference)
                                                                        .ThenByDescending(t => t.PointsDifference)
                                                                        .ThenByDescending(t => t.Wins)
                                                                        .ThenBy(t => t.Losses)
                                                                        .Take(1)) // Take the top team from each group
                                                .ToList();

            // Step 2: Adjust the number of teams to match the selected power of two
            if (sortedTeams.Count > selectedPowerOfTwo)
            {
                sortedTeams = sortedTeams.Take(selectedPowerOfTwo).ToList();
            }
            else if (sortedTeams.Count < selectedPowerOfTwo)
            {
                var additionalTeamsNeeded = selectedPowerOfTwo - sortedTeams.Count;

                var additionalTeams = groupedRankedTeams.SelectMany(g => g.Value.OrderByDescending(t => t.Points)
                                                                                .ThenByDescending(t => t.SetDifference)
                                                                                .ThenByDescending(t => t.PointsDifference)
                                                                                .ThenByDescending(t => t.Wins)
                                                                                .ThenBy(t => t.Losses)
                                                                                .Skip(1)) // Skip the top team already taken
                                                       .Take(additionalTeamsNeeded)
                                                       .ToList();

                sortedTeams.AddRange(additionalTeams);
            }

            // Step 3: Retrieve the corresponding `Team` entities using the repository
            var teamIds = sortedTeams.Select(t => t.TeamId).ToList();
            var teams = await _unitOfWork.TeamRepository.GetListTeamByIdAsync(teamIds);

            // Step 4: Maintain the order of teams as per sorting logic
            var orderedTeams = teams.OrderBy(t => teamIds.IndexOf(t.Id)).ToList();

            return orderedTeams;
        }

        private List<RoundGroup> GenerateRoundGroups(List<Round> rounds)
        {
            List<RoundGroup> roundGroups = new List<RoundGroup>();

            foreach (var round in rounds)
            {
                RoundGroup roundGroup = new RoundGroup
                {
                    RoundId = round.Id,
                    Round = round,
                    RoundGroupName = round.RoundName,
                    Matches = new List<PickleballMatch>(), // Ensure Matches is initialized
                    TeamRoundGroups = new List<TeamRoundGroup>(), // Initialize if necessary
                    Statistics = new List<TeamRoundGroupStatistics>() // Initialize if necessary
                };

                round.RoundGroups.Add(roundGroup);
                roundGroups.Add(roundGroup);
            }

            return roundGroups;
        }

        private List<TeamRoundGroup> GenerateTeamRoundGroup(RoundGroup roundGroup, List<Team> teams)
        {            
            List<TeamRoundGroup> teamRoundGroups = new List<TeamRoundGroup>();
            for(int i = 0; i < teams.Count; i++)
            {
                TeamRoundGroup teamRoundGroup = new TeamRoundGroup
                {
                    RoundGroupId = roundGroup.Id,
                    RoundGroup = roundGroup,
                    TeamId = teams[i].Id,
                    Team = teams[i],
                    TeamStatus = Domain.Enums.TeamStatus.TBD,
                    TeamRoundGroupStatistics = new TeamRoundGroupStatistics()                    
                };
                teamRoundGroups.Add(teamRoundGroup);
            }
            return teamRoundGroups;
        }

        private List<TeamRoundGroupStatistics> GenerateTeamStatistics(List<TeamRoundGroup> teamRoundGroups)
        {
            List<TeamRoundGroupStatistics> teamRoundGroupStatistics = new List<TeamRoundGroupStatistics>();
            for(int i = 0; i < teamRoundGroups.Count; i++)
            {
                TeamRoundGroupStatistics teamStatistics = new TeamRoundGroupStatistics
                {
                    TeamRoundGroup = teamRoundGroups[i],
                    TeamRoundGroupId = teamRoundGroups[i].Id,
                    Wins = 0,
                    Losses = 0,
                    Points = 0,
                    SetDifference = 0,
                    PointsDifference = 0
                };
                teamRoundGroupStatistics.Add(teamStatistics);
            }
            return teamRoundGroupStatistics;
        }

        private List<PickleballMatch> GenerateMatches(List<RoundGroup> roundGroups,int numTeams, ref int matchOrderCounter)
        {
            List<PickleballMatch> matches = new List<PickleballMatch>();
            int matchesInCurrentRound = numTeams / 2;
            foreach(var roundGroup in roundGroups)
            {
                for (int i = 0; i < matchesInCurrentRound; i++)
                {
                    PickleballMatch match = new PickleballMatch
                    {
                        RoundGroup = roundGroup,
                        RoundGroupId = roundGroup.Id, // Assuming RoundId is the Id of the Round entity
                        MatchStatus = Domain.Enums.MatchStatus.Scheduling,
                        MatchOrder = matchOrderCounter++,
                        FirstTeamId = null,
                        SecondTeamId = null,
                        MatchDate = DateTime.Now,
                    };
                    matches.Add(match);

                    if (roundGroup.Matches == null)
                    {
                        roundGroup.Matches = new List<PickleballMatch>();
                    }
                    roundGroup.Matches.Add(match);
                }
                matchesInCurrentRound /= 2;            
            }
            return matches;
        }
        private List<Set> GenerateSetsForMatch(List<PickleballMatch> matches, int numberOfSets)
        {
            List<Set> sets = new List<Set>();
            foreach (var match in matches)
            {
                for (int i = 1; i <= numberOfSets; i++)
                {
                    Set set = new Set
                    {
                        PickleballMatchId = match.Id,
                        FirstTeamScore = 0,
                        SecondTeamScore = 0,
                        SetOrder = i,
                        SetStatus = MatchStatus.Scheduling,
                        PickleballMatch = match,
                        WinningTeamId = null
                    };
                    sets.Add(set);
                }
            }
            return sets;
        }
    }
}
