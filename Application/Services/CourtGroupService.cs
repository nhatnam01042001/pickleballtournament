﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.CommentViewModel;
using Application.ViewModels.CourtGroupViewModels;
using Application.ViewModels.ParticipantViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class CourtGroupService : ICourtGroupService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CourtGroupService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ApiResponseModel<List<CourtGroupViewModel>>> GetCourtGroups()
        {
            var response = new ApiResponseModel<List<CourtGroupViewModel>>()
            {
                Data = null,
                Success = false
            };
            var courtGroups = await _unitOfWork.CourtGroupRepository.GetAllAsync();
            if (courtGroups is null)
            {
                response.Message = "List of Court Groups is empty";
                return response;
            }

            response.Success = true;
            response.Message = "List of Court Groups !";
            response.Data = _mapper.Map<List<CourtGroupViewModel>>(courtGroups);
            return response;
        }

        public async Task<ApiResponseModel<CourtGroupViewModel>> GetCourtGroupByID(uint id)
        {
            var response = new ApiResponseModel<CourtGroupViewModel>()
            {
                Data = null,
                Success = false
            };

            var courtGroup = await _unitOfWork.CourtGroupRepository.GetByIdAsync(id);
            if (courtGroup is null)
            {
                response.Message = "Court group is not found";
                return response;
            }

            response.Success = true;
            response.Message = "Court Group Data !";
            response.Data = _mapper.Map<CourtGroupViewModel>(courtGroup);
            return response;
        }

        public async Task<ApiResponseModel<CourtGroupViewModel>> AddCourtGroup(AddCourtGroupViewModel model)
        {
            var response = new ApiResponseModel<CourtGroupViewModel>()
            {
                Data = null,
                Success = false
            };

            var courtGroup = _mapper.Map<CourtGroup>(model);
            await _unitOfWork.CourtGroupRepository.AddAsync(courtGroup);
            await _unitOfWork.SaveChangeAsync();

            response.Success = true;
            response.Message = "Court Group Created !";
            response.Data = _mapper.Map<CourtGroupViewModel>(courtGroup);
            return response;
        }

        public async Task<ApiResponseModel<CourtGroupViewModel>> UpdateCourtGroup(uint id, UpdateCourtGroupViewModel model)
        {
            var response = new ApiResponseModel<CourtGroupViewModel>()
            {
                Data = null,
                Success = false
            };

            var existingCourtGroup = _unitOfWork.CourtGroupRepository.GetByIdAsync(id).Result;
            if (existingCourtGroup is null)
            {
                response.Message = "Court group is not found";
                return response;
            }

            var updateCourtGroup = _mapper.Map(model, existingCourtGroup);
            _unitOfWork.CourtGroupRepository.Update(updateCourtGroup);
            await _unitOfWork.SaveChangeAsync();

            response.Success = true;
            response.Message = "Court Group Updated !";
            response.Data = _mapper.Map<CourtGroupViewModel>(updateCourtGroup);
            return response;
        }

        public async Task<ApiResponseModel<CourtGroupViewModel>> DeleteCourtGroup(uint id)
        {
            var response = new ApiResponseModel<CourtGroupViewModel>()
            {
                Data = null,
                Success = false
            };

            var existingCourtGroup = _unitOfWork.CourtGroupRepository.GetByIdAsync(id).Result;
            if (existingCourtGroup is null)
            {
                response.Message = "Court group is not found";
                return response;
            }

            _unitOfWork.CourtGroupRepository.SoftRemove(existingCourtGroup);
            await _unitOfWork.SaveChangeAsync();

            response.Success = true;
            response.Message = "Court Group Updated !";
            return response;
        }

        public async Task<ApiResponseModel<List<CourtGroupViewModel>>> AddCourtGroupToCampaignAsync(uint campaignId, List<CourtGroupCampaignDto> courtGroupCampaignDtos)
        {
            var response = new ApiResponseModel<List<CourtGroupViewModel>>
            {
                Success = false,
                Data = null
            };

            // Fetch the campaign
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(campaignId);
            if (campaign == null)
            {
                response.Message = "Tournament Campaign does not exist!";
                return response;
            }

            // Get the court groups that are not yet associated with the campaign
            var existingCourtGroups = await _unitOfWork.CourtGroupRepository.GetCourtGroupNotExistedinCampaign(campaignId);
            var courtGroupIds = courtGroupCampaignDtos.Select(c => c.CourtGroupId).ToList();
            // Filter court groups to add (that are in courtGroupIds but not in the existing ones)
            var newCourtGroups = courtGroupIds
                .Where(cgId => !existingCourtGroups.Any(ecg => ecg.Id == cgId))
                .ToList();

            if (!newCourtGroups.Any())
            {
                response.Message = "No new court groups to add.";
                return response;
            }

            // Create the list of court group-tournament campaign associations
            var courtGroupTournamentCampaigns = newCourtGroups.Select(courtId => new CourtGroupTournamentCampaign
            {
                CourtGroupId = courtId,
                TournamentCampaign = campaign,
                TournamentCampaignId = campaign.Id
            }).ToList();

            try
            {
                // Add the new associations and save changes
                await _unitOfWork.CourtGroupCampaignRepository.AddRangeAsync(courtGroupTournamentCampaigns);
                await _unitOfWork.SaveChangeAsync();

                response.Success = true;
                response.Message = $"Added {courtGroupTournamentCampaigns.Count} court groups to campaign!";
                response.Data = courtGroupTournamentCampaigns.Select(cg => cg.CourtGroupViewModel()).ToList();
            }
            catch (Exception ex)
            {
                response.Message = $"Error: {ex.Message}";
            }

            return response;
        }
        public async Task<ApiResponseModel<List<CourtGroupViewModel>>> GetCourtGroupsNotExistedInCampaign(uint campaignId)
        {
            var response = new ApiResponseModel<List<CourtGroupViewModel>>
            {
                Success = true,
                Data = null
            };
            var courtGroups = await _unitOfWork.CourtGroupRepository.GetCourtGroupNotExistedinCampaign(campaignId);
            if(courtGroups is null)
            {
                response.Message = "List court groups is empty !";
                return response;
            }
            var campaign = await _unitOfWork.TournamentCampaignRepository.GetByIdAsync(campaignId);
            if (campaign is null)
            {
                response.Message = "Tournament Campaign does not existed !";
                return response;
            }
            response.Message = $"List Court Group in Campaign {campaign.TournamentName}";
            response.Data = courtGroups.Select(c => c.ToCourtGroupViewModel()).ToList();
            return response;
        }
    }
}
