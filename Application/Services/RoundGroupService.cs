﻿using Application.Interfaces;
using Application.ViewModels.RoundGroupViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class RoundGroupService : IRoundGroupService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoundGroupService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<RoundGroupViewModel>> CreateRoundGroup(uint roundId, string roundGroupName)
        {
            var response = new ApiResponseModel<RoundGroupViewModel>()
            {
                Data = null,
                Success = false
            };
            var round = await _unitOfWork.RoundRepository.GetByIdAsync(roundId);
            if(round is null)
            {
                response.Message = "Round does not existed !";
                return response;
            }
            RoundGroup roundGroup = new RoundGroup
            {
                RoundGroupName = roundGroupName,
                RoundId = roundId,
                Round = round,                
            };
            try
            {
                await _unitOfWork.RoundGroupRepository.AddAsync(roundGroup);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Create Round group success !";
                response.Data = new RoundGroupViewModel
                {
                    RoundGroupName = roundGroup.RoundGroupName,
                    RoundId = roundGroup.RoundId,
                    RoundGroupId = roundGroup.Id,
                    RoundName =round.RoundName
                    
                };
                return response;
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponseModel<List<RoundGroupViewModel>>> GetRoundGroupByRoundIdAsync(uint roundId)
        {
            var response = new ApiResponseModel<List<RoundGroupViewModel>>()
            {
                Success = false,
                Data = null
            };
            var listRoundGroup = await _unitOfWork.RoundGroupRepository.GetRoundGroupByRoundIdAsync(roundId);
            if(listRoundGroup == null)
            {
                return response;
            }
            var round = await _unitOfWork.RoundRepository.GetByIdAsync(roundId);
            List<RoundGroupViewModel> viewModels = new List<RoundGroupViewModel>();
            foreach (var roundGroup in listRoundGroup)
            {
                RoundGroupViewModel roundGroupViewModel = new RoundGroupViewModel
                {
                    RoundGroupName = roundGroup.RoundGroupName,
                    RoundId = roundGroup.RoundId,
                    RoundGroupId = roundGroup.Id,
                    RoundName = round.RoundName
                };
                viewModels.Add(roundGroupViewModel);
            }
            response.Success = true;
            response.Data = viewModels;
            response.Message = "List Round Group !";
            return response;
        }
    }
}
