﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;
using Application.ViewModels.EmailViewModels;
using Domain.Enums;

namespace Application.Services
{
    public class TournamentRegistrationService : ITournamentRegistrationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClaimsService _claimsService;
        private readonly AppConfiguration _configuration;
        public TournamentRegistrationService(IUnitOfWork unitOfWork, IClaimsService claimsService, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _claimsService = claimsService;
            _configuration = configuration;
        }

        public async Task<ApiResponseModel<TournamentRegistViewModel>> RegistToTournament(uint tournamentId, TournamentRegistDto tournamentRegistDto)
        {
            var response = new ApiResponseModel<TournamentRegistViewModel>()
            {
                Success = false,
                Data = null
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament == null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            if (tournament.TournamentCampaign.RegisterExpiredDate <= DateTime.Now)
            {
                response.Message = "Tournament Registration has already been expired !";
                return response;
            }
            if (tournament.TournamentStatus == Domain.Enums.MatchStatus.InProgress)
            {
                response.Message = "Tournament is in progress !";
                return response;
            }
            if(tournament.TournamentAthletes.Count == tournament.GetNumberOfAthletes())
            {
                response.Message = "Number of Athletes in Tournament has been full !";
                return response;
            }
            if (tournamentRegistDto.Email == null || tournamentRegistDto.PhoneNumber == null)
            {
                response.Message = "All fields are required !";
                return response;

            }
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(tournament.TournamentCampaignId);
            var athlete = await _unitOfWork.AthleteRepository.GetAthleteByEmailAndPhoneNumber(tournament.TournamentCampaignId, tournamentRegistDto.Email, tournamentRegistDto.PhoneNumber);
            if (!athletes.Any(a => a.Id == athlete.Id))
            {
                response.Message = "Athlete does not existed in Campaign ! Please regist campaign of tournament !";
                return response;
            }
            var exsitedAthlete = await _unitOfWork.TournamentAthleteRepository.GetSpecificTournamentAthlete(athlete.Id, tournament.Id);
            if (exsitedAthlete != null)
            {
                response.Message = "Athlete has already existed in tournament !";
                return response;
            }
            TournamentRegistration registration = new TournamentRegistration
            {
                TournamentId = tournament.Id,
                Tournament = tournament,
                AthleteId = athlete.Id,
                Athlete = athlete,
                Status = Domain.Enums.TournamentRegistrationStatus.WaitingApprove
            };
            try
            {
                await _unitOfWork.TournamentRegistrationRepository.AddAsync(registration);
                await _unitOfWork.SaveChangeAsync();
                response.Success = true;
                response.Message = "Tournament registration detail !";
                response.Data = new TournamentRegistViewModel
                {
                    Id = registration.Id,
                    AthleteId = athlete.Id,
                    AthleteName = athlete.GuestPlayer.FullName,
                    Gender = athlete.GuestPlayer.Gender,
                    Rank = athlete.GuestPlayer.Rank,
                    TournamentId = tournament.Id,
                    TournamentName = tournament.TournamentName,
                    RegistrationStatus = Domain.Enums.TournamentRegistrationStatus.WaitingApprove.ToString()
                };
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.ToString();
                return response;
            }
        }

        public async Task<ApiResponseModel<List<TournamentRegistViewModel>>> GetRegistrationByTournamentIdAsync(uint tournamentId)
        {
            var response = new ApiResponseModel<List<TournamentRegistViewModel>>()
            {
                Success = true,
                Data = null
            };
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var listRegistration = await _unitOfWork.TournamentRegistrationRepository.GetTournamentRegistrationsAsync(tournamentId);
            if (listRegistration is null)
            {
                response.Message = "List is empty !";
                return response;
            }
            List<TournamentRegistViewModel> listRegisViewModel = new List<TournamentRegistViewModel>();
            foreach (var regist in listRegistration)
            {
                TournamentRegistViewModel registViewModel = new TournamentRegistViewModel()
                {
                    Id = regist.Id,
                    AthleteId = regist.AthleteId,
                    AthleteName = regist.Athlete.GuestPlayer.FullName,
                    Gender = regist.Athlete.GuestPlayer.Gender,
                    Rank = regist.Athlete.GuestPlayer.Rank != null ? regist.Athlete.GuestPlayer.Rank : default,
                    TournamentId = tournament.Id,
                    TournamentName = tournament.TournamentName,
                    RegistrationStatus = regist.Status.ToString(),
                };
                listRegisViewModel.Add(registViewModel);
            }
            response.Data = listRegisViewModel;
            return response;
        }

        public async Task<ApiResponseModel<Pagination<TournamentRegistViewModel>>> GetRegistrationByTournamentIdPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<TournamentRegistViewModel>>()
            {
                Success = true,
                Data = null
            };
            var tournament = await _unitOfWork.TournamentRepository.GetByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            var listRegistration = await _unitOfWork.TournamentRegistrationRepository.GetTournamentRegistrationsAsync(tournamentId);
            if (listRegistration is null)
            {
                response.Message = "List is empty !";
                return response;
            }
            response.Message = $"List Tournament {tournament.TournamentName} registrations !";
            response.Data = listRegistration.ToPagination();
            return response;
        }

        public async Task<ApiResponseModel<AthleteViewModel>> ParticipateTournamentForUser(uint tournamentId)
        {
            var response = new ApiResponseModel<AthleteViewModel>()
            {
                Data = null,
                Success = false
            };
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(tournamentId);
            if (tournament is null)
            {
                response.Message = "Tournament does not existed !";
                return response;
            }
            if(tournament.TournamentCampaign.RegisterExpiredDate <= DateTime.Now)
            {
                response.Message = "Tournament Registration has already been expired !";
                return response;
            }
            if (tournament.TournamentAthletes.Count == tournament.GetNumberOfAthletes())
            {
                response.Message = "Number of Athletes in Tournament has been full !";
                return response;
            }
            if (tournament.TournamentStatus == Domain.Enums.MatchStatus.InProgress)
            {
                response.Message = "Tournament is in progress !";
                return response;
            }
            var userId = _claimsService.GetCurrentUserId;
            if (userId == 0)
            {
                response.Message = "Participate tournament for user required login !";
                return response;
            }
            var user = await _unitOfWork.UserRepository.GetByIdAsync(userId);
            if (user is null)
            {
                response.Message = "User does not existed !";
                return response;
            }
            else
            {
                if (user.IsDeleted)
                {
                    response.Message = "User's account has been deleted !";
                    return response;
                }
                var athletes = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(tournament.TournamentCampaignId);
                if (!athletes.Any(a => a.UserId == user.Id))
                {
                    response.Message = "Athlete does not existed in campaign ! Please regist campaign of tournament !";
                    return response;
                }
                var tournamentAthletes = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tournamentId);
                if (tournamentAthletes.Any(t => t.UserId == user.Id))
                {
                    response.Message = "Athelte is already existed in tournament !";
                    return response;
                }
                var athlete = athletes.Where(a => a.UserId == user.Id).FirstOrDefault();
                TournamentAthlete tournamentAthlete = new TournamentAthlete()
                {
                    TournamentId = tournament.Id,
                    Tournament = tournament,
                    AthleteId = athlete.Id,
                    Athlete = athlete,
                };
                try
                {
                    await _unitOfWork.TournamentAthleteRepository.AddAsync(tournamentAthlete);
                    await _unitOfWork.SaveChangeAsync();
                    response.Success = true;
                    response.Message = "Participate Tournament success !";
                    AthleteViewModel athleteViewModel = athlete.ToAthleteViewModel();
                    athleteViewModel.TournamentId = tournamentId.ToString();
                    athleteViewModel.TournamentCampaignId = tournament.TournamentCampaignId.ToString();
                    response.Data = athleteViewModel;
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message;
                    return response;
                }
            }
            return response;
        }

        public async Task<ApiResponseModel<EmailResponseViewModel>> UpdateRegistrationStatus(uint registId, string status)
        {
            var response = new ApiResponseModel<EmailResponseViewModel>()
            {
                Success = false,
                Data = null
            };
            var regist = await _unitOfWork.TournamentRegistrationRepository.GetByIdAsync(registId);
            if (regist is null)
            {
                response.Message = "Regist does not existed !";
                return response;
            }
            if (regist.Status != Domain.Enums.TournamentRegistrationStatus.WaitingApprove)
            {
                response.Message = "Registration has been updated ! Can not re-update !";
                return response;
            }
            var tournament = await _unitOfWork.TournamentRepository.GetTournamentByIdAsync(regist.TournamentId);
            var athletes = await _unitOfWork.AthleteRepository.GetAthletesByCampaignIdAsync(regist.Tournament.TournamentCampaignId);
            if (!athletes.Any(a => a.Id == regist.AthleteId))
            {
                response.Message = "Athlete does not existed in campaign ! Please regist campaign of tournament !";
            }
            var managerEmail = _configuration.ManagerEmailForSend;
            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(managerEmail.Email, managerEmail.Password),
            };
            var message = new MailMessage();
            var athlete = athletes.Where(a => a.Id == regist.AthleteId).FirstOrDefault();
            switch (status)
            {
                case "Approved":
                    regist.Status = Domain.Enums.TournamentRegistrationStatus.Approved;
                    int maxAthletes = tournament.FormatType switch
                    {
                        FormatType.MenSingles or FormatType.WomenSingles => tournament.NumberOfTeams,
                        FormatType.MenDual or FormatType.WomenDual or FormatType.DualMixed => tournament.NumberOfTeams * 2,
                        _ => 0
                    };

                    var athletesInTournament = await _unitOfWork.TournamentAthleteRepository.GetAthleteByTournamentIdAsync(tournament.Id);
                    // Check if the tournament is full
                    if (athletesInTournament.Count >= maxAthletes)
                    {
                        response.Message = "The number of athletes in this tournament is full!";
                        return response;
                    }
                    TournamentAthlete tournamentAthlete = new TournamentAthlete()
                    {
                        AthleteId = athlete.Id,
                        Athlete = athlete,
                        TournamentId = tournament.Id,
                        Tournament = tournament
                    };
                    using (var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            _unitOfWork.TournamentRegistrationRepository.Update(regist);
                            await _unitOfWork.TournamentAthleteRepository.AddAsync(tournamentAthlete);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();

                            message.Subject = "Athlete Information !";
                            message.Body = $"AthleteId: {athlete.Id}\n" +
                   $"Full Name: {athlete.GuestPlayer.FullName}\n" +
                   $"Tournament : {tournament.TournamentName}\n" +
                   $"Format Type : {tournament.FormatType.ToString()}\n" +
                   $"Tournaemnt Type : {tournament.TournamentType.ToString()} \n" +
                   $"Tournament Campaign : {tournament.TournamentCampaign.TournamentName}\n" +
                   $"Athlete Type : Guest Player\n" +
                   $"Gender : {athlete.GetAthleteGender}";
                            message.From = new MailAddress(managerEmail.Email);

                            message.To.Add(new MailAddress(athlete.GuestPlayer.Email));
                            await smtpClient.SendMailAsync(message);
                            response.Message = "Registration has been approved !";
                            
                        }
                        catch (SmtpException smtpEx)
                        {
                            response.Message = $"Failed to send email: {smtpEx.Message}";
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = ex.Message;
                            return response;
                        }
                    }
                    break;
                case "Denied":
                    regist.Status = Domain.Enums.TournamentRegistrationStatus.Denied;
                    regist.IsDeleted = true;
                    try
                    {
                        _unitOfWork.TournamentRegistrationRepository.Update(regist);
                        await _unitOfWork.SaveChangeAsync();

                        message.Subject = $"{tournament.TournamentName} tournament registration announcement !";
                        message.Body = $"You registration to {tournament.TournamentName} tournament has been denied by Tournament manager !";
                        message.From = new MailAddress(managerEmail.Email);

                        message.To.Add(new MailAddress(athlete.GuestPlayer.Email));
                        await smtpClient.SendMailAsync(message);
                        response.Message = "Registration has been denied !";
                        
                    }
                    catch (SmtpException smtpEx)
                    {
                        response.Message = $"Failed to send email: {smtpEx.Message}";
                    }
                    catch (Exception ex)
                    {
                        response.Message = ex.Message;
                        return response;
                    }
                    break;
                default:
                    response.Message = "Invalid status !";
                    return response;
            }
            response.Success = true;
            response.Data = new EmailResponseViewModel
            {
                SenderEmail = managerEmail.Email,
                ReceiverEmail = athlete.GuestPlayer.Email,
                Message = message.Body
            };
            return response;
        }
    }
}
