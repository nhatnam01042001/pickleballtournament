﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.AccountViewModels;
using Application.ViewModels.EmailViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class EmailService : IEmailService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly string _emailForSend = "netjprogram@gmail.com";
        private readonly string _appPasswordConfiguration = "nruuhifutemjxgac";
        private readonly AppConfiguration _configuration;

        public EmailService(IUnitOfWork unitOfWork, AppConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }
        public async Task SendEmailAsync(EmailDto emailDto)
        {
            //emailDto.To = await _unitOfWork.UserRepository.GetListEmail();
            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_emailForSend, _appPasswordConfiguration),
            };

            var message = new MailMessage
            {
                Subject = emailDto.Subject,
                Body = emailDto.Body,
                From = new MailAddress(_emailForSend),
            };



            message.To.Add(new MailAddress("minhducbin54@gmail.com"));


            await smtpClient.SendMailAsync(message);

        }

        public async Task<ApiResponseModel<EmailResponseViewModel>> SendAccountDetailsAsync(AccountSendingDetails accountDetails)
        {
            var response = new ApiResponseModel<EmailResponseViewModel>()
            {
                Success = false,
                Data = null
            };

            try
            {
                var managerEmail = _configuration.ManagerEmailForSend;
                var account = await _unitOfWork.AccountRepository.GetAccountByUserNameAsync(accountDetails.UserName);
                if (account == null)
                {
                    response.Message = "UserName does not exist!";
                    return response;
                }
                if(account.User.Email == null)
                {
                    response.Message = "Account does not has email !";
                    return response;
                }
                var passwordHash = accountDetails.Password + account.Salt;
                if (account.PasswordHash != passwordHash.Hash())
                {
                    response.Message = "Password of the account is incorrect!";
                    return response;
                }

                var smtpClient = new SmtpClient
                {
                    Port = 587,
                    EnableSsl = true,
                    Host = "smtp.gmail.com",
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(managerEmail.Email, managerEmail.Password),
                };

                var message = new MailMessage
                {
                    Subject = "Account Information!",
                    Body = $"Username: {accountDetails.UserName}" +
                           $"Password: {accountDetails.Password}",
                    From = new MailAddress(managerEmail.Email),
                };

                message.To.Add(new MailAddress(account.User.Email));

                await smtpClient.SendMailAsync(message);

                response.Success = true;
                response.Message = "Email sent successfully!";
                response.Data = new EmailResponseViewModel
                {
                    SenderEmail = managerEmail.Email,
                    ReceiverEmail = account.User.Email,
                    Message = message.Body
                };
            }
            catch (SmtpException smtpEx)
            {
                response.Message = $"Failed to send email: {smtpEx.Message}";
            }
            catch (Exception ex)
            {
                response.Message = $"An error occurred: {ex.Message}";
            }

            return response;
        }

        public async Task SendPasswordResetEmail(string toEmail, string username, string newPassword)
        {
            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_emailForSend, _appPasswordConfiguration),
            };

            var message = new MailMessage
            {
                Subject = "Password Reset",
                Body = $"Your username is: {username}\nYour new password is: {newPassword}",
                From = new MailAddress(_emailForSend),
            };

            message.To.Add(new MailAddress(toEmail));

            await smtpClient.SendMailAsync(message);
        }
    }
}
