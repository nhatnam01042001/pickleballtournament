﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.UserRegistrationViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Services
{
    public class UserRegistrationService : IUserRegistrationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserRegistrationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<ApiResponseModel<Pagination<UserRegistrationViewModel>>> GetUserRegistrationPagingsion(int pageIndex = 0, int pageSize = 10)
        {
            var response = new ApiResponseModel<Pagination<UserRegistrationViewModel>>()
            {
                Success = true,
                Data = null
            };
            var registration = await _unitOfWork.UserRegistrationRepository.ToPagination(pageIndex, pageSize);
            if(registration is null)
            {
                response.Message = "List user registraion is empty !";
                return response;
            }            
            response.Data = registration.ToPagingsionViewModel();
            response.Message = "List of user registration !";
            return response;
        }

        public async Task<ApiResponseModel<UserRegistrationViewModel>> GetRegistrationById(uint registId)
        {
            var response = new ApiResponseModel<UserRegistrationViewModel>
            {
                Success = false,
                Data = null
            };
            var registration = await _unitOfWork.UserRegistrationRepository.GetByIdAsync(registId);
            if(registration is null)
            {
                response.Message = "Registration does not existed !";
                return response;
            }
            response.Data = registration.ToViewModel();
            response.Message = "User Registration detail !";
            response.Success = true;
            return response;
        }

        public async Task<ApiResponseModel<UserRegistrationViewModel>> RegistUser(RegistUserDto registUserDto)
        {
            var response = new ApiResponseModel<UserRegistrationViewModel>
            {
                Success = false,
                Data = null
            };
            if (!registUserDto.Email.ValidateEmail())
            {
                response.Message = "Invalid email type ! Please try again !";
                return response;
            }
            if (!registUserDto.PhoneNumber.ValidatePhone())
            {
                response.Message = "Invalid phone number ! Please try again !";
                return response;
            }
            if(await _unitOfWork.UserRegistrationRepository.CheckExistedUserRegistration(registUserDto.Email,registUserDto.PhoneNumber))
            {
                response.Message = "Email or phone has been used to regist ! Please try again !";
                return response;
            }
            var userRegistration = registUserDto.ToUserRegistration();
            try
            {
                await _unitOfWork.UserRegistrationRepository.AddAsync(userRegistration);
                await _unitOfWork.SaveChangeAsync();
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
                return response;
            }
            response.Success = true;
            response.Data = userRegistration.ToViewModel();
            response.Message = "Regist success ! Please wait for approved !";
            return response;
        }

        public async Task<ApiResponseModel<UserRegistrationViewModel>> UpdateRegistration(uint id, UpdateUserReigstDto userReigstDto)
        {
            var response = new ApiResponseModel<UserRegistrationViewModel>()
            {
                Data = null,
                Success = false
            };
            var registration = await _unitOfWork.UserRegistrationRepository.GetByIdAsync(id);
            if (registration is null)
            {
                response.Message = "Registration does not existed !";
                return response;
            }
            if(registration.Status == Domain.Enums.TournamentRegistrationStatus.Approved)
            {
                response.Message = "Registration has already been approved !";
                return response;
            }
            switch(userReigstDto.Status)
            {
                case "Approved":
                    registration.Status = Domain.Enums.TournamentRegistrationStatus.Approved;
                    User user = new User
                    {
                        FullName = registration.FullName,
                        Email = registration.Email,
                        PhoneNumber = registration.PhoneNumber,
                        Role = Domain.Enums.Role.Athlete,
                        Address = registration.Address,
                        Gender = registration.Gender,
                        Registration = registration,
                        DateOfBirth = registration.DateOfBirth
                    };
                    using(var transaction = await _unitOfWork.BeginTransactionAsync())
                    {
                        try
                        {
                            await _unitOfWork.UserRepository.AddAsync(user);
                            await _unitOfWork.SaveChangeAsync();
                            registration.UserId = user.Id;
                            registration.User = user;
                            _unitOfWork.UserRegistrationRepository.Update(registration);
                            await _unitOfWork.SaveChangeAsync();
                            await transaction.CommitAsync();
                        }
                        catch (Exception ex)
                        {
                            await transaction.RollbackAsync();
                            response.Message = ex.Message;
                            return response;
                        }
                    }
                    break;
                case "Denied":
                    registration.Status = Domain.Enums.TournamentRegistrationStatus.Denied;
                    try
                    {
                        _unitOfWork.UserRegistrationRepository.Update(registration);
                        await _unitOfWork.SaveChangeAsync();
                    }
                    catch (Exception ex)
                    {
                        response.Message = ex.Message;
                        return response;
                    }
                    break;
                default:
                    response.Message = "Invalid status provided ! Nothing changed !";
                    return response; ;
            }            
            response.Success = true;
            response.Data = registration.ToViewModel();
            response.Message = "Update user registration success !";
            return response;
        }
    }
}
