﻿using Application.Commons;
using Application.ViewModels.TournamentRankingViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ITournamentRankingTeamService
    {
        public Task<ApiResponseModel<Pagination<RankingTeamModel>>> GetTournamentRankingTeamPaging(uint tournamentId, int pageIndex = 0, int pageSize = 10);
    }
}
