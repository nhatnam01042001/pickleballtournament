﻿using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.RoundViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IRoundService
    {
        public Task<ApiResponseModel<List<RoundViewModel>>> GetRoundByTournamentIdAsync(uint tournamentId);
        public Task<ApiResponseModel<List<PickleballMatchViewModel>>> GenerateRoundsTournament(CreateRoundsDto createRoundsDto);
    }
}
