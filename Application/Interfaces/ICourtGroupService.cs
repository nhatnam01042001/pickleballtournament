﻿using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.CourtGroupViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ICourtGroupService
    {
        public Task<ApiResponseModel<List<CourtGroupViewModel>>> GetCourtGroups();
        public Task<ApiResponseModel<CourtGroupViewModel>> GetCourtGroupByID(uint id);
        public Task<ApiResponseModel<CourtGroupViewModel>> AddCourtGroup(AddCourtGroupViewModel model);
        public Task<ApiResponseModel<CourtGroupViewModel>> UpdateCourtGroup(uint id, UpdateCourtGroupViewModel model);
        public Task<ApiResponseModel<CourtGroupViewModel>> DeleteCourtGroup(uint id); Task<ApiResponseModel<List<CourtGroupViewModel>>> GetCourtGroupsNotExistedInCampaign(uint campaignId);
        public Task<ApiResponseModel<List<CourtGroupViewModel>>> AddCourtGroupToCampaignAsync(uint campaignId, List<CourtGroupCampaignDto> courtGroupCampaignDtos);
    }
}
