﻿using Application.Commons;
using Application.ViewModels.AccountViewModels;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IAccountService
    {
        public Task<ApiResponseModel<EmailResponseViewModel>> CreateAccountAsync(uint userId, CreateAccountDTO accountObject);
        public Task<ApiResponseModel<string?>> LoginAsync(UserLoginDTO userObject);
        public Task<Account> CreateManagerAccount();
        public Task<ApiResponseModel<string?>> LoginManagerAccountAsync(UserLoginDTO accountObject);
        Task<ApiResponseModel<string>> ChangePasswordAsync(uint userId, ChangePasswordDTO changePasswordDTO);
    }
}
