﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ICampaignRegistService
    {
        public Task<ApiResponseModel<CampaignRegistViewModel>> RegistToTournament(uint campaignId, CampaignRegistrationDTO campaignRegistDTO);
        public Task<ApiResponseModel<EmailResponseViewModel>> UpdateRegistrationStatus(uint id, string status);
        public Task<ApiResponseModel<AthleteViewModel>> ParticipateCampaignForUser(uint campaignId);
        public Task<ApiResponseModel<List<CampaignRegistViewModel>>> GetListRegistrationByCampaignId(uint campaignId);
        public Task<ApiResponseModel<Pagination<CampaignRegistViewModel>>> GetCampaignRegistrationPagingsions(uint campaignId, int pageIndex = 0, int pageSize = 10);
    }
}
