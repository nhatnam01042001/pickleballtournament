﻿using Application.Commons;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IUserService
    {
        public Task RegisterAsync(UserRegisterDto userObject);
        public Task<UserViewModel> UpdateUserProfile(uint id, UpdatedUserDTO updatedUser);
        
        public Task<List<GetUserDTO>> GetUsers();
        Task<Pagination<GetUserDTO>> GetUsersPaged(int pageIndex, int pageSize);
        public Task<GetUserDTO> GetUserById(uint id);
        public Task CreateUser(CreateUserDTO createUserDTO);
        //public Task UpdateUser(uint id, UpdatedUserDTO updateUserDTO);
        public Task DeleteUser(uint id);
        public Task UpdateManagerAsync(uint id, UpdateManagerDTO updateManagerDTO);
        public Task<ApiResponseModel<EmailResponseViewModel>> ResetPasswordAsync(ResetPasswordDto resetPasswordDto);
        public Task<ApiResponseModel<Pagination<UserViewModel>>> GetUserNotInCampaignAsync(uint campaignId, int pageIndex = 0, int pageSize = 10);
        public Task<ApiResponseModel<UserViewModel>> GetCurrentUser();
    }
}
