﻿using Application.ViewModels.SetViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ISetService
    {
        //public Task<ApiResponseModel<List<SetViewModel>>> CreateSetsForMatch(uint matchId, IEnumerable<UpdateSetDto> createSetDtos);
        public Task<ApiResponseModel<List<SetViewModel>>> GetSetsOfMatchAsync(uint matchId);
        public Task<ApiResponseModel<SetViewModel>> UpdateSet(uint setId, UpdateSetDto setDto);
    }
}
