﻿using Application.Commons;
using Application.ViewModels.CourtViewModels;
using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IPickleballMatchService
    {
        public Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetMatchesByTournamentIdAsync(uint tournamentId);
        public Task<ApiResponseModel<PickleballMatchViewModel>> AddSingleTeamToMatch(uint matchId, AssignPlayerViewModel viewModel);
        public Task<ApiResponseModel<PickleballMatchViewModel>> AssignDoubleTeamToMatch(uint matchId, AssignTeamsViewModel teamDTO);
        public Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchResult(uint matchId, UpdateMatchResultDTO matchDTO);
        public Task<ApiResponseModel<NextRoundMatchViewModel>> GetTeamsForAdvancingMatches(uint matchId);
        public Task<ApiResponseModel<NextRoundMatchViewModel>> AssignTeamsForNextRoundMatch(uint matchId, AssignTeamsNextRoundMatch teamsDTO);
        public Task<ApiResponseModel<NextRoundMatchViewModel>> AssignTeamsForNextRoundMatch1(uint matchId, AssignTeamsNextRoundMatch teamsDTO);
        public Task<ApiResponseModel<PickleballMatchViewModel>> GetMatchByIdAsync(uint matchId);
        public Task<ApiResponseModel<List<PickleballMatchViewModel>>> GenerateMatchesForRoundGroup(uint roundGroupId);
        public Task<ApiResponseModel<PickleballMatchViewModel>> UpdateRoundGroupMatchResult(uint matchId, UpdateMatchResultDTO matchDTO);
        public Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetMatchesByRoundGroupId(uint roundGroupId);
        public Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetMatchesByRoundId(uint roundId);
        public Task<ApiResponseModel<List<PickleballMatchViewModel>>> GetNextRoundsMatchesByTournamentId(uint tournamentId);
        public Task<ApiResponseModel<PickleballMatchViewModel>> AssignTeamToMatch(uint matchId, AssignTeamsDto teamsDto);
        public Task<ApiResponseModel<Pagination<PickleballMatchViewModel>>> GetMatchesByTournamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10);
        public Task<ApiResponseModel<Pagination<PickleballMatchViewModel>>> GetNextRoundMatchesInTournamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10);
        public Task<ApiResponseModel<Pagination<PickleballMatchViewModel>>> GetMatchesByRoundIdPagingsions(uint roundId, int pageIndex = 0, int pageSize = 10);
        Task<ApiResponseModel<PickleballMatchViewModel>> AddCourtToMatch(uint matchId, CourtIdViewModel courtIdViewModel);
        Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchDate(uint matchId, UpdateMatchDateDto updateMatchDateDto);
        Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchStatus(uint matchId, UpdateMatchStatusDto matchStatusDto);
        public Task<ApiResponseModel<PickleballMatchViewModel>> UpdateMatchDetails(uint matchId, UpdateMatchDetailDto matchDetailDto);
    }
}

