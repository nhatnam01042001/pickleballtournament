﻿using Application.ViewModels.TournamentViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ITournamentService
    {
        public Task<ApiResponseModel<TournamentViewModel>> CreateTournament(CreateTournamentDTO createTournamentDTO);
        public Task<ApiResponseModel<TournamentViewModel>> GetTournamentByIdAsync(uint tournamentId);
        public Task<ApiResponseModel<List<TournamentViewModel>>> GettAllTournamentAsync(uint campaignId);
        Task<ApiResponseModel<TournamentViewModel>> UpdateTournamentStatus(uint tournamentId, UpdateTournamentStatusDto tournamentStatusDto);
    }
}
