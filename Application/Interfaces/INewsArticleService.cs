﻿using Application.Commons;
using Application.ViewModels.CourtGroupViewModels;
using Application.ViewModels.NewsArticleViewModels;
using Microsoft.AspNetCore.Http;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces;

public interface INewsArticleService
{
    public Task<ApiResponseModel<List<NewsArticleViewModel>>> GetNewsArticles();
    public Task<ApiResponseModel<NewsArticleViewModel>> GetNewsArticlesById(uint id);
    public Task<Pagination<NewsArticleViewModel>> GetNewsArticlesPaging(int pageIndex = 0, int pageSize = 10);
    public Task<ApiResponseModel<NewsArticleViewModel>> AddNewsArticle(AddNewsArticleViewModel model);
    public Task<ApiResponseModel<NewsArticleViewModel>> UpdateNewsAritcle(uint id, UpdateNewsArticleViewModel model);
    public Task<ApiResponseModel<NewsArticleViewModel>> DeleteNewsArticle(uint id);
}