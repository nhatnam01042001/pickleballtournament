﻿using Application.Commons;
using Application.ViewModels.UserRegistrationViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IUserRegistrationService
    {
        public Task<ApiResponseModel<Pagination<UserRegistrationViewModel>>> GetUserRegistrationPagingsion(int pageIndex = 0, int pageSize = 10);
        public Task<ApiResponseModel<UserRegistrationViewModel>> GetRegistrationById(uint registId);
        public Task<ApiResponseModel<UserRegistrationViewModel>> RegistUser(RegistUserDto registUserDto);
        public Task<ApiResponseModel<UserRegistrationViewModel>> UpdateRegistration(uint id, UpdateUserReigstDto userReigstDto);

    }
}
