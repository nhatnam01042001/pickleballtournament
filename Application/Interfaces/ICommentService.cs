﻿using Application.Commons;
using Application.ViewModels.CommentViewModel;
using Application.ViewModels.CourtGroupViewModels;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces;

public interface ICommentService
{
    public Task<ApiResponseModel<List<CommentViewModel>>> GetComments();
    public Task<ApiResponseModel<List<CommentViewModel>>> GetCommentByTournamentId(uint id);
    public Task<ApiResponseModel<CommentViewModel>> GetCommentById(uint id);
    public Task<ApiResponseModel<CommentViewModel>> AddComment(AddCommentViewModel model);
    public Task<ApiResponseModel<CommentViewModel>> UpdateComment(uint id, UpdateCommentViewModel model);
    public Task<ApiResponseModel<bool>> DeleteComment(uint id);
    public Task<ApiResponseModel<bool>> SoftDeleteComment(uint id);
    public Task<ApiResponseModel<Pagination<CommentViewModel>>> GetCommentByTournamentIdPaging(uint tournamentId, int pageIndex = 0, int pageSize = 10);
}