﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.ParticipantViewModels;
using Application.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IAthleteService
    {
       // public Task<ApiResponseModel<List<AthleteViewModel>>> AddUserPlayer(uint tournamentId);
        //public Task<ApiResponseModel<List<AthleteViewModel>>> AddGuestPlayer(uint tournamentId);
        public Task<ApiResponseModel<List<AthleteViewModel>>>? GetTournamentParticipantsAsync(uint tournamentId);
        public Task<ApiResponseModel<List<AthleteViewModel>>>? GetNonTeamAthletes(uint tournamentId);
        public Task<ApiResponseModel<List<AthleteViewModel>>> GetAthletesByCampaignIdAsync(uint campaignId);
        public Task<ApiResponseModel<List<AthleteViewModel>>> GetAthleteByTournamentIdAsync(uint tounamentId);
        public Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetAthletesInTourmamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10);
        public Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetTournamentParticipantsPagingsions(uint campaignId, int pageIndex = 0, int pageSize = 10);
        public Task<ApiResponseModel<List<AthleteViewModel>>> AddUserToCampaign(uint campaignId, List<UserIdViewModel> users);
        public Task<ApiResponseModel<List<AthleteViewModel>>> AddAthletesToTournament(uint tournamentId, List<AthleteIdViewModel> athleteIds);
        public Task<ApiResponseModel<Pagination<AthleteViewModel>>> GetAthletesNotInTournamentPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10);
    }
}
