﻿using Application.ViewModels.RoundGroupViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IRoundGroupService
    {
        public Task<ApiResponseModel<RoundGroupViewModel>> CreateRoundGroup(uint roundId, string roundGroupName);
        public Task<ApiResponseModel<List<RoundGroupViewModel>>> GetRoundGroupByRoundIdAsync(uint roundId);
    }
}
