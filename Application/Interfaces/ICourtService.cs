﻿using Application.ViewModels.CourtViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ICourtService
    {
        Task<ApiResponseModel<List<CourtViewModel>>> GetCourts();
        Task<ApiResponseModel<CourtViewModel>> GetCourtById(uint id);
        Task<ApiResponseModel<List<CourtViewModel>>> AddCourtToCourtGroup(uint courtGroupId, IEnumerable<CreateCourtDTO> courtDTOs);
        Task<ApiResponseModel<CourtViewModel>> UpdateCourt(uint id, UpdateCourtDTO updateCourtDTO);
        Task<ApiResponseModel<CourtViewModel>> DeleteCourt(uint id);
        Task<ApiResponseModel<List<CourtViewModel>>> GetCourtsByCourtGroupId(uint courtGroupId);
        Task<ApiResponseModel<List<CourtViewModel>>> GetAvailableCourts(uint matchId);
    }
}