﻿using Application.ViewModels.WinConditionViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IWinCondtionService
    {
        public Task<ApiResponseModel<List<WinConditionViewModel>>> GetAllWinConditionAsync();
        public Task<ApiResponseModel<WinConditionViewModel>> CreateWinConditionAsync(InsertOrUpdateWinConditionDTO winConditionDTO);
        public Task<ApiResponseModel<WinConditionViewModel>> GetWinConditionById(uint id);
        public Task<ApiResponseModel<WinConditionViewModel>> UpdateWinConditionAsync(uint id, InsertOrUpdateWinConditionDTO winConditionDTO);

    }
}
