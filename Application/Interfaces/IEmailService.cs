﻿using Application.Commons;
using Application.ViewModels.AccountViewModels;
using Application.ViewModels.EmailViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailDto emailDto);
        Task<ApiResponseModel<EmailResponseViewModel>> SendAccountDetailsAsync(AccountSendingDetails accountDetails);
        Task SendPasswordResetEmail(string toEmail, string username, string newPassword);
    }
}
