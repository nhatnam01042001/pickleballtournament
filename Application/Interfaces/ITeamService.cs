﻿using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ITeamService
    {
        public Task<ApiResponseModel<List<SingleTeamViewModel>>> AddSingleTeamList(uint tournamentId);

        public Task<ApiResponseModel<List<SingleTeamViewModel>>> GetSingleTeams(uint tournamentId);
        public Task<ApiResponseModel<TeamGroupViewModel>> AddSingleTeamToGroup(uint roundGroupId, uint athleteId);
        public Task<ApiResponseModel<TeamGroupViewModel>> AddDoubleTeamToGroup(uint roundGroupId, AssignPlayerViewModel assignPlayerDto);
        public Task<ApiResponseModel<TeamGroupViewModel>> UpdateSingleTeam(uint teamRoundGroupId, uint athleteId);
        public Task<ApiResponseModel<TeamGroupViewModel>> UpdateDoubleTeamToGroup(uint teamRoundGroupId, AssignPlayerViewModel assignPlayerDto);
        public Task<ApiResponseModel<List<int>>> GetListNumberOfAdvancedTeam(uint roundId);
        public Task<ApiResponseModel<List<TeamViewModel>>> GetTeamsWithoutMatch(uint roundId);
    }
}
