﻿using Application.ViewModels.TeamRoundGroupViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ITeamRoundGroupService
    {
        public Task<ApiResponseModel<List<TeamRoundGroupViewModel>>> GetTeamRoundGroupByGroupId(uint roundGroupId);
        public Task<ApiResponseModel<List<int>>> GetNumberOfAdvanceTeamsInRound(uint roundId);
        public Task<ApiResponseModel<Dictionary<uint, List<TeamRoundGroupViewModel>>>> GetRankedGroupTeam(uint roundId);
    }
}
