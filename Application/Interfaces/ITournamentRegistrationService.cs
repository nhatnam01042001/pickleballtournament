﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.EmailViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ITournamentRegistrationService
    {
        public Task<ApiResponseModel<TournamentRegistViewModel>> RegistToTournament(uint tournamentId, TournamentRegistDto tournamentRegistDto);
        public Task<ApiResponseModel<List<TournamentRegistViewModel>>> GetRegistrationByTournamentIdAsync(uint tournamentId);
        public Task<ApiResponseModel<AthleteViewModel>> ParticipateTournamentForUser(uint tournamentId);
        public Task<ApiResponseModel<EmailResponseViewModel>> UpdateRegistrationStatus(uint registId, string status);
        public Task<ApiResponseModel<Pagination<TournamentRegistViewModel>>> GetRegistrationByTournamentIdPagingsions(uint tournamentId, int pageIndex = 0, int pageSize = 10);
    }
}
