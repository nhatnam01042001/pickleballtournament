﻿using Application.Commons;
using Application.ViewModels.TournamentCampaignViewModels;
using Application.ViewModels.TournamentsViewModels;
using Application.ViewModels.TournamentViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.ApiResponseModels;

namespace Application.Interfaces
{
    public interface ITournamentCampaignService 
    {
        public Task<List<TournamentCampaignViewModel>> GetTournamentCampaignsAsync();
        //public Task<TournamentCampaignViewModel?> CreateTournamentCampaignAsync(CreateTournamentCampaignViewModel tournament);
        public Task<Pagination<TournamentCampaignViewModel>> GetTournamentCampaignPagingAsync(int pageIndex = 0, int pageSize = 10);
        public Task<TournamentCampaignViewModel>? GetTournamentCampaignByIdAsync(uint id);
        public Task<ApiResponseModel<TournamentCampaignViewModel>> CreateTournamentCampaignAsync(CreateTournamentCampaignDto creatCampaignDto);
        public Task<ApiResponseModel<TournamentCampaignViewModel>> UpdateCampaignStatus(uint campaignId, UpdateCampaignStatusDto campaignStatusDto);
        public Task<ApiResponseModel<TournamentCampaignViewModel>> UpdateCampaignDate(uint campaignId, UpdateCampaignDateDto campaignDateDto);
    }
}
