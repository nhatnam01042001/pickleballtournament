﻿using Application.Interfaces;
using Application.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace Application
{
    public interface IUnitOfWork
    {
        public IAthleteRepository AthleteRepository { get; }
        public IPickleballMatchRepository PickleballMatchRepository { get; }
        public ITeamRepository TeamRepository { get; }
        public ITournamentCampaignRepository TournamentCampaignRepository { get; }
        public ICampaignRegistRepository CampaignRegistRepository { get; }
        public IUserRepository UserRepository { get; }
        public IAccountRepository AccountRepository { get; }
        public ITournamentRepository TournamentRepository { get; }
        public IRoundRepository RoundRepository { get; }        
        public IRoundGroupRepository RoundGroupRepository { get; }
        public ITeamRoundGroupRepository TeamRoundGroupRepository { get; }
        public IWinConditionRepository WinConditionRepository { get; }
        public Task<int> SaveChangeAsync();
        public Task<IDbContextTransaction> BeginTransactionAsync();
        public Task CommitAsync();
        public Task RollbackAsync();
        public ICommentRepository CommentRepository { get; }
        public ICourtRepository CourtRepository { get; }
        public INewsArticleRepository NewsArticleRepository { get; }
        public ICourtGroupRepository CourtGroupRepository { get; }
        public IGroupStatisticRepository GroupStatisticRepository { get; }
        public ITournamentAthleteRepository TournamentAthleteRepository { get; }
        public ITournamentRegistrationRepository TournamentRegistrationRepository { get; }
        public ISetRepository SetRepository { get; }
        public IUserRegistrationRepository UserRegistrationRepository { get; }
        public ICourtGroupCampaignRepository CourtGroupCampaignRepository { get; }
        public ITournamentRankingTeamRepository TournamentRankingTeamRepository { get; }
    }
}
