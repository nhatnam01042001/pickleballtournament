﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RoundViewModels
{
    public class RoundViewModel
    {
        public uint RoundId { get; set; }
        public int RoundOrder { get; set; }
        public string RoundName { get; set; }
        public uint TournamentId { get; set; }
        public string TournamentName { get; set; }  
        public string RoundStatus { get; set; }
        //public int NumberOfTeams { get; set; }
    }
}
