﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RoundViewModels
{
    public class CreateRoundsDto
    {
        public uint TournamentId { get; set; }
        public int NumberOfTeams { get; set; }
    }
}
