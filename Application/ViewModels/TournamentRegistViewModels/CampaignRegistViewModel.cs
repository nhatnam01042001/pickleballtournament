﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentRegistViewModels
{
    public class CampaignRegistViewModel
    {
        public uint Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public double Rank { get; set; }
        public string RegistrationStatus { get; set; }
        public uint TournamentId { get; set; }
    }
}
