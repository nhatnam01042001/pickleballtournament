﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentRegistViewModels
{
    public class CampaignRegistrationDTO
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public double Rank { get; set; }
        public uint TournamentCampaignId { get; set; }
    }
}
