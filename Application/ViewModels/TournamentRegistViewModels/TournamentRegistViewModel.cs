﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentRegistViewModels
{
    public class TournamentRegistViewModel
    {
        public uint Id { get; set; }
        public uint AthleteId { get; set; }
        public string AthleteName { get; set; }
        public double Rank { get; set; }    
        public string Gender { get; set; }
        public uint TournamentId { get; set; }
        public string TournamentName { get; set; }
        public string RegistrationStatus { get; set; }
    }
}
