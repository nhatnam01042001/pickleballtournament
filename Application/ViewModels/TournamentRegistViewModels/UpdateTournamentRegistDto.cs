﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentRegistViewModels
{
    public class UpdateTournamentRegistDto
    {
        public string Status { get; set; }
    }
}
