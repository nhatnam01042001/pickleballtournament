﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CourtViewModels
{
    public class CourtIdViewModel
    {
        public uint CourtId { get; set; }
    }
}
