﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CourtViewModels
{
    public class UpdateCourtDTO
    {
        public string CourtName { get; set; }
        public uint? CourtGroupId { get; set; }
    }
}
