﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.SetViewModels
{
    public class SetViewModel
    {
        public uint SetId { get; set; }
        public uint MatchId { get; set; }
        public uint? FirstTeamId { get; set; }
        public int FirstTeamScore { get; set; }
        public string FirstTeamName { get; set; }
        public uint? SecondTeamId { get; set; }
        public int? SecondTeamScore { get; set; }
        public string SecondTeamName { get; set; }
        public uint? WinningTeamId { get; set; }
        public string? WinningTeamName { get; set; }
        public string? SetStatus { get; set; }
        public int SetOrder { get; set; }

    }
}
