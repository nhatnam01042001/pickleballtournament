﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.SetViewModels
{
    public class UpdateSetDto
    {
        public int FirstTeamScore { get; set; }
        public int SecondTeamScore { get; set; }
    }
}
