﻿using Domain.Enums;

namespace Application.ViewModels.NewsArticleViewModels;

public class UpdateNewsArticleViewModel
{
    public string? NewsTitle { get; set; }
    public string? SubTitle { get; set; }
    public string? NewsContent { get; set; }
    public bool NewsArticleStatus { get; set; }
    public NewsType NewsType { get; set; }
    public string? ImageUrl { get; set; }
}