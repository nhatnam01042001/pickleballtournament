﻿using Domain.Enums;
using Microsoft.AspNetCore.Http;

namespace Application.ViewModels.NewsArticleViewModels;

public class AddNewsArticleViewModel
{
    public string? NewsTitle { get; set; }
    public string? SubTitle { get; set; }
    public string? NewsContent { get; set; }
    public bool NewsArticleStatus { get; set; }
    public NewsType NewsType { get; set; }
    public string? ImageUrl { get; set; }
}