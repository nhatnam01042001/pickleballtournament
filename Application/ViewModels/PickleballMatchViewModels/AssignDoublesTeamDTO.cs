﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class AssignDoublesTeamDTO
    {
        public AssignPlayerViewModel FirstTeam { get; set; }
        public AssignPlayerViewModel SecondTeam { get; set; }
    }
}
