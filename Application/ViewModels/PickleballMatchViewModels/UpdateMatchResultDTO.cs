﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class UpdateMatchResultDTO
    {
        public uint WinningTeamId { get; set; }
        public uint WinConditionId { get; set; }
    }
}
