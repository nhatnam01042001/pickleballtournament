﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class UpdateMatchDateDto
    {
        public DateTime? MatchDate { get; set; }
        public uint? CourtId { get; set; }
    }
}
