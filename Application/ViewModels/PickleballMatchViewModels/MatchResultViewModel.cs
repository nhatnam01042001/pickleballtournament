﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class MatchResultViewModel
    {
        public uint MatchId { get; set; }
        public uint WinningTeamId { get; set; }
        public string TeamName { get; set; }
    }
}
