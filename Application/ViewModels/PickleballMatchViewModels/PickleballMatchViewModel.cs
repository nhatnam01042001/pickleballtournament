﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class PickleballMatchViewModel
    {
        public uint MatchId { get; set; }
        public string? Court { get; set; }
        public uint? CourtId { get; set; }
        public uint TournamentId { get; set; }
        public uint RoundId { get; set; }
        public int RoundOrder {  get; set; }
        public uint RoundGroupId { get; set; }
        public int MatchOrder { get; set; }
        public uint? FirstTeamId { get; set; }
        public string? FirstTeam { get; set; }
        public uint? SecondTeamId { get; set; }
        public string? SecondTeam { get; set; }
        public uint? WinningTeamId { get; set; }
        public string? WinningTeam { get; set; }
        public uint? WinConditionId { get; set; }
        public DateTime MatchDate { get; set; }        
        public string? WinCondition { get; set; }
        public int NumberOfSet { get; set; }
        public string? MatchStatus { get; set; }
    }
}
