﻿using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class NextRoundMatchViewModel
    {
        public uint Id { get; set; }
        public int MatchOrder { get; set; }
        public string? Round {  get; set; }
        public uint RoundGroupId { get; set; }
        public uint RoundId { get; set; }
        public List<TeamViewModel>? FirstTeams { get; set; }
        public List<TeamViewModel>? SecondTeams { get; set; }
    }
}
