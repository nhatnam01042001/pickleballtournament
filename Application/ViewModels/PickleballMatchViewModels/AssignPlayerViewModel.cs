﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PickleballMatchViewModels
{
    public class AssignPlayerViewModel
    {
        public uint? FirstAthleteId { get; set; }
        public uint? SecondAthleteId { get; set; }
    }
}
