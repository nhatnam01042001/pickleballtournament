﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CourtGroupViewModels
{
    public class CourtGroupBasicDetail
    {
        public uint CourtGroupId { get; set; }
        public string? CourtGroupName { get; set; }
    }
}
