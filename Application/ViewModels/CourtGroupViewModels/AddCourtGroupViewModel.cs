﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CourtGroupViewModels
{
    public class AddCourtGroupViewModel
    {
        public string CourtGroupName { get; set; }
        public string Address { get; set; }
        public string? EmailContact { get; set; }
        public string? PhoneNumber { get; set; }
        public string? latitude { get; set;}
        public string? longitude { get; set;}
    }
}
