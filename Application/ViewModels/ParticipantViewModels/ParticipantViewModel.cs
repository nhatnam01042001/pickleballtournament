﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ParticipantViewModels
{
    public class ParticipantViewModel
    {
        public uint Id { get; set; }
        public string ParticipantName { get; set; }
        public double? Rank { get; set; }
        public string Gender { get; set; }
        public string ParticipantType { get; set; }
    }
}
