﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.WinConditionViewModels
{
    public class WinConditionViewModel
    {
        public uint WinConditionId { get; set; }
        public string? ConditionName { get; set; }
        public string? Description { get; set; }
    }
}
