﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RoundGroupViewModels
{
    public class CreateRoundGroupDto
    {
        public string RoundGroupName { get; set; }
    }
}
