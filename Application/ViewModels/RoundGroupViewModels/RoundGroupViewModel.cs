﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RoundGroupViewModels
{
    public class RoundGroupViewModel
    {
        public string RoundGroupName { get; set; }
        public string RoundName { get; set; }
        public uint RoundId { get; set; }     
        public uint RoundGroupId { get; set; }
    }
}
