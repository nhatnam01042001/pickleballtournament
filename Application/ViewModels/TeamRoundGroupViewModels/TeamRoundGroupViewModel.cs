﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamRoundGroupViewModels
{
    public class TeamRoundGroupViewModel
    {
        public uint TeamId { get; set; }
        public string TeamName { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int Draws { get; set; }
        public int Points { get; set; }
        public int SetDifference { get; set; }
        public int PointsDifference { get; set; }
        public string TeamStatus { get; set; }
        public uint TeamRoundGroupId { get; set; }
        //public uint RoundGroupId { get; set; }
    }
}
