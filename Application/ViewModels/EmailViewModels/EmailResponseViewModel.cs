﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.EmailViewModels
{
    public class EmailResponseViewModel
    {
        public string SenderEmail { get; set; }
        public string ReceiverEmail {  get; set; }
        public string Message { get; set; }
    }
}
