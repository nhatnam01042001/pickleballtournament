﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentRankingViewModel
{
    public class RankingTeamModel
    {
        public uint TournamentId { get; set; }
        public string TournamentName { get; set; }
        public uint TeamId { get; set; }
        public string TeamName { get; set; }
        public string? AchievedRound { get; set; }
        public int Rank { get; set; }
        public int MatchWins { get; set; }
        public int MatchLosses { get; set; }
        public int SetDifference { get; set; }
        public int ScoresDifference { get; set; }
        public int TotalPoints { get; set; }
        public double PrizeMoney { get; set; }
    }
}
