﻿namespace Application.ViewModels.UserViewModels;


public class CreateUserDTO
{
   
    public string FullName { get; set; }
    public string Gender { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public double? Rank { get; set; }
    public string UserName { get; set; } = string.Empty;
    public string Password { get; set; }
}