﻿using Domain.Enums;

namespace Application.ViewModels.UserViewModels;



public class GetUserDTO
{
    public uint Id { get; set; }
    public string? FullName { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public string? Address { get; set; }
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
    public string? Gender { get; set; }
    public double? Rank { get; set; }
    public string? ImageUrl { get; set; }
    public UserStatus Status { get; set; } = UserStatus.Active;
}