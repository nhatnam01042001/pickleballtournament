﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Enums;

namespace Application.ViewModels.UserViewModels
{
    public class UpdatedUserDTO
    {
        public string? FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string? Address { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Gender { get; set; }
        public string? ImageUrl { get; set; }
        public UserStatus Status { get; set; } = UserStatus.Active;
    }
}
