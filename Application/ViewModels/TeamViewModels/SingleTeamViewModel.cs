﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class SingleTeamViewModel
    {
        public uint TeamId { get; set; }    
        public string TeamName { get; set; }
        public string? AverageRank { get; set; }
        public string TeamMember {  get; set; }
        public string TeamType { get; set; }
    }
}
