﻿using Application.ViewModels.PickleballMatchViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class AssignTeamsViewModel
    {
        public AssignPlayerViewModel? FirstTeam { get; set; }
        public AssignPlayerViewModel? SecondTeam { get; set; }
    }
}
