﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class TeamGroupViewModel
    {
        public uint TeamId { get; set; }
        public string TeamName { get; set; }
        public uint RoundGroupId { get; set; }
        public string RoundGroupName { get; set; }
        public uint TeamRoundGroupId { get; set; }
        public int MatchWin { get; set; }
        public int MatchDraw { get; set; }
        public int MatchLose { get; set; }
        public int Point {  get; set; }
    }
}
