﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class UpdateSingleTeamDto
    {
        public uint TeamId { get; set; }
    }
}
