﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class TeamViewModel
    {
        public uint TeamId { get; set; }
        public string? TeamName { get; set; }
    }
}
