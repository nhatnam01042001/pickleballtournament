﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class AssignTeamsDto
    {
        public uint? FirstTeamId { get; set; }
        public uint? SecondTeamId { get; set; }
    }
}
