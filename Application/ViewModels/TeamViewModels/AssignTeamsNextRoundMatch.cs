﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TeamViewModels
{
    public class AssignTeamsNextRoundMatch
    {
        public uint? FirstTeamId { get; set; }
        public uint? SecondTeamId { get; set; }
        public uint? FirstMatchWinConditionId { get; set; }
        public uint? SecondMatchWinConditionId { get; set; }
    }
}
