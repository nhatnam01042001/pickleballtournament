﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AthleteViewModels
{
    public class AthleteViewModel
    {
        public uint Id { get; set; }
        public string AthleteName { get; set; }
        public double? Rank { get; set; }
        public string Gender { get; set; }
        public string AthleteType { get; set; }
        public string? TournamentId { get; set; }
        public string? TournamentCampaignId { get; set; }
    }
}
