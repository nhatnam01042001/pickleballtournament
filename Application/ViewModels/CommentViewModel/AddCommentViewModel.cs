﻿namespace Application.ViewModels.CommentViewModel;

public class AddCommentViewModel
{
    public string? CommentText { get; set; }
    public uint TournamentId { get; set; }
    public uint AccountId { get; set; }

}