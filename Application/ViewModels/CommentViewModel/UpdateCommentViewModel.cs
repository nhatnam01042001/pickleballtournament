﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CommentViewModel
{
    public class UpdateCommentViewModel
    {
        public string? CommentText { get; set; }

    }
}
