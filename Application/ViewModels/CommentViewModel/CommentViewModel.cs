﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.CommentViewModel
{
    public class CommentViewModel
    {
        public uint Id { get; set; }
        public uint UserId { get; set; }
        public string? FullName { get; set; }
        public string? CommentText { get; set; }
        public uint TournamentId { get; set; }
        public uint CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
        public bool? IsDeleted { get; set; }
        public uint? DeleteBy { get; set; }
        public string? ImageUrl { get; set; }

    }
}
