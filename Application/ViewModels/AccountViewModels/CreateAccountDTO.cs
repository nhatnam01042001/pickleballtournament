﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.AccountViewModels
{
    public class CreateAccountDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
