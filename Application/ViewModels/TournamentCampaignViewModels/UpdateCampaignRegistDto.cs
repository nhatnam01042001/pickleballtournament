﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentCampaignViewModels
{
    public class UpdateCampaignRegistDto
    {
        public string Status { get; set; }
    }
}
