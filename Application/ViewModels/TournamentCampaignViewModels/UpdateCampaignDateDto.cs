﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentCampaignViewModels
{
    public class UpdateCampaignDateDto
    {
        public DateTime? RegistrationExpiredDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set;}
    }
}
