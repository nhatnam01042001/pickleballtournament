﻿using Application.ViewModels.CourtGroupViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentViewModels
{
    public class TournamentCampaignViewModel
    {
        public uint Id { get; set; }
        public string TournamentName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RegistrationExpiredDate { get; set; }
        public List<CourtGroupBasicDetail>? Location { get; set; }
        public DateTime CreationDate { get; set; }

        public DateTime? ModificationDate { get; set; }

        public DateTime? DeletionDate { get; set; }
        public string? ImageUrl { get; set; }        
        public string? CampaignStatus { get; set; }
    }
}
