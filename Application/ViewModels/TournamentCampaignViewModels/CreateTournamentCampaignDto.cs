﻿using Application.ViewModels.CourtGroupViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentsViewModels
{
    public class CreateTournamentCampaignDto
    {
        public string TournamentName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string RegistrationExpiredDate { get; set; }
        //public string Location { get; set; }
        public string? ImageUrl { get; set; }
        public List<CourtGroupCampaignDto>? CourtGroups { get; set; }
    }
}
