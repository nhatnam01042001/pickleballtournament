﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentViewModels
{
    public class UpdateTournamentStatusDto
    {
        public string? TournamentStatus {  get; set; }
    }
}
