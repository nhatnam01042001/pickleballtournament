﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.TournamentViewModels
{
    public class CreateTournamentDTO
    {
        public string? TournamentName { get; set; }
        public string FormatType{ get; set; }
        public string TournamentType { get; set; }
        public int NumberOfTeams { get; set; }
        public int NumberOfSets { get; set; }
        public double Rank { get; set; }
        public uint TournamentCampaignId { get; set; }
        public string? ImageUrl { get; set; }
    }
}

