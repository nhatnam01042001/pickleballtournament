﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commons
{
    public class AppConfiguration
    {
        public string DatabaseConnection { get; set; }
        public JWTSection JWTSection { get; set; }
        public ManagerAccount ManagerAccount { get; set; }
        public ManagerEmailForSend ManagerEmailForSend { get; set; }
    }
}
