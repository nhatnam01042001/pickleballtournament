﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class DoubleUtils
    {
        public static double GetAthleteRank(this Athlete athlete)
        {
            if (athlete.User is not null)
            {
                return (double)athlete.User.Rank;
            }
            return athlete.GuestPlayer.Rank;
        }

        public static double GetTeamAverageRank(this Team team)
        {
            // Ensure that the FirstAthlete and SecondAthlete properties are not null
            if (team.FirstAthlete == null && team.SecondAthlete == null)
            {
                throw new InvalidOperationException("Both athletes in the team cannot be null.");
            }

            // Get the rank of the first athlete
            double firstAthleteRank = team.FirstAthlete != null ? team.FirstAthlete.GetAthleteRank() : 0;

            // Get the rank of the second athlete, if present
            double secondAthleteRank = team.SecondAthlete != null ? team.SecondAthlete.GetAthleteRank() : 0;

            // Calculate the average rank. If there's only one athlete, return their rank
            if (team.SecondAthlete == null)
            {
                return firstAthleteRank;
            }
            else
            {
                return (firstAthleteRank + secondAthleteRank) / 2;
            }
        }
    }
}
