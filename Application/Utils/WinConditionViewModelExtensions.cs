﻿using Application.ViewModels.WinConditionViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class WinConditionViewModelExtensions
    {
        public static WinConditionViewModel ToViewModel(this WinCondition winCondition)
        {
            return new WinConditionViewModel
            {
                WinConditionId = winCondition.Id,
                ConditionName = winCondition.ConditionName,
                Description = winCondition.Description,
            };
        }
        public static WinCondition ToWinCondition(this InsertOrUpdateWinConditionDTO winConditionDTO)
        {
            return new WinCondition
            {
                ConditionName = winConditionDTO.ConditionName,
                Description = winConditionDTO.Description,
            };
        }

        public static void UpdateWinCondition(this WinCondition winCondition, InsertOrUpdateWinConditionDTO winConditionDTO)
        {
            winCondition.ConditionName = winConditionDTO.ConditionName;
            winCondition.Description = winConditionDTO.Description;
        }
    }
}
