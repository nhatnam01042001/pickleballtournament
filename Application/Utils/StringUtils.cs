﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class StringUtils
    {
        public static string Hash(this string input)
        {
            SHA256 hash256 = SHA256.Create();
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            byte[] hash = hash256.ComputeHash(inputBytes);

            IEnumerable<string> hex = hash.Select(x => x.ToString("x2")); // chuyển sang cơ số 16(hex) nó sẽ trả về 64 ký tự hex

            string passwordHash = string.Join("", hex);
            return passwordHash;
        }
        public static string GenerateSalt()
        {
            byte[] randomBytes = RandomNumberGenerator.GetBytes(15); // random 15 bytes
            IEnumerable<string> saltHex = randomBytes.Select(x => x.ToString("x2"));
            return string.Join("", saltHex);

        }

        public static string FormatDoubleNumber(this double number)
        {
            return number.ToString("0.0", System.Globalization.CultureInfo.InvariantCulture);
        }
        public static string GetAthleteGender(this Athlete athlete)
        {
            if (athlete.User is not null)
            {
                return athlete.User.Gender;
            }
            return athlete.GuestPlayer.Gender;
        }

        public static string GetTeamName(this Team team)
        {
            var teamName = "";

            if (team.FirstAthlete != null)
            {
                if (team.FirstAthlete.User != null)
                {
                    teamName += team.FirstAthlete.User.FullName;
                }
                else if (team.FirstAthlete.GuestPlayer != null)
                {
                    teamName += team.FirstAthlete.GuestPlayer.FullName;
                }
            }

            if (team.SecondAthlete != null)
            {
                if (team.SecondAthlete.User != null)
                {
                    teamName += " & " + team.SecondAthlete.User.FullName;
                }
                else if (team.SecondAthlete.GuestPlayer != null)
                {
                    teamName += " & " + team.SecondAthlete.GuestPlayer.FullName;
                }
            }

            return teamName;
        }
    }
}
