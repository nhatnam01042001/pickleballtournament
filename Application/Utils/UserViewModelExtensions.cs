﻿using Application.Commons;
using Application.ViewModels.TournamentViewModels;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class UserViewModelExtensions
    {
        public static void UpdatedUserViewModel(this User user, UpdatedUserDTO updatedUser)
        {
            user.FullName = updatedUser.FullName;
            string dateFormat = "dd-MM-yyyy";
            if (DateTime.TryParseExact(updatedUser.DateOfBirth, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDate))
            {
                user.DateOfBirth = parsedDate;
            }
            else
            {
                Console.WriteLine("Invalid date format.");
            }
            user.Address = updatedUser.Address;
            user.Email = updatedUser.Email;
            user.PhoneNumber = updatedUser.PhoneNumber;
            user.Gender = updatedUser.Gender;
            user.ImageUrl = updatedUser.ImageUrl;
        }

        public static UserViewModel ToUserViewModel(this User user)
        {
            return new UserViewModel
            {
                Id = user.Id,
                FullName = user.FullName,
                DateOfBirth = user.DateOfBirth,
                Address = user.Address,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Gender = user.Gender,
                Rank = user.Rank.HasValue ? user.Rank.Value : default,
                Role = user.Role,
                ImageUrl = user.ImageUrl
            };
        }

        public static Pagination<UserViewModel> ToPagination(this List<User> users,int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = users.Count();
            var items = users.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<UserViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items.Select(u => u.ToUserViewModel()).ToList(),
            };
            return result;
        }
    }
}
