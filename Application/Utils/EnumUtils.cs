﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class FormatTypeExtensions
    {
        public static FormatType ToFormatType(this string type)
        {
            switch (type)
            {
                case "Men's Single":
                    return FormatType.MenSingles;

                case "Women's Single":
                    return FormatType.WomenSingles;

                case "Dual Mix":
                    return FormatType.DualMixed;

                case "Men Dual":
                    return FormatType.MenDual;

                case "Women Dual":
                    return FormatType.WomenDual;
                default:
                    throw new ArgumentException("Invalid Format Type !");
            }
        }
        public static TournamentType ToTournamentType(this string type)
        {
            switch (type)
            {
                case "Elimination":
                    return TournamentType.Elimination;
                case "Group Stage":
                    return TournamentType.GroupStage;
                default:
                    throw new ArgumentException("Invalid Tournament Type !");
            }
        }

        public static string ToFormatTypeString(this FormatType type)
        {
            switch (type)
            {
                case 0:
                    return "Men's Single";
                case FormatType.WomenSingles:
                    return "Women's Single";
                case FormatType.DualMixed:
                    return "Dual Mix";
                case FormatType.MenDual:
                    return "Men Dual";
                case FormatType.WomenDual:
                    return "Women Dual";
                default:
                    throw new ArgumentException("Invalid Format Type !");
            }
        }

        public static MatchStatus ToMatchStatus(this string type)
        {
            switch (type)
            {
                case "Scheduling":
                    return MatchStatus.Scheduling;
                case "Scheduled":
                    return MatchStatus.Scheduled;
                case "Postponed":
                    return MatchStatus.Postponed;
                case "InProgress":
                    return MatchStatus.InProgress;
                case "Canceled":
                    return MatchStatus.Canceled;
                case "Completed":
                    return MatchStatus.Completed;
                default:
                    throw new ArgumentException("Invalid Match status provided !");
            }
        }
        public static MatchStatus ToCampaignStatus(this string type)
        {
            switch (type)
            {
                case "Scheduling":
                    return MatchStatus.Scheduling;
                case "Scheduled":
                    return MatchStatus.Scheduled;
                case "Postponed":
                    return MatchStatus.Postponed;
                case "InProgress":
                    return MatchStatus.InProgress;
                case "Canceled":
                    return MatchStatus.Canceled;
                default:
                    throw new ArgumentException("Invalid Match status provided !");
            }
        }
    }
}
