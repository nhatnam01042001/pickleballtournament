﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.CommentViewModel;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class CommentViewModelExtensions
    {
        public static CommentViewModel ToCommentViewModel(this Comment comment)
        {
            return new CommentViewModel
            {
                Id = comment.Id,
                UserId = comment.Account?.UserId ?? 0,
                FullName = comment.Account?.User?.FullName,
                ImageUrl = comment.Account?.User?.ImageUrl,
                CommentText = comment.CommentText,
                TournamentId = comment.TournamentId,
                CreatedBy = comment.CreatedBy,
                CreateDate = comment.CreateDate,
                IsDeleted = comment.IsDeleted,
                DeleteBy = comment.DeleteBy
            };
        }

        public static Pagination<CommentViewModel> ToPagination(this List<Comment> comments,int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = comments.Count();
            var items = comments.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<CommentViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items.Select(c => c.ToCommentViewModel()).ToList(),
            };
            return result;
        }
    }
}
