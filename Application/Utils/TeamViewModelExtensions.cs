﻿using Application.ViewModels.PickleballMatchViewModels;
using Application.ViewModels.TeamViewModels;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class TeamViewModelExtensions
    {
        /*public static SingleTeamViewModel ToTeamViewModel(this Team team)
        {
            if (team.FirstParticipant.User is not null)
            {
                return new SingleTeamViewModel
                {
                    TeamId = team.FirstParticipant.User.Id,
                    TeamName = team.TeamName,
                    AverageRank = team.AverageRank,
                    TeamMember = team.FirstParticipant.User.FullName,
                    TeamType = team.TeamType.ToString()
                };
            }
            else if(team.FirstParticipant.GuestPlayer is not null)
            {
                return new SingleTeamViewModel
                {
                    TeamId = team.FirstParticipant.GuestPlayer.Id,
                    TeamName = team.TeamName,
                    AverageRank = team.AverageRank,
                    TeamMember = team.FirstParticipant.GuestPlayer.FullName,
                    TeamType = team.TeamType.ToString()
                };
            }
            else
            {
                throw new Exception("Invalid team member: both User and GuestPlayer are null.");
            }
        }*/
        public static void UpdateTeam(this Team team, Team updateTeam)
        {
            team.TeamName = updateTeam.TeamName;
            team.AverageRank = updateTeam.AverageRank;
            team.FirstAthleteId = updateTeam.FirstAthleteId;
            team.FirstAthlete = updateTeam.FirstAthlete;
            team.SecondAthleteId = updateTeam.SecondAthleteId;
            team.SecondAthlete = updateTeam.SecondAthlete;
            team.TeamRoundGroups = updateTeam.TeamRoundGroups;
        }

        public static Team ToTeam(this Athlete athlete)
        {
            if (athlete.User != null)
            {
                return new Team
                {
                    //TeamName = athlete.User.FullName,
                    FirstAthleteId = athlete.User.Id,
                    FirstAthlete = athlete,
                    //AverageRank = (double)athlete.User.Rank,
                    //TeamType = Domain.Enums.TeamType.MenSingle,                    
                    TeamRoundGroups = new List<TeamRoundGroup>(),
                };
            }
            else
            {
                return new Team
                {
                    //TeamName = athlete.GuestPlayer.FullName,
                    FirstAthleteId = athlete.GuestPlayer.Id,
                    FirstAthlete = athlete,
                    //AverageRank = athlete.GuestPlayer.Rank,
                    //TeamType = Domain.Enums.TeamType.MenSingle,
                    TeamRoundGroups = new List<TeamRoundGroup>()
                };
            }
        }

        /*public static Team CreateTeam(Athlete firstAthlete, Athlete secondAthlete)
        {
            return new Team
            {
                FirstAthleteId = firstAthlete.Id,
                AverageRank = firstAthlete.GetAthleteRank(),
                SecondAthleteId = secondAthlete.Id,
                FirstAthlete = firstAthlete,
                SecondAthlete = secondAthlete,
            };
        }*/

        public static Team CreateTeam(Athlete firstAthlete, Athlete? secondAthlete = null)
        {
            double averageRank = secondAthlete == null
                ? firstAthlete.GetAthleteRank()
                : (firstAthlete.GetAthleteRank() + secondAthlete.GetAthleteRank()) / 2;

            return new Team
            {
                FirstAthleteId = firstAthlete.Id,
                AverageRank = averageRank,
                SecondAthleteId = secondAthlete?.Id,
                FirstAthlete = firstAthlete,
                SecondAthlete = secondAthlete,
            };
        }
        public static Team CreateDoubleTeam(Athlete firstAthlete, Athlete secondAthlete)
        {
            double averageRank = (firstAthlete.GetAthleteRank() + secondAthlete.GetAthleteRank()) / 2;

            return new Team
            {
                FirstAthleteId = firstAthlete.Id,
                AverageRank = averageRank,
                SecondAthleteId = secondAthlete?.Id,
                FirstAthlete = firstAthlete,
                SecondAthlete = secondAthlete,                
            };
        }
        public static AssignPlayerViewModel SanitizeAssignPlayerViewModel(this AssignPlayerViewModel playerViewModel)
        {
            if (playerViewModel == null)
                return null;

            if (!playerViewModel.FirstAthleteId.HasValue || playerViewModel.FirstAthleteId.Value == 0)
            {
                playerViewModel.FirstAthleteId = null;
            }

            if (!playerViewModel.SecondAthleteId.HasValue || playerViewModel.SecondAthleteId.Value == 0)
            {
                playerViewModel.SecondAthleteId = null;
            }

            return playerViewModel;
        }

        public static TeamViewModel ToTeamViewModel(this Team team)
        {
            return new TeamViewModel
            {
                TeamId = team.Id,
                TeamName = team.TeamName
            };
        }
    }
}
