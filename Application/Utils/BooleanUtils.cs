﻿using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class BooleanUtils
    {
        public static bool IsValidRegistrationStatus(string input, out TournamentRegistrationStatus status)
        {
            return Enum.TryParse(input, true, out status) && Enum.IsDefined(typeof(TournamentRegistrationStatus), status);
        }

        public static bool IsValidDate(string dateString, string dateFormat, out DateTime parsedDate)
        {
            return DateTime.TryParseExact(
                dateString,
                dateFormat,
                CultureInfo.InvariantCulture,
                DateTimeStyles.None,
                out parsedDate);
        }
        public static bool CompareTime(DateTime date1, DateTime date2) => date1 < date2;

        public static bool CheckValidateTeam(this Team team, FormatType type)
        {
            var firstAthleteGender = team.FirstAthlete.GetAthleteGender();
            //var firstAthleteRank = team.FirstAthlete.GetAthleteRank();
            //var secondAthleteRank = team.SecondAthlete?.GetAthleteRank();
            var secondAthleteGender = team.SecondAthlete?.GetAthleteGender();

            bool isTeamValid = false;

            switch (type)
            {
                case FormatType.MenSingles:
                    if (team.SecondAthlete == null && firstAthleteGender == "Male" /*&& AreDoublesEqual(firstAthleteRank, rank)*/)
                    {
                        isTeamValid = true;
                    }
                    break;

                case FormatType.WomenSingles:
                    if (team.SecondAthlete == null && firstAthleteGender == "Female" /*&& AreDoublesEqual(firstAthleteRank, rank)*/)
                    {
                        isTeamValid = true;
                    }
                    break;

                case FormatType.MenDual:
                    if (secondAthleteGender != null && firstAthleteGender == "Male" && secondAthleteGender == "Male")
                    {
                        isTeamValid = true;
                    }
                    break;

                case FormatType.WomenDual:
                    if (secondAthleteGender != null && firstAthleteGender == "Female" && secondAthleteGender == "Female")
                    {
                        isTeamValid = true;
                    }
                    break;

                case FormatType.DualMixed:
                    if (secondAthleteGender != null && ((firstAthleteGender == "Male" && secondAthleteGender == "Female") || (firstAthleteGender == "Female" && secondAthleteGender == "Male")))
                    {
                        isTeamValid = true;
                    }
                    break;

                default:
                    return false;
            }

            return isTeamValid;
        }

        public static bool AreDoublesEqual(double value1, double value2, double tolerance = 1e-10)
        {
            return Math.Abs(value1 - value2) < tolerance;
        }

        public static bool CheckValidateGender(this string gender, FormatType type)
        {
            bool isValid = false;
            switch (type)
            {
                case FormatType.MenSingles:
                    if (gender != null && gender == "Male")
                    {
                        isValid = true;
                    }
                    break;

                case FormatType.WomenSingles:
                    if (gender != null && gender == "Female")
                    {
                        isValid = true;
                    }
                    break;

                case FormatType.MenDual:
                    if (gender != null && gender == "Male")
                    {
                        isValid = true;
                    }
                    break;

                case FormatType.WomenDual:
                    if (gender != null && gender == "Female")
                    {
                        isValid = true;
                    }
                    break;
                case FormatType.DualMixed:
                    if (gender != null && gender == "Male" || gender == "Female")
                    {
                        isValid = true;
                    }
                    break;
                default:
                    return false;
            }
            return isValid;
        }
        public static bool ValidatePhone(this string phone)
        {
            if ((phone.Length < 10 && phone.Length > 11) || !phone.All(char.IsDigit))
            {
                return false;
            }
            return true;
        }

        public static bool ValidateEmail(this string email)
        {
            var regex = new Regex(@"^[^\d][a-zA-Z0-9]*@gmail\.com$");
            if (!regex.IsMatch(email))
            {
                return false;
            }
            return true;
        }

        public static bool ValidateMatch(this PickleballMatch match, FormatType formatType, out string message)
        {
            bool isValid = true; // Start with true, then mark invalid as needed
            message = string.Empty;

            // Check if the format is singles
            if (formatType == FormatType.MenSingles || formatType == FormatType.WomenSingles)
            {
                if (match.FirstTeam == null)
                {
                    isValid = false;
                    message += "FirstTeam is missing for a singles match.\n";
                }
            }
            else // For non-singles formats
            {
                // Check if FirstTeam and SecondTeam have valid athletes
                if (match.FirstTeam == null || match.FirstTeam.FirstAthleteId == 0 || match.FirstTeam.SecondAthleteId == 0)
                {
                    isValid = false;
                    message += "FirstTeam is missing or does not have valid athletes.\n";
                }

                if (match.SecondTeam == null || match.SecondTeam.FirstAthleteId == 0 || match.SecondTeam.SecondAthleteId == 0)
                {
                    isValid = false;
                    message += "SecondTeam is missing or does not have valid athletes.\n";
                }
            }
            /*if(match.MatchDate != DateTime.Now)
            {
                isValid = false;
                message += $"Can not start match now! Match Date is {match.MatchDate}\n";
            }*/

            // Check if the court is assigned
            if (!match.CourtId.HasValue)
            {
                isValid = false;
                message += "Court is not assigned to the match.\n";
            }

            // Return the validation result and message
            return isValid;
        }
    }
}
