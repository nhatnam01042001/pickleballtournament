﻿using Application.ViewModels.ParticipantViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class ParticipantViewModelExtensions
    {
        public static ParticipantViewModel ToParticipantViewModel(this Athlete participant)
        {
            if (participant.User is not null)
            {

                return new ParticipantViewModel
                {
                    ParticipantName = participant.User.FullName,
                    Gender = participant.User.Gender,
                    ParticipantType = participant.ParticipantType.ToString(),
                    Rank = participant.User.Rank
                };
            }
            return new ParticipantViewModel
            {
                ParticipantName = participant.GuestPlayer.FullName,
                Gender = participant.GuestPlayer.Gender,
                ParticipantType = participant.ParticipantType.ToString(),
                Rank = participant.GuestPlayer.Rank
            };
        }
    }
}
