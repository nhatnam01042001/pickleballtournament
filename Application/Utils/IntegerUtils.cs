﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class IntegerUtils
    {
        public static List<int> GetPreviousMatchOrders(int targetMatchOrder, int currentRoundFirstMatchOrder, int totalMatches)
        {

            var previousMatchOrders = new List<int>();

            if (targetMatchOrder == totalMatches)
            {
                // Special case handling for the final match
                // Assuming that the final match is the last match and the previous round is semi-finals with two matches
                int semiFinalMatchOrder1 = totalMatches - 2;
                int semiFinalMatchOrder2 = totalMatches - 1;

                previousMatchOrders.Add(semiFinalMatchOrder1);
                previousMatchOrders.Add(semiFinalMatchOrder2);

                return previousMatchOrders;
            }
            // Calculate the two possible currentMatchOrders
            int previousMatchOrder1 = 2 * (targetMatchOrder - currentRoundFirstMatchOrder + 1) - 1;
            int previousMatchOrder2 = 2 * (targetMatchOrder - currentRoundFirstMatchOrder + 1);

            previousMatchOrders.Add(previousMatchOrder1);
            previousMatchOrders.Add(previousMatchOrder2);

            return previousMatchOrders;
        }

        public static List<int> GetPowersOfTwo(this int max)
        {
            List<int> powersOfTwo = new List<int>();
            int power = 2;
            while(power <= max)
            {
                powersOfTwo.Add(power);
                power *= 2;
            }
            return powersOfTwo;
        }
        public static int GetNumberOfAthletes(this Tournament tournament)
        {
            if(tournament.FormatType == Domain.Enums.FormatType.MenSingles
                || tournament.FormatType == Domain.Enums.FormatType.WomenSingles)
            {
                return tournament.NumberOfTeams;
            }
            else
            {
                return tournament.NumberOfTeams * 2;
            }
        }
    }
}
