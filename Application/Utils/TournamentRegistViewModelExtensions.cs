﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.TournamentRegistViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class CampaignRegistViewModelExtensions
    {
        public static CampaignRegistration ToTournamentRegistration(this CampaignRegistrationDTO registrationDTO)
        {
            return new CampaignRegistration
            {
                FullName = registrationDTO.FullName,
                Email = registrationDTO.Email,
                PhoneNumber = registrationDTO.PhoneNumber,
                Gender = registrationDTO.Gender,
                Rank = registrationDTO.Rank,
                TournamentCampaignId = registrationDTO.TournamentCampaignId,
            };
        }

        public static CampaignRegistViewModel ToRegistrationViewModel(this CampaignRegistration registration)
        {
            return new CampaignRegistViewModel
            {
                Id = registration.Id,
                FullName = registration.FullName,
                Email = registration.Email,
                PhoneNumber = registration.PhoneNumber,
                Gender = registration.Gender,
                Rank = registration.Rank,
                TournamentId = registration.TournamentCampaignId,
                RegistrationStatus = registration.RegistrationStatus.ToString(),
            };
        }

        public static Pagination<CampaignRegistViewModel> ToPagination(this List<CampaignRegistration> registrations, int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = registrations.Count();
            var items = registrations.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<CampaignRegistViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items.Select(r => r.ToRegistrationViewModel()).ToList(),
            };
            return result;
        }

        public static TournamentRegistViewModel ToTournamentRegistViewModel(this TournamentRegistration registration)
        {
            return new TournamentRegistViewModel
            {
                AthleteId = registration.AthleteId,
                AthleteName = registration.Athlete.GuestPlayer.FullName,
                Gender = registration.Athlete.GuestPlayer.Gender,
                Id = registration.Id,
                Rank = registration.Athlete.GuestPlayer.Rank != null ? registration.Athlete.GuestPlayer.Rank : 0,
                RegistrationStatus = registration.Status.ToString(),
                TournamentId = registration.TournamentId,
                TournamentName = registration.Tournament.TournamentName
            };
        }

        public static Pagination<TournamentRegistViewModel> ToPagination(this List<TournamentRegistration> registrations, int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = registrations.Count();
            var items = registrations.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<TournamentRegistViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items.Select(r => r.ToTournamentRegistViewModel()).ToList(),
            };
            return result;
        }
    }
}
