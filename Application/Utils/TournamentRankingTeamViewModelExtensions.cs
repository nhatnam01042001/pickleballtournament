﻿using Application.Commons;
using Application.ViewModels.TournamentRankingViewModel;
using Application.ViewModels.UserViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class TournamentRankingTeamViewModelExtensions
    {
        public static TournamentRankingTeam CreateTournamentRankingTeam(this Tournament tournament,Team team)
        {
            return new TournamentRankingTeam
            {
                TournamentId = tournament.Id,
                Tournament = tournament,
                TeamId = team.Id,
                Team = team,
                AchievedRound = null,
                MatchWins = 0,
                MatchLosses = 0,
                SetDifference = 0,
                ScoresDifference= 0,
                TotalPoints = 0,
                PrizeMoney = 0,
                Rank = 0
            };
        }

        public static RankingTeamModel ToRankingTeamModel(this TournamentRankingTeam tournamentRanking)
        {
            return new RankingTeamModel
            {
                TournamentId = tournamentRanking.TournamentId,
                TournamentName = tournamentRanking.Tournament.TournamentName,
                TeamId = tournamentRanking.TeamId,
                TeamName = tournamentRanking.Team.TeamName,
                AchievedRound = tournamentRanking.AchievedRound,
                MatchWins = tournamentRanking.MatchWins,
                MatchLosses = tournamentRanking.MatchLosses,
                Rank = tournamentRanking.Rank,
                ScoresDifference = tournamentRanking.ScoresDifference,
                SetDifference = tournamentRanking.SetDifference,
                TotalPoints = tournamentRanking.TotalPoints,
                PrizeMoney = tournamentRanking.PrizeMoney
            };
        }

        public static Pagination<RankingTeamModel> ToPagination(this List<TournamentRankingTeam> tournamentRanking, int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = tournamentRanking.Count();
            var items = tournamentRanking.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<RankingTeamModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items.Select(u => u.ToRankingTeamModel()).ToList(),
            };
            return result;
        }
    }
}

