﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class ChangePasswordViewModelExtensions
    {
        public static bool VerifyPassword(string inputPassword, string storedHashedPassword, string salt)
        {
            var hashedInputPassword = (inputPassword + salt).Hash();
            return storedHashedPassword == hashedInputPassword;
        }
    }
}
