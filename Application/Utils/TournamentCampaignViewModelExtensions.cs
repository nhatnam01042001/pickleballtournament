﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.CourtGroupViewModels;
using Application.ViewModels.TournamentsViewModels;
using Application.ViewModels.TournamentViewModels;
using Application.ViewModels.UserRegistrationViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class TournamentCampaignViewModelExtensions
    {
        public static TournamentCampaignViewModel ToTournamentViewModel(this TournamentCampaign tournament)
        {
            return new TournamentCampaignViewModel
            {
                Id = tournament.Id,
                CreationDate = tournament.CreationDate,
                EndDate = tournament.EndDate,
                DeletionDate = tournament.DeletionDate,
                //Location = tournament.Location,
                ModificationDate = tournament.ModificationDate,
                StartDate = tournament.StartDate,
                TournamentName = tournament.TournamentName,
                ImageUrl = tournament.ImageUrl,
                CampaignStatus = tournament.CampaignStatus.ToString(),
                RegistrationExpiredDate = tournament.RegisterExpiredDate != null ? (DateTime)tournament.RegisterExpiredDate
                : default,
                Location = tournament.CourtGroupTournamentCampaigns != null ?
                tournament.CourtGroupTournamentCampaigns.Select(c => new CourtGroupBasicDetail
                            {
                                CourtGroupId = c.CourtGroup != null ? c.CourtGroup.Id : default,
                                CourtGroupName = c.CourtGroup != null ? c.CourtGroup.CourtGroupName
                                : null,
                            })
                        .ToList() : null,
        };
        }
        public static Pagination<TournamentCampaignViewModel> ToPagination(this List<TournamentCampaign> campaigns, int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = campaigns.Count();
            var items = campaigns.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<TournamentCampaignViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items.Select(a => a.ToTournamentViewModel()).ToList(),
            };
            return result;
        }

        public static TournamentCampaign ToTournamentCampaign(this CreateTournamentCampaignDto campaignDto)
        {
            string format = "dd-MM-yyyy";
            return new TournamentCampaign
            {
                TournamentName = campaignDto.TournamentName,
                ImageUrl = campaignDto.ImageUrl,
                CampaignStatus = Domain.Enums.MatchStatus.Scheduling,
                RegisterExpiredDate = DateTime.ParseExact(campaignDto.RegistrationExpiredDate, format, CultureInfo.InvariantCulture),
                StartDate = DateTime.ParseExact(campaignDto.StartDate, format, CultureInfo.InvariantCulture),              
                EndDate = DateTime.ParseExact(campaignDto.EndDate, format, CultureInfo.InvariantCulture),                                
            };
        }
    }
}
