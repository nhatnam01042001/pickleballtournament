﻿using Application.ViewModels.CourtViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class CourtViewModelExtensions
    {
        public static CourtViewModel ToCourtViewModel(this Court court)
        {
            return new CourtViewModel
            {
                CourtId = court.Id,
                CourtName = court.CourtName,
                CourtGroupId = court.CourtGroupId.HasValue ? court.CourtGroupId.Value : null,
                CourtGroupName = court.CourtGroup != null ? court.CourtGroup.CourtGroupName : null,
            };
        }
    }
}
