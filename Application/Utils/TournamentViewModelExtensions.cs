﻿using Application.ViewModels.TournamentViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class TournamentViewModelExtensions
    {
        public static Tournament ToTournament(this CreateTournamentDTO createTournamentDTO) 
        {
            return new Tournament
            {
                TournamentName = createTournamentDTO.TournamentName,
                Rank = createTournamentDTO.Rank,
                NumberOfTeams = createTournamentDTO.NumberOfTeams,
                TournamentCampaignId = createTournamentDTO.TournamentCampaignId,
                NumberOfSets = createTournamentDTO.NumberOfSets,
                TournamentStatus = Domain.Enums.MatchStatus.Scheduling,
                ImageUrl = createTournamentDTO.ImageUrl,
            };
        }

        public static TournamentViewModel ToTournamentViewModel(this Tournament tournament)
        {
            return new TournamentViewModel
            {
                TournamentId = tournament.Id,
                TournamentName = tournament.TournamentName,
                Rank = tournament.Rank,
                NumberOfTeams = tournament.NumberOfTeams,
                TournamentCampaignId = tournament.TournamentCampaignId,
                NumberOfSets = tournament.NumberOfSets,
                ImageUrl = tournament.ImageUrl,
                FormatType = tournament.FormatType.ToFormatTypeString(),
                TournamentType = tournament.TournamentType.ToString(),
                CurrentParticipants = tournament.TournamentAthletes != null ? tournament.TournamentAthletes.Count : 0,
                RegistrationExpiredDate = tournament.TournamentCampaign.RegisterExpiredDate != null
                ? (DateTime)tournament.TournamentCampaign.RegisterExpiredDate : default,
                RequiredAthletesNumber = tournament.GetNumberOfAthletes(),
                TournamentStatus = tournament.TournamentStatus.ToString()
            };
        }
    }
}
