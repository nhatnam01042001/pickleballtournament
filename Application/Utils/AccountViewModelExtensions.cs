﻿using Application.Commons;
using Application.ViewModels.AccountViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class AccountViewModelExtensions
    {
        public static Account ToAccount(this CreateAccountDTO accountObject) 
        {
            return new Account
            {
                UserName = accountObject.UserName,
                PasswordHash = accountObject.Password
            };
        }

        public static Account ToAccount(this ManagerAccount managerAccount)
        {
            return new Account
            {
                UserName = managerAccount.UserName,
                Salt = StringUtils.GenerateSalt(),
                PasswordHash = managerAccount.AccountPassword
            };
        }
    }
}
