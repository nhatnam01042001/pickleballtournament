﻿using Application.Commons;
using Application.ViewModels.AthleteViewModels;
using Application.ViewModels.ParticipantViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class AthleteViewModelExtensions
    {
        public static AthleteViewModel ToAthleteViewModel(this Athlete athlete)
        {
            if (athlete.User is not null)
            {

                return new AthleteViewModel
                {
                    Id = athlete.Id,
                    AthleteName = athlete.User.FullName,
                    Gender = athlete.User.Gender,
                    AthleteType = athlete.ParticipantType.ToString(),
                    Rank = athlete.User.Rank
                };
            }
            else
            {
                return new AthleteViewModel
                {
                    Id = athlete.Id,
                    AthleteName = athlete.GuestPlayer.FullName,
                    Gender = athlete.GuestPlayer.Gender,
                    AthleteType = athlete.ParticipantType.ToString(),
                    Rank = athlete.GuestPlayer.Rank
                };
            }
        }
        public static Athlete ToAthlete(this User user)
        {
            return new Athlete
            {
                User = user,
                UserId = user.Id,
                ParticipantType = Domain.Enums.ParticipantType.User,                
            };
        }
        public static Pagination<AthleteViewModel> ToPagination(this List<AthleteViewModel> athletes, int pageIndex = 0, int pageSize = 10)
        {
            var itemCounts = athletes.Count();
            var items = athletes.Skip(pageIndex * pageSize)
                                .Take(pageSize)
                                .ToList();

            var result = new Pagination<AthleteViewModel>
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCounts,
                Items = items
            };
            return result;
        }
    } 
}
