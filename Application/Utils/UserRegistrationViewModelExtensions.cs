﻿using Application.Commons;
using Application.ViewModels.TournamentViewModels;
using Application.ViewModels.UserRegistrationViewModels;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class UserRegistrationViewModelExtensions
    {
        public static UserRegistrationViewModel ToViewModel(this UserRegistration userRegistration)
        {
            return new UserRegistrationViewModel
            {
                Id = userRegistration.Id,
                FullName = userRegistration.FullName,
                Email = userRegistration.Email,
                PhoneNumber = userRegistration.PhoneNumber,
                Address = userRegistration.Address,
                DateOfBirth = userRegistration.DateOfBirth,
                Gender = userRegistration.Gender,
                Status = userRegistration.Status.ToString(),
                UserId = userRegistration.UserId.HasValue ? userRegistration.UserId.Value : (uint?)null
            };
        }

        public static Pagination<UserRegistrationViewModel> ToPagingsionViewModel(this Pagination<UserRegistration> pagination)
        {
            return new Pagination<UserRegistrationViewModel>
            {
                TotalItemsCount = pagination.TotalItemsCount,
                PageSize = pagination.PageSize,
                PageIndex = pagination.PageIndex,
                Items = pagination.Items.Select(t => t.ToViewModel()).ToList()
            };
        }

        public static UserRegistration ToUserRegistration(this RegistUserDto registUserDto)
        {
            string format = "dd-MM-yyyy";
            return new UserRegistration
            {
                FullName = registUserDto.FullName,
                Email = registUserDto.Email,
                PhoneNumber = registUserDto.PhoneNumber,
                Address = registUserDto.Address,
                DateOfBirth = DateTime.ParseExact(registUserDto.DateOfBirth, format, CultureInfo.InvariantCulture),
                Gender = registUserDto.Gender,
            Status = Domain.Enums.TournamentRegistrationStatus.WaitingApprove
            };
        }
    }
}
