﻿using Application.Commons;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace Application.Utils
{
    public static class GenerateJsonWebTokenString
    {
        public static string GenerateJsonWebToken(this User user, JWTSection jwtSection, DateTime now)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection.SecretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new []
                    {
                        new Claim("UserId", user.Id.ToString()),
                        new Claim("UserName", user.FullName),
                        new Claim("Role", user.Role.ToString())
                    };
            var token = new JwtSecurityToken(
                issuer: jwtSection.Issuer,
                audience: jwtSection.Audience,
                claims: claims,
                expires: now.AddHours(24),
                signingCredentials: credentials);


            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public static string GenerateJsonWebToken(this ManagerAccount account, JWTSection jwtSection, DateTime now)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection.SecretKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
                    {
                        new Claim("UserId", account.AccountId.ToString()),
                        new Claim("UserName", account.AccountName),
                        new Claim("Role", account.AccountRole.ToString())
                    };
            var token = new JwtSecurityToken(
                issuer: jwtSection.Issuer,
                audience: jwtSection.Audience,
                claims: claims,
                expires: now.AddHours(24),
                signingCredentials: credentials);


            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
