﻿using Application.Commons;
using Application.ViewModels.PickleballMatchViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class PickleballMatchExtensions
    {
        public static PickleballMatchViewModel ToMatchesViewModel(this PickleballMatch match)
        {
            return new PickleballMatchViewModel
            {
                MatchId = match.Id,
                FirstTeamId = match.FirstTeamId.HasValue ? match.FirstTeamId.Value : (uint?)null,
                FirstTeam = match.FirstTeam != null ? match.FirstTeam.TeamName : null,
                SecondTeamId = match.SecondTeamId.HasValue ? match.SecondTeamId.Value : (uint?)null,
                SecondTeam = match.SecondTeam != null ? match.SecondTeam.TeamName : null,
                TournamentId = match.RoundGroup.Round.TournamentId,
                RoundGroupId = (uint)match.RoundGroupId,
                RoundId = match.RoundGroup.RoundId,
                RoundOrder = match.RoundGroup.Round.RoundOrder,
                MatchOrder = match.MatchOrder,
                MatchDate = match.MatchDate,
                MatchStatus = match.MatchStatus.ToString(),
                WinConditionId = match.WinConditionId.HasValue ? match.WinConditionId.Value : (uint?)null,
                WinCondition = match.WinCondition != null ? match.WinCondition.ConditionName : null,
                WinningTeamId = match.WinningTeamId.HasValue ? match.WinningTeamId.Value : (uint?)null,
                WinningTeam = match.WinningTeam != null ? match.WinningTeam.TeamName : null,
                NumberOfSet = match.Sets.Count(),
                Court = match.Court != null ? match.Court.CourtName : null,
                CourtId = match.CourtId.HasValue ? match.CourtId.Value : null
            };
        }
        public static MatchResultViewModel ToMatchResultViewModel(this PickleballMatch pickleballMatch)
        {
            return new MatchResultViewModel
            {
                MatchId = pickleballMatch.Id,
                WinningTeamId = (uint)pickleballMatch.WinningTeamId,
            };
        }

        public static Pagination<PickleballMatchViewModel> ToMatchViewModelPaging(this Pagination<PickleballMatch> pagination)
        {
            return new Pagination<PickleballMatchViewModel>
            {
                TotalItemsCount = pagination.TotalItemsCount,
                PageSize = pagination.PageSize,
                PageIndex = pagination.PageIndex,
                Items = pagination.Items.Select(m => m.ToMatchesViewModel()).ToList()
            };
        }

        public static Pagination<PickleballMatchViewModel> ToPagination(this List<PickleballMatchViewModel> matchesViewModel, int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = matchesViewModel.Count();
            var items = matchesViewModel.Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .ToList();

            var result = new Pagination<PickleballMatchViewModel>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        /*public static List<PickleballMatch>? GeneratePickleballMatches(this Tournament tournament)
        {
            List<PickleballMatch> matches = new List<PickleballMatch>();
            int numTeams = tournament.NumberOfTeams;
            int rounds = (int)Math.Ceiling(Math.Log(numTeams, 2));

            int matchOrderCounter = 1;
            for (int i = 0; i < numTeams; i++)
            {
                PickleballMatch match = new PickleballMatch
                {
                    TournamentWithFormatTypeId = tournament.Id,
                    Round = 1,
                    MatchStatus = Domain.Enums.MatchStatus.Scheduled,
                    MatchOrder = matchOrderCounter++,
                };
                matches.Add(match);
            }

            for (int round = 2; round <= rounds; round++)
            {
                int matchesInPreviousRound = (int)Math.Pow(2, rounds - round + 1);
                for (int i = 0; i < matchesInPreviousRound; i += 2)
                {
                    PickleballMatch match = new PickleballMatch
                    {
                        TournamentWithFormatTypeId = tournament.Id,
                        Round = round,
                        MatchStatus = Domain.Enums.MatchStatus.Scheduled,
                        MatchOrder = matchOrderCounter++,
                    };
                    matches.Add(match);
                }
            }
            return matches;
        }*/
    }
}
