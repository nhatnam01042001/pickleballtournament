﻿using Application.ViewModels.CourtGroupViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class CourtGroupViewModelExtensions
    {
        public static CourtGroupViewModel ToCourtGroupViewModel(this CourtGroup courtGroup)
        {
            return new CourtGroupViewModel
            {
                Id = courtGroup.Id,
                CourtGroupName = courtGroup.CourtGroupName,
                Address = courtGroup.Address,
                PhoneNumber = courtGroup.PhoneNumber,
                EmailContact = courtGroup.EmailContact,
                IsDeleted = courtGroup.IsDeleted,
                latitude = courtGroup.latitude,
                longitude = courtGroup.longitude,
            };
        }
        public static CourtGroupViewModel CourtGroupViewModel(this CourtGroupTournamentCampaign courtGroup)
        {
            return new CourtGroupViewModel
            {
                Id = courtGroup.CourtGroupId,
                CourtGroupName = courtGroup.CourtGroup != null ? courtGroup.CourtGroup.CourtGroupName : null,
                Address = courtGroup.CourtGroup != null ? courtGroup.CourtGroup.Address : null,
                EmailContact = courtGroup.CourtGroup != null ? courtGroup.CourtGroup.EmailContact : null,
                IsDeleted = courtGroup.IsDeleted,
                PhoneNumber = courtGroup.CourtGroup != null ? courtGroup.CourtGroup.PhoneNumber : null,
                latitude = courtGroup.CourtGroup != null ? courtGroup.CourtGroup.latitude : null,
                longitude = courtGroup.CourtGroup != null ? courtGroup.CourtGroup.longitude : null
            };
        }
    }
}
