﻿using Application.Commons;
using Application.ViewModels.NewsArticleViewModels;
using Application.ViewModels.TournamentViewModels;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class NewsArticleViewModelExtensions
    {
        public static NewsArticleViewModel ToNewsArticleViewModel(this NewsArticle news)
        {
            return new NewsArticleViewModel
            {
                Id = news.Id,
                NewsTitle = news.NewsTitle,
                NewsContent = news.NewsContent,
                NewsArticleStatus = news.NewsArticleStatus,
                NewsType = news.NewsType,
                ImageUrl = news.ImageUrl
            };
        }
        public static Pagination<NewsArticleViewModel> ToNewsArticleViewModel(this Pagination<NewsArticle> pagination)
        {
            return new Pagination<NewsArticleViewModel>
            {
                TotalItemsCount = pagination.TotalItemsCount,
                PageSize = pagination.PageSize,
                PageIndex = pagination.PageIndex,
                Items = pagination.Items.Select(t => t.ToNewsArticleViewModel()).ToList()
            };
        }
    }
}
