﻿using Application.ViewModels.SetViewModels;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Application.Utils
{
    public static class SetViewModelExtensions
    {
        public static void ToSet(this Set set,UpdateSetDto viewModel)
        {
            set.FirstTeamScore = viewModel.FirstTeamScore;
            set.SecondTeamScore = viewModel.SecondTeamScore;
            set.SetStatus = Domain.Enums.MatchStatus.Completed;
        }
        public static Set ToSet (this CreateSetDto createSetDto)
        {
            return new Set
            {
                FirstTeamScore = createSetDto.FirstTeamScore,
                SecondTeamScore = createSetDto.SecondTeamScore,
                PickleballMatchId = createSetDto.MatchId,
                SetStatus = Domain.Enums.MatchStatus.Completed
            };
        }
    }
}
